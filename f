#!/bin/bash
yes | g13 generate-all lf.Cando &
yes | g13 generate-all lf.CandoStatement &
yes | g13 generate-all lf.Domain &
yes | g13 generate-all lf.DomainSet
yes | g13 generate-all lf.Evidence
yes | g13 generate-all lf.Folio &
yes | g13 generate-all lf.LfUser &
yes | g13 generate-all lf.Portfolio &
yes | g13 generate-all lf.Skill &
yes | g13 generate-all lf.Mode
yes | g13 generate-all lf.Student
yes | g13 generate-all lf.Teacher
yes | g13 generate-all lf.Topic

