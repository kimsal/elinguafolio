import grails.plugins.springsecurity.SecurityConfigType
import org.codehaus.groovy.grails.commons.GrailsApplication
import grails.util.GrailsUtil
println "THIS WAS LOADED"
def env = GrailsUtil.getEnvironment()
println "ENV="+env


defaultAdminPassword="foobar8"
brand="eLinguafolio"

// set per-environment serverURL stem for creating absolute links
hibernate {
    cache.use_second_level_cache=true
    cache.use_query_cache=true
    cache.provider_class='net.sf.ehcache.hibernate.EhCacheProvider'
}

if(env=="development") {
	dataSource.username = "lfdev"
	dataSource.password= "lfdev"
	dataSource.dbCreate= "update"
	dataSource.dialect = org.hibernate.dialect.MySQL5InnoDBDialect
	dataSource.url = "jdbc:mysql://localhost:3306/lfdev?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
	dataSource.driverClassName = "com.mysql.jdbc.Driver"

	lf.base= "/home/lf/tc7prod/webapps/elf/"
	grails.attachmentable.uploadDir = "/home/lf/elinguafolio/web-app/ul/"
	grails.attachmentable.path = "ul/"
	grails.serverURL = "http://dev.elinguafolio.com"

	grails.mail.host = "localhost"
	grails.mail.port = 25
	grails.mail.props = [
			"mail.smtp.auth":"false",
			"mail.smtp.socketFactory.fallback":"false",
			"mail.debug":"true"
	]
	grails.mail.default.from="do.not.reply@elinguafolio.org"

	grails.attachmentable.maxInMemorySize = 16024
	grails.attachmentable.maxUploadSize = 10024000000

	grails.attachmentable.poster.evaluator = { getPrincipal() }

	grails.attachmentable.searchableFileConverter="attachmentFileConverter"


}

if(env=="production") {
	dataSource.username = "lf"
	dataSource.password= "lf"
	dataSource.dbCreate= "update"
	dataSource.dialect = org.hibernate.dialect.MySQL5InnoDBDialect
	dataSource.url = "jdbc:mysql://localhost:3306/lf?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
	dataSource.driverClassName = "com.mysql.jdbc.Driver"

        grails.serverURL = "http://www.elinguafolio.org"
	lf.base = "/home/lf/apps/tc7prod/webapps/elf/"
	grails.attachmentable.uploadDir = "/home/lf/tc7prod/webapps/elf/"
	grails.attachmentable.path = "ul/"

	grails.mail.host = "localhost"
	grails.mail.port = 25
	grails.mail.props = [
		"mail.smtp.auth":"false",						   
		"mail.smtp.socketFactory.fallback":"false",
		"mail.debug":"true"
	]

	grails.mail.default.from="do.not.reply@elinguafolio.org"

	grails.attachmentable.maxInMemorySize = 6024
	grails.attachmentable.maxUploadSize = 10024000000
	grails.attachmentable.poster.evaluator = { getPrincipal() }
	grails.attachmentable.searchableFileConverter="attachmentFileConverter"


}

fileuploader {
	avatar {
		maxSize = 941024 * 1256 //256 kbytes
		allowedExtensions = ["jpg","jpeg","gif","png"]
		path = "/srv/avatar/"
	}
	docs {
		maxSize = 1000 * 1024 * 90 //90 mbytes
		allowedExtensions = ["txt","wmv","wma","m4v","mp3","mp4","wav","ogg","mpeg","mpg","jpg","png","gif","jpeg","bmp","mov","avi","xls","xlsx","ppt","pptx","doc","docx", "pdf", "rtf","odt","ods", "key","pages","zip","flv"]
		path = "/srv/docs/"
	}
}

// log4j configuration
log4j = {
    appenders {
        console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
		file name:"mylog", file:"/tmp/mylog.txt", layout: pattern(conversionPattern: '%d{dd-MM-yyyy HH:mm:ss,SSS} %5p %c{1} - %m%n')
    }


    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
	       'org.codehaus.groovy.grails.web.pages', //  GSP
	       'org.codehaus.groovy.grails.web.sitemesh', //  layouts
	       'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
	       'org.codehaus.groovy.grails.web.mapping', // URL mapping
	       'org.codehaus.groovy.grails.commons', // core / classloading
	       'org.codehaus.groovy.grails.plugins', // plugins
	       'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
	       'org.springframework',
	       'org.hibernate',
           'net.sf.ehcache.hibernate'

    warn   'org.mortbay.log'

	info 	'grails.app'

	root
	{
		info 'stdout','mylog'
		error 'stdout','mylog'
	}
}
     
//authenticationUserClass=LfUser
grails.sitemesh.default.layout='main'

grails.app.context="/"

lf.layout="main"

// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'lf.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'lf.UserRole'
grails.plugins.springsecurity.authority.className = 'lf.Role'
grails.plugins.springsecurity.securityConfigType = SecurityConfigType.InterceptUrlMap
grails.plugins.springsecurity.useSwitchUserFilter=true

grails.plugins.springsecurity.interceptUrlMap = [
'/j_spring_security_switch_user': ['ROLE_ADMIN', 'ROLE_SWITCH_USER', 'IS_AUTHENTICATED_FULLY'],
'/admin/**':    ['ROLE_ADMIN'],
'/user/**':    ['ROLE_ADMIN'],
'/finance/**':   ['ROLE_FINANCE', 'IS_AUTHENTICATED_FULLY'],
'/main/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/logout/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/login/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/confirm/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/register/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/js/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/css/**':       ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/images/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/*':            ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/login/**':     ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/logout/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/securityInfo/**':    ['ROLE_ADMIN'],
'/registrationCode/**':    ['ROLE_ADMIN'],
'/persistentLogin/**':    ['ROLE_ADMIN'],
'/layout/**':    ['ROLE_ADMIN'],
'/aclObjectIdentity/**':    ['ROLE_ADMIN'],
'/aclSid/**':    ['ROLE_ADMIN'],
'/auth/**':    ['ROLE_ADMIN'],
'/aclClass/**':    ['ROLE_ADMIN'],
'/aclEntry/**':    ['ROLE_ADMIN'],
'/skill/**':    ['ROLE_ADMIN'],
'/subject/**':    ['ROLE_ADMIN'],
'/topic/**':    ['ROLE_ADMIN'],
'/uFile/**':    ['ROLE_ADMIN'],
'/school/**':    ['ROLE_ADMIN'],
'/scale/**':    ['ROLE_ADMIN'],
'/scaleSet/**':    ['ROLE_ADMIN'],
'/portfolio/**':    ['ROLE_ADMIN'],
'/mode/**':    ['ROLE_ADMIN'],
'/lfUser/**':    ['ROLE_ADMIN'],
'/folio/**':    ['ROLE_ADMIN'],
'/candoEntry/**':    ['ROLE_ADMIN'],
'/cando/**':    ['ROLE_ADMIN'],
'/emaildomain/**':    ['ROLE_ADMIN'],
'/checklist/**':    ['ROLE_ADMIN'],
'/student':    ['ROLE_ADMIN'],
'/teacher':    ['ROLE_ADMIN'],
'/student/**':    ['ROLE_ADMIN'],
'/teacher/**':    ['ROLE_ADMIN'],
'/assessmentStatement/**':    ['ROLE_ADMIN']
]

// per the http://www.grails.org/plugin/jquery notes
grails.views.javascript.library="jquery"
jquery {
	sources = 'jquery' // Holds the value where to store jQuery-js files /web-app/js/
	version = '1.7.1' // The jQuery version in use
}


grails.plugins.appinfo.dotPath = "/usr/local/bin/dot"

lf.clam = "/usr/bin/clamscan --quiet -i "

convertpath = "/usr/bin/convert"
ffmpegpath = "/usr/bin/ffmpeg"
