import grails.plugins.springsecurity.SecurityConfigType


// place this file in $HOME/.lf/config.groovy
// the BuildConfig.groovy file will be looking for 
// $HOME/.lf/config.groovy for configuration data


// set per-environment serverURL stem for creating absolute links
environments {
	production {

		dataSource {
			username = "user"
			password = "pass"
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect
			dbCreate = "update" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost:3306/lfdev?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
		}


		grails.serverURL = "http://www.elinguafolio.org"
		grails.mail.host = "localhost"
		grails.mail.port = 25
		grails.mail.props = [
		"mail.smtp.auth":"false",						   
		"mail.smtp.socketFactory.fallback":"false",
		"mail.debug":"true"
		]
		grails.mail.default.from="do.not.reply@elinguafolio.org"
// path to tomcat webapps root	
		lf.base = "/home/tomcat7/webapps/ROOT/"

		rabbitmq {
			connectionfactory {
				username = 'guest'
				password = 'guest'
				hostname = 'localhost'
				consumers = 5
			}
			queues = {
				thumbnail()
			}
		}

		grails.attachmentable.maxInMemorySize = 6024
		grails.attachmentable.maxUploadSize = 10024000000
		// path to where uploaded files will live
		// experimental
		grails.attachmentable.uploadDir = "/home/tomcat7/webapps/ROOT/"
		grails.attachmentable.poster.evaluator = { getPrincipal() }
		grails.attachmentable.searchableFileConverter="attachmentFileConverter"

		fileuploader {
			avatar {
				maxSize = 941024 * 1256 //256 kbytes
				allowedExtensions = ["jpg","jpeg","gif","png"]
				path = "/srv/avatar/"
			}
			docs {
				maxSize = 1000 * 1024 * 90 //90 mbytes
				allowedExtensions = ["txt","wmv","wma","m4v","mp3","mp4","wav","ogg","mpeg","mpg","jpg","png","gif","jpeg","bmp","mov","avi","xls","xlsx","ppt","pptx","doc","docx", "pdf", "rtf","odt","ods", "key","pages","zip","flv"]
				path = "/srv/docs/"
			}
		}
	}

	development {
		grails.serverURL = "http://localhost:8080"
		grails.mail.host = "localhost"
		grails.mail.port = 25
		grails.mail.props = [
		"mail.smtp.auth":"false",
		//"mail.smtp.socketFactory.port":"25",
		//"mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
		"mail.smtp.socketFactory.fallback":"false",
		"mail.debug":"true"
		]
		grails.mail.default.from="do.not.reply@elinguafolio.org"
		// path to where dev version's base is
		lf.base = "/home/myname/apps/elinguafolio/"

		rabbitmq {
			connectionfactory {
				username = 'guest'
				password = 'guest'
				hostname = 'localhost'
				consumers = 5
			}
			queues = {
				thumbnail()
			}
		}
		grails.attachmentable.maxInMemorySize = 16024
		grails.attachmentable.maxUploadSize = 10024000000

		// path is relative portion from upload dir
		grails.attachmentable.path = "ul/"
		grails.attachmentable.uploadDir = "/home/myhome/app/elinguafolio/web-app/ul/"
		grails.attachmentable.poster.evaluator = { getPrincipal() }

		grails.attachmentable.searchableFileConverter="attachmentFileConverter"

		fileuploader {
			avatar {
				maxSize = 941024 * 1256 //256 kbytes
				allowedExtensions = ["jpg","jpeg","gif","png"]
				path = "/tmp/avatar/"
			}
			docs {
				maxSize = 1000 * 1024 * 90 //90 mbytes
				allowedExtensions = ["txt","wmv","wma","m4v","mp3","mp4","wav","ogg","mpeg","mpg","jpg","png","gif","jpeg","bmp","mov","avi","xls","xlsx","ppt","pptx","doc","docx", "pdf", "rtf","odt","ods", "key","pages","zip","flv"]
				path = "/tmp/docs/"
			}
		}
		dataSource {
			username = "user"
			password = "pass"
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect
			dbCreate = "update" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost:3306/lfdev?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
		}

	}



	test {
		grails.serverURL = "http://localhost:8080/${appName}"
		grails.mail.host = "localhost"
		grails.mail.port = 25
		grails.mail.props = [
		"mail.smtp.auth":"false",						   
		"mail.smtp.socketFactory.port":"25",
		"mail.smtp.socketFactory.fallback":"false",
		"mail.debug":"true"
		]
		grails.mail.default.from="do.not.reply@elinguafolio.org"
	}
}

// log4j configuration
log4j = {
	// Example of changing the log pattern for the default console
	// appender:
	//
	//appenders {
	//    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
	//}
	appenders {
	//	console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
		file name:"mylog", file:"/tmp/mylog.txt", layout: pattern(conversionPattern: '%d{dd-MM-yyyy HH:mm:ss,SSS} %5p %c{1} - %m%n')
		file name:"mylog2", file:"/tmp/mylog2.txt", layout: pattern(conversionPattern: '%d{dd-MM-yyyy HH:mm:ss,SSS} %5p %c{1} - %m%n')
	}


	error mylog: 'org.codehaus.groovy.grails.web.servlet',  //  controllers
	'org.codehaus.groovy.grails.web.pages', //  GSP
	'org.codehaus.groovy.grails.web.sitemesh', //  layouts
	'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
	'org.codehaus.groovy.grails.web.mapping', // URL mapping
	'org.codehaus.groovy.grails.commons', // core / classloading
	'org.codehaus.groovy.grails.plugins', // plugins
	'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
	'org.springframework',
	'org.hibernate',
	'net.sf.ehcache.hibernate'

	warn  mylog2: 'org.mortbay.log'

	info mylog2: 'grails.app'

//	root
//	{
//		info 'mylog'
//		error 'mylog'
//	}
}



//authenticationUserClass=LfUser
grails.sitemesh.default.layout='main'



grails.app.context="/"

lf.layout="main"

// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'lf.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'lf.UserRole'
grails.plugins.springsecurity.authority.className = 'lf.Role'
grails.plugins.springsecurity.securityConfigType = SecurityConfigType.InterceptUrlMap
grails.plugins.springsecurity.useSwitchUserFilter=true

grails.plugins.springsecurity.interceptUrlMap = [
'/j_spring_security_switch_user': ['ROLE_ADMIN', 'ROLE_SWITCH_USER', 'IS_AUTHENTICATED_FULLY'],
'/admin/**':    ['ROLE_ADMIN'],
'/notice/**':    ['ROLE_ADMIN'],
'/user/**':    ['ROLE_ADMIN'],
'/finance/**':   ['ROLE_FINANCE', 'IS_AUTHENTICATED_FULLY'],
'/main/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/logout/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/login/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/confirm/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/register/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/js/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/css/**':       ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/images/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/*':            ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/login/**':     ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/logout/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/securityInfo/**':    ['ROLE_ADMIN'],
'/registrationCode/**':    ['ROLE_ADMIN'],
'/persistentLogin/**':    ['ROLE_ADMIN'],
'/layout/**':    ['ROLE_ADMIN'],
'/aclObjectIdentity/**':    ['ROLE_ADMIN'],
'/aclSid/**':    ['ROLE_ADMIN'],
'/auth/**':    ['ROLE_ADMIN'],
'/aclClass/**':    ['ROLE_ADMIN'],
'/aclEntry/**':    ['ROLE_ADMIN'],
'/skill/**':    ['ROLE_ADMIN'],
'/subject/**':    ['ROLE_ADMIN'],
'/topic/**':    ['ROLE_ADMIN'],
'/uFile/**':    ['ROLE_ADMIN'],
'/school/**':    ['ROLE_ADMIN'],
'/scale/**':    ['ROLE_ADMIN'],
'/scaleSet/**':    ['ROLE_ADMIN'],
'/portfolio/**':    ['ROLE_ADMIN'],
'/mode/**':    ['ROLE_ADMIN'],
'/lfUser/**':    ['ROLE_ADMIN'],
'/folio/**':    ['ROLE_ADMIN'],
'/candoEntry/**':    ['ROLE_ADMIN'],
'/cando/**':    ['ROLE_ADMIN'],
'/emaildomain/**':    ['ROLE_ADMIN'],
'/checklist/**':    ['ROLE_ADMIN'],
'/student':    ['ROLE_ADMIN'],
'/teacher':    ['ROLE_ADMIN'],
'/student/**':    ['ROLE_ADMIN'],
'/teacher/**':    ['ROLE_ADMIN'],
'/assessmentStatement/**':    ['ROLE_ADMIN']
]

// per the http://www.grails.org/plugin/jquery notes
grails.views.javascript.library="jquery"
jquery {
sources = 'jquery' // Holds the value where to store jQuery-js files /web-app/js/
version = '1.7.1' // The jQuery version in use
}


// path for clamav scanning - this will go away
// in favor of hosted web service at some point

lf.clam = "/usr/bin/clamscan --quiet -i "

// burning image plugin configs 
bi.imageMagickQuality = 90
bi.imageMagickCompression= 10

bi.Student = [
outputDir: '../web-app/thumbs/',
prefix: 'elf',
images: ['large':[scale:[width:800, height:600, type:ScaleType.APPROXIMATE],
watermark:[sign:'images/watermark.png', offset:[top:10, left:10]]],
'small':[scale:[width:100, height:100, type:ScaleType.ACCURATE],
watermark:[sign:'images/watermark.png', offset:[top:10, left:10]]]],
constraints:[
nullable:true,
maxSize:5000,
contentType:['image/gif', 'image/png', 'image/jpg', 'image/jpeg']
]
]


// imagemagick converstion
convertpath = "/usr/local/bin/../Cellar/imagemagick/6.7.5-7/bin/convert"
ffmpegpath = "/usr/local/bin/ffmpeg"
ffmpeg2theorapath = "ffmpeg2theora"
