package lf
import lf.*

class StatusCode {

	static String CANDO_COMPLETE = "I can do this"
	static String CANDO_PARTIAL = "Need some help"
	static String CANDO_NO = "I can not do this yet"
	static String CANDO_NA = "N/A"

	static String INITIATED = 'initiated'
	static String BLOCKED = 'blocked'
	static String CANCELLED = 'cancelled'
	static String DENIED = 'denied'
	static String ACCEPTED = 'accepted'

	static String MYPORTFOLIO_INUSE = 'in use'
	static String MYPORTFOLIO_REMOVED= 'removed'
	static String PORTFOLIO_INUSE = 'in use'
	static String PORTFOLIO_REMOVED= 'removed'

}
