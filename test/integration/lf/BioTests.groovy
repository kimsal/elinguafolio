package lf

import grails.test.*

class BioTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testBasic() {
			init()
			def s = Subject.findByName("test")
			assert(s.name=='test')
			done()
    }

    void testUserMybio() {
			init()
			def u = User.findByUsername("abctest")
			assert(u.username=='abctest')
			def m = Mybiography.findAllByOwner(u)
			assert(m.size()==1)
			println m.answers
			assert(m[0].answers.size()==2)

			def mybio = m[0]
			def a3 = new MybiographyAnswer(
					statement: BioStatement.findByCaption("I can do bar"), 
					value:"4", 
					mybio:mybio).save()
			mybio.addToAnswers(a3)
			assert(mybio.answers.size()==3)
			mybio.removeFromAnswers(a3)
			a3.delete()
			assert(mybio.answers.size()==2)

			done()
    }


		def init() { 
			def s = new Subject(name:"test").save()
			def f = new Folio(title:'LF test', subject:s).save()
			def b = new Biography(name:"language bio questions", folio:f).save()
			def bs1 = new BioSection(caption: "section1", sortOrder:1, bio: b).save()
			def s1 = new BioStatement(caption:"I can do foo", sortOrder:1, section: bs1).save()
			def s2 = new BioStatement(caption:"I can do bar", sortOrder:2, section: bs1).save()

			def u = Student.findByUsername('abctest')
			if(u==null)
			{
				u = new Student(myCode:"123", username:"abctest", password:"a", fullName: "abc")
				u.validate()
				println u.errors
				u.save()
			}
			def mybio = new Mybiography(owner:u, bio: b)
			mybio.validate()
			println mybio.errors
			mybio.save()
			def a1 = new MybiographyAnswer(statement: s1, value:"1", mybio:mybio).save()
			def a2 = new MybiographyAnswer(statement: s2, value:"1", mybio:mybio).save()
			mybio.addToAnswers(a1)
			mybio.addToAnswers(a2)
		}

		def done() {
			def b = Biography.findByName("language bio questions")
			def mybio = Mybiography.findByBioAndOwner(b, Student.findByUsername("abctest"))
			mybio.delete()
			/*
			mybio.answers.each { a->
				mybio.removeFromAnswers(a)
			}
			*/

			def s1 = BioStatement.findByCaption("I can do foo")
			s1.delete()
			def s2 = BioStatement.findByCaption("I can do bar")
			s2.delete()
			def bs2 = BioSection.findByCaption("section1")
			bs2.delete()
			b.delete()
			def f = Folio.findByTitle("LF test")
			f?.delete()
			def s = Subject.findByName("test")
			s?.delete()
			def u = Student.findByUsername('abctest')
			u.delete(flush:true)
			println u.errors

		}
}
