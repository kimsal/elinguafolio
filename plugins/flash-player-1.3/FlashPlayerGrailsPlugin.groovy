class FlashPlayerGrailsPlugin {
    def version = 1.3
    def dependsOn = [:]

    // TODO Fill in these fields
    def author = "Paul Fernley"
    def authorEmail = "paul@pfernley.orangehome.co.uk"
    def title = "Grails TagLib plugin for the JW FLV Media Player"
    def description = '''\
The flash-player plugin provides a Grails TagLib wrapper around the JW FLV Media
Player (an Adobe Flash Player which is free for non-commercial use). It dynamically
embeds the flash player using the swfobject JavaScript library. The plugin includes
a sample skin for the player, the Adobe expressInstall system for automatically
updating a browser's Flash Player plugin version, and a proxy YouTube player.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/FlashPlayer+Plugin"

    def doWithSpring = {
        // TODO Implement runtime spring config (optional)
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional)
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }
}
