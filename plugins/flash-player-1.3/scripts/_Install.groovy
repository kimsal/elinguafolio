//
// This script is executed by Grails after plugin was installed to project.
// This script is a Gant script so you can use all special variables provided
// by Gant (such as 'baseDir' which points on project base dir). You can
// use 'Ant' to access a global instance of AntBuilder
//
// For example you can create directory under project tree:
// Ant.mkdir(dir:"C:\Users\Paul\Documents\NetBeansProjects/flash-player/grails-app/jobs")
//

Ant.property(environment:"env")
grailsHome = Ant.antProject.properties."env.GRAILS_HOME"
Ant.copy(file:"${pluginBasedir}/web-app/js/swfobject.js", todir:"${basedir}/web-app/js")
Ant.mkdir(dir:"${basedir}/web-app/swf")
Ant.copy(file:"${pluginBasedir}/web-app/swf/expressInstall.swf", todir:"${basedir}/web-app/swf")
Ant.copy(file:"${pluginBasedir}/web-app/swf/player.swf", todir:"${basedir}/web-app/swf")
Ant.copy(file:"${pluginBasedir}/web-app/swf/snel.swf", todir:"${basedir}/web-app/swf")
Ant.copy(file:"${pluginBasedir}/web-app/swf/yt.swf", todir:"${basedir}/web-app/swf")
Ant.mkdir(dir:"${basedir}/web-app/movies")
Ant.copy(file:"${pluginBasedir}/web-app/movies/video.flv", todir:"${basedir}/web-app/movies")
