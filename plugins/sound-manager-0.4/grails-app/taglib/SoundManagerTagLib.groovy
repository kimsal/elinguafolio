class SoundManagerTagLib 
{
	static namespace = "sm"
	
	def soundManagerAPI = {attrs ->
		out << render(template: "/shared/soundManagerScript", contextPath: pluginContextPath,
						model: ['pContextPath': pluginContextPath])
	}
	
	def inlinePlayer = {attrs ->
		if(!attrs.useDefaultStyle || attrs.useDefaultStyle == 'true')
		{
			out << render(template: "/shared/inlinePlayerStyle", contextPath: pluginContextPath, 
					model: ['pContextPath': pluginContextPath])	
		}
		
		out << render(template: "/shared/soundManagerScript", contextPath: pluginContextPath,
					model: ['pContextPath': pluginContextPath])
		out << render(template: "/shared/inlinePlayerScript", contextPath: pluginContextPath, 
					model: ['pContextPath': pluginContextPath])
	}
	
	def pagePlayer = {attrs ->		
		if(!attrs.useDefaultStyle || attrs.useDefaultStyle == 'true')
		{
			out << render(template: "/shared/pagePlayerStyle-min", contextPath: pluginContextPath, 
					model: ['pContextPath': pluginContextPath])	
		}
		
		out << render(template: "/shared/soundManagerScript", contextPath: pluginContextPath,
					model: ['pContextPath': pluginContextPath])
		
		if(attrs.autoStart && attrs.autoStart == 'true')
		{
			out << '<script type=\"text/javascript\">var PP_CONFIG = {autoStart: true}</script>'
		}
					
		out << render(template: "/shared/pagePlayerScript", contextPath: pluginContextPath, 
					model: ['pContextPath': pluginContextPath])
	}
	
	def playlist = {attrs, body ->
		out << '<ul class=\"playlist\">'
		
		def links = body().split('\\s*</\\s*a\\s*>\\s*')
		for(link in links)
		{
			out << '<li>'
			out << link.trim() + '</a>'
			out << '</li>'
		}
		
		out << '</ul>'
		out << render(template: "/shared/pagePlayerControlBox", contextPath: pluginContextPath)
	}
}
