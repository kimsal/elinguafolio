class EncryptedDataController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [encryptedDataInstanceList: EncryptedData.list(params), encryptedDataInstanceTotal: EncryptedData.count()]
    }

    def create = {
        def encryptedDataInstance = new EncryptedData()
        encryptedDataInstance.properties = params
        return [encryptedDataInstance: encryptedDataInstance]
    }

    def save = {
        def encryptedDataInstance = new EncryptedData(params)
        if (encryptedDataInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), encryptedDataInstance.id])}"
            redirect(action: "show", id: encryptedDataInstance.id)
        }
        else {
            render(view: "create", model: [encryptedDataInstance: encryptedDataInstance])
        }
    }

    def show = {
        def encryptedDataInstance = EncryptedData.get(params.id)
        if (!encryptedDataInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), params.id])}"
            redirect(action: "list")
        }
        else {
            [encryptedDataInstance: encryptedDataInstance]
        }
    }

    def edit = {
        def encryptedDataInstance = EncryptedData.get(params.id)
        if (!encryptedDataInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [encryptedDataInstance: encryptedDataInstance]
        }
    }

    def update = {
        def encryptedDataInstance = EncryptedData.get(params.id)
        if (encryptedDataInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (encryptedDataInstance.version > version) {
                    
                    encryptedDataInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'encryptedData.label', default: 'EncryptedData')] as Object[], "Another user has updated this EncryptedData while you were editing")
                    render(view: "edit", model: [encryptedDataInstance: encryptedDataInstance])
                    return
                }
            }
            encryptedDataInstance.properties = params
            if (!encryptedDataInstance.hasErrors() && encryptedDataInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), encryptedDataInstance.id])}"
                redirect(action: "show", id: encryptedDataInstance.id)
            }
            else {
                render(view: "edit", model: [encryptedDataInstance: encryptedDataInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def encryptedDataInstance = EncryptedData.get(params.id)
        if (encryptedDataInstance) {
            try {
                encryptedDataInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'encryptedData.label', default: 'EncryptedData'), params.id])}"
            redirect(action: "list")
        }
    }
}
