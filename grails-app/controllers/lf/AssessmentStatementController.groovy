package lf

class AssessmentStatementController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [assessmentStatementInstanceList: AssessmentStatement.list(params), assessmentStatementInstanceTotal: AssessmentStatement.count()]
    }

    def create = {
        def assessmentStatementInstance = new AssessmentStatement()
        assessmentStatementInstance.properties = params
        return [assessmentStatementInstance: assessmentStatementInstance]
    }

    def save = {
        def assessmentStatementInstance = new AssessmentStatement(params)
        if (assessmentStatementInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), assessmentStatementInstance.id])}"
            redirect(action: "show", id: assessmentStatementInstance.id)
        }
        else {
            render(view: "create", model: [assessmentStatementInstance: assessmentStatementInstance])
        }
    }

    def show = {
        def assessmentStatementInstance = AssessmentStatement.get(params.id)
        if (!assessmentStatementInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), params.id])}"
            redirect(action: "list")
        }
        else {
            [assessmentStatementInstance: assessmentStatementInstance]
        }
    }

    def edit = {
        def assessmentStatementInstance = AssessmentStatement.get(params.id)
        if (!assessmentStatementInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [assessmentStatementInstance: assessmentStatementInstance]
        }
    }

    def update = {
        def assessmentStatementInstance = AssessmentStatement.get(params.id)
        if (assessmentStatementInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (assessmentStatementInstance.version > version) {
                    
                    assessmentStatementInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'assessmentStatement.label', default: 'AssessmentStatement')] as Object[], "Another user has updated this AssessmentStatement while you were editing")
                    render(view: "edit", model: [assessmentStatementInstance: assessmentStatementInstance])
                    return
                }
            }
            assessmentStatementInstance.properties = params
            if (!assessmentStatementInstance.hasErrors() && assessmentStatementInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), assessmentStatementInstance.id])}"
                redirect(action: "show", id: assessmentStatementInstance.id)
            }
            else {
                render(view: "edit", model: [assessmentStatementInstance: assessmentStatementInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def assessmentStatementInstance = AssessmentStatement.get(params.id)
        if (assessmentStatementInstance) {
            try {
                assessmentStatementInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement'), params.id])}"
            redirect(action: "list")
        }
    }
}
