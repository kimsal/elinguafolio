package lf

class EmaildomainController {

    static allowedMethods = [ update: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
		def all = Emaildomain.all.sort{it.domain}
		[all:all]
    }

    def update = {
		def all = params.domains.replace("\r","").split("\n")
		Emaildomain.list().each { it.delete() }
		all.each { e->
			def t = new Emaildomain(domain: e.trim()).save()
		}
		redirect(action: "list")
		return

        def emaildomainInstance = Emaildomain.get(params.id)
        if (emaildomainInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (emaildomainInstance.version > version) {
                    
                    emaildomainInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'emaildomain.label', default: 'Emaildomain')] as Object[], "Another user has updated this Emaildomain while you were editing")
                    render(view: "edit", model: [emaildomainInstance: emaildomainInstance])
                    return
                }
            }
            emaildomainInstance.properties = params
            if (!emaildomainInstance.hasErrors() && emaildomainInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'emaildomain.label', default: 'Emaildomain'), emaildomainInstance.id])}"
                redirect(action: "show", id: emaildomainInstance.id)
            }
            else {
                render(view: "edit", model: [emaildomainInstance: emaildomainInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'emaildomain.label', default: 'Emaildomain'), params.id])}"
            redirect(action: "list")
        }
    }


}