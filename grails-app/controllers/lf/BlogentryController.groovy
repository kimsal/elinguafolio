package lf

import org.springframework.dao.DataIntegrityViolationException

class BlogentryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [blogentryInstanceList: Blogentry.list(params), blogentryInstanceTotal: Blogentry.count()]
    }

    def create() {
        [blogentryInstance: new Blogentry(params)]
    }

    def save() {
        def blogentryInstance = new Blogentry(params)
        if (!blogentryInstance.save(flush: true)) {
            render(view: "create", model: [blogentryInstance: blogentryInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), blogentryInstance.id])
        redirect(action: "show", id: blogentryInstance.id)
    }

    def show() {
        def blogentryInstance = Blogentry.get(params.id)
        if (!blogentryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), params.id])
            redirect(action: "list")
            return
        }

        [blogentryInstance: blogentryInstance]
    }

    def edit() {
        def blogentryInstance = Blogentry.get(params.id)
        if (!blogentryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), params.id])
            redirect(action: "list")
            return
        }

        [blogentryInstance: blogentryInstance]
    }

    def update() {
        def blogentryInstance = Blogentry.get(params.id)
        if (!blogentryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (blogentryInstance.version > version) {
                blogentryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'blogentry.label', default: 'Blogentry')] as Object[],
                          "Another user has updated this Blogentry while you were editing")
                render(view: "edit", model: [blogentryInstance: blogentryInstance])
                return
            }
        }

        blogentryInstance.properties = params

        if (!blogentryInstance.save(flush: true)) {
            render(view: "edit", model: [blogentryInstance: blogentryInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), blogentryInstance.id])
        redirect(action: "show", id: blogentryInstance.id)
    }

    def delete() {
        def blogentryInstance = Blogentry.get(params.id)
        if (!blogentryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), params.id])
            redirect(action: "list")
            return
        }

        try {
            blogentryInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'blogentry.label', default: 'Blogentry'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
