package lf

class PortfolioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [portfolioInstanceList: Portfolio.list(params), portfolioInstanceTotal: Portfolio.count()]
    }

    def create = {
        def portfolioInstance = new Portfolio()
        portfolioInstance.properties = params
        return [portfolioInstance: portfolioInstance]
    }

    def save = {
        def portfolioInstance = new Portfolio(params)
        if (portfolioInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), portfolioInstance.id])}"
            redirect(action: "show", id: portfolioInstance.id)
        }
        else {
            render(view: "create", model: [portfolioInstance: portfolioInstance])
        }
    }

    def show = {
        def portfolioInstance = Portfolio.get(params.id)
        if (!portfolioInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), params.id])}"
            redirect(action: "list")
        }
        else {
            [portfolioInstance: portfolioInstance]
        }
    }

    def edit = {
        def portfolioInstance = Portfolio.get(params.id)
        if (!portfolioInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [portfolioInstance: portfolioInstance]
        }
    }

    def update = {
        def portfolioInstance = Portfolio.get(params.id)
        if (portfolioInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (portfolioInstance.version > version) {
                    
                    portfolioInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'portfolio.label', default: 'Portfolio')] as Object[], "Another user has updated this Portfolio while you were editing")
                    render(view: "edit", model: [portfolioInstance: portfolioInstance])
                    return
                }
            }
            portfolioInstance.properties = params
            if (!portfolioInstance.hasErrors() && portfolioInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), portfolioInstance.id])}"
                redirect(action: "show", id: portfolioInstance.id)
            }
            else {
                render(view: "edit", model: [portfolioInstance: portfolioInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def portfolioInstance = Portfolio.get(params.id)
        if (portfolioInstance) {
            try {
                portfolioInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'portfolio.label', default: 'Portfolio'), params.id])}"
            redirect(action: "list")
        }
    }
}
