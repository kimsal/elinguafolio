package lf
import grails.converters.JSON
import com.macrobit.grails.plugins.attachmentable.core.Attachmentable

class ChecklistController implements Attachmentable {

	def importingService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [checklistInstanceList: Checklist.list(params), checklistInstanceTotal: Checklist.count()]
    }

    def create = {
        def checklistInstance = new Checklist()
        checklistInstance.properties = params
        return [checklistInstance: checklistInstance]
    }

    def save = {
        def checklistInstance = new Checklist(params)
	checklistInstance.folio = Folio.get(params.folio.id)
checklistInstance.dateCreated = new Date()
checklistInstance.lastUpdated = new Date()
        if (checklistInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'checklist.label', default: 'Checklist'), checklistInstance.id])}"
            redirect(action: "show", id: checklistInstance.id)
        }
        else {
            render(view: "create", model: [checklistInstance: checklistInstance])
        }
    }

    def show = {
        def checklistInstance = Checklist.get(params.id)
        if (!checklistInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
            redirect(action: "list")
        }
        else {
            [checklistInstance: checklistInstance]
        }
    }

    def edit = {
        def checklistInstance = Checklist.get(params.id)
        if (!checklistInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [checklistInstance: checklistInstance]
        }
    }

    def update = {
        def checklistInstance = Checklist.get(params.id)
        if (checklistInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (checklistInstance.version > version) {
                    
                    checklistInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'checklist.label', default: 'Checklist')] as Object[], "Another user has updated this Checklist while you were editing")
                    render(view: "edit", model: [checklistInstance: checklistInstance])
                    return
                }
            }
            checklistInstance.properties = params
            if (!checklistInstance.hasErrors() && checklistInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'checklist.label', default: 'Checklist'), checklistInstance.id])}"
                redirect(action: "show", id: checklistInstance.id)
            }
            else {
                render(view: "edit", model: [checklistInstance: checklistInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def checklistInstance = Checklist.get(params.id)
        if (checklistInstance) {
            try {
                checklistInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
            redirect(action: "list")
        }
    }


	def modify = {
		def checklistInstance = Checklist.get(params.id)
		if (!checklistInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'checklist.label', default: 'Checklist'), params.id])}"
			redirect(action: "list")
		}
		else {
			return [checklistInstance: checklistInstance]
		}
	}
	
	def movescaleset = 
	{
		def scaleset = ScaleSet.get(params.id)
		if(params.direction=="left") {
			scaleset.sortOrder--
		}
		if(params.direction=="right") {
			scaleset.sortOrder++
		}
		scaleset.save()
		redirect(action:"modify", id: params.mid)
	}

	def delscaleset =
		{
			def scaleset = ScaleSet.get(params.id)
			scaleset.delete()
			redirect(action:"modify", id: params.mid)
		}

	def movescale =
		{
			def scale = Scale.get(params.id)
			if(params.direction=="left") {
				scale.sortOrder--
			}
			if(params.direction=="right") {
				scale.sortOrder++
			}
			scale.save()
			redirect(action:"modify", id: params.mid)
		}

	def delscale =
		{
			def scale = Scale.get(params.id)
			scale.delete()
			redirect(action:"modify", id: params.mid)
		}

	def addscale = 
		{
			def scaleSet = ScaleSet.get(params.id)
			def scale = new Scale(scaleSet: scaleSet, name:params.scale, sortOrder: params.sortOrder)
			scale.validate()
			println scale.errors
			scale.save()
			scaleSet.addToScales(scale)
			scaleSet.save()
			redirect(action:"modify", id: params.mid)
		}

	def addscaleset =
		{
			def checklist = Checklist.get(params.id)
			def scaleset = new ScaleSet(checklist: checklist, name:params.scaleset, sortOrder: params.sortOrder ?: 1)
			println scaleset.errors
			scaleset.save()
			redirect(action:"modify", id: params.id)
		}
	
	def modifycandos =
		{
			def st = null
			if(params.st!=null)
			{
				st = CandoStatement.get(params.st)
			}

			def candos = CandoStatement.findAllBySkillAndScale(Skill.get(params.skill), Scale.get(params.scale))

			[st:st, candos:candos.sort{it.sortOrder}, skill: params.skill, scale: params.scale,
			checklist: params.checklist
			]
		}
	def movestatement =
		{
			def x = CandoStatement.get(params.id)
			def scale = x.scale.id
			def skill = x.skill.id
			if(params.direction=="up") {
				x.sortOrder--
			}
			if(params.direction=="down") {
				x.sortOrder++
			}
			x.save()
			redirect(action:"modifycandos", 
			params: ['skill':skill, 'scale':scale, 'checklist':params.checklist]
			)
		}

	def delstatement =
		{
			def x = CandoStatement.get(params.id)
			def scale = x.scale.id
			def skill = x.skill.id

			x.delete()
			redirect(action:"modifycandos",
					params: ['skill':skill, 'scale':scale, 'checklist':params.checklist]
			)
		}
	def addstatement = 
		{
			def x
			if(params.id)
			{
			x = CandoStatement.get(params?.id as Long)
			}
			if (!x)
			{
				x = new CandoStatement()
			}
			x.statement = params.statement
			x.skill = Skill.get(params.skill)
			x.scale = Scale.get(params.scale)
			x.sortOrder = params.sortOrder as Long
			x.save()
			if(params.pictures.size>0)
			{
			x.removeAttachments()
			attachUploadedFilesTo(x)
			}
			redirect(action:"modifycandos",
					params: ['skill':x.skill.id, 'scale':x.scale.id, 'checklist':params.checklist]
					)
		}

	def delpic =
		{
			def x
			if(params.id)
			{
				x = CandoStatement.get(params?.id as Long)
			}
			if (x)
			{
				x.removeAttachments()
			}
			redirect(action:"modifycandos",
					params: ['skill':x.skill.id, 'scale':x.scale.id, 'checklist':params.checklist]
			)
		}
	def addmode =
		{
			def checklist = Checklist.get(params.id)
			def mode = new Mode(checklist: checklist, name:params.mode, sortOrder: params.sortOrder ?: 1)
			mode.save()
			checklist.addToModes(mode)
			checklist.save()
			redirect(action:"modify", id: params.id)
		}
	def movemode =
		{
			def x = Mode.get(params.id)
			if(params.direction=="up") {
				x.sortOrder--
			}
			if(params.direction=="down") {
				x.sortOrder++
			}
			x.save()
			redirect(action:"modify", id: params.checklist)
		}
	def delmode =
		{
			def x = Mode.get(params.id)
			x.delete()
			redirect(action:"modify", id: params.checklist)
		}
	def moveskill =
		{
			def x = Skill.get(params.id)
			if(params.direction=="up") {
				x.sortOrder--
			}
			if(params.direction=="down") {
				x.sortOrder++
			}
			x.save()
			redirect(action:"modify", id: params.checklist)
		}
	def delskill =
		{
			def x = Skill.get(params.id)
			x.delete()
			redirect(action:"modify", id: params.checklist)
		}
	def addskill =
		{
			def mode = Mode.get(params.id)
			def skill = new Skill(mode: mode, name:params.skill, sortOrder: params.sortOrder)
			skill.validate()
			println skill.errors
			skill.save()
			mode.addToSkills(skill)
			mode.save()
			if(params.pictures.size>0)
			{
				skill.removeAttachments()
				attachUploadedFilesTo(skill)
			}
			redirect(action:"modify", id: params.mid)
		}
	def delskillpic =
		{
			def x
			if(params.id)
			{
				x = Skill.get(params?.id as Long)
			}
			if (x)
			{
				x.removeAttachments()
			}
			redirect(action:"modify", id: params.checklist)
		}
	def updateskill =
		{
			def skill = Skill.get(params.id)
			if(skill)
			{
				skill.sortOrder = params.sortOrder as Long
				skill.name = params.skill
skill.save(flush:true)
				if (params.pictures.size>0)
				{
					skill.removeAttachments()
skill.save(flush:true)
					attachUploadedFilesTo(skill)
				}
			}
			redirect(action:"modify", id: params.mid)
		}
	
	def addtopic = 
		{
			def c = Checklist.get(params.checklist as Long)
			def t = new Topic(checklist: c, name:params.name)
			t.save()
			c.addToTopics(t)
			redirect(action:"modify", id: params.checklist)
		}
	def deltopic =
		{
			def t = Topic.get(params.id as Long)
			t.delete()
			redirect(action:"modify", id: params.checklist)
		}

	def copyscalemode = {
		def from = Checklist.get(params.from)
		def toc = Checklist.get(params.to)
		toc.scaleSets.clear()
		toc.modes.clear()
		toc.save(flush:true)
toc.validate()
println "TO errors----"
println toc.errors
println "END TO errors----"

		from.scaleSets.each { fss->
			def newss = new ScaleSet(name:fss.name, sortOrder:  fss.sortOrder, checklist: Checklist.get(toc.id))
newss.checklist = Checklist.get(params.to)
newss.validate()
log.info newss.errors
println newss.errors
	newss.save(flush:true)
			newss.scales = []
			fss.scales.each { fsc->
				def temps = new Scale(name: fsc.name, sortOrder:  fsc.sortOrder)
				temps.scaleSet = ScaleSet.get(newss.id)
				temps.validate()
				println "temps="+temps.errors
				temps.save()
				newss.scales << temps
			}
			toc.scaleSets << newss
		}
		from.modes.each { fm->
			def newm = new Mode(name:fm.name, sortOrder: fm.sortOrder, checklist: Checklist.get(toc.id))
newm.checklist = Checklist.get(params.to)
newm.validate()
println "NEWM-------"
println newm.errors
println "ENDNEWM"
newm.save(flush:true)
			newm.skills = []
			fm.skills.each { fms->
				def temp = new Skill(name:fms.name, sortOrder: fms.sortOrder)
				temp.mode = Mode.get(newm.id)
				temp.validate()
				println "TE="+temp.errors 
				temp.save(flush:true)
				newm.skills << temp
			}
			toc.modes << newm
		}
		toc.save()
		redirect(action:"modify", id:toc.id)
	}

        def exportInventory = {
                def inventoryList = Inventory.list()
                JSON.use("deep")
		def t = new JSON(inventoryList)
		[export:t]
        }
 
        def exportBiography = {
                def ilist = Biography.list()
                JSON.use("deep")
		def t = new JSON(ilist)
		[export:t]
        }
 
        def export = {
                def checklistInstance = Checklist.get(params.id)
                JSON.use("deep")
		def t = new JSON(checklistInstance)
		[export:t]
//                render checklistInstance as JSON
        }
        
	def imp = {}

	def impInventory = {}

	def impBiography= {}

        def importBiography2 = {
		def j = JSON.parse(params.imp)

// debug print the JSON coming in
if(0) { 
	def t = new JSON(j)
	t.setPrettyPrint(true)
	render(t)
	return
}
		def f = Folio.get(params.folio.toLong())
		def c = new Biography(name:params.name)
		c.folio = f
		c.validate()
		println c.errors
		c.save()

		j.sections.each { st1->
st1.each { st->

			def invSec = new BioSection()
			invSec.bio = c
			invSec.caption = st.caption
			invSec.sortOrder = st.sortOrder.toInteger()
			invSec.sectionType = st.sectionType
			invSec.validate()
			log.info invSec.errors
			invSec.save()

			st.statements.each { st2->
				def invSt = new BioStatement()
				invSt.section = invSec
				invSt.caption = st2.caption
				invSt.sortOrder = st2.sortOrder.toInteger()
				invSt.statementType = st2.statementType
				invSt.validate()
				log.info invSt.errors
				invSt.save()
			}
}
		}

		
		redirect(action:"index", controller:"admin")
        }

        def importInventory2 = {
		def j = JSON.parse(params.imp)

// debug print the JSON coming in
if(0) { 
	def t = new JSON(j)
	t.setPrettyPrint(true)
	render(t)
	return
}
		def f = Folio.get(params.folio.toLong())
		def c = new Inventory(name:params.name)
		c.folio = f
		c.validate()
		println c.errors
		c.save()

		j.sections.each { st1->
st1.each { st->
println "st="+st
			def invSec = new InventorySection()
			invSec.inventory = c
			invSec.caption = st.caption
			invSec.sortOrder = st.sortOrder.toInteger()
			invSec.sectionType = st.sectionType
			invSec.validate()
			log.info invSec.errors
			invSec.save()

			st.statements.each { st2->
				def invSt = new InventoryStatement()
				invSt.section = invSec
				invSt.caption = st2.caption
				invSt.sortOrder = st2.sortOrder.toInteger()
				invSt.validate()
				log.info invSt.errors
				invSt.save()
			}
}
		}

		
		redirect(action:"index", controller:"admin")
        }

        def import2 = {
		def j = JSON.parse(params.imp)

// debug print the JSON coming in
if(0) { 
	def t = new JSON(j)
	t.setPrettyPrint(true)
	render(t)
	return
}
		def f = Folio.get(params.folio.toLong())
		def c = new Checklist(name:params.name, sortOrder: 10)
		c.folio = f
		c.validate()
		println c.errors
		c.save()

		j.assessmentStatements.each { st->
			def assessmentStatement = new AssessmentStatement()
			assessmentStatement.checklist = c
			assessmentStatement.sortOrder = st.sortOrder.toInteger()
			assessmentStatement.isDone = st.isDone
			assessmentStatement.name = st.name
			assessmentStatement.validate()
			log.info assessmentStatement.errors
			assessmentStatement.save()
		}
		j.modes.each { m->
			def newmode = new Mode(name:m.name, sortOrder:m.sortOrder.toInteger())
			newmode.checklist = c
			newmode.validate()
			println newmode.errors
			newmode.save()
		}
		j.scaleSets.each { m->
			def newScaleSet = new ScaleSet(name:m.name, sortOrder:m.sortOrder.toInteger())
			newScaleSet.checklist = c
			
			newScaleSet.validate()
			println newScaleSet.errors
			newScaleSet.save()

			m.scales.each { sc->
				def newScale = new Scale(scaleSet:newScaleSet, 
					name: sc.name, sortOrder: sc.sortOrder.toInteger())
					newScale.scaleSet = newScaleSet
					newScale.validate()
					println newScale.errors
					newScale.save()
			}
		}

		j.modes.each { m->
			def newmode = Mode.findByChecklistAndName(c, m.name)
			m.skills.each { ms->
				def skill = new Skill(name: ms.name, 
				sortOrder:ms.sortOrder.toInteger())
				skill.mode= newmode
				skill.validate()
				println skill.errors
				skill.save()
				ms.candoStatements.each { cds->
					def cs = new CandoStatement(statement:cds.statement)
					cs.sortOrder = cds.sortOrder.toInteger()
					cs.skill = skill
					def sstemp = ScaleSet.findByChecklistAndName(c, cds.scale.scaleSet.name)
					def scaletemp = Scale.findByNameAndScaleSet(cds.scale.name, sstemp)
					cs.scale = scaletemp
					cs.validate()
					println "cserrors = " + cs.errors
					cs.save()
				}
			}

		}

		redirect(action:"list")
        }


	def importSpreadsheet1()
	{

	}

	def importSpreadsheet2()
	{
		def f = request.getFile('file')
		def name = "/tmp/upload.xlsx"
		if(f)
		{
			f.transferTo(new File(name))
			def folio = Folio.findByTitle("linguaFolio")
			importingService.checklistImportIntoFolio(name, folio)
		}
	}
}
