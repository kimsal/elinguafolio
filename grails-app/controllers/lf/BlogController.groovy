package lf

import org.springframework.dao.DataIntegrityViolationException

class BlogController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]


	def index() 
	{
		def entries = Blogentry.list().sort{it.publishDate}.reverse()
		[entries:entries]
	}

    def view() {
	def e = Blogentry.read(params.id)
	[e:e]
    }

}
