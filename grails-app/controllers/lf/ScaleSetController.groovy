package lf

class ScaleSetController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [scaleSetInstanceList: ScaleSet.list(params), scaleSetInstanceTotal: ScaleSet.count()]
    }

    def create = {
        def scaleSetInstance = new ScaleSet()
        scaleSetInstance.properties = params
        return [scaleSetInstance: scaleSetInstance]
    }

    def save = {
        def scaleSetInstance = new ScaleSet(params)
        if (scaleSetInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), scaleSetInstance.id])}"
            redirect(action: "show", id: scaleSetInstance.id)
        }
        else {
            render(view: "create", scaleSetl: [scaleSetInstance: scaleSetInstance])
        }
    }

    def show = {
        def scaleSetInstance = ScaleSet.get(params.id)
        if (!scaleSetInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), params.id])}"
            redirect(action: "list")
        }
        else {
            [scaleSetInstance: scaleSetInstance]
        }
    }

    def edit = {
        def scaleSetInstance = ScaleSet.get(params.id)
        if (!scaleSetInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [scaleSetInstance: scaleSetInstance]
        }
    }

    def update = {
        def scaleSetInstance = ScaleSet.get(params.id)
        if (scaleSetInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (scaleSetInstance.version > version) {
                    
                    scaleSetInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'scaleSet.label', default: 'ScaleSet')] as Object[], "Another user has updated this ScaleSet while you were editing")
                    render(view: "edit", scaleSetl: [scaleSetInstance: scaleSetInstance])
                    return
                }
            }
            scaleSetInstance.properties = params
            if (!scaleSetInstance.hasErrors() && scaleSetInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), scaleSetInstance.id])}"
                redirect(action: "show", id: scaleSetInstance.id)
            }
            else {
                render(view: "edit", scaleSetl: [scaleSetInstance: scaleSetInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def scaleSetInstance = ScaleSet.get(params.id)
        if (scaleSetInstance) {
            try {
                scaleSetInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'scaleSet.label', default: 'ScaleSet'), params.id])}"
            redirect(action: "list")
        }
    }
}
