package lf

import lf.*
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.*
import org.apache.poi.*



class InviteController {

	def springSecurityService
	def importingService
	def emailConfirmationService
	def userService

	def index =
	{
		def s = springSecurityService

		if (!s?.isLoggedIn()) {
			redirect(controller: "main")
			return false
		}
		if(session.user.userType=='user') { // not student, not teacher, we're an admin... ?
println "session user type was "+session.user.userType
			redirect(controller: "account")
			return false
		}
println "sss userType is "+session.user.userType

	}

	def choose()
	{
		session.names = params.list("name")
		session.emails= params.list("email")
		session.usernames= params.list("username")
		session.pass = params.password
		def folios = Folio.list()
		[folios:folios]
	}

	def invite()
	{

def topics =params.list("topic")[0].findAll{it.value!=''}
		def realTopics = []
		topics.each { t->
			realTopics << Topic.read(t.value as Long)
		}
		def n = session.names
		def s = session.usernames
		def e = session.emails
		def r = new Random()
		n[0].each { x->
			def fullname = x.value
			def useremail = e[0][x.key as String]
			def username = s[0][x.key as String] ?: useremail
			def u = new Student(fullName:fullname, username:username, email:useremail)
			u.password = springSecurityService.encodePassword(session.pass)
			if(fullname!='' && fullname!=null) { 

				u.myCode = userService.encrypt(r.nextInt(9999999).toString()+u.username).substring(0,8)
				u.myCode += System.currentTimeMillis().toString().reverse().substring(0,7)
				u.validate()
println u.errors
				u.save()
				u.save(flush:true)

				userService.makeRelationship(session.user, u, StatusCode.ACCEPTED)

				Myportfolio userportfolio = addFolio(u)

				realTopics.each { topic->
					def p = new Portfolio(topic:topic)
					p.myportfolio = userportfolio
					p.name = topic.name
					p.save(flush:true)
					userportfolio.addToPortfolios(p)

					def newShare = new Sharing(portfolio:p, owner:u, sharedWith:session.user)
					newShare.save(flush:true)
				}

				
				if(u.save()) { 
					emailConfirmationService.sendConfirmation(
					to:u.email, subject:"You've been invited to eLinguaFolio ", 
					view:'/emailconfirmation/mail/invitation',
					from:"eLinguaFolio <webmaster@elinguafolio.org>",
					model:[password:session.pass, username:u.username, name:u.fullName, 
					inviter:session.user.fullName
					]
					)
				}

			}
		}

		[realTopics:realTopics, names:n[0], emails:e[0], pass:session.pass, usernames:s[0]]
	}



	def addFolio(user)
	{
		def folios = Folio.findAll()
		if(folios.size()==1)
		{
			def myportfolio = Myportfolio.findByFolioAndOwner(folios[0],user)
			if(!myportfolio)
			{
				myportfolio = new Myportfolio(folio:folios[0], owner:user)
				myportfolio.folio = folios[0]
				myportfolio.owner = user
				myportfolio.save()
			} 
			myportfolio.status=StatusCode.MYPORTFOLIO_INUSE
			myportfolio.save()
			return myportfolio
		}

	}
}
