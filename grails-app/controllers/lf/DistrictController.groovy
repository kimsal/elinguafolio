package lf

import org.springframework.dao.DataIntegrityViolationException

class DistrictController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		def all
		def cnt
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		if (session.search!=null)
		{
			def c = District.createCriteria()
			def c2 = District.createCriteria()
		 all = c.list(params) {
			    ilike('name', '%'+session.search+"%")
		}
			cnt = c2.count {
				ilike('name', '%'+session.search+"%")
			}

		} else {
			all = District.list(params)
			cnt = District.count()
		}

        [search: session.search, districtInstanceList: all, districtInstanceTotal: cnt]
    }

    def create() {
        [districtInstance: new District(params)]
    }

    def save() {
        def districtInstance = new District(params)
        if (!districtInstance.save(flush: true)) {
            render(view: "create", model: [districtInstance: districtInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'district.label', default: 'District'), districtInstance.id])
        redirect(action: "show", id: districtInstance.id)
    }

    def show() {
        def districtInstance = District.get(params.id)
        if (!districtInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'district.label', default: 'District'), params.id])
            redirect(action: "list")
            return
        }

        [districtInstance: districtInstance]
    }

	def search = {
		session.search = params.search
		redirect(action:"list")
	}
	def clear = {
		session.search = null
		redirect(action:"list")
	}
    def edit() {
        def districtInstance = District.get(params.id)
        if (!districtInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'district.label', default: 'District'), params.id])
            redirect(action: "list")
            return
        }

        [districtInstance: districtInstance]
    }

    def update() {
        def districtInstance = District.get(params.id)
        if (!districtInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'district.label', default: 'District'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (districtInstance.version > version) {
                districtInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'district.label', default: 'District')] as Object[],
                          "Another user has updated this District while you were editing")
                render(view: "edit", model: [districtInstance: districtInstance])
                return
            }
        }

        districtInstance.properties = params

        if (!districtInstance.save(flush: true)) {
            render(view: "edit", model: [districtInstance: districtInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'district.label', default: 'District'), districtInstance.id])
        redirect(action: "show", id: districtInstance.id)
    }

    def delete() {
        def districtInstance = District.get(params.id)
        if (!districtInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'district.label', default: 'District'), params.id])
            redirect(action: "list")
            return
        }

        try {
            districtInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'district.label', default: 'District'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'district.label', default: 'District'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
