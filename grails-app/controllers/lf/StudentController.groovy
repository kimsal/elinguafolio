package lf

class StudentController {

	def filterService
	def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

	def search = {
		session.studentSearch = params.search
		redirect(action:"list")
	}
	def clear = {
		session.studentSearch = null
		redirect(action:"list")
	}

	def list = {
		def all
		def cnt
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		if (session.studentSearch!=null)
		{
println "SSSS"
			def c = Student.createCriteria()
			def c2 = Student.createCriteria()
			all = c.list(params) {
				or {
					ilike('username', '%'+session.studentSearch+"%")
					ilike('fullName', '%'+session.studentSearch+"%")
					ilike('email', '%'+session.studentSearch+"%")
				}
			}
			cnt = c2.count {
				or {
					ilike('username', '%'+session.studentSearch+"%")
					ilike('fullName', '%'+session.studentSearch+"%")
					ilike('email', '%'+session.studentSearch+"%")
				}
			}

		} else {
			all = Student.list(params)
			cnt = Student.count()
		}

		[studentInstanceList: all, studentInstanceTotal: cnt, search:session.studentSearch]
	}

    def list2 = {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [studentInstanceList: Student.list(params), studentInstanceTotal: Student.count()]
    }
	def filter = {
		if(!params.max) 
		{
			params.max = 20
		}
		render( view:'list', 
	    	model:[ studentInstanceList: filterService.filter( params, Student), 
		studentInstanceCount: filterService.count( params, Student), 
		studentInstanceTotal: Student.count(),
		filterParams: com.zeddware.grails.plugins.filterpane.FilterUtils.extractFilterParams(params), 
		params:params ] )
	}


    def create = {
        def studentInstance = new Student()
        studentInstance.properties = params
        return [studentInstance: studentInstance]
    }

    def save = {
        def studentInstance = new Student(params)
	studentInstance.school = School.get(params.school.toLong()) ?: null
		if(params.password!=null && params.password!='') { 
			studentInstance.password = springSecurityService.encodePassword(params.password)
		}

        if (studentInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'student.label', default: 'Student'), studentInstance.id])}"
            redirect(action: "show", id: studentInstance.id)
        }
        else {
            render(view: "create", model: [studentInstance: studentInstance])
        }
    }

    def show = {
        def studentInstance = Student.get(params.id)
        if (!studentInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), params.id])}"
            redirect(action: "list")
        }
        else {
            [studentInstance: studentInstance]
        }
    }

    def edit = {
        def studentInstance = Student.get(params.id)
        if (!studentInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [studentInstance: studentInstance]
        }
    }

    def update = {
        def studentInstance = Student.get(params.id)
        if (studentInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (studentInstance.version > version) {
                    
                    studentInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'student.label', default: 'Student')] as Object[], "Another user has updated this Student while you were editing")
                    render(view: "edit", model: [studentInstance: studentInstance])
                    return
                }
            }
            studentInstance.properties = params
		studentInstance.school = School.get(params.school.toLong()) ?: null

		if(params.password!=null && params.password!='') { 
		studentInstance.password = springSecurityService.encodePassword(params.password)
		}
            if (!studentInstance.hasErrors() && studentInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'student.label', default: 'Student'), studentInstance.id])}"
                redirect(action: "show", id: studentInstance.id)
            }
            else {
                render(view: "edit", model: [studentInstance: studentInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def studentInstance = Student.get(params.id)
        if (studentInstance) {
            try {
                studentInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'student.label', default: 'Student'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'student.label', default: 'Student'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), params.id])}"
            redirect(action: "list")
        }
    }


	def become = {
		session.user.userType='student'
		session.user = User.findById(params.id)
		session.userId = session.user.id
		session.userid = session.user.id
            redirect(controller:"account", action:"index")
	}
}
