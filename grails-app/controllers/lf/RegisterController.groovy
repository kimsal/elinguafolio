package lf
import lf.*

import groovy.text.SimpleTemplateEngine

import org.codehaus.groovy.grails.commons.ApplicationHolder as AH
import org.codehaus.groovy.grails.plugins.springsecurity.NullSaltSource
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.codehaus.groovy.grails.plugins.springsecurity.ui.RegistrationCode

import grails.plugins.springsecurity.ui.*

class RegisterController extends AbstractS2UiController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def userService
	def cryptoService
	def emailConfirmationService
	def springSecurityService
	def mailService
	def saltSource
//	def grailsApplication

	def index = { }
	
	def student = { }
	def teacher = { }

	def studentProcess = { 
		def r = new Random()
		def lfUserInstance = new Student(params)
		lfUserInstance.password = springSecurityService.encodePassword(params.password)
		lfUserInstance.accountConfirmed = false
		lfUserInstance.myCode = userService.encrypt(r.nextInt(9999999).toString()+lfUserInstance.username).substring(0,8)
		lfUserInstance.myCode += System.currentTimeMillis().toString().reverse().substring(0,7)
		
		if (lfUserInstance.save(flush: true)) {
flash.message = "Your account was created - please check your email "+lfUserInstance.email.encodeAsHTML()+" for a confirmation link"
		    flash.message = "${message(code: 'default.created.message', args: [message(code: 'lfUser.label', default: 'LfUser'), lfUserInstance.id])}"
flash.message = ""
				emailConfirmationService.sendConfirmation(
					lfUserInstance.email, "Please confirm your eLinguaFolio account", 
					[
					view:'/emailconfirmation/mail/confirmationRequest'
					], lfUserInstance.id.toString()
					)
		    redirect(action: "confirm", controller:"main")
		}
		else {
		    render(view: "student", model: [studentInstance: lfUserInstance])
		}
	}

	def teacherProcess = { 
		def r = new Random()
		def lfUserInstance = new Teacher(params)


		if(!userService.isValidTeacherDomain(params.email)) { 
			flash.error = "This domain is not allowed to register as a teacher"
			def e = "This domain is not allowed to register as a teacher"
			render(view: "teacher", model: [teacherInstance: lfUserInstance, message:e])
			println "bad domain"
			return false
		}
		
		lfUserInstance.password = springSecurityService.encodePassword(params.password)
		lfUserInstance.accountConfirmed = false
		lfUserInstance.myCode = userService.encrypt(r.nextInt(9999999).toString()+lfUserInstance.username).substring(0,8)
		lfUserInstance.myCode += System.currentTimeMillis().toString().reverse().substring(0,7)

		
		if (lfUserInstance.save(flush: true)) {
		    flash.message = "${message(code: 'default.created.message', args: [message(code: 'lfUser.label', default: 'LfUser'), lfUserInstance.id])}"
flash.message = ""
//			userService.initialTeacher(lfUserInstance)
				emailConfirmationService.sendConfirmation(
					lfUserInstance.email, "Please confirm your eLinguaFolio account", 
					[
					view:'/emailconfirmation/mail/confirmationRequest'
					], lfUserInstance.id.toString()
					)
		    redirect(action: "confirm", controller:"main")
		}
		else {
		    render(view: "teacher", model: [teacherInstance: lfUserInstance])
		}
	}





	def sendEmail = {
		mailService.sendMail{
			to "mgkimsal@gmail.com"
			from "LinguaFolio NC <webmaster@elinguafolio.org>"
			subject "Testing"
			body "Another test"
		}

	}

	def checkEmail = {
		// Only do this if not confirmed already!
		emailConfirmationService.sendConfirmation(params.emailAddress,
		"Please confirm", [from:"eLinguaFolio <webmaster@elinguafolio.org>"])
	}
	
	def forgotUsername = {

		if (!request.post) {
			// show the form
			return
		}

		String email = params.email
		if ( !email) {
			flash.error = message(code: 'spring.security.ui.forgotUsername.details.missing')
			return
		}

		def user
		if(email) {
			user = lookupUserClass().findByEmail(email)
			if (!user) {
				flash.error = message(code: 'spring.security.ui.forgotUsername.email.notFound')
				return
			}
		}

		def conf = SpringSecurityUtils.securityConfig
//		def body = conf.ui.forgotUsername.emailBody
		def body = "You requested your username be sent to you.  The username we have on file for this email address is \""+user.username+"\""
		mailService.sendMail {
			to user.email
			subject "Your requested username"
			html body.toString()
		}

		[emailSent: true, user:user]
	}

	def resetPassword = { ResetPasswordCommand command ->

		String token = params.t

		def registrationCode = token ? RegistrationCode.findByToken(token) : null
		if (!registrationCode) {
			flash.error = message(code: 'spring.security.ui.resetPassword.badCode')
			redirect uri: SpringSecurityUtils.securityConfig.successHandler.defaultTargetUrl
			return
		}

		if (!request.post) {
			return [token: token, command: new ResetPasswordCommand()]
		}

		command.username = registrationCode.username
		command.validate()

		if (command.hasErrors()) {
			return [token: token, command: command]
		}

		String salt = saltSource instanceof NullSaltSource ? null : registrationCode.username
		RegistrationCode.withTransaction { status ->
			def user = lookupUserClass().findByUsername(registrationCode.username)
			user.password = springSecurityUiService.encodePassword(command.password, salt)
			user.save()
			registrationCode.delete()
		}

		springSecurityService.reauthenticate registrationCode.username

		flash.message = message(code: 'spring.security.ui.resetPassword.success')

		def conf = SpringSecurityUtils.securityConfig
		String postResetUrl = conf.ui.register.postResetUrl ?: conf.successHandler.defaultTargetUrl
		redirect uri: postResetUrl
	}

	def forgotPassword = {

		if (!request.post) {
			// show the form
			return
		}

		String username = params.username
		String email = params.email
		if (!username && !email) {
			flash.error = "Something's missing..."
			return
		}

		def user
		if(username && !email) {
			user = lookupUserClass().findByUsername(username)
			if (!user) {
//				flash.error = message(code: 'spring.security.ui.forgotPassword.user.notFound')
				flash.error = "Username not found"
				return
			}
		}
		if(!username && email) {
			user = lookupUserClass().findByEmail(email)
			if (!user) {
//				flash.error = message(code: 'spring.security.ui.forgotPassword.email.notFound')
				flash.error = "Supplied email of "+email.encodeAsHTML()+" not found"
				return
			}
		}
		if(username && email) {
			user = lookupUserClass().findByUsername(username)
			if (!user) {
				user = lookupUserClass().findByUsername(email)
//				flash.error = message(code: 'spring.security.ui.forgotPassword.neither.notFound')
				flash.error = "Couldn't find that information"
				return
			}
		}



		def registrationCode = new RegistrationCode(username: user.username)
		registrationCode.dateCreated = new Date()
		registrationCode.validate()
		log.info "rcerrors="+registrationCode.errors
		registrationCode.save()

		String url = generateLink('resetPassword', [t: registrationCode.token])

		def conf = SpringSecurityUtils.securityConfig
		def body = conf.ui.forgotPassword.emailBody
		if (body.toString().contains('$')) {
			body = evaluate(body, [user: user, url: url])
		}
		mailService.sendMail {
			to user.email
			subject conf.ui.forgotPassword.emailSubject.toString()
			html body.toString()
		}

		flash.message = "Reset email sent to "+email.encodeAsHTML()+".  Please check your spam filter for the email..."
		[emailSent: true, email:email]
	}

	protected String generateLink(String action, linkParams) {
		def base
		if(grailsApplication.config.grails.serverURL!="")
		{
			base = grailsApplication.config.grails.serverURL 
		} else {
			base = "$request.scheme://$request.serverName:$request.serverPort$request.contextPath"
		}
		createLink(base: base,
				controller: 'register', action: action,
				params: linkParams)

	}

	protected String evaluate(s, binding) {
		new SimpleTemplateEngine().createTemplate(s).make(binding)
	}

	static final passwordValidator = { String password, command ->
		if (command.username && command.username.equals(password)) {
			return 'command.password.error.username'
		}

		if (password && password.length() >= 8 && password.length() <= 64 &&
				(!password.matches('^.*\\p{Alpha}.*$') ||
				!password.matches('^.*\\p{Digit}.*$') ||
				!password.matches('^.*[!@#$%^&].*$'))) {
			return 'command.password.error.strength'
		}
	}

	static final password2Validator = { value, command ->
		if (command.password != command.password2) {
			return 'command.password2.error.mismatch'
		}
	}

}
class RegisterCommand {

	String username
	String email
	String password
	String password2

	static constraints = {
		username blank: false, validator: { value, command ->
			if (value) {
				def User = AH.application.getDomainClass(
					SpringSecurityUtils.securityConfig.userLookup.userDomainClassName).clazz
				if (User.findByUsername(value)) {
					return 'registerCommand.username.unique'
				}
			}
		}
		email blank: false, email: true
		password blank: false, minSize: 8, maxSize: 64, validator: RegisterController.passwordValidator
		password2 validator: RegisterController.password2Validator
	}
}

class ResetPasswordCommand {
	String username
	String password
	String password2

	static constraints = {
		password blank: false, minSize: 8, maxSize: 64, validator: RegisterController.passwordValidator
		password2 validator: RegisterController.password2Validator
	}
}
