package lf

class CandoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [candoInstanceList: Cando.list(params), candoInstanceTotal: Cando.count()]
    }

    def create = {
        def candoInstance = new Cando()
        candoInstance.properties = params
        return [candoInstance: candoInstance]
    }

    def save = {
        def candoInstance = new Cando(params)
        if (candoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'cando.label', default: 'Cando'), candoInstance.id])}"
            redirect(action: "show", id: candoInstance.id)
        }
        else {
            render(view: "create", model: [candoInstance: candoInstance])
        }
    }

    def show = {
        def candoInstance = Cando.get(params.id)
        if (!candoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cando.label', default: 'Cando'), params.id])}"
            redirect(action: "list")
        }
        else {
            [candoInstance: candoInstance]
        }
    }

    def edit = {
        def candoInstance = Cando.get(params.id)
        if (!candoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cando.label', default: 'Cando'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [candoInstance: candoInstance]
        }
    }

    def update = {
        def candoInstance = Cando.get(params.id)
        if (candoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (candoInstance.version > version) {
                    
                    candoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'cando.label', default: 'Cando')] as Object[], "Another user has updated this Cando while you were editing")
                    render(view: "edit", model: [candoInstance: candoInstance])
                    return
                }
            }
            candoInstance.properties = params
            if (!candoInstance.hasErrors() && candoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'cando.label', default: 'Cando'), candoInstance.id])}"
                redirect(action: "show", id: candoInstance.id)
            }
            else {
                render(view: "edit", model: [candoInstance: candoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cando.label', default: 'Cando'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def candoInstance = Cando.get(params.id)
        if (candoInstance) {
            try {
                candoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'cando.label', default: 'Cando'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'cando.label', default: 'Cando'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'cando.label', default: 'Cando'), params.id])}"
            redirect(action: "list")
        }
    }
}
