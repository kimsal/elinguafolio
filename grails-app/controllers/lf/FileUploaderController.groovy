package lf
import lf.*

/**
 * originally from a plugin at http://github.com/lucastex/grails-file-uploader
 * but incorporated in to the project directly to accommodate user tie-in directly
 */

class FileUploaderController {
	
	//messagesource
	def messageSource

	//defaultaction
	def defaultAction = "process"

	def process = {

		//upload group
		def upload = params.upload
				
		//config handler
		def config = grailsApplication.config.fileuploader[upload]
		
		//request file
		def file = request.getFile("file")
		
		//base path to save file
		def path = config.path
		if (!path.endsWith('/'))
		{
			path = path+"/"
		}
		
log.info "ulpath=" + path		
//		render("file="+upload)
		/**************************
			check if file exists
		**************************/
		if (file.size == 0) {
			def msg = messageSource.getMessage("fileupload.upload.nofile", null, request.locale)
			log.debug msg
			flash.message = msg
			redirect controller: params.successController, action: "viewEvidences", id:params.candoStatementId
			return
		}
		
		/***********************
			check extensions
		************************/
		def fileExtension = file.originalFilename.substring(file.originalFilename.lastIndexOf('.')+1).toLowerCase()
		if (!config.allowedExtensions[0].equals("*")) {
			if (!config.allowedExtensions.contains(fileExtension)) {
				def msg = messageSource.getMessage("fileupload.upload.unauthorizedExtension", [fileExtension, config.allowedExtensions] as Object[], request.locale)
				log.debug msg
				flash.message = msg
				redirect controller: params.successController, action: "viewEvidences", id:params.candoStatementId
				return
			}
		}
		
		
		/*********************
			check file size
		**********************/
		if (config.maxSize) { //if maxSize config exists
			def maxSizeInKb = ((int) (config.maxSize/1024))
			if (file.size > config.maxSize) { //if filesize is bigger than allowed
				log.debug "FileUploader plugin received a file bigger than allowed. Max file size is ${maxSizeInKb} kb"
				flash.message = messageSource.getMessage("fileupload.upload.fileBiggerThanAllowed", [maxSizeInKb] as Object[], request.locale)
				redirect controller: params.errorController, action: params.errorAction
				return
			}
		} 
		
		//reaches here if file.size is smaller or equal config.maxSize or if config.maxSize ain't configured (in this case
		//plugin will accept any size of files).
		
		// get the currentTime
		def currentTime = System.currentTimeMillis()

		def username = session.user.username

		path = path+username+"/"

		if (!new File(path).mkdirs())
		{
			log.error "FileUploader plugin couldn't create directories: [${path}]"
		}

		// don't ever want to store the original filename
		// on disk - might be security risk
		// if we can come up with a way to neuter the name
		// then yes, store it - would be easier to debug later
		// path = path+file.originalFilename
		path = path + currentTime
		
		//move file
		log.info "FileUploader plugin received a ${file.size}b file. Moving to ${path}"
		file.transferTo(new File(path))
		
		//save it on the database
		def ufile = new UFile()

		def tika = new org.apache.tika.Tika()
		def detectedMime = tika.detect(new File(path))

		ufile.mime = detectedMime
		ufile.name = file.originalFilename
		ufile.size = file.size
		ufile.extension = fileExtension
		ufile.dateUploaded = new Date(currentTime)
		ufile.path = path
		ufile.downloads = 0
		ufile.user = session.user
		ufile.save()
		
		flash.fileId = ufile.id
		flash.candoEntryId = params.candoEntry
		flash.candoStatementId = params.candoStatementId
		flash.topicid = params.topicId
		redirect(controller: params.successController, action: params.successAction)
	}

}
