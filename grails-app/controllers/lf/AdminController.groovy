package lf
import lf.*

class AdminController {

        def springSecurityService
	def setupService

    def index = { }

	def u = {}

	def loaduser = {
		def p = params.users.split("\r")
		p.each { l->
			def parts = l.split("_")
			def k = Teacher.findByUsername(parts[0])
			if(k==null) { 
				k = new Teacher()
			}
			k.myCode=parts[4].trim()
			k.fullName=parts[1].trim()
			k.email=parts[2].trim()
			k.username=parts[0].trim()
			k.password = springSecurityService.encodePassword(parts[3])
			k.validate()
//render("<HR>")
//render(k.errors)
			k.save(flush:true)
//
render("update user set password='" + k.password + "', my_code='" + k.myCode + "' where username ='" + k.username + "';<br/>")
//			render(parts[0] + "\t created with password " + parts[3] + " looks like " + k.password + " and code = " + k.myCode + "<BR>")
		}

	}

	def importSchools = 
	{
		def num = setupService.importSchools()
		flash.message = num + " schools imported or reset"
		redirect(url:"/admin")
	}

	def changePassword = 
	{
	}

	def changePasswordProcess = 
	{
		def u = session.user
		u.password = springSecurityService.encodePassword(params.password)
		u.validate()
		println "validating="+u.errors
		u.save(flush:true)
		session.user = u
		redirect(action:"index")
	}
}
