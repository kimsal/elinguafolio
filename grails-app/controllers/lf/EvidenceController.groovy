package lf

class EvidenceController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [evidenceInstanceList: Evidence.list(params), evidenceInstanceTotal: Evidence.count()]
    }

    def create = {
        def evidenceInstance = new Evidence()
        evidenceInstance.properties = params
        return [evidenceInstance: evidenceInstance]
    }

    def save = {
        def evidenceInstance = new Evidence(params)
        if (evidenceInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'evidence.label', default: 'Evidence'), evidenceInstance.id])}"
            redirect(action: "show", id: evidenceInstance.id)
        }
        else {
            render(view: "create", model: [evidenceInstance: evidenceInstance])
        }
    }

    def show = {
        def evidenceInstance = Evidence.get(params.id)
        if (!evidenceInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'evidence.label', default: 'Evidence'), params.id])}"
            redirect(action: "list")
        }
        else {
            [evidenceInstance: evidenceInstance]
        }
    }

    def edit = {
        def evidenceInstance = Evidence.get(params.id)
        if (!evidenceInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'evidence.label', default: 'Evidence'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [evidenceInstance: evidenceInstance]
        }
    }

    def update = {
        def evidenceInstance = Evidence.get(params.id)
        if (evidenceInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (evidenceInstance.version > version) {
                    
                    evidenceInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'evidence.label', default: 'Evidence')] as Object[], "Another user has updated this Evidence while you were editing")
                    render(view: "edit", model: [evidenceInstance: evidenceInstance])
                    return
                }
            }
            evidenceInstance.properties = params
            if (!evidenceInstance.hasErrors() && evidenceInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'evidence.label', default: 'Evidence'), evidenceInstance.id])}"
                redirect(action: "show", id: evidenceInstance.id)
            }
            else {
                render(view: "edit", model: [evidenceInstance: evidenceInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'evidence.label', default: 'Evidence'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def evidenceInstance = Evidence.get(params.id)
        if (evidenceInstance) {
            try {
                evidenceInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'evidence.label', default: 'Evidence'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'evidence.label', default: 'Evidence'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'evidence.label', default: 'Evidence'), params.id])}"
            redirect(action: "list")
        }
    }
}
