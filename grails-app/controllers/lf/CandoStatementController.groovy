package lf

class CandoStatementController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [candoStatementInstanceList: CandoStatement.list(params), candoStatementInstanceTotal: CandoStatement.count()]
    }

    def create = {
        def candoStatementInstance = new CandoStatement()
        candoStatementInstance.properties = params
        return [candoStatementInstance: candoStatementInstance]
    }

    def save = {
        def candoStatementInstance = new CandoStatement(params)
        if (candoStatementInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), candoStatementInstance.id])}"
            redirect(action: "show", id: candoStatementInstance.id)
        }
        else {
            render(view: "create", model: [candoStatementInstance: candoStatementInstance])
        }
    }

    def show = {
        def candoStatementInstance = CandoStatement.get(params.id)
        if (!candoStatementInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), params.id])}"
            redirect(action: "list")
        }
        else {
            [candoStatementInstance: candoStatementInstance]
        }
    }

    def edit = {
        def candoStatementInstance = CandoStatement.get(params.id)
        if (!candoStatementInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [candoStatementInstance: candoStatementInstance]
        }
    }

    def update = {
        def candoStatementInstance = CandoStatement.get(params.id)
        if (candoStatementInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (candoStatementInstance.version > version) {
                    
                    candoStatementInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'candoStatement.label', default: 'CandoStatement')] as Object[], "Another user has updated this CandoStatement while you were editing")
                    render(view: "edit", model: [candoStatementInstance: candoStatementInstance])
                    return
                }
            }
            candoStatementInstance.properties = params
            if (!candoStatementInstance.hasErrors() && candoStatementInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), candoStatementInstance.id])}"
                redirect(action: "show", id: candoStatementInstance.id)
            }
            else {
                render(view: "edit", model: [candoStatementInstance: candoStatementInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def candoStatementInstance = CandoStatement.get(params.id)
        if (candoStatementInstance) {
            try {
                candoStatementInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoStatement.label', default: 'CandoStatement'), params.id])}"
            redirect(action: "list")
        }
    }
}
