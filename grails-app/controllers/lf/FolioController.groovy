package lf

class FolioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [folioInstanceList: Folio.list(params), folioInstanceTotal: Folio.count()]
    }

    def create = {
        def folioInstance = new Folio()
        folioInstance.properties = params
        return [folioInstance: folioInstance]
    }

    def save = {
        def folioInstance = new Folio(params)
        if (folioInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'folio.label', default: 'Folio'), folioInstance.id])}"
            redirect(action: "show", id: folioInstance.id)
        }
        else {
            render(view: "create", model: [folioInstance: folioInstance])
        }
    }

    def show = {
        def folioInstance = Folio.get(params.id)
        if (!folioInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'folio.label', default: 'Folio'), params.id])}"
            redirect(action: "list")
        }
        else {
            [folioInstance: folioInstance]
        }
    }

    def edit = {
        def folioInstance = Folio.get(params.id)
        if (!folioInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'folio.label', default: 'Folio'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [folioInstance: folioInstance]
        }
    }

    def update = {
        def folioInstance = Folio.get(params.id)
        if (folioInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (folioInstance.version > version) {
                    
                    folioInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'folio.label', default: 'Folio')] as Object[], "Another user has updated this Folio while you were editing")
                    render(view: "edit", model: [folioInstance: folioInstance])
                    return
                }
            }
            folioInstance.properties = params
            if (!folioInstance.hasErrors() && folioInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'folio.label', default: 'Folio'), folioInstance.id])}"
                redirect(action: "show", id: folioInstance.id)
            }
            else {
                render(view: "edit", model: [folioInstance: folioInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'folio.label', default: 'Folio'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def folioInstance = Folio.get(params.id)
        if (folioInstance) {
            try {
                folioInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'folio.label', default: 'Folio'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'folio.label', default: 'Folio'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'folio.label', default: 'Folio'), params.id])}"
            redirect(action: "list")
        }
    }
}
