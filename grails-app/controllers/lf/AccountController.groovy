package lf
import org.grails.plugins.settings.*
import grails.converters.JSON

import lf.*

class AccountController {
	
	def burningImageService
	def grailsApplication
	def imageUploadService
	def evidenceService
	def userService
	def springSecurityService
	def invService
//	def mqvideoService
	def thumbnailingService
	
//	def beforeInterceptor = [action:this.&selectschool,except:['selectschool','setschool','schoolsearch']]
	def beforeInterceptor = 
	{

		if(session.user.userType=='user') { // not student, not teacher, we're an admin... ?
			redirect(controller:"admin")
			return false
		}
		if(session.user?.school == null)
		{
//			if(!['selectschool','setschool','schoolsearch'].contains(actionName))
//			{
//				redirect(action:"selectschool")
//				return false
//			}
		} else { 

			def myp = Myportfolio.findAllByOwner(session.user).portfolios[0]?.toList()

			if(myp==null || myp.size()==0)
			{
				def f = new grails.util.GrailsNameUtils().getShortName(session.user.class).toLowerCase()
				if(f=='student' || f=='teacher' || f=='lf.student' || f=='lf.teacher')
				{
					if(!['updatePortfolios','addMyportfolio','editPortfolios','addPortfolios','schoolsearch'].contains(actionName))
					{
						redirect(action:"addPortfolios")
						return false
					}
				}
			}
		}

	}

	def setschool = 
	{
		session.user.school = School.get(params.school)
		if(params.grade!=null)
		{
			session.user.grade = params.grade
		}
		session.user.save()
		flash.message = "Your school info was updated"
		redirect(action:"index")
		return false
	}

	def updateschool = 
	{
println params
		if(session.user.userType!='teacher') { // not student, not teacher, we're an admin... ?
			if(params.grade!='') { 
				session.user.grade = params.grade
			}
		}
		if(params.school!='') { 
			session.user.school = School.get(params.school)
		}
		session.user.save(flush:true)
		flash.message = "Your school/grade was updated"
		redirect(action:"my")
return
	}


	def selectschool = 
	{
		def userType = new grails.util.GrailsNameUtils().getShortName(session.user.class).toLowerCase()
		[userType:userType]
	}

	def schoolsearch = 
	{
		def q = params.query?.replace(" ","%")
		def schools= lf.School.findAllByNameLike("%"+q+"%")

	//Create XML response 
			 render(contentType: "text/xml") { 
					results() { 
							schools.each { school-> 
										result(){ 
											name(school.district.name + " : " + school.name) 
												//Optional id which will be available in onItemSelect 
												id(school.id) 
									} 
							} 
					} 
			 } 
	}

	def index = { 
		redirect(action:"myPortfolios")
		return false
		[myportfolios:
			Myportfolio.findAllByOwnerAndStatus(
			session.user, StatusCode.MYPORTFOLIO_INUSE
		)]
	}

	def addPortfolios = {
		def folios = Folio.findAll()
		if(folios.size()==1)
		{
			def myportfolio = Myportfolio.findByFolioAndOwner(folios[0],session.user)
			if(!myportfolio)
			{
				myportfolio = new Myportfolio(folio:folios[0], owner:session.user)
				myportfolio.folio = folios[0]
				myportfolio.owner = session.user
				myportfolio.save()
			} 
			myportfolio.status=StatusCode.MYPORTFOLIO_INUSE
			myportfolio.save()
			session.currentMyportfolio = myportfolio
			redirect(action:"editPortfolios", id:myportfolio.id)
			return
		}
		[folios:folios]
	}

	def removePortfolio = 
	{
		/*
		def folio = Folio.get(params.id)
		if(folio==null)
		{
			flash.message = "Selected portfolio not found"
			redirect(action:"index")
			return false
		}
		*/
		def myportfolio = Myportfolio.findByIdAndOwner(params.id,session.user)
		if(!myportfolio)
		{
			flash.message = "Not your portfolio"
			redirect(action:"index")
			return false
		}
		myportfolio.status = StatusCode.MYPORTFOLIO_REMOVED
		myportfolio.save()
		flash.message = "Portfolio removed "
		redirect(action:"myPortfolios")
		return 
	}

	def addMyportfolio =
	{
		def folio = Folio.get(params.id)
		if(folio==null)
		{
			flash.message = "Selected portfolio not found"
			redirect(action:"addPortfolios")
			return false
		}
		def myportfolio = Myportfolio.findByFolioAndOwner(folio,session.user)
		if(myportfolio)
		{
			myportfolio.status = StatusCode.MYPORTFOLIO_INUSE
			myportfolio.save()
			redirect(action:"myPortfolios")
			return false
		}
		def m = new Myportfolio(owner:session.user, folio:folio).save()
		flash.message = folio.title + " added to your account"
		session.currentMyportfolio = m
		redirect(action:"editPortfolios")
	}
	
	def viewMyportfolio = 
	{
		def m = Myportfolio.findByIdAndOwner(params.id, session.user)
		if(m==null)
		{
			flash.message = "Portfolio not found"
			redirect(action:"myPortfolios")
			return false
		}

		[myportfolio:m]

	}

	def biography = 
	{
		def theUser = session.viewingFriend ?: session.user

		def p = Myportfolio.findByIdAndOwner(params.id, theUser)
		if(!p || p==null)
		{
			redirect(action:"index")
			return false
		} 
		session.folio = p.folio
		def bio = Biography.findByFolio(p.folio)
		if(bio==null)
		{
			bio = new Biography(folio:p.folio, name:"Biography for " + theUser + "'s " + p.folio.title)
			bio.save()
		}
		def mybio = Mybiography.findByOwnerAndBio(theUser, bio)
		if(mybio==null)
		{
			mybio = new Mybiography(owner:theUser, bio:bio)
		mybio.validate()
		println mybio.errors
		mybio.save()
		}
		[myportfolio:p, mybio:mybio, viewingFriend: session.viewingFriend, user:theUser]
	}


/*
 * hack passport for now - assuming only langauge?
 */
	def passport = 
	{
		def theUser = session.viewingFriend ?: session.user

		def myportfolio = Myportfolio.findByIdAndOwner(params.id as Long, theUser)
		if(myportfolio==null)
		{
			redirect(action:"index")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = Passport.findByFolioAndOwner(f, theUser)
		if(p==null)
		{
			if(f.title=='LinguaFolio') {
				p = new PassportLanguage(folio:f, owner: theUser)
				p.validate()
				println p.errors
				p.save()
			}
		}
		[tab:params.tab, passport:p, myportfolio:myportfolio, viewingFriend: session.viewingFriend]
	}

	def updateLearningInventory = 
	{
		def myinv= MyInventory.get(params.myli)

		if(myinv==null || myinv.owner!=session.user)
		{
			redirect(action:"index")
			return
		}

		params.answer.each { b->
			def ba = MyInventoryAnswer.findByMyinventoryAndStatement(myinv, InventoryStatement.get(b.key))
			if(ba==null)
			{
				ba = new MyInventoryAnswer(myinventory:myinv, statement:InventoryStatement.get(b.key)).save()
			}
			ba.value = b.value
			ba.save()
		}
		render text:"Updated!", contentType:"text/plain"
		return false
	}

	def updateHowDoILearn = 
	{
		// main 'folio' from session
		def folio = session.folio
		if(folio==null)
		{
			redirect(action:"index")
			return
		}
		def mybio = Mybiography.findByOwnerAndBio(session.user, Biography.findByFolio(folio))
		if(mybio==null)
		{
			redirect(action:"index")
			return
		}

		params.bioAnswer.each { b->
			def ba = MybiographyAnswer.findByMybioAndStatement(mybio, BioStatement.get(b.key))
			if(ba==null)
			{
				ba = new MybiographyAnswer(mybio:mybio, statement:BioStatement.get(b.key)).save()
			}
			ba.value = b.value
			ba.save()
		}
		flash.message = "Learning Biography updated"
		redirect(action:"biography", id:mybio.bio.id)
		return


	}


	/*
	 * show my account 
	 * list my portfolios and allow me to add new portfolios based on existing folio options
	 */
	def my= { 
		session.user.refresh()
		/*
if(session.user.hasRolename("admin"))	{
	render("is admin!")
}	else {
render("NOPE!")
}
return false
*/		
		
		def p = session.user.myportfolios*.portfolios
		def f = Folio.list()
		def folios = []

		def userType = new grails.util.GrailsNameUtils().getShortName(session.user.class).toLowerCase()
		[userType:userType, portfolios: p, folios: folios]
	}
	
	
	/*
	 * edit my portfolios
	 * list available portfolios in this folio (assume language for now)
	 * show checkboxes of available options and ones for the current student/user 
	 */
	def editPortfolios= { 
		session.user.refresh()

		def theUser = session.viewingFriend ?: session.user


		def myp = Myportfolio.get(params.id)
		if(myp.owner.id!=theUser.id)
		{
			flash.message = "You do not own this portfolio"
			redirect(action:"myPortfolios")
			return
		}
		session.currentMyportfolio = myp
		def p = myp.portfolios
		/*
		p.each { i->
			render(i.status + "<BR>")
		}
		return
		*/
		def folio = myp.folio
		def cl = Checklist.findAll().sort{it.sortOrder}
		[ viewingFriend: session.viewingFriend, myportfolio: myp, folios:Folio.findAll(), subjects:Subject.findAll(), checklist: cl, portfolios: p, folio: folio]
	}	
	
	def updatePortfolios = {
		def selectedTopics = []
		params.topic.each { t->
			if(t.value!="") 
			{
				selectedTopics << Topic.findById(t.value.toInteger())
			}
		}
		println "st="+selectedTopics
		session.user.refresh()
		def portfolios = session.currentMyportfolio.portfolios
		def u = session.user

		def myp = Myportfolio.get(session.currentMyportfolio.id)
		
		selectedTopics.each { s->
			if(portfolios!=null)
			{
				def p = Portfolio.withCriteria {
					and {
						eq('myportfolio',session.currentMyportfolio)
						eq('topic',s)
					}
				}[0]
			//	def p = Portfolio.findByOwnerAndTopic(u,s)
				if(p==null)
				{
					p = new Portfolio(myportfolio:myp,
						status:StatusCode.PORTFOLIO_INUSE,
							user:u, topic: s, name: s.name + " portfolio")
					p.validate()
println p.errors
p.save()
					myp.addToPortfolios(p)
					//session.currentMyportfolio.merge(flush:true)
					//session.currentMyportfolio.save(flush:true)
				} else {
					if(p.status==StatusCode.PORTFOLIO_REMOVED)
					{
						p.status=StatusCode.PORTFOLIO_INUSE
					}
					p.save();
					myp.addToPortfolios(p)
				}
			} else {
					def p2 = new Portfolio(myportfolio:myp,
							status:StatusCode.PORTFOLIO_INUSE,
							user:u, topic: s, name: s.name + " portfolio").save()
					myp.addToPortfolios(p2)
			}
		}
		portfolios.each { p3->
			def newp = Portfolio.get(p3.id)
			if(!selectedTopics.contains(newp.topic))
			{
				println newp.topic.toString() + " not found in selected topics"
				//render(newp.topic.toString() + " not found in selected topics<BR>")
					newp.status = StatusCode.PORTFOLIO_REMOVED
					newp.validate()
				println newp.errors
					newp.save(flush:true)
			}
		}
			myp.validate()
		myp.save(flush:true)
		session.currentMyportfolio = myp
		
		flash.message = "Subjects updated!"
		redirect(action:"index")
		
	}
	
	def addPortfolio = {
		def f = Folio.get(params.id)
		def m = Myportfolio.findByFolioAndOwner(f,session.user)
		if(m==null)
		{
			m = new Myportfolio(owner:session.user, folio:f).save()
		}
		m.status = StatusCode.MYPORTFOLIO_INUSE
		def p = new lf.Portfolio(name:"Portfolio for " + session.user.name, folio:f).save(flush:true)
		m.addToPortfolios(p)
		m.save()
		println p.errors
		//		session.user.addToPortfolios(p)
		//		session.user.save(flush:true)
		redirect(action:"my")
	}
	
	def viewFriends = {
		session.user.refresh()
		redirect(action:"friends")
		def blocked = userService.getAllBlockedUsers(session.user)
		def friends = userService.getAllFriendsByStatus(session.user, StatusCode.ACCEPTED)
		def teachers = userService.getAllMyTeachersByStatus(session.user, StatusCode.ACCEPTED)
		def accepted = userService.getAllMyFriendsByStatus(session.user, StatusCode.ACCEPTED)
		def acceptedTeacher = userService.getAllMyTeachersByStatus(session.user, StatusCode.ACCEPTED)
		def waitingToHear = userService.getAllMyFriendsByStatus(session.user,  StatusCode.INITIATED)
		def waitingToHearTeacher = userService.getAllMyTeachersByStatus(session.user,  StatusCode.INITIATED)
		def requests = userService.getAllMyFriendRequests(session.user)
		//def requestTeachers = userService.getAllMyTeacherRequests(session.user)
		[requests: requests,
	//	requestTeachers:requestTeachers,
			acceptedTeachers:acceptedTeacher,
			acceptedFriends:accepted, waitingToHearBackTeachers:waitingToHearTeacher,
			waitingToHearBackFriends: waitingToHear,
			blocked:blocked, friends:friends, teachers:teachers]
	}	

	def myPortfolios = {
		session.viewingFriend = null

		def folios = Folio.findAll()
		def myportfolios = Myportfolio.findAllByOwnerAndStatus(
			session.user, StatusCode.MYPORTFOLIO_INUSE
		)
		if(myportfolios.size()!=folios.size()) { 
			folios.each { folio->
				def myportfolio = Myportfolio.findByFolioAndOwner(folio,session.user)
				if(!myportfolio)
				{
					myportfolio = new Myportfolio(folio:folio, owner:session.user).save()
				} 
				myportfolio.status=StatusCode.MYPORTFOLIO_INUSE
				myportfolio.save()
				session.currentMyportfolio = myportfolio
			}
			myportfolios = Myportfolio.findAllByOwnerAndStatus(
				session.user, StatusCode.MYPORTFOLIO_INUSE
			)
		}
	

		[
		selected: params.id,
		folios:folios,
		myportfolios: myportfolios
		]
	}

	def sharedFriend = {
		def folios = Folio.findAll()
		def myportfolios = Myportfolio.findAllByOwnerAndStatus(
				session.viewingFriend, StatusCode.MYPORTFOLIO_INUSE
		)

		[
				viewingFriend: session.viewingFriend,
				selected: params.id,
				folios:folios,
				myportfolios: myportfolios
		]
	}


	def viewPortfolios = {
		def sharedWithMe = []
		if (session.viewingFriend)
		{
			def x = getSharedWithMe(session.user)
			x.each { sw->
				sharedWithMe << sw.portfolio
			}
		}

		def theUser = session.viewingFriend ?: session.user

		def p = Myportfolio.findByIdAndOwner(params.id, theUser)

		def myFriends = null
		if(session.user.userType=='teacher') { 
			myFriends = userService.getAllMyFriends(theUser)
		} else {
			myFriends = null
		}

		def t=Myportfolio.findAllByIdAndOwner(params.id, theUser)*.portfolios[0]
// 4/1/12
// ugly hack - if we delete a checklist (urdu, for example)
// and someone's got it in their portfolio, we'll get stuff broken
// this gets around it for now
		def finallist = []
		def removes= []
		def mypid = null
		t.each { temp->
			try {
				if(
						(session.user==temp.myportfolio.owner)
					||
						(sharedWithMe*.id.contains(temp.id))
				)
				{
					finallist<<temp
				} else {
				}
			} catch (Exception ex) {
				println ex
//				removes << temp.id
//				mypid = temp.myportfolio.id as Long
			} 
		}

                def csCounts = CandoStatement.withCriteria {
                    projections{
                        groupProperty "skill.id"
                        groupProperty "scale.id"
                        count "id" ,"c"
                    }
                }

		[
		csCounts:csCounts,
		viewingFriend: session.viewingFriend,
		myPortfolio:p,
		portfolios:finallist,
		myFriends: myFriends,
		myTeachers: userService.getAllMyTeachers(theUser),
		user:User.get(theUser.id)
//		myLearningInventory: myLearningInventory
			]
	}
	
	def viewSharedStatements = {
		
		def s 
		def d
		def cs

		session.scale= null
		if(params.scale) { 
			session.scale = params.scale
		}
		session.skill= null
		if(params.skill) { 
			session.skill= params.skill
		}
		if(params.portfolio) { 
			session.portfolio= params.portfolio
		}

		if(params.id) { 
			session.portfolio= params.id
		}
	
		d = Skill.get(session.skill)
		s = Scale.get(session.scale)

		def view = "viewSharedStatements"
		if(d!=null && s!=null)
		{
			cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
		} else if (d!=null) {
			view = "viewSharedStatementsSkill"
			cs = lf.CandoStatement.findAllBySkill(d)
		} else {
			view = "viewSharedStatementsScale"
			cs = lf.CandoStatement.findAllByScale(s)
		}

/*
		def s = Scale.get(params.scale)
		def d = Skill.get(params.skill)
		def cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
*/
		def portfolio = Portfolio.get(params.portfolio)

		def isShared = false
		def t = Sharing.findBySharedWithAndPortfolio(session.user, portfolio)
println "sharing info found... "+t
		if(t!=null) { 
			isShared = true
		}

		if(isShared==false)
		{
println "not shared with you..."
			flash.message = "This is not shared with you"
			redirect(action:"myPortfolios")
			return false
		}
	
		def myCandos = lf.CandoEntry.findAllByUserAndPortfolio(portfolio.myportfolio.owner, portfolio).findAll{
			cs.contains(it.statement) }.sort{it.statement.sortOrder}

		def myp = Myportfolio.findByOwner(session.user)
println "going to render view "+view
		render(view:view, model:[shared: t, myportfolio: myp, skill:d, portfolio:portfolio, candoStatements:cs, myCandos:myCandos, scale:s])
	}

	def viewStatements = {
		

		def s 
		def d
		def cs

		session.scale= null
		if(params.scale) { 
			session.scale = params.scale
		}
		session.skill= null
		if(params.skill) { 
			session.skill= params.skill
		}
		if(params.portfolio) { 
			session.portfolio= params.portfolio
		}

		if(params.id) { 
			session.portfolio= params.id
		}
	
		d = Skill.get(session.skill)
		s = Scale.get(session.scale)

		def view = "viewStatements"
		if(d!=null && s!=null)
		{
			cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
		} else if (d!=null) {
			view = "viewStatementsSkill"
			cs = lf.CandoStatement.findAllBySkill(d)
		} else {
			view = "viewStatementsScale"
			cs = lf.CandoStatement.findAllByScale(s)
		}
		
		def portfolio = Portfolio.get(session.portfolio)

		/**
		 * if current user doesn't have candoentry objects associated for this portfolio, create them
		 */
		//def myCandos = lf.CandoEntry.findAllByUserAndPortfolio(session.user, portfolio).sort{it.statement.sortOrder}
			
		cs.each { origCando->
			def mine
			def lc = lf.CandoEntry.createCriteria()
			if (lc.count{
				eq('user', session.user)
				eq('statement', origCando)
				eq('portfolio', portfolio)
				}==0)
			{
				def t = new lf.CandoEntry()
				t.user = session.user
				t.statement = origCando
				t.portfolio = portfolio
				t.validate()
				println "errors="+t.errors
				t.save(flush:true)
			}
		}
		
		/**
		 * then reload them
		 */
		def myCandos = lf.CandoEntry.findAllByUserAndPortfolio(session.user, portfolio).sort{it.statement.sortOrder}

		render(view:view, model:[skill:d, portfolio:portfolio, candoStatements:cs, myCandos:myCandos, scale:s])
	}
	
	def resetParams(params) {
		if(params.skill!='' && params.skill!=null) {
			session.skill = params.skill
			session.scale = params.scale
			session.portfolio = params.portfolio
		} else {
			params.skill = session.skill
			params.scale = session.scale
			params.portfolio = session.portfolio
		}
		return params
	}
	
	def viewSharedEvidence = {

		def s 
		def d
		def cs

		session.scale= null
		if(params.scale) { 
			session.scale = params.scale
		}
		session.skill= null
		if(params.skill) { 
			session.skill= params.skill
		}
		if(params.portfolio) { 
			session.portfolio= params.portfolio
		}

		if(params.id) { 
			session.portfolio= params.id
		}
	
		d = Skill.get(session.skill)
		s = Scale.get(session.scale)

		def view = "viewStatements"
		if(d!=null && s!=null)
		{
			cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
		} else if (d!=null) {
			view = "viewStatementsSkill"
			cs = lf.CandoStatement.findAllBySkill(d)
		} else {
			view = "viewStatementsScale"
			cs = lf.CandoStatement.findAllByScale(s)
		}
		
		def portfolio = Portfolio.get(params.portfolio)
		println "po="+ params.portfolio
		def p = Sharing.findBySharedWithAndPortfolio(session.user, portfolio)
		if(p==null)
		{
			flash.message = "That is not shared with you"
			redirect(action:"myPortfolios")
			return false
		}

		def candoStatementId = params.id.toInteger()
		
		// @todo - do we have permission to view evidences for this cando?
		def cando = CandoStatement.get(candoStatementId)

		
		def myCando = lf.CandoEntry.findByStatementAndUserAndPortfolio(cando, portfolio.myportfolio.owner, portfolio)

		def myp = Myportfolio.findByOwner(portfolio.myportfolio.owner)
		[myportfolio: myp, scale:cando.scale, skill:cando.skill, shared: p,
				  portfolio:portfolio, candoStatement: cando, cando:myCando,
				  evidences: myCando?.evidences?.sort{it.dateCreated}?.reverse()]
	}

	def viewEvidences = {
		def p = resetParams(params)
		def s = Scale.get(p.skill?.toInteger())
		def d = Skill.get(p.skill?.toInteger())
		def topicId = params.topic as Long
		def portfolio = Portfolio.get(params.portfolio as Long)

		log.info(params)
		log.info(p)

		def candoStatementId = p.id?.toInteger()
		
		// @todo - do we have permission to view evidences for this cando?
		def cando = CandoStatement.get(candoStatementId)

		def myCando = lf.CandoEntry.findByStatementAndUserAndPortfolio(cando, session.user, portfolio)
		def ae = grailsApplication.config.fileuploader.docs.allowedExtensions
		def ms = grailsApplication.config.fileuploader.docs.maxSize.toInteger() / 1024 / 1024
		def maxbytes = grailsApplication.config.fileuploader.docs.maxSize.toInteger()
		[maxBytes:maxbytes, maxSize: Math.ceil(ms), allowedExtensions: ae, topicId: topicId, user: session.user, skill:d, portfolio:portfolio, candoStatement: cando, cando:myCando, evidences: myCando?.evidences?.sort{it.dateCreated
		}.reverse()]
	}
	
	def addEvidence = {
		def id = flash.fileId
		def candoId = flash.candoEntryId
		def candoStatementId = flash.candoStatementId
		def topicId = flash.topicId
		def topic = Topic.get(topicId)
		//render(flash)
		def evidence = new lf.Evidence()

		def hasVirus = false
		hasVirus = evidenceService.checkVirus(id.toInteger())
		if(hasVirus) { 
			flash.message = "The uploaded file tested positive for a virus and was not processed or accepted."
			redirect(action:"viewEvidences", id: candoStatementId)
			return false
		}

		evidence.file = UFile.get(id)
		evidence.candoEntry = lf.CandoEntry.get(candoId)
		evidence.status='new'
		if(!evidence.validate())
		{
			log.info evidence.errors
			println evidence.errors
println "NOT sending to rabbit"
		} else {
println "sending to thumbnail bgs"
			evidence.save(flush:true)
			thumbnailingService.handleMessage(evidence.id.toInteger())
		}

		redirect(action:"viewEvidences", id: candoStatementId, params:['portfolio':evidence.candoEntry.portfolio.id])
	}
	
	def thumbnail = { e->
		evidenceService.thumbnail(e.id)
	}
	
	def profilePhotoThumbnail = { e->
		userService.thumbnail(e.id)
	}
	
	def deleteEvidence = {
		def e = Evidence.get(params.id)
		if(e==null) {
			flash.message = "Evidence was blank?"
			return false
		}
		if(session.user.id!=e.candoEntry.user.id) {
			flash.message = "You can't remove this evidence!"
			redirect(action:"viewEvidences", id: e.candoEntry.id)
			return false
		}
		def t = e?.candoEntry?.topic?.id
		def p = e?.candoEntry?.portfolio?.id
		def s = e.candoEntry.statement?.id
		//c.removeEvidences(e)
		//c.save()	
		evidenceService.delete(e)
		flash.message = "Evidence removed!"
		redirect(action:"viewEvidences", id: s, params: [portfolio: p, topic: t])
		return	
	}
	
	
	def updateCandoStatus = {

		def topic = Topic.get(params.topicId as Long)
		params.candoStatus.each { k,v->
			println k
			def myCandoEntry = CandoEntry.findByIdAndUser(k, session.user)
			myCandoEntry.topic = topic
			myCandoEntry.candoStatus = v
			if(!myCandoEntry.validate()) {
				flash.errors = "We had trouble saving your information - please try again"
				println "err=" + myCandoEntry.errors
						redirect(action:"viewStatements")
						return false
			}
			myCandoEntry.save(flush:true)
		//	flash.errors += myCandoEntry.id.toString() + "/"
		}

		flash.message = "Can Do Status Updated!"
		//redirect(mapping:"candoview", action:"viewStatements", portfolio:session.portfolio, skill:session.skill, scale:session.scale)

		if(session.scale!=null && session.skill==null)
		{
			redirect(action:"viewStatements", params:[portfolio:session.portfolio, scale:session.scale])
		}

		if(session.scale==null && session.skill!=null)
		{
			redirect(action:"viewStatements", params:[portfolio:session.portfolio, skill:session.skill])
		}

		if(session.scale!=null && session.skill!=null)
		{
			redirect(action:"viewStatements", params:[portfolio:session.portfolio, skill:session.skill, scale:session.scale])
		}
	}
	
	
	def updatePassword = {
			def p = params.password
			def p2 = params.password2
			if(p!=p2)
			{
				flash.errors = "Your password fields do not match.  Please enter your passwords again"
				redirect(action:"my")
				return false
			}
			def u = User.get(session.user.id)
println session.user.id
			u.password = springSecurityService.encodePassword(p)
			u.save()
			flash.message = "Password updated"
			redirect(action:"my")
	}
	
	def updateEmail = {
			def p = params.email
			def p2 = params.email2
			if(p!=p2)
			{
				flash.errors = "Your emails  do not match.  Please enter your email address again"
				redirect(action:"my")
				return false
			}
			def u = User.get(session.user.id)
			u.email = p
			u.validate()
			
			if(u.hasErrors() && u.errors)
			{
				flash.errors = u.errors
				redirect(action:"my")
				return false
			}
			u.save()
			flash.message = "Email updated"
			redirect(action:"my")
	}

	def updateProfilepic =
	{
	 	User u = User.get(session.user.id)
		imageUploadService.save(u)
		u.save()
		session.user = u
		redirect(action:"my")
	}

	def updatePhoto = {
		def id = flash.fileId
		session.user.refresh()
		session.user.file = UFile.get(id)
		session.user.file.pathToThumbnail =  Setting.valueFor("defaultThumbnail")
		session.user.file.save(flush:true)
		session.user.save()
		def x = session.user.file


		//rabbitSend 'thumbnail', id
		redirect(action:"my")
				
	}
	

	def initiateFriend = 
	{
		def friend = User.findByMyCode(params.friendCode)
		if(friend==null)
		{
			session.flashErrors = "That friend code is invalid"
			redirect(action:"viewFriends")
			return
		}

		def rel = userService.getRelationship(session.user, friend)

		if(session.user.id==friend.id)
		{
			session.flashErrors = "Silly goose!  You can't be friends with yourself!"
			redirect(action:"viewFriends")
			return
		}

		if(rel?.status==StatusCode.INITIATED)
		{
			session.flashMessage = "You've already asked for a relationship with this person.  \
				We're just waiting for a confirmation."
			redirect(action:"viewFriends")
			return
		}
		if(rel?.status==StatusCode.BLOCKED)
		{
			session.flashErrors = "You are blocked from friending this user."
			redirect(action:"viewFriends")
			return
		}
		if(rel?.status==StatusCode.DENIED)
		{
			session.flashErrors = "Your earlier request was denied."
			redirect(action:"viewFriends")
			return
		}
		if(rel?.status==StatusCode.ACCEPTED)
		{
			session.flashErrors = "You are already friends with this person."
			redirect(action:"viewFriends")
			return
		}

		if(rel==null)
		{

			userService.initiateRelationship(session.user, friend)
			session.flashMessage = "We've invited the other person to become your friend.\
			When they accept, their information will be listed on this page."
			redirect(action:"viewFriends")
			return
		}


	}

	def confirmFriend = 
	{
		def friend = User.get(params.id)
		if(friend==null)
		{
			flash.errors = "That user does not exist"
			redirect(action:"viewFriends")
			return
		}
		def rel = Relationship.findByParty2AndParty1(session.user, friend)
		if(rel==null)
		{
			flash.errors = "That relationship could not be found"
			redirect(action:"viewFriends")
			return
		}

		if(rel.status==StatusCode.ACCEPTED)
		{
			flash.message = "You've already accepted this person as your friend"
		}
		if(rel.status==StatusCode.DENIED)
		{
			flash.message = "This relationship has been denied"
		}
		if(rel.status==StatusCode.BLOCKED)
		{
			flash.message = "This relationship has been blocked"
		}
		if(rel.status==StatusCode.BLOCKED)
		{
			flash.message = "This relationship has been blocked"
		}

		if(rel.status==StatusCode.INITIATED)
		{
			rel.status = StatusCode.ACCEPTED;
			rel.save()
			flash.message = "You've accepted this friend request."
		}

		redirect(action:"viewFriends")
		return


	}

	def blockFriend = 
	{
		def friend = User.get(params.id)
		if(friend==null)
		{
			flash.errors = "That user does not exist"
			redirect(action:"viewFriends")
			return
		}
		def rel = Relationship.findByParty2AndParty1(session.user, friend)
		if(rel==null)
		{
			rel = Relationship.findByParty1AndParty2(session.user, friend)
		}
		if(rel==null)
		{
			flash.errors = "That relationship could not be found"
			redirect(action:"viewFriends")
			return
		}

		if(rel.status==StatusCode.BLOCKED)
		{
			flash.message = "This person is already blocked"
		}

		if(rel.status==StatusCode.INITIATED || rel.status==StatusCode.ACCEPTED)
		{
			rel.party1 = session.user
			rel.party2 = friend
			rel.status = StatusCode.BLOCKED;
			rel.save()
			flash.message = "You've blocked this person"
		}

		redirect(action:"viewFriends")
		return


	}


	def denyFriend = 
	{
		def friend = User.get(params.id)
		if(friend==null)
		{
			flash.errors = "That user does not exist"
			redirect(action:"viewFriends")
			return
		}
		def rel = Relationship.findByParty2AndParty1(session.user, friend)
		if(rel==null)
		{
			flash.errors = "That relationship could not be found"
			redirect(action:"viewFriends")
			return
		}

		if(rel.status==StatusCode.BLOCKED)
		{
			flash.message = "This person is already blocked"
		}

		if(rel.status==StatusCode.INITIATED || rel.status==StatusCode.ACCEPTED)
		{
			rel.status = StatusCode.DENIED;
			rel.save()
			flash.message = "You've denied this relationship"
		}

		redirect(action:"viewFriends")
		return


	}

	def cancelFriend = 
	{
		def friend = User.get(params.id)
		if(friend==null)
		{
			flash.errors = "That user does not exist"
			redirect(action:"viewFriends")
			return
		}
		def rel = Relationship.findByParty2AndParty1(session.user, friend)
		if(rel==null)
		{
			rel = Relationship.findByParty1AndParty2(session.user, friend)
		}

		if(rel==null)
		{
			flash.errors = "That relationship could not be found"
			redirect(action:"viewFriends")
			return
		}

		if(rel.status==StatusCode.BLOCKED)
		{
			flash.message = "This person is already blocked"
		}
		if(rel.status==StatusCode.DENIED)
		{
			flash.message = "This person is already denied"
		}


		if(rel.status==StatusCode.INITIATED || rel.status==StatusCode.ACCEPTED)
		{
			rel.delete()
			flash.message = "You've cancelled this relationship"
		}

		redirect(action:"viewFriends")
		return


	}

	def unblockFriend = 
	{
		def friend = User.get(params.id)
		if(friend==null)
		{
			flash.errors = "That user does not exist"
			redirect(action:"viewFriends")
			return
		}
		def rel = Relationship.findByParty2AndParty1(session.user, friend)
		if(rel==null)
		{
			rel = Relationship.findByParty1AndParty2(session.user, friend)
		}
		if(rel==null)
		{
			flash.errors = "That relationship could not be found"
			redirect(action:"viewFriends")
			return
		}

		if(rel.status==StatusCode.BLOCKED)
		{
			rel.delete()
			flash.message = "The block has been removed - refriend to see this person again"
		}

		redirect(action:"viewFriends")
		return


	}

	def viewSharedFriend = {
		def friend = User.get(params.id)
		def rel = userService.getRelationship(session.user, friend)
		if(rel?.status!=StatusCode.ACCEPTED)
		{
			flash.message = "That is not shared with you"
			redirect(action:"myPortfolios")
			return false
		}
		render(friend)
		session.viewingFriend = friend
		redirect(action:"sharedFriend", id: params.id)
	}

	def viewShared = {
		// need to find who this is shared *by*, and use them as the 'viewingFriend' user
		def p = Sharing.findBySharedWithAndPortfolio(session.user, Portfolio.get(params.id))
		if(p==null)
		{
			flash.message = "That is not shared with you"
			redirect(action:"myPortfolios")
			return false
		}
		def theUser = p.owner
		def myp = Myportfolio.findByOwner(p.owner)
		[shared:p, myportfolio: myp, user: theUser]
	}

	def shares = {
		def myportfolio = Myportfolio.findByIdAndOwner(params.id, session.user)
		if(myportfolio?.owner!=session.user)
		{
			flash.message = "You do not own this portfolio"
			render(flash.message)
			return
			redirect(action:"myPortfolios")
			return false
		}

		def sharedWithMe = getSharedWithMe(session.user)

//		def myFriends = userService.getAllFriendsByStatus(session.user, StatusCode.ACCEPTED)
//		def sharedWithMe
//		if(myFriends && myFriends.size()>0)
//		{
//			sharedWithMe = Sharing.withCriteria {
//				eq('sharedWith', session.user)
//				inList('owner', myFriends)
//				order('owner','asc')
//			}
//		}

		[sharedWithMe: sharedWithMe]
	}

	def getSharedWithMe(meUser)
	{
		def myFriends = userService.getAllFriendsByStatus(meUser, StatusCode.ACCEPTED)
		def sharedWithMe
		if(myFriends && myFriends.size()>0)
		{
			sharedWithMe = Sharing.withCriteria {
				eq('sharedWith', meUser)
// not sure if we need to check this
// it's not working for some people, and if it's sharedWith already, 
// why check the owner list too?
// issue is the 'myfriends' is coming up as the meUser too... 
//				inList('owner', myFriends)
				order('owner','asc')
				portfolio {
					eq('status', StatusCode.PORTFOLIO_INUSE)
				}
			}
		}

		sharedWithMe
	}


	def stopSharing = {
		def p = Portfolio.get(params.pid)
		def friend = User.get(params.id)
		if(p.myportfolio.owner!=session.user)
		{
			flash.message = "You do not own this portfolio"
			render(flash.message)
			return
			redirect(action:"viewPortfolios")
			return false
		}

		def myFriends = userService.getAllMyFriends(session.user)

		/**
		* hold off on this for a bit - if the other person cancels
		* the friendship, it *should* cancel the shares, but we
		* don't do that right now
		*
		if(!myFriends.contains(friend))
		{
			flash.message = "You are not friends with this person"
			redirect(action:"viewPortfolios")
			return false
		}
		*/

		def s = Sharing.findByPortfolioAndSharedWith(p,friend)
		if(s==null)
		{
			flash.message = "You are not sharing with that person"
			render(flash.message)
			return
			redirect(action:"viewPortfolios")
			return false
		}

		s.delete()
		flash.message = "Stopped sharing " + p.topic.toString() + " portfolio with " + friend
		redirect(action:"myPortfolios", controller: "account", 	id:p.myportfolio.folio.id)
	}


	def shareWith = {
		def p = Portfolio.get(params.portfolio)
		def friend = User.get(params.friend)
println params
println "mypid=" + p.myportfolio.id
		if(p.myportfolio.owner!=session.user)
		{
			flash.message = "You do not own this portfolio"
			render(flash.message)
			return
			redirect(action:"viewPortfolios", id:p.myportfolio.id)
			return false
		}

		def myFriends = userService.getAllMyFriends(session.user) + userService.getAllMyTeachers(session.user)
println "friend=" + friend
println myFriends
		if(!myFriends.contains(friend))
		{
			log.info "You are not friends with this person"
			println "You are not friends with this person"
			flash.message = "You are not friends with this person"
			redirect(action:"viewPortfolios", id:p.myportfolio.id)
			return false
		}

		def s = Sharing.findByPortfolioAndSharedWith(p,friend)
		if(s!=null)
		{
			log.info  "You are already sharing with that person"
			println "You are already sharing with that person"
			flash.message = "You are already sharing with that person"
			redirect(action:"viewPortfolios", id:p.myportfolio.id)
			return false
		}

		def newShare = new Sharing(portfolio:p, owner:session.user, sharedWith:friend)
		newShare.save()
		flash.message = p.topic.toString() + " portfolio shared with " + friend
		redirect(action:"viewPortfolios", id:p.myportfolio.id)
	}

	def getMyLearningInventory(myportfolio, user)  
	{
		println myportfolio?.folio
		def i = Inventory.findByFolio(myportfolio.folio)
		def myi = MyInventory.findByInventoryAndOwner(i, user)
		if(!myi || myi==null)
		{
			myi = new MyInventory(inventory:i, owner:user, portfolio: portfolio)
				myi.validate()
				println myi.errors
				myi.save()
				println "making a new one"
		} 
println myi
		myi	
	}

	def updatePassportLanguageLearnedFromFamily= 
	{
		def p = PassportLanguage.findByIdAndOwner(params.p, session.user)
		if(p!=null) {
			p.family = params.family
			p.save()	
			redirect(action:"passport", id:p.id)
		} else {
			redirect(action:"index")
		}	
	}

	def addPassportLanguageLearnedInSchool = 
	{
		def p = PassportLanguage.findByIdAndOwner(params.p, session.user)
		if(p!=null) {
			def temp = new PassportLanguageLearnedInSchool(params)
			temp.passport = p
			temp.save()
			def myp = Myportfolio.findByFolioAndOwner(p.folio, session.user)
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"in"])
		} else {
			redirect(action:"index")
		}	
	}

	def addPassportLanguageLearnedOutsideSchool = 
	{
		def p = PassportLanguage.findByIdAndOwner(params.p, session.user)
		if(p!=null) {
			def temp = new PassportLanguageLearnedOutsideSchool(params)
			temp.passport = p
			temp.save()
			def myp = Myportfolio.findByFolioAndOwner(p.folio, session.user)
			redirect (url: createLink(mapping:'passportTab', params:[id:myp.id, tab:"out"]))
		} else {
			redirect(action:"index")
		}	
	}

	def addPassportLanguageExperience = 
	{
		def p = PassportLanguage.findByIdAndOwner(params.p, session.user)
		if(p!=null) {
			def temp = new PassportLanguageExperience(params)
			temp.passport = p
			temp.save()
			def myp = Myportfolio.findByFolioAndOwner(p.folio, session.user)
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"exp"])
		} else {
			redirect(action:"index")
		}	
	}

	def addPassportLanguageTest = 
	{
		def p = PassportLanguage.findByIdAndOwner(params.p, session.user)
		if(p!=null) {
			def temp = new PassportLanguageTest(params)
			temp.passport = p
			temp.save()
			def myp = Myportfolio.findByFolioAndOwner(p.folio, session.user)
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"test"])
		} else {
			redirect(action:"index")
		}	
	}

	def delPassportLanguageLearnedInSchool= 
	{
		def p = PassportLanguageLearnedInSchool.get(params.id)
		if(p!=null && p.passport.owner==session.user) {
			def myp = Myportfolio.findByFolioAndOwner(p.passport.folio, session.user)
			p.delete()
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"in"])
		} else {
			redirect(action:"index")
		}	
	}
	def delPassportLanguageLearnedOutsideSchool= 
	{
		def p = PassportLanguageLearnedOutsideSchool.get(params.id)
		if(p!=null && p.passport.owner==session.user) {
			def myp = Myportfolio.findByFolioAndOwner(p.passport.folio, session.user)
			p.delete()
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"out"])
		} else {
			redirect(action:"index")
		}	
	}
	def delPassportLanguageExperience = 
	{
		def p = PassportLanguageExperience.get(params.id)
		if(p!=null && p.passport.owner==session.user) {
			def myp = Myportfolio.findByFolioAndOwner(p.passport.folio, session.user)
			p.delete()
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"exp"])
		} else {
			redirect(action:"index")
		}	
	}
	def delPassportLanguageTest = 
	{
		def p = PassportLanguageTest.get(params.id)
		if(p!=null && p.passport.owner==session.user) {
			def myp = Myportfolio.findByFolioAndOwner(p.passport.folio, session.user)
			p.delete()
			redirect(action:"passport", mapping:'passportTab', params:[id:myp.id, tab:"test"])
		} else {
			redirect(action:"index")
		}	
	}

	def activity = 
	{
		def theUser = session.viewingFriend ?: session.user

		def myportfolio = Myportfolio.findByIdAndOwner(params.id, theUser)
		if(myportfolio==null)
		{
			redirect(action:"myPortfolios")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = ActivityList.findByFolioAndOwner(f, theUser)
		if(p==null)
		{
			if(f.title=='LinguaFolio') { 
				p = new LanguageActivityList(folio:f, owner:theUser)
				p.validate()
				println p.errors
				p.save()
			}
		}
		session.myportfolioId = myportfolio.id
		[tab:params.tab, activityList:p, myportfolio:myportfolio,
				viewingFriend: session.viewingFriend]
	}


	def learningActivityDel =
	{
		def la = Activity.get(params.id)
		if(la.activityList.owner==session.user)
		{
			la.delete()
		}
		flash.message = "Activity removed!"
		redirect(action:"activity", id:session.myportfolioId)
		return false
	}

	def learningActivityAdd =
	{
		def myportfolio = Myportfolio.findByIdAndOwner(params.p, session.user)
		if(myportfolio==null)
		{
			redirect(action:"myPortfolios")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = ActivityList.findByFolioAndOwner(f,session.user)
		if(p==null)
		{
			p = new ActivityList()
			p.owner = session.user
			p.folio = f
			p.validate()
			log.info "actlist errors = "+p.errors
			p.save()
		}
		def l = new LanguageActivity()
		l.activityList = p
		l.description = params.description
		l.activityDate = params.date
		l.activity = params.activity
log.info "adding learning activity params = "+params
		l.topic = Topic.get(params.topic.toInteger())	
l.validate()
log.info "l errors = " + l.errors
		l.save()
		p.addToActivities(l)
		flash.message = "Activity added!"
		redirect(action:"activity", id:myportfolio.id)
		return false
	}

	def experience = 
	{
		def theUser = session.viewingFriend ?: session.user

		def myportfolio = Myportfolio.findByIdAndOwner(params.id, theUser)
		if(myportfolio==null)
		{
			redirect(action:"myPortfolios")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = ExperienceList.findByFolioAndOwner(f, theUser)

		if(p==null)
		{
			if(f.title=='LinguaFolio') { 
				p = new LanguageExperienceList(folio:f, owner: theUser)
				p.validate()
				println p.errors
				p.save()
			}
		}

		session.myportfolioId = myportfolio.id
		[viewingFriend: session.viewingFriend, tab:params.tab, experienceList:p, myportfolio:myportfolio]
	}


	def learningExperienceDel =
	{
		def la = Experience.get(params.id)
		if(la.experienceList.owner==session.user)
		{
			la.delete()
		}
		flash.message = "Experience removed!"
		redirect(action:"experience", id:session.myportfolioId)
		return false
	}

	def learningExperienceAdd =
	{
		def myportfolio = Myportfolio.findByIdAndOwner(params.p, session.user)
		if(myportfolio==null)
		{
			redirect(action:"myPortfolios")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = ExperienceList.findByFolioAndOwner(f,session.user)
		def l = new LanguageExperience()
		l.description = params.description
		l.experienceDate = params.date
		l.experience = params.experience
		l.topic = Topic.get(params.topic.toInteger())	
l.validate()
		l.save()
		p.addToExperiences(l)
		p.save(flush:true)
		flash.message = "Experience added!"
		redirect(action:"experience", id:myportfolio.id)
		return false
	}

	def encounter = 
	{
		def theUser = session.viewingFriend ?: session.user


		def myportfolio = Myportfolio.findByIdAndOwner(params.id, theUser)
		if(myportfolio==null)
		{
			redirect(action:"myPortfolios")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = EncounterList.findByFolioAndOwner(f, theUser)
		if(p==null)
		{
			if(f.title=='LinguaFolio') { 
				p = new LanguageEncounterList(folio:f, owner: theUser)
				p.validate()
				println p.errors
				p.save()
			}
		}
		session.myportfolioId = myportfolio.id
		[tab:params.tab, encounterList:p, myportfolio:myportfolio, viewingFriend: session.viewingFriend]
	}


	def learningEncounterDel =
	{
		def la = Encounter.get(params.id)
		if(la.encounterList.owner==session.user)
		{
			la.delete()
		}
		flash.message = "Encounter removed!"
		redirect(action:"encounter", id:session.myportfolioId)
		return false
	}

	def learningEncounterAdd =
	{
		def myportfolio = Myportfolio.findByIdAndOwner(params.p, session.user)
		if(myportfolio==null)
		{
			redirect(action:"myPortfolios")
			return false
		}
		def f = Folio.get(myportfolio.folio.id)
		def p = EncounterList.findByFolioAndOwner(f,session.user)
		def l = new LanguageEncounter()
		l.description = params.description
		l.encounterDate = params.date
		l.topic = Topic.get(params.topic.toInteger())	
l.validate()
		l.save()
		p.addToEncounters(l)
		p.save(flush:true)
		flash.message = "Encounter added!"
		redirect(action:"encounter", id:myportfolio.id)
		return false
	}

	def m = {}


	def viewAllEvidences =
	{
		def e = Evidence.createCriteria()
		def t = e.list() { 
			file {
				eq('user', session.user)
			}
		}	
		[evidences:t, user:session.user]
	}


	def friends =
	{
		session.user.refresh()
		def blocked = userService.getAllBlockedUsers(session.user)
		def friends = userService.getAllFriendsByStatus(session.user, StatusCode.ACCEPTED)
		def teachers = userService.getAllMyTeachersByStatus(session.user, StatusCode.ACCEPTED)
		friends = friends + teachers
		def accepted = userService.getAllMyFriendsByStatus(session.user, StatusCode.ACCEPTED)
		def acceptedTeacher = userService.getAllMyTeachersByStatus(session.user, StatusCode.ACCEPTED)
		def waitingToHear = userService.getAllMyFriendsByStatus(session.user,  StatusCode.INITIATED)
		def waitingToHearTeacher = userService.getAllMyTeachersByStatus(session.user,  StatusCode.INITIATED)
		def requests = userService.getAllMyFriendRequests(session.user)
		//def requestTeachers = userService.getAllMyTeacherRequests(session.user)
		[requests: requests,
				//	requestTeachers:requestTeachers,
				acceptedTeachers:acceptedTeacher,
				acceptedFriends:accepted, waitingToHearBackTeachers:waitingToHearTeacher,
				waitingToHearBackFriends: waitingToHear,
				blocked:blocked, friends:friends, teachers:teachers]


	}

}
