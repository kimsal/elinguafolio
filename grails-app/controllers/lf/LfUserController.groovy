package lf
import lf.*

class LfUserController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def userService
	def cryptoService
	def emailConfirmationService

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [lfUserInstanceList: LfUser.list(params), lfUserInstanceTotal: LfUser.count()]
    }

	def logout = {
		session.user = null
		session.userid = null
		redirect(uri:"/")
	}

	def login = {

	}

	def loginProcess = {
		if(userService.goodLogin(params.username, params.password))
		{
			def u = userService.loadUser(params.username, params.password)
			session.userid = u.id
			session.user = u
			redirect(controller:"account")
		} else {
			flash.message = "Trouble logging in"
			redirect(action:"login")
		}
	}
	
	
	def register= { }
	def registerStudent = { }
	def registerTeacher = { }

	def registerStudentProcess = { 
		def r = new Random()
		def lfUserInstance = new Student(params)
		lfUserInstance.isConfirmed = false
		lfUserInstance.isActive = false
		lfUserInstance.myCode = userService.encrypt(r.nextInt(9999999).toString()+lfUserInstance.username)
		
		if (lfUserInstance.save(flush: true)) {
		    flash.message = "${message(code: 'default.created.message', args: [message(code: 'lfUser.label', default: 'LfUser'), lfUserInstance.id])}"
				emailConfirmationService.sendConfirmation(
					lfUserInstance.email, "Please confirm your eLinguaFolio account", 
					[from:"eLinguaFolio <webmaster@elinguafolio.org>"], lfUserInstance.id.toString()
					)
		    redirect(action: "confirm", controller:"main")
		}
		else {
		    render(view: "registerStudent", model: [studentInstance: lfUserInstance])
		}
	}

	def registerTeacherProcess = { 
		def r = new Random()
		def lfUserInstance = new Teacher(params)
		lfUserInstance.isConfirmed = false
		lfUserInstance.isActive = false
		lfUserInstance.myCode = userService.encrypt(r.nextInt(9999999).toString()+lfUserInstance.username)
		
		if (lfUserInstance.save(flush: true)) {
		    flash.message = "${message(code: 'default.created.message', args: [message(code: 'lfUser.label', default: 'LfUser'), lfUserInstance.id])}"
//			userService.initialTeacher(lfUserInstance)
				emailConfirmationService.sendConfirmation(
					lfUserInstance.email, "Please confirm your eLinguaFolio account", 
					[from:"eLinguaFolio <webmaster@elinguafolio.org>"], lfUserInstance.id.toString()
					)
		    redirect(action: "confirm", controller:"main")
		}
		else {
		    render(view: "registerTeacher", model: [teacherInstance: lfUserInstance])
		}
	}



    def create = {
        def lfUserInstance = new LfUser()
        lfUserInstance.properties = params
        return [lfUserInstance: lfUserInstance]
    }

    def save = {
        def lfUserInstance = new LfUser(params)
        if (lfUserInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'lfUser.label', default: 'LfUser'), lfUserInstance.id])}"
            redirect(action: "show", id: lfUserInstance.id)
        }
        else {
            render(view: "create", model: [lfUserInstance: lfUserInstance])
        }
    }

    def show = {
        def lfUserInstance = LfUser.get(params.id)
        if (!lfUserInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'lfUser.label', default: 'LfUser'), params.id])}"
            redirect(action: "list")
        }
        else {
            [lfUserInstance: lfUserInstance]
        }
    }

    def edit = {
        def lfUserInstance = LfUser.get(params.id)
        if (!lfUserInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'lfUser.label', default: 'LfUser'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [lfUserInstance: lfUserInstance]
        }
    }

    def update = {
        def lfUserInstance = LfUser.get(params.id)
        if (lfUserInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (lfUserInstance.version > version) {
                    
                    lfUserInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'lfUser.label', default: 'LfUser')] as Object[], "Another user has updated this LfUser while you were editing")
                    render(view: "edit", model: [lfUserInstance: lfUserInstance])
                    return
                }
            }
            lfUserInstance.properties = params
            if (!lfUserInstance.hasErrors() && lfUserInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'lfUser.label', default: 'LfUser'), lfUserInstance.id])}"
                redirect(action: "show", id: lfUserInstance.id)
            }
            else {
                render(view: "edit", model: [lfUserInstance: lfUserInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'lfUser.label', default: 'LfUser'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def lfUserInstance = LfUser.get(params.id)
        if (lfUserInstance) {
            try {
                lfUserInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'lfUser.label', default: 'LfUser'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'lfUser.label', default: 'LfUser'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'lfUser.label', default: 'LfUser'), params.id])}"
            redirect(action: "list")
        }
    }


	def checkEmail = {
		// Only do this if not confirmed already!
		emailConfirmationService.sendConfirmation(params.emailAddress,
		"Please confirm", [from:"server@yourskill.com"])
	}
	
}
