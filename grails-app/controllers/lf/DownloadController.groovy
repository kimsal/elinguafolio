package lf

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry
import java.nio.channels.FileChannel
import org.apache.tika.parser.AutoDetectParser
import org.apache.tika.metadata.Metadata
import org.apache.tika.sax.BodyContentHandler
import org.apache.tika.Tika
import org.apache.tools.ant.helper.DefaultExecutor
import java.util.concurrent.Executor

/**
 * originally from a plugin at http://github.com/lucastex/grails-file-uploader
 * but incorporated in to the project directly to accomodate user tie-in directly
 */

class DownloadController {

	def messageSource
	def userService


	def isFileSharedWithUser(file, user)
	{
		def evidence = Evidence.findByFile(file)
		if (!evidence)
		{
			return false
		}
		def portfolio = evidence.candoEntry.portfolio
		if (isPortfolioSharedWithMe(user, portfolio))
		{
			return true
		} else {
			return false
		}
	}

	def index = {

		UFile ufile = UFile.findById(params.id)
		if (!ufile || (ufile.user.id!=session.user.id && !isFileSharedWithUser(ufile, session.user))) {
			def msg = messageSource.getMessage("fileupload.download.nofile", [params.id] as Object[], request.locale)
			log.debug msg
			flash.message = msg
			redirect(url: "/images/default.png");
			return false
//			redirect controller: params.errorController, action: params.errorAction
			return
		}

		/**
		 * need to update the path to new system
		 */
		if (ufile.path.startsWith("/tmp/")) {
			ufile.path = ufile.path.replace("/tmp/", "/srv/")
			ufile.save()
		}

		def file = new File(ufile.path)
		if (file.exists()) {
			log.debug "Serving file id=[${ufile.id}] for the ${ufile.downloads} to ${request.remoteAddr}"
			ufile.downloads++
			ufile.save()
			def tika = new org.apache.tika.Tika()
			def detectedMime = tika.detect(file)
			response.setContentType(detectedMime ?: "application/octet-stream")
			response.setHeader("Content-disposition", "${params.contentDisposition}; filename=\"${ufile.name}\"")
			response.outputStream << file.newInputStream()
			return
		} else {
			def msg = messageSource.getMessage("fileupload.download.filenotfound", [ufile.name] as Object[], request.locale)
			log.error msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return
		}
	}

	def thumbnail = {

		UFile ufile = UFile.(params.id)
		if (!ufile) {
			def msg = messageSource.getMessage("fileupload.download.nofile", [params.id] as Object[], request.locale)
			log.debug msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return
		}

		log.info "62=" + ufile.pathToThumbnail
		println "62="+ufile.pathToThumbnail
		def file
		if(ufile.pathToThumbnail!=null){
		file = new File(ufile.pathToThumbnail)
		}
		if (file?.exists()) {
			def tika = new org.apache.tika.Tika()
			def detectedMime = tika.detect(file)
			response.setContentType(detectedMime ?: "application/octet-stream")
			response.setHeader("Content-disposition", "${params.contentDisposition}; filename=\"${ufile.name}\"")
			response.outputStream << file.newInputStream()
			return
		} else {
			def msg = messageSource.getMessage("fileupload.download.filenotfound", [ufile.name] as Object[], request.locale)
			log.error msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return
		}
	}

	def vid = {

		def ev = Evidence.get(params.id)
		def ufile = ev.file
		if (!ufile || ufile.user.id!=session.user.id) {
			def msg = messageSource.getMessage("fileupload.download.nofile", [params.id] as Object[], request.locale)
			log.debug msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return
		}

		def file = new File(ufile.pathToProcessedFile)
		if (file.exists()) {
			def tika = new org.apache.tika.Tika()
			def detectedMime = tika.detect(file)
			def end = '.mp4'
			/*
			if(params.type=="temp.ogv")
			{
				end = '.ogv'
				detectedMime = "video/ogg"
			}
			*/
			response.setContentType(detectedMime ?: "application/octet-stream")
			response.setHeader("Content-disposition", "attachment; filename=\"${ufile.name + end}\"")
			response.outputStream << file.newInputStream()
			return
		} else {
			def msg = messageSource.getMessage("fileupload.download.filenotfound", [ufile.name] as Object[], request.locale)
			log.error msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return
		}
	}

	def vidthumb = {

		def ev = Evidence.get(params.id)
		def ufile = ev.file
		if (!ufile) {
			def msg = messageSource.getMessage("fileupload.download.nofile", [params.id] as Object[], request.locale)
			log.debug msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return false
		}

		log.info "here! " + ufile.pathToThumbnail
		def file = new File(ufile.pathToThumbnail)
		if (file.exists()) {
			def tika = new org.apache.tika.Tika()
			def detectedMime = tika.detect(file)
			if (params.type == "temp.ogv") {
				detectedMime = "video/ogg"
			}
			response.setContentType(detectedMime ?: "application/octet-stream")
			response.setHeader("Content-disposition", "${params.contentDisposition}; filename=\"${ufile.name}\"")
			response.outputStream << file.newInputStream()
			return false
		} else {
			def msg = messageSource.getMessage("fileupload.download.filenotfound", [ufile.name] as Object[], request.locale)
			log.error msg
			flash.message = msg
			redirect controller: params.errorController, action: params.errorAction
			return false
		}
	}

	def allold = {
		def e = Evidence.createCriteria()
		def t = e.list() {
			file {
				eq('user', session.user)
			}
		}
		def path = grailsApplication.config.fileuploader.docs.path.toString() + session.user.username + "/all_evidences"
		log.info path
		def temp = new File(path).mkdir()
		t.each { ev ->
			/**
			 * need to update the path to new system
			 */
			if (ev.file.path.startsWith("/tmp/")) {
				ev.file.path = ev.file.path.replace("/tmp/", "/srv/")
				ev.file.save()
			}
			def newfile = ev.file.name.replace("?", "_")
			newfile = newfile.replace("#", "")
			newfile = newfile.replace(" ", "_")
			path = path.replace("?", "_")
			def st = "/bin/cp " + ev.file.path + " " + path + "/" + newfile + ""
			log.info "st=" + st
			def proc = st.execute()
			proc.waitFor()
			log.info "return code: ${ proc.exitValue()}"
			log.info "stderr: ${proc.err.text}"
			log.info "stdout: ${proc.in.text}"
//render(st + "<BR>")
		}
		def stzip1 = "rm " + path + ".zip "
		def t1 = stzip1.execute()
		t1.waitFor()
def bash = "#!/bin/bash\ncd "+path+"\n/usr/bin/zip -r "+path+".zip ./"
def filename = session.user.id.toString() + System.currentTimeMillis()
log.info "filename="+filename
def newfile = new File("/tmp/"+filename)
newfile << bash
def runfile = "/tmp/"+filename

def  t5 = "chmod 777 "+runfile
def t6 = t5.execute()
t6.waitFor()
		def t2 = runfile.execute()
		t2.waitFor()

			log.info "return code: ${ t2.exitValue()}"
			log.info "stderr: ${t2.err.text}"
			log.info "stdout: ${t2.in.text}"
newfile.delete()

//		Executor exec = new DefaultExecutor();
//		exec.setWorkingDirectory("C:\\My\\Dir\\")
//		CommandLine cl = new CommandLine("mybatch.bat");
//		int exitvalue = exec.execute(cl);


//render(stzip + "<BR>")
/*
		ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(path + ".zip"))
		def buffer = new byte[50240000]
log.info "path="+path+"/"
		new File(path + "/").eachFile() { file ->
log.info "file="+file.getName()
}
		new File(path + "/").eachFile() { file ->
			zipFile.putNextEntry(new ZipEntry(file.getName()))
			file.withInputStream { i ->
				def l = i.read(buffer)
// check wether the file is empty  
				if (l > 0) {
					zipFile.write(buffer, 0, l)
				}
			}
			zipFile.closeEntry()
		}
		zipFile.close()
*/

		def finalpath = new File(path + ".zip")
log.info "path+zip="+path+".zip"

		response.setContentType("application/octet-stream")
		response.setHeader("Content-disposition", "application/zip; filename=all_evidences.zip")
		response.outputStream << finalpath.newInputStream()
		return


	}




	def isPortfolioSharedWithMe(meUser, givenPortfolio)
	{
		def myFriends = userService.getAllFriendsByStatus(meUser, StatusCode.ACCEPTED)
		def sharedWithMe
		if(myFriends && myFriends.size()>0)
		{
			sharedWithMe = Sharing.withCriteria {
				eq('sharedWith', meUser)
				inList('owner', myFriends)
				portfolio {
					eq('status', StatusCode.PORTFOLIO_INUSE)
					eq('id', givenPortfolio?.id)
				}

			}
		}

		sharedWithMe
	}


	def all = {
		def e = Evidence.createCriteria()
		def t = e.list() {
			file {
				eq('user', session.user)
			}
		}
		def path = grailsApplication.config.fileuploader.docs.path.toString() + session.user.username + "/all_evidences"
		log.info path

		def zipname = "elinguafolio_files.zip"
		def p2 = "/tmp/elinguafolio"+session.user.id+".zip"
		ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(p2));

		t.each { ev ->
			/**
			 * need to update the path to new system
			 */
			if (ev.file.path.startsWith("/tmp/")) {
				ev.file.path = ev.file.path.replace("/tmp/", "/srv/")
				ev.file.save()
			}
			def newfile = ev.file.name.replace("?", "_")
			newfile = newfile.replace("#", "")
			newfile = newfile.replace(" ", "_")
			path = path.replace("?", "_")
//			def st = "/bin/cp " + ev.file.path + " " + path + "/" + newfile + ""

			def tempfile = new File(ev?.file?.path)
			if (tempfile.isFile())
			{
				ZipEntry ze = new ZipEntry(newfile)
				ze.time = tempfile?.lastModified()
				zipOutput.putNextEntry(ze)
				zipOutput << new FileInputStream(ev.file.path)
			}
		}

		zipOutput.close()
		def finalfile = new File(p2)

		response.setHeader("Content-Type", "application/zip")
		response.setHeader("Content-disposition", "attachment;filename=${zipname}")
		response.outputStream << finalfile.newInputStream()
		return false

	}

}
