package lf

class ModeController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [modeInstanceList: Mode.list(params), modeInstanceTotal: Mode.count()]
    }

    def create = {
        def modeInstance = new Mode()
        modeInstance.properties = params
        return [modeInstance: modeInstance]
    }

    def save = {
        def modeInstance = new Mode(params)
        if (modeInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'mode.label', default: 'Mode'), modeInstance.id])}"
            redirect(action: "show", id: modeInstance.id)
        }
        else {
            render(view: "create", model: [modeInstance: modeInstance])
        }
    }

    def show = {
        def modeInstance = Mode.get(params.id)
        if (!modeInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mode.label', default: 'Mode'), params.id])}"
            redirect(action: "list")
        }
        else {
            [modeInstance: modeInstance]
        }
    }

    def edit = {
        def modeInstance = Mode.get(params.id)
        if (!modeInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mode.label', default: 'Mode'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [modeInstance: modeInstance]
        }
    }

    def update = {
        def modeInstance = Mode.get(params.id)
        if (modeInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (modeInstance.version > version) {
                    
                    modeInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'mode.label', default: 'Mode')] as Object[], "Another user has updated this Mode while you were editing")
                    render(view: "edit", model: [modeInstance: modeInstance])
                    return
                }
            }
            modeInstance.properties = params
            if (!modeInstance.hasErrors() && modeInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'mode.label', default: 'Mode'), modeInstance.id])}"
                redirect(action: "show", id: modeInstance.id)
            }
            else {
                render(view: "edit", model: [modeInstance: modeInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mode.label', default: 'Mode'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def modeInstance = Mode.get(params.id)
        if (modeInstance) {
            try {
                modeInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'mode.label', default: 'Mode'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'mode.label', default: 'Mode'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'mode.label', default: 'Mode'), params.id])}"
            redirect(action: "list")
        }
    }
}
