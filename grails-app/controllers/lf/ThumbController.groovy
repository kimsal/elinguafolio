package lf
import org.grails.plugins.settings.*
import grails.converters.JSON
import grails.util.*
import lf.*

class ThumbController {

	def userService
	def grailsApplcation


	def user =
	{
		def u = User.get(params.id)
		def thumb = userService.getThumbnail(u)
println "line 17 thumb="+thumb
		def file = new File(thumb)
println "line 19 file="+file
		if(!file.exists())
		{
			file = new File(servletContext.getRealPath("images/defaultUser.png"))
		}
println "line 24 file="+file
		if (file.exists()) {
			def tika = new org.apache.tika.Tika()
			def detectedMime = tika.detect(file) ?: "application/octet-stream"
			response.setContentType(detectedMime)
println "29 mime="+detectedMime
			response.setHeader("Content-disposition", "filename=\"profile.jpg\"")
println "pushed down filename"
//			response.setHeader("Content-disposition", "${detectedMime}; filename=\"${u?.file?.name}\"")
			response.outputStream << file.newInputStream()
println "pushed down outputstream"
			return
		} else {

		}

	}


}
