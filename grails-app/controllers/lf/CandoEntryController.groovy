package lf

class CandoEntryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [candoEntryInstanceList: CandoEntry.list(params), candoEntryInstanceTotal: CandoEntry.count()]
    }

    def create = {
        def candoEntryInstance = new CandoEntry()
        candoEntryInstance.properties = params
        return [candoEntryInstance: candoEntryInstance]
    }

    def save = {
        def candoEntryInstance = new CandoEntry(params)
        if (candoEntryInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), candoEntryInstance.id])}"
            redirect(action: "show", id: candoEntryInstance.id)
        }
        else {
            render(view: "create", model: [candoEntryInstance: candoEntryInstance])
        }
    }

    def show = {
        def candoEntryInstance = CandoEntry.get(params.id)
        if (!candoEntryInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), params.id])}"
            redirect(action: "list")
        }
        else {
            [candoEntryInstance: candoEntryInstance]
        }
    }

    def edit = {
        def candoEntryInstance = CandoEntry.get(params.id)
        if (!candoEntryInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [candoEntryInstance: candoEntryInstance]
        }
    }

    def update = {
        def candoEntryInstance = CandoEntry.get(params.id)
        if (candoEntryInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (candoEntryInstance.version > version) {
                    
                    candoEntryInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'candoEntry.label', default: 'CandoEntry')] as Object[], "Another user has updated this CandoEntry while you were editing")
                    render(view: "edit", model: [candoEntryInstance: candoEntryInstance])
                    return
                }
            }
            candoEntryInstance.properties = params
            if (!candoEntryInstance.hasErrors() && candoEntryInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), candoEntryInstance.id])}"
                redirect(action: "show", id: candoEntryInstance.id)
            }
            else {
                render(view: "edit", model: [candoEntryInstance: candoEntryInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def candoEntryInstance = CandoEntry.get(params.id)
        if (candoEntryInstance) {
            try {
                candoEntryInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'candoEntry.label', default: 'CandoEntry'), params.id])}"
            redirect(action: "list")
        }
    }
}
