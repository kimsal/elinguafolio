package lf

import lf.*
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.*
import org.apache.poi.*



class MainController {


	def springSecurityService
	def importingService

	def index =
	{
		def s = springSecurityService

		if (s?.isLoggedIn()) {
			redirect(controller: "account")
			return false
		}

		def now = new Date()
		def n1 = News.createCriteria()
		def sticky = n1.list {
			lte('startDate', now)
			gte('endDate', now)
			eq('sticky', true)
		}
		def n2 = News.createCriteria()
		def nonsticky = n2.list {
			lte('startDate', now)
			gte('endDate', now)
			eq('sticky', false)
		}

		[sticky: sticky, nonsticky: nonsticky]
	}


	def foo =
		{
			def bar = "mike"
					{
					}
		}

	def confirm = {}


	def m1 = {
	}



	def m2()
	{
		def folio = Folio.findByTitle("linguafolio")
		def filename = "/Users/michael/grails/elinguafolio/data/wordllanguage_checklist.xlsx"
		importingService.checklistImportIntoFolio(filename, folio)
	}

	def m3()
	{
		def all = StatementForTopic.list()
		[all:all]
	}


	def t1()
	{
		def csCounts = CandoStatement.withCriteria {
		    projections{
			groupProperty "skill.id"
			groupProperty "scale.id"
			count "id" ,"c"
		    }
		}
	def skill = Skill.findByName("Listening")
	def scale = Scale.findByName("Low")
render(scale)
render(skill)
csCounts.each { it->
//render(it[1])
//render("<BR>")
}
def t = csCounts.find{it[0]==skill.id&& it[1]==scale.id}
render(t[2])

return
	}
}
