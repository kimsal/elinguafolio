package lf

class ErrorController {

	def mailService

    def index() { }

	def notfound()
	{
		def url = request.forwardURI
		println request.headerNames*.toString()
//		sendMail {
//			to "mgkimsal@gmail.com"
//			subject "elf error"
//			body "error trying to hit $url"
//		}

		def dogs = ["dog1.jpg","dog2.jpg","dog3.jpg","dog4.jpg"]
		Collections.shuffle(dogs)
		[dog:dogs[0]]
	}

	def broke()
	{
		def e = request.exception
// where do we get the session from?
//		def err = generateErrorFromException(e, session)
		def err = generateErrorFromException(e)

		def url = request.forwardURI
		try {
/*
			sendMail {
				to "mgkimsal@gmail.com"
				subject "elf error"
				body "error trying to hit $url\n----------\n"+err
			}
*/
		} catch (Exception ex)
		{

		}


		def dogs = ["dog1.jpg","dog2.jpg","dog3.jpg","dog4.jpg"]
		Collections.shuffle(dogs)
		[dog:dogs[0]]
	}

//	def generateErrorFromException(e, session)
	def generateErrorFromException(e)  
	{
		def err = e.cause.toString() + "\n"

		//session.refresh()
		//err += session?.toString()
//		session.each { s->
//			err+="s="+s+"---\n"
//		}


		err += "---------\n"
		err += e.codeSnippet*.join("\n")
		err += "---------\n"
		err += e.stackTraceLines.join("\n")


		return err
	}
}
