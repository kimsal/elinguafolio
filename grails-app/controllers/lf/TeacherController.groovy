package lf

class TeacherController {

	def filterService
	def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

	def search = {
		session.teacherSearch = params.search
		redirect(action:"list")
	}
	def clear = {
		session.teacherSearch = null
		redirect(action:"list")
	}

	def list = {
		def all
		def cnt
		params.max = Math.min(params.max ? params.int('max') : 20, 100)
		if (session.teacherSearch!=null)
		{
			def c = Teacher.createCriteria()
			def c2 = Teacher.createCriteria()
			all = c.list(params) {
				or {
					ilike('username', '%'+session.teacherSearch+"%")
					ilike('fullName', '%'+session.teacherSearch+"%")
					ilike('email', '%'+session.teacherSearch+"%")
				}
			}
			cnt = c2.count {
				or {
					ilike('username', '%'+session.teacherSearch+"%")
					ilike('fullName', '%'+session.teacherSearch+"%")
					ilike('email', '%'+session.teacherSearch+"%")
				}
			}

		} else {
			all = Teacher.list(params)
			cnt = Teacher.count()
		}

		[teacherInstanceList: all, teacherInstanceTotal: cnt, search:session.teacherSearch]
	}


	def list2 = {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [teacherInstanceList: Teacher.list(params), teacherInstanceTotal: Teacher.count()]
	}

	def filter = {
		if(!params.max) 
		{
			params.max = 20
		}
		render( view:'list', 
	    	model:[ teacherInstanceList: filterService.filter( params, Teacher), 
		teacherInstanceCount: filterService.count( params, Teacher), 
		teacherInstanceTotal: Teacher.count(),
		filterParams: com.zeddware.grails.plugins.filterpane.FilterUtils.extractFilterParams(params), 
		params:params ] )
	}

    def create = {
        def teacherInstance = new Teacher()
        teacherInstance.properties = params
        return [teacherInstance: teacherInstance]
    }

    def save = {
        def teacherInstance = new Teacher()
	teacherInstance.fullName = params.fullName
	teacherInstance.username = params.username
	teacherInstance.email = params.email
	teacherInstance.myCode = params.myCode
	teacherInstance.school = School.get(params.school.toLong()) ?: null
	if(params.password!='' && params.password!=null) {
		teacherInstance.password = springSecurityService.encodePassword(params.password)	
	}
        if (teacherInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'teacher.label', default: 'Teacher'), teacherInstance.id])}"
            redirect(action: "show", id: teacherInstance.id)
        }
        else {
            render(view: "create", model: [teacherInstance: teacherInstance])
        }
    }

    def show = {
        def teacherInstance = Teacher.get(params.id)
        if (!teacherInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'teacher.label', default: 'Teacher'), params.id])}"
            redirect(action: "list")
        }
        else {
            [teacherInstance: teacherInstance]
        }
    }

    def edit = {
        def teacherInstance = Teacher.get(params.id)
        if (!teacherInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'teacher.label', default: 'Teacher'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [teacherInstance: teacherInstance]
        }
    }

    def update = {
        def teacherInstance = Teacher.get(params.id)
        if (teacherInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (teacherInstance.version > version) {
                    
                    teacherInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'teacher.label', default: 'Teacher')] as Object[], "Another user has updated this Teacher while you were editing")
                    render(view: "edit", model: [teacherInstance: teacherInstance])
                    return
                }
            }
//            teacherInstance.properties = params
		teacherInstance.fullName = params.fullName
		teacherInstance.username = params.username
		teacherInstance.email = params.email
		teacherInstance.myCode = params.myCode
		teacherInstance.school = School.get(params.school.toLong()) ?: null

		if(params.password!='' && params.password!=null) {
			teacherInstance.password = springSecurityService.encodePassword(params.password)	
		}
            if (!teacherInstance.hasErrors() && teacherInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'teacher.label', default: 'Teacher'), teacherInstance.id])}"
                redirect(action: "show", id: teacherInstance.id)
            }
            else {
                render(view: "edit", model: [teacherInstance: teacherInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'teacher.label', default: 'Teacher'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def teacherInstance = Teacher.get(params.id)
        if (teacherInstance) {
            try {
                teacherInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'teacher.label', default: 'Teacher'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'teacher.label', default: 'Teacher'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'teacher.label', default: 'Teacher'), params.id])}"
            redirect(action: "list")
        }
    }
}
