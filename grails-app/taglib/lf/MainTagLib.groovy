package lf
import lf.*

class MainTagLib {

	def studentService
	def springSecurityService
	def invService

	static namespace='lf'

	def login = { attr->
		if(springSecurityService.isLoggedIn())
		{
			out << render(template:"/shared/loggedin")
		} else {
			out << render(template:"/shared/notloggedin")
		}


	}

	def displayEvidencesShared = { attr->
		def evidences = attr.evidences
		out << render(template:'/shared/evidencesTop')
		evidences.each { e->
			if(e.file !=null)
			{
				switch(e.fileType) { 
					case "mov":
					out << render(template:"/shared/evidenceVideoShared", model:[e:e])
					break

					case "mp3":
					out << render(template:"/shared/evidenceAudioShared", model:[e:e])
					break

					default:
					out << render(template:"/shared/evidenceFileShared", model:[e:e])
				}
			} else {
				out << render(template:"/shared/evidenceTextShared", model:[e:e])
			}

		}
		out << render(template:'/shared/evidencesBottom')
	}

	def displayEvidences = { attr->
		def evidences = attr.evidences
		def user = attr.user
		out << render(template:'/shared/evidencesTop')
		evidences.each { e->
			if(e?.file !=null)
			{
				if(e?.file.userId == user.id)
				{
					switch(e.fileType) {
						case ["mov",'movie']:
						out << render(template:"/shared/evidenceVideo", model:[e:e])
						break

						case "mp3":
						out << render(template:"/shared/evidenceAudio", model:[e:e])
						break

						default:
						out << render(template:"/shared/evidenceFile", model:[e:e])
					}
				} else {
					out << render(template:"/shared/evidenceText", model:[e:e])
				}
			} else {
			}

		}
		out << render(template:'/shared/evidencesBottom')
	}

	def displayEvidencesFlow = { attr->
		def evidences = attr.evidences
		out << render(template:'/shared/evidencesFlowTop', model:[evidences:evidences])
	}

	def displayEvidencesCarousel = { attr->
		def evidences = attr.evidences
		out << render(template:'/shared/evidencesCarouselTop', model:[evidences:evidences])
	}





	def renderCandoProgress = { attr->
		def mine = attr.myCandos
		def poolsize = attr.candopoolsize
		println "time="+System.currentTimeMillis()
		def temp = ''
		if(poolsize==0)
		{
			temp = render(template:"/shared/candoProgressEmpty")
			out << temp
			return
		}
		if(mine==null || mine?.size()==0)
		{
			temp = render(template:"/shared/candoProgressEmpty")
			out << temp
			return
		}

		def candoString = studentService.getCanDoStatus(mine, poolsize)
		switch(candoString)
		{
			case "empty":
				temp = render(template:"/shared/candoProgressEmpty")
				break
			case "none":
				temp = render(template:"/shared/candoProgressNone")
				break
			case "majority":
				temp = render(template:"/shared/candoProgressDone")
				break
			case "any":
				temp = render(template:"/shared/candoProgressSome")
				break
			default:
				println "TEMP was still EMPTY!!!\n"
				temp = render(template:"/shared/candoProgressEmpty")
		  break
		}
		out << temp
		return

		def iCanDoTotal = 0
		if(studentService.canDoEmpty(mine, poolsize))
		{
			temp = render(template:"/shared/candoProgressEmpty")
			out << temp
			return
		}

		if(studentService.canDoNone(mine, poolsize))
		{
			temp = render(template:"/shared/candoProgressNone")
			out << temp
			return
		}
		if(studentService.canDoMajority(mine, poolsize))
		{
			temp = render(template:"/shared/candoProgressDone")
			out << temp
			return
		}
		if(studentService.canDoAny(mine, poolsize))
		{
			temp = render(template:"/shared/candoProgressSome")
			out << temp
			return
		}

		if(temp=='') {
			println "TEMP was still EMPTY!!!\n"
			temp = render(template:"/shared/candoProgressEmpty")
		}
		out << temp

	}

	def displayEvidencesCountShared = { attr->
		def evidences = attr.evidences
		def p = attr.portfolio
		def evidencesCount = evidences?.size()
		def eicon = ""
		out << g.link(mapping:"viewSharedEvidence", controller:"account", action:"viewSharedEvidence", params:[id:attr.id, portfolio:p]) { 
			" view " +  (evidencesCount ?: 0)
		}
	}




	def displayEvidencesCount = { attr->
		def topicId = attr.topic
		def id = attr.id
		def portfolioId = attr.portfolioId
		def candoentryid = attr.candoentryid
		def evidences = attr.evidences
		def evidencesCount = evidences?.size()
		def evicon = "folder_documents.png"
		if(evidencesCount==null || evidencesCount==0) 
		{
			evicon = "folder_blue.png"
			evidencesCount=0
		}
//		def eicon = "<p><img border=\"0\" width=\"40\"  src=\"" + g.resource(dir:"images", file:evicon) + "\"/></p>"
		def eicon = ""
		out << g.link(controller:"account", action:"viewEvidences", id:id, params:['portfolio':portfolioId, 'topic':topicId]) {
			eicon + evidencesCount + " "
		}
		out << g.link(controller:"account", action:"viewEvidences", id:id, params:['portfolio':portfolioId, 'topic':topicId]) {
			" upload"
		}

	}


	def displayChecklistPortfolios={attr->
		def cl = attr.checklist
		def portfolios = attr.portfolios
		if(cl.name!=null && cl.name!='null'){
			out << "<h2>" + cl.name + "</h2>"
			cl.topics.sort{it.name}.each { t->
				def isChecked = false
				def foundP = portfolios.find{it.topic.id==t.id}
				if(foundP)
				{
					if(foundP.status==StatusCode.PORTFOLIO_INUSE)
					{
						isChecked = true
					}
				}
				// wasn't working???
//				if(portfolios.find{it.topic.id==t.id}.find{it.status==StatusCode.PORTFOLIO_INUSE})
//				{
//					isChecked = true
//				}
				out << "<div class='clearfix portfoliolistentry'>\n"
				out << g.checkBox(class:"left checkleft portfoliolistentry", name:"topic."+t.id, value:t.id, checked:isChecked)
				out << "<label style='' class='' for=\"topic." + t.id + "\">"
				out << t.name + "</label>"
				out << "</div>\n"
			}
		}
		
	}


	def renderSectionHeader = { attr->
		def type = attr.type
		if(type=='checkbox14')
		{
			out << g.render(template:"/shared/bioSectionHeaderChecklist14")
		}


	}

	def renderBioStatement = { attr->
		def s = attr.statement
		def type = attr.type
		def u = attr.user
		def bio = s.section.bio
		def mybio = Mybiography.findByOwnerAndBio(u,bio)
		def answer = MybiographyAnswer.findByMybioAndStatement(mybio, s)
		if(type=='checkbox14')
		{
			out << g.render(template:"/shared/bioStatementChecklist14", model:[viewingFriend: attr.viewingFriend, s:s, a:answer])
		}


	}

	def renderLearningInventoryHeader = { attr->
		def type = attr.type
		if(type=='checkbox14')
		{
			out << g.render(template:"/shared/learningInventorySectionHeaderChecklist14")
		}
	}

	def renderLearningInventoryStatementShared = { attr->
		def s = attr.statement
		def type = attr.type
		def portfolio = attr.portfolio
		def u = attr.user
		def inv = s.section.inventory

		def myinv = invService.loadByPortfolioAndInventory(portfolio,inv, u)
		def answer = MyInventoryAnswer.findByMyinventoryAndStatement(myinv, s)

		if(type=='checkbox14')
		{
			out << g.render(template:"/shared/learningInventoryStatementChecklist14", model:[s:s, a:answer, shared: true, disabled:true])
		}


	}


	def renderLearningInventoryStatement = { attr->
		def s = attr.statement
		def type = attr.type
		def portfolio = attr.portfolio
		def u = attr.user
		def inv = s.section.inventory

		def myinv = invService.loadByPortfolioAndInventory(portfolio,inv, u)
		def answer = MyInventoryAnswer.findByMyinventoryAndStatement(myinv, s)

		if(type=='checkbox14')
		{
			out << g.render(template:"/shared/learningInventoryStatementChecklist14", model:[s:s, a:answer, shared: false, disabled:false])
		}


	}

	def renderLearningInventory={attr->
		def cl = attr.checklist
		def portfolios = attr.portfolios
		if(cl.name!=null && cl.name!='null'){
			out << "<h2>" + cl.name + "</h2>"
			cl.topics.sort{it.name}.each { t->
				def isChecked = false
				if(portfolios.find{it.topic.id==t.id}.find{it.status==StatusCode.PORTFOLIO_INUSE})
				{
					isChecked = true
				}
				out << "<p>"
				out << g.checkBox(name:"topic."+t.id, value:t.id, checked:isChecked)
				out << "<label for=\"topic." + t.id + "\">" + t.name + "</label>"
				out << "</p>\n"
			}
		}
		
	}

	def thumbnail = { attr->
		return
		def e = attr.evidence
		def file = new File(e.file.pathToThumbnail)
		if (file.exists()) {
			def tika = new org.apache.tika.Tika()
			def detectedMime = tika.detect(file)
			response.setContentType(detectedMime ?: "application/octet-stream")
			response.setHeader("Content-disposition", "${params.contentDisposition}; filename=\"${file.name}\"")
			response.outputStream << file.newInputStream()
		}

	}

	def renderStatement = { a->
		def cando = a.cando
		def topic = a.topic
		def originalStatement = a.originalStatement
		def s = cando.topicStatements.find{it.topic==topic}
		if (!s) {
			out << "<span class='eng_statement'>"+originalStatement+"</span>"
		} else {
			out << "<span class='eng_statement' style='display:none'>"+originalStatement+"</span>"
			out << "<span class='alt_statement'>"+s.statement+"</span>"
		}
	}

}
