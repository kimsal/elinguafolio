package lf
import lf.*

class UserTagLib {

	static namespace = "lf"

	def userphoto = { attr->
		def u = attr.user
		out << "<img src='"
		out << g.createLink(controller:"thumb",action:"user", id:u.id)
		out << "' "
		out << " alt='Profile pic for " + u.username + "' "
		out << "/>"

//		out << "<img src='"
//		out << resource(file:UserService.getThumbnail(u))
//		out << "' "
//		out << " alt='Profile pic for " + u.username + "' "
//		out << "/>"
	}


	def userinfo = { attr->
		def u = attr.user
		out << render(template:"/shared/userinfo", model:[linkShared:attr.linkShared, user:u, confirm:attr.confirm, cancel:attr.cancel])
	}
}
