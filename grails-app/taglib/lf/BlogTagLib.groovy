package lf

class BlogTagLib {

	static namespace='blog'

	def render = { attr->
		def num = attr.number ?: 3
		def header = attr.header ?: "Recent entries"
		def be = Blogentry.withCriteria {
			maxResults(num as Integer)
			order('lastUpdated', 'desc')
		}
		out << g.render(template:"/main/blogtag", model:[header:header, entries:be, num:num as Integer])
	}
}
