package lf

class ChecklistTagLib {
	static namespace='lfchecklist'
	def render = { attr->
		attr?.scalesets?.each { ss->
			out << ss.id + "<hr/>"
		}
		out << "a"
	}
}
