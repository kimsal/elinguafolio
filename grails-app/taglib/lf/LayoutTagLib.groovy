package lf

class LayoutTagLib {

	static namespace="lf"
	def grailsApplication

	def layout = {
		def l = grailsApplication.config.lf.layout
		out << "\n\t<meta name=\"layout\" temp=\"foo\" content=\"" + l + "\"></meta>"
	}
	def ga =
	{
			def ga = grailsApplication.config.ga
			out << render(template:"/shared/ga", model:[ga:ga])
	}


	def chartbeat =
		{
			def cb = grailsApplication.config.chartbeat
			if(cb)
			{
				out << render(template:"/shared/chartbeat", model:[uid:cb.uid, domain:cb.domain])
			}
		}

	def chartbeathead =
		{
			def cb = grailsApplication.config.chartbeat
			if(cb?.uid)
			{
				out << render(template:"/shared/chartbeathead")
			}
		}

	def supportTag = {
		def email = grailsApplication.config.lf.support.email
		if (email)
		{
		out << "Need support?  Email <a href=\"mailto:${email}\">${email}</a>"
		}
	}
}
