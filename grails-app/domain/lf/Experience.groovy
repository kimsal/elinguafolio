package lf

class Experience {

        String description
        String experienceDate
        Topic topic

        String toString() { 
                name
        }

        static belongsTo = [experienceList: ExperienceList]

        Date dateCreated
        Date lastUpdated

        static constraints = {
                description(nullable:true, maxSize:20000)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
                experienceDate(nullable:true)
        }
}
