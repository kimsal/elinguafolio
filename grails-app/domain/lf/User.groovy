package lf

class User {

	def springSecurityService

	String fullName
	String username
	String password
	String myCode
	boolean enabled = true
	boolean accountExpired = false
	boolean accountLocked = false
	boolean accountConfirmed = false
	boolean passwordExpired = false
	String userType = "user"
	UFile profilepic
	Date lastLogin = null

	String email
	UFile file

	static hasMany = [myportfolios: Myportfolio]

	static constraints = {
		username blank: false, unique: true
		password blank: false
		email(nullable:true, unique:true, email:true)
		file(nullable:true)
		myportfolios(nullable:true)
		profilepic(nullable: true, blank:true)
		lastLogin(nullable: true)
	}

	static mapping = {
		password column: '`password`'
		table: 'xuser'
	}

	String toString() { fullName } 

	/*
	def setPassword(p)
	{
		this.password = springSecurityService.encodePassword(p)
	}

	def getPassword()
	{
		this.password
	}
	*/

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}
}
