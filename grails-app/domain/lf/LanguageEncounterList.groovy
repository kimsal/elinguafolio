package lf

class LanguageEncounterList extends EncounterList {


	String toString() { 
		id
	}

	static hasMany = [encounters:LanguageEncounter]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
