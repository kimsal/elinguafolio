package lf

class Notice
{


	String text
	Date start
	Date end
	String status

	static constraints =
		{
			status(inList: ['on','off'])
			start(nullable:true)
			end(nullable:true)
		}
}
