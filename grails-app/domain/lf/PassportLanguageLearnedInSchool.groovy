package lf

class PassportLanguageLearnedInSchool implements Serializable {

	String language
	String program
	String years
	String hours

        static belongsTo = [passport: PassportLanguage]

	String toString() { 
		id
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		language(nullable:true)
		program(nullable:true)
		years(nullable:true)
		hours(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
