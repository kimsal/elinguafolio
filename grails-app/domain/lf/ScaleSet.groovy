package lf
import lf.*

class ScaleSet {

	String name
	Integer sortOrder

	String toString() { 
		name
	}

	static belongsTo = [checklist: Checklist]
	static hasMany = [scales: Scale]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}
}
