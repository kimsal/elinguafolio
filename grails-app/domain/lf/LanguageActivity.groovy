package lf

class LanguageActivity extends Activity {

	String description
	String activityDate
	Topic topic
	String activity

	String toString() { 
		description
	}


	Date dateCreated
	Date lastUpdated

	static constraints = {
		description(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		activityDate(nullable:true)
		activity(inList:[
		'Exchanges with speakers of this language (letters, email, telephone)',
		'Participation in Language Clubs',
		'Presentations of projects carried out in the language',
		'Films viewed in original version or plays in the language',
		'Magazines, journals, newspapers I have read',
		'Books I have read',
		'Pariticipation in conferences or publications',
		'Other'
		])
	}

}
