package lf

class StatementForTopic {

	Topic topic
	String statement

	static belongsTo = [candoStatement: CandoStatement]

	static constraints = {
	}
}
