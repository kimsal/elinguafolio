package lf

class Inventory {

	String name
	Folio folio

	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static hasMany = [sections:InventorySection]

	static constraints = {
		sections(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
