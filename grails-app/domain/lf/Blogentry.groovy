package lf

class Blogentry {

	String title
	String message
	Date publishDate
	
	Date dateCreated
	Date lastUpdated

    static constraints = {
	message(nullable:false, maxSize:50000)
	title(nullable:false)
	publishDate(nullable:false)
    }
}
