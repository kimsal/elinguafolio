package lf

class Role {

	String authority

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true, matches: "^ROLE_.*"
	}
}
