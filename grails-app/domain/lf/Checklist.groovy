package lf

class Checklist {

	String name
	Integer sortOrder
	
	static mapping = {
		autoTimestamp true
	}

	String toString() { 
		name
	}

	static belongsTo = [folio:Folio]
	static hasMany = [topics:Topic, assessmentStatements: AssessmentStatement, modes: Mode, scaleSets:ScaleSet]
	
	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
//		lastUpdated(nullable:true, display:false)
//		dateCreated(display:false)
	}

}
