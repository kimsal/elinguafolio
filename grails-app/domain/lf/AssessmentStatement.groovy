package lf

class AssessmentStatement {

	String name
	Boolean isDone
	Integer sortOrder
	
	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [checklist:Checklist]

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		checklist(nullable:true)
	}

}
