package lf

class PassportLanguage extends Passport implements Serializable {

	String ptype = "language"
	String family

	String toString() { 
		id
	}

	static hasMany = [
		schools:PassportLanguageLearnedInSchool,
		outsideSchools: PassportLanguageLearnedOutsideSchool,
		experiences: PassportLanguageExperience,
		tests: PassportLanguageTest
	]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		family(nullable:true, maxSize:20000)
		schools(nullable:true)
		outsideSchools(nullable:true)
		experiences(nullable:true)
		tests(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
