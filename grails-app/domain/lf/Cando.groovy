package lf
import lf.*

class Cando {

	String name

	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
