package lf
import lf.*
import com.macrobit.grails.plugins.attachmentable.core.Attachmentable

class CandoStatement implements Attachmentable {

	String statement
	Integer sortOrder

	String toString() { 
		statement
	}

	static belongsTo = [scale:Scale, skill:Skill]

	static hasMany = [topicStatements: StatementForTopic]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

	transient def beforeDelete = {
		withNewSession{
			removeAttachments()
		}
	}
}
