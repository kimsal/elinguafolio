package lf
import lf.*

class Scale {

	String name
	Integer sortOrder

	String toString() { 
		name
	}

	static belongsTo = [scaleSet: ScaleSet]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
