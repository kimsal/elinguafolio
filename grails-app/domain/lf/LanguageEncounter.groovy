package lf

class LanguageEncounter extends Encounter{

	String toString() { 
		id
	}

//	static belongsTo = [encounterList: LanguageEncounterList]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
