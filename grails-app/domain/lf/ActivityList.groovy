package lf
import lf.*

class ActivityList {

	Folio folio
	User owner

	String toString() { 
		"language activities for " + folio.title
	}

	static hasMany = [activities:Activity]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		activities(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
