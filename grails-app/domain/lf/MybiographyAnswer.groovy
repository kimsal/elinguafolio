package lf

class MybiographyAnswer {

	String value
	BioStatement statement

	String toString() { 
		id + ' : ' + value
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [mybio: Mybiography]

	static constraints = {
		value(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
