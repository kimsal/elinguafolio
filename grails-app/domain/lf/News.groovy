package lf

class News {

	Boolean sticky
	Date startDate
	Date endDate
	String title
	String body
	
	Date lastUpdated
	Date dateCreated

	static hasMany = [tags:Newstag]

    static constraints = {
		tags(nullable:true)
		endDate(nullable:true)
		body(maxSize:50000, nullable:true)
		title(maxSize:500, nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
    }
}
