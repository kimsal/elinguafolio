package lf

class LanguageExperience extends Experience {

	String experience

	String toString() { 
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
                experience(inList:[
		'Hosting a foreign language speaking guest from a partner school, institution or family',
		'Participating in a home-stay exchange with a partner school, institution or family where foreign language is spoken',
		'Participating in a volunteer service project',
		'Participating in an immersiion language camp/academy',
		'Traveling for pleasure in a country where the target language is spoken',
		'Participating in personal interaction in the community or through social media',
		'Taking a formal course in the language',
		'Other'
                ])
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
