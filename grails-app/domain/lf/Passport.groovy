package lf

class Passport implements Serializable {

	String ptype = ""
	Folio folio
	User owner

	String toString() { 
		folio.toString() + " for " + owner
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
