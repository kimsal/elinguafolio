package lf

class PassportLanguageLearnedOutsideSchool implements Serializable {

	String language
	String acquisition
	String age

	static belongsTo = [passport: PassportLanguage]

	String toString() { 
		id		
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		language(nullable:true)
		acquisition(nullable:true)
		age(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
