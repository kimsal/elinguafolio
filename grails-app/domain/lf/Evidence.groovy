package lf
import lf.*

class Evidence {

	CandoEntry candoEntry
	String evidenceText
	String fileType
	UFile file
	String status

	String toString() { 
//		candoEntry + " " + evidenceText
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		evidenceText(nullable:true)
		file(nullable:true)
		status(nullable:true, inList:["new","pending","processing","failed","finished"])
		fileType(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
