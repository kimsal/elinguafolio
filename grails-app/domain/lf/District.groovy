package lf

class District {

	String name
	String code
	

	String toString() { 
		name
	}

	static belongsTo = [state:State]
	static hasMany = [schools:School]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		schools(nullable:true)
	}

}
