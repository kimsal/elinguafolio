package lf

class MyInventoryAnswer {

	String value
	InventoryStatement statement

	String toString() { 
		id + " : "  + value
	}

	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [myinventory: MyInventory]

	static constraints = {
		value(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
