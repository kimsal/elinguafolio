package lf

class InventorySection {

	String caption
	Integer sortOrder
	String sectionType = 'checkbox14'

	String toString() { 
		caption
	}

	static belongsTo = [inventory:Inventory]
	static hasMany = [statements:InventoryStatement]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
