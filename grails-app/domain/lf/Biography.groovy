package lf

class Biography {

	String name
	Folio folio

	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static hasMany = [sections:BioSection]

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		sections(nullable:true)
	}

}
