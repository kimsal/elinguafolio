package lf
import lf.*

class LanguageExperienceList extends ExperienceList {


	String toString() { 
		id
	}

        static hasMany = [experiences:LanguageExperience]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
