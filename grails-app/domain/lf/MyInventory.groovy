package lf

class MyInventory {

	User owner
	Inventory inventory
	Portfolio portfolio


	String toString() { 
		inventory
	}

	Date dateCreated
	Date lastUpdated

	static hasMany = [answers:MyInventoryAnswer]

	static constraints = {
		answers(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
