package lf
import lf.*

class Portfolio {

	String name
	Topic topic
	String status = StatusCode.PORTFOLIO_INUSE
	
	String toString() { 
		topic.name + " portfolio for " + myportfolio.owner.fullName
	}

	static hasMany = [candos: CandoEntry] //, evidences:Evidence]
	static belongsTo = [myportfolio:Myportfolio]


	Date dateCreated
	Date lastUpdated

	static constraints = {
		candos(nullable:true)
		status(nullable:true, inList:[StatusCode.PORTFOLIO_INUSE, StatusCode.PORTFOLIO_REMOVED])
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
