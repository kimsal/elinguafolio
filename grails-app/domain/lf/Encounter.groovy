package lf

class Encounter {

        String description
        String encounterDate
        Topic topic

        String toString() {
                name
        }

        static belongsTo = [encounterList: EncounterList]

        Date dateCreated
        Date lastUpdated

        static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
                description(nullable:true, maxSize: 100000)
                encounterDate(nullable:true)
        }

}
