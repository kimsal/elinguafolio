package lf

class School {

	String name
	String code

	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [district:District]

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
