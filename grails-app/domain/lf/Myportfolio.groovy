package lf

class Myportfolio {

	Folio folio
	String status = StatusCode.MYPORTFOLIO_INUSE

	String toString() { 
		folio.toString() + " for " + owner.toString()
	}

	Date dateCreated
	Date lastUpdated

	static hasMany = [portfolios:Portfolio, biographies:Biography]
	static belongsTo = [owner:User]

	static constraints = {
		portfolios(nullable:true)
		status(nullable:true, inList:[StatusCode.MYPORTFOLIO_INUSE, StatusCode.MYPORTFOLIO_REMOVED])
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
