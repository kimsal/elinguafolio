package lf
import lf.*

class Folio {

	String title

	String toString() { 
		title
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
