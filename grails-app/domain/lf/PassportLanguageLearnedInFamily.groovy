package lf

class PassportLanguageLearnedInFamily implements Serializable {

	String text

	String toString() { 
		id
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		text(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
