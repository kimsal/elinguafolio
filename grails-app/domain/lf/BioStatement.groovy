package lf

class BioStatement {

	String caption
	Integer sortOrder
	String statementType = "checkbox14" // or textarea

	String toString() { 
		caption
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [section:BioSection]

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
