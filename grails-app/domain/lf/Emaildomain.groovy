package lf
import lf.*

class Emaildomain {

	String domain

	String toString() { 
		domain
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
