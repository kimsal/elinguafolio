package lf
import lf.*

class ExperienceList {

        Folio folio
        User owner

        String toString() { 
                "language experiences for " + folio.title
        }

        static hasMany = [experiences:Experience]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		experiences(nullable:true)
	}

}
