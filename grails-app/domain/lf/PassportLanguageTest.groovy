package lf

class PassportLanguageTest implements Serializable {

	String language
	String description
	String testDate
	String score

        static belongsTo = [passport: PassportLanguage]

	String toString() { 
		id
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		language(nullable:true)
		description(nullable:true)
		testDate(nullable:true)
		score(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
