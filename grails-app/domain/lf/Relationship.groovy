package lf

class Relationship {

	User party1
	User party2
	Date dateInitiated
	Date dateConfirmed
	Date dateBlocked
	String status = StatusCode.INITIATED

	String toString() { 
		party1.fullName + " to " + party2 + " : " + status
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateInitiated(nullable:true)
		dateConfirmed(nullable:true)
		dateBlocked(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}


	static create(User user, User friend)
	{
		def f = new Relationship(party1:user, party2: friend, 
					dateInitiated: new Date()).save()
	}

	static remove(User user, User friend)
	{
		def all = allRelationships(user, friend);
		all.each { temp->
			temp.delete()
		}
	}

	static block(User user, User friend)
	{
		def all = allRelationships(user, friend);
		all.each { temp->
			temp.status=StatusCode.blocked
			temp.save()
		}
	}


	static allRelationships(User user, User friend)
	{
		def f = Relationship.findAllByParty1AndParty2(user, friend)
		def f2 = Relationship.findAllByParty1AndParty2(friend, user)

		f+f2
	}

}
