package lf
import lf.*

class Mode {

	String name
	Integer sortOrder

	String toString() { 
		name
	}
	static belongsTo = [checklist: Checklist]
	static hasMany = [skills: Skill]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
