package lf
import lf.*

class Topic {

	String name
	Checklist checklist
	
	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		checklist(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

	String fullName()
	{
		this.checklist.name + " " + this.name
	}
}
