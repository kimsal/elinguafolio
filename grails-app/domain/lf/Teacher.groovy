package lf

class Teacher extends User {

	School school
	String userType = "teacher"

	static hasMany = [students:Student, colleagues:Teacher]

	Date dateCreated
	Date lastUpdated

	static constraints = {
//		schoolName(nullable:true)
//		schoolAddress(nullable:true)
		colleagues(nullable:true)
		school(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
