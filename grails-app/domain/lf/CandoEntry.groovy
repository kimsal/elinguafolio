package lf

class CandoEntry {

	static attachmentable = true

	CandoStatement statement
	String candoStatus
	User user
	Topic topic


	String toString() { 
		statement.toString() // + " for " + user.fullName
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [portfolio: Portfolio]
	static hasMany = [evidences: Evidence]

	static constraints = {
		topic(nullable: true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		candoStatus(nullable:true) //, inList:[lf.StatusCode.CANDO_NO, lf.StatusCode.CANDO_PARTIAL, lf.StatusCode.CANDO_COMPLETE, lf.StatusCode.CANDO_NA])
	}

}
