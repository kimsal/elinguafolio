package lf

class Student extends User {

	School school
	String grade
	String userType = "student"
	
	static hasMany = [friends:Student, teachers:Teacher]
	static belongsTo = Teacher

	Date dateCreated
	Date lastUpdated

	static constraints = {
		myportfolios(nullable:true)
		friends(nullable:true)
		teachers(nullable:true)
		school(nullable:true)
		grade(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
