package lf
import lf.*
import com.macrobit.grails.plugins.attachmentable.core.Attachmentable

class Skill implements Attachmentable {

	String name
	Integer sortOrder

	String toString() { 
		name
	}

	static belongsTo = [mode: Mode]
	static hasMany = [candoStatements: CandoStatement]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
