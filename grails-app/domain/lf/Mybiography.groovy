package lf

class Mybiography {

	Biography bio
	User owner

	String toString() { 
		bio
	}

	Date dateCreated
	Date lastUpdated
	
	static hasMany = [answers:MybiographyAnswer]
	
	static constraints = {
		answers(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
