package lf

class Subject {

	String name

	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [folio:Folio]

	static constraints = {
		folio(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
