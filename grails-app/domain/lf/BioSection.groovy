package lf

class BioSection {

	String caption
	Integer sortOrder
	String sectionType = "checkbox14" // or textarea

	String toString() { 
		caption
	}

	Date dateCreated
	Date lastUpdated

	static belongsTo = [bio:Biography]
	static hasMany= [statements:BioStatement]

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
