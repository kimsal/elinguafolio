package lf

class Content {

	String name
	String type
	String body
	String status
	Date startDate
	Date endDate
	Date dateCreated
	Date lastUpdated

    static constraints = {
		type(inList: ['page','snippet','other'])
		status(inList: ['on','off'])
		body(maxSize:50000)
	}

}
