package lf

class EncounterList {

	Folio folio
	User owner

	String toString() { 
		id
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
