package lf

class Activity {

	String description
	String activityDate
	Topic topic
	String activity

	String toString() { 
		name
	}

        static belongsTo = [activityList: ActivityList]

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		description(nullable:true)
		activityDate(nullable:true)
	}

}
