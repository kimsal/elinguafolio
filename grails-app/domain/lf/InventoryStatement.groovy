package lf

class InventoryStatement {

	String caption
	Integer sortOrder
	

	String toString() { 
		caption
	}

	Date dateCreated
	Date lastUpdated
	static belongsTo = [section:InventorySection]

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
