package lf

class State {

	String name

	String toString() { 
		name
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
