package lf

/**
 * originally from a plugin at http://github.com/lucastex/grails-file-uploader
 * but incorporated in to the project directly to accomodate user tie-in directly
 */



class UFile {

	Long size
	String path
	String name
	String extension
	Date dateUploaded
	Integer downloads
	User user
	Date dateCreated
	Date lastUpdated
	String pathToThumbnail
	String pathToProcessedFile
	String fileType
	String mime

    static constraints = {
		size(min:0L)
		path()
		name()
		extension()
		dateUploaded()
		downloads()
		user(nullable:false)
		pathToThumbnail(nullable:true)
		pathToProcessedFile(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
		fileType(nullable: true)
		mime(nullable: true)
    }

	def afterDelete() {
		try {
			File f = new File(path)
			if (f.delete()) {
				log.debug "file [${path}] deleted"
			} else {
				log.error "could not delete file: ${file}"
			}
		} catch (Exception exp) {
			log.error "Error deleting file: ${e.message}"
			log.error exp
		}
	}
}
