package lf

class PassportLanguageExperience implements Serializable {

	String country
	String type 
	String age
	String length

        static belongsTo = [passport: PassportLanguage]

	String toString() { 
		id
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		country(nullable:true)
		type(nullable:true)
		age(nullable:true)
		length(nullable:true)
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
