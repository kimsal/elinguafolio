package lf
import lf.*

class Sharing {

	Portfolio portfolio
	User owner
	User sharedWith

	String toString() { 
			"Portfolio: " + portfolio + " owned by " + owner + " and shared with " + sharedWith
	}

	Date dateCreated
	Date lastUpdated

	static constraints = {
		dateCreated(nullable:true)
		lastUpdated(nullable:true)
	}

}
