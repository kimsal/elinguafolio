import lf.*
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

class lfFilters {

	def springSecurityService

	def filters = {
		all(controller: '*', action: '*') {
			before = {

				def s = springSecurityService
				def u = s.getPrincipal()
//				println u
//				println controllerName
//				println actionName
				if (!s?.isLoggedIn()) {
//					println "not logged in"
					session.user = null
					session.security = null
					if (controllerName!='error' && controllerName!='blog' 
						&&controllerName!='emailConfirmation' 
						&& controllerName != 'logout' && controllerName != 'register' &&
							controllerName != 'login' && controllerName != 'main') {
						redirect(controller: "login")
						session.invalidate()
						return false
					}
					return true
				} else {
//					println "is logged in"
					session.user = User.findById(u.id)
					session.userid = session.user.id
					session.security = s
					def now = new Date()-1
if(session.lastLogin == null) { session.lastLogin = now; }
					if(session.user.lastLogin < session.lastLogin )
					{
						session.user?.refresh()
						session.user.lastLogin = session.lastLogin
//						session.user.save(flush:true)
						log.info "updating last login for " +session.user
					}
//					println "session user is " + session.user
//					println "session user last login is " + session.user.lastLogin
//					println "session security is " + session.security

					def adminControllers = ['admin', 'adminnews', 'student', 'teacher', 'assessmentStatement',
							'cando', 'candoEntry', 'checklist', 'emaildomain',
							'folio', 'lfUser', 'mode', 'portfolio', 'scale', 'scaleSet',
							'skill', 'subject', 'topic', 'uFile', 'school',
							'user', 'role',
							'aclClass', 'aclEntry',
							'aclObjectIdentity', 'aclSid', 'auth',
							'layout', 'persistentLogin', 'registrationCode',
							'securityInfo']
//					println "checking for " + controllerName + " in -" + adminControllers
//					println "auth=" + SpringSecurityUtils.getPrincipalAuthorities()
//					println "2=" + SpringSecurityUtils.authoritiesToRoles()
//if( (controllerName in adminControllers) && (!SpringSecurityUtils.ifAnyGranted('ROLE_ADMIN'))) {
					if (controllerName in adminControllers) {
						if(!SpringSecurityUtils.ifAnyGranted('ROLE_ADMIN'))
						{
//							println "foo!"
							return false
						} else {
							return true
						}
					}
					return true
				}
			}
			afterView = {
//							session.user = null
//							session.security = null
			}
		}


               profiler(controller: '*', action: '*') {
                        before = {
                                request._timeBeforeRequest = System.currentTimeMillis()
                        }
                        after = {
                                request._timeAfterRequest = System.currentTimeMillis()
                        }
                        afterView = {
                                // add comment for testing again
                                if (1 || params.showTime) {
                                        session._showTime = true // params.showTime == "on"
                                }
                                if (session._showTime) {
                                        if (request?._timeAfterRequest!=null && request?._timeBeforeRequest!=null)
                                        {
                                                def viewDuration = System.currentTimeMillis() - request._timeAfterRequest
                                                def actionDuration = request._timeAfterRequest - request._timeBeforeRequest
                                                log.info("Request duration for (${controllerName}/${actionName}): ${actionDuration}ms/${viewDuration}ms")
                                 //               println("Request duration for (${controllerName}/${actionName}): ${actionDuration}ms/${viewDuration}ms")
                                        } else {
                                                log.info("Request had a null timeAfterRequest or timeBeforeRequest")
                                //                println("Request had a null timeAfterRequest or timeBeforeRequest")
                                        }
                                }
                        }

                }
}
}
