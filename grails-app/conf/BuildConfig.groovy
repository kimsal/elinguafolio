grails.project.class.dir = "target/classes"
grails.project.plugins.dir="./plugins"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir	= "target/test-reports"
//grails.project.war.file = "target/${appName}-${appVersion}.war"
//grails.project.dependency.resolver="maven"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits( "global" ) {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {        
        grailsPlugins()
        grailsHome()
        grailsCentral()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        //mavenLocal()
        mavenCentral()
   //     mavenRepo "http://snapshots.repository.codehaus.org"
//        mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
   //     mavenRepo "http://repository.jboss.com/maven2/"
//mavenRepo "http://localhost:8081/artifactory/plugins-releases-local/"
mavenRepo "http://download.java.net/maven/2/"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        compile "mysql:mysql-connector-java:5.1.26"
		compile (	'org.apache.poi:poi:3.8'){ excludes "xmlbeans" }
		compile ('org.apache.poi:poi-ooxml:3.8'){excludes "xmlbeans"}
		compile ('org.apache.poi:poi-ooxml-schemas:3.8'){ excludes "xmlbeans" }
		runtime 'javax.activation:activation:1.1.1'
		runtime 'javax.mail:mail:1.4.3'

		compile "org.grails.plugins:spring-security-core:1.2.4"
		compile "org.grails.plugins:spring-security-ui:0.2"
		runtime "org.grails.plugins:resources:1.1.6"

//		compile "org.grails.plugins:spring-security-core:1.2.7"
//		compile "org.grails.plugins:spring-security-ui:0.2"
//		runtime "org.grails.plugins:resources:1.1.6"
	}

	plugins {
		compile ":ckeditor:3.6.3.0"
		runtime ':external-config-reload:1.2.1'
//		compile ":grails-ui:1.2.3"
compile (":grails-ui:1.2.3") {
     excludes 'yui'
}
		compile ":uploadr:0.6.0.1"
		runtime ":modernizr:2.6.2"
		compile ":quartz:0.4.2"
		compile ":jquery-validation:1.9"
//		compile ":jquery-validation-ui:1.4"
compile ":jquery-validation-ui:1.4.7"
		runtime ":jquery:1.7.2"
		compile ":jquery-ui:1.8.24"
//compile ":yui:2.8.2.1"
//runtime ":simple-blog:0.2.1"
compile ":attachmentable:0.3.0"
compile ":taggable:1.0.1"
compile ":commentable:0.8.1"
compile ":feeds:1.6"
compile ":email-confirmation:2.0.8"
  }

}
