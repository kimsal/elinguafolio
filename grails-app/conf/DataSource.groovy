hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
environments {
	development {
		dataSource {
			//dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
//			url = "jdbc:mysql://localhost:3306/ntc2"
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect
			url = "jdbc:mysql://localhost:3306/lftest?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
			driverClassName = "com.mysql.jdbc.Driver"
			username = "root"
			password = "root"
		}
	}
}
