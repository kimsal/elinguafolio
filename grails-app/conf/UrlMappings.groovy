
class UrlMappings {
    static mappings = {
      "/$controller/$action?/$id?"{
	      constraints {
			 // apply constraints here
		  }
	  }
		name candoview: "/account/viewStatements/$portfolio/$skill/$scale" (controller:"account", action:"viewStatements")
		name candoviewShared: "/account/viewSharedStatements/$portfolio/$skill?/$scale?" (controller:"account", action:"viewSharedStatements")
      	name viewSharedEvidence: "/account/viewSharedEvidence/$portfolio/$id" (controller:"account", action:"viewSharedEvidence")
		name passportTab: "/account/passport/$id/$tab" (controller: "account", action: "passport")

		"/download/vid/$id/$type?" {
			controller = "download"
			action = "vid"
		}
		"/download/vidthumb/$id/$type?" {
			controller = "download"
			action = "vidthumb"
		}


		"/"(controller:"main")
//      "/login"(controller:"lfUser", action:"login")
//	  	"500"(controller: "error", action: "broke")
		"404"(controller: "error", action: "notfound")
	}
}
