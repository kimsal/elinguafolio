import lf.*
import org.grails.plugins.settings.*
import org.springframework.context.* 

import grails.util.GrailsUtil
import org.codehaus.groovy.grails.commons.GrailsApplication

import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.*
import org.apache.poi.*



class BootStrap {

	def emailConfirmationService
	def springSecurityService
	def userService
	def dataimportService
	def setupService

	def init = { servletContext ->

		development {
		//	importSchools()
		//	importChecklists()
		//	createRolesAndUsers()
		//	createSettings()
			setupEmail()
			setupService.createRolesAndUsers()
			setupService.setupNews()
//			setupService.development()
		//	createPortfolioAndBio()
		}
		production { 
			setupEmail()
			setupService.createRolesAndUsers()
			setupService.setupNews()
		}
		test { 
			setupEmail()
			//createRolesAndUsers() 
		}
	

	}

	def destroy = {
	}


	def setupEmail() {
		emailConfirmationService.onConfirmation = { email, uid ->
			println("User with id $uid has confirmed their email address $email")
			log.info("User with id $uid has confirmed their email address $email")
			def u = User.get(uid)
			u.accountConfirmed= true
			u.enabled= true
u.validate()
println u.errors
			u.save(flush:true)
			// now do something…
			// Then return a map which will redirect the user to this destination
			return {
				flash.message = "Your account is now activated.  Please log in!"
				redirect(controller:'login')
				}
		}
		emailConfirmationService.onInvalid = { uid -> 
		log.warn("User with id $uid failed to confirm email address after 30 days")
		}
		emailConfirmationService.onTimeout = { email, uid -> 
		log.warn("User with id $uid failed to confirm email address after 30 days")
		}
	}


}
