import grails.plugins.springsecurity.SecurityConfigType
import org.codehaus.groovy.grails.commons.GrailsApplication
import grails.util.GrailsUtil
println "THIS WAS LOADED"
def env = GrailsUtil.getEnvironment()


// from http://www.baselogic.com/blog/development/java-javaee-j2ee/getting-grails-external-configuration-working-in-the-real-world/
println "APPNAME="+appName

def basename = appName+"-"+env
println "BASENAME="+basename
def ENV_NAME = "${appName}.config.location"
if (!grails.config.locations || !(grails.config.locations instanceof List)) {
    grails.config.locations = []
}
println "--------------------------------------------------------"
 grails.config.locations = [ "classpath:${appName}-config.properties",
                             "classpath:${appName}-config.groovy",
                             "file:${userHome}/.grails/${appName}-config.properties",
                             "file:${userHome}/.grails/${appName}-config.groovy"]
println grails.config.locations

// 1: A command line option should override everything.
// Test by running:
// grails -Ddivr.config.location=C:\temp\divr-config.groovy run-app
// or
// grails -Ddivr.config.location=C:\temp\divr-config.properties run-app


grails.mail.default.from="do.not.reply@elinguafolio.org"


// http://www.anyware.co.uk/2005/2011/09/12/optimising-your-application-with-grails-resources-plugin/
grails.resources.adhoc.excludes = ["/plugins/richui-0.8/*", "/plugins/richui-0.8/**"]

grails.plugins.springsecurity.useSecurityEventListener = true
grails.plugins.springsecurity.onInteractiveAuthenticationSuccessEvent = { ev, ap ->
/*
	lf.User.withTransaction { status ->
		def user = lf.User.findByUsername(ev.source.principal.username.toString())
user?.refresh()
		user?.lastLogin = new Date()
		user?.save()
	}
*/
}
// Added by the JQuery Validation UI plugin:
jqueryValidationUi {
	errorClass = 'error'
	validClass = 'valid'
	onsubmit = true
	renderErrorsOnTop = false
	
	qTip {
		packed = true
	  classes = 'ui-tooltip-red ui-tooltip-shadow ui-tooltip-rounded'  
	}
	
	/*
	  Grails constraints to JQuery Validation rules mapping for client side validation.
	  Constraint not found in the ConstraintsMap will trigger remote AJAX validation.
	*/
	StringConstraintsMap = [
		blank:'required', // inverse: blank=false, required=true
		creditCard:'creditcard',
		email:'email',
		inList:'inList',
		minSize:'minlength',
		maxSize:'maxlength',
		size:'rangelength',
		matches:'matches',
		notEqual:'notEqual',
		url:'url',
		nullable:'required',
		unique:'unique',
		validator:'validator'
	]
	
	// Long, Integer, Short, Float, Double, BigInteger, BigDecimal
	NumberConstraintsMap = [
		min:'min',
		max:'max',
		range:'range',
		notEqual:'notEqual',
		nullable:'required',
		inList:'inList',
		unique:'unique',
		validator:'validator'
	]
	
	CollectionConstraintsMap = [
		minSize:'minlength',
		maxSize:'maxlength',
		size:'rangelength',
		nullable:'required',
		validator:'validator'
	]
	
	DateConstraintsMap = [
		min:'minDate',
		max:'maxDate',
		range:'rangeDate',
		notEqual:'notEqual',
		nullable:'required',
		inList:'inList',
		unique:'unique',
		validator:'validator'
	]
	
	ObjectConstraintsMap = [
		nullable:'required',
		validator:'validator'
	]
	
	CustomConstraintsMap = [
		phone:'true', // International phone number validation
		phoneUS:'true',
		alphanumeric:'true',
		letterswithbasicpunc:'true',
    lettersonly:'true'
	]	
}

grails.views.default.codec="none" // none, html, base64
grails.views.gsp.encoding="UTF-8"

grails.app.context="/"
	defaultTitle = "eLinguaFolio - LinguaFolio software for North Carolina"
	dataSource.username = "lf"
	dataSource.password= "lf"
	dataSource.dbCreate= "update"
	dataSource.dialect = org.hibernate.dialect.MySQL5InnoDBDialect
	dataSource.url = "jdbc:mysql://localhost:3306/lf?autoReconnect=true&zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=UTF-8&failOverReadOnly=false&maxReconnects=20"
	dataSource.driverClassName = "com.mysql.jdbc.Driver"

        grails.serverURL = "http://www.elinguafolio.org"
	lf.base = "/home/lf/apps/tc8/webapps/ROOT/"
	grails.attachmentable.uploadDir = "/home/lf/apps/lf8/webapps/ROOT/ul/"
	grails.attachmentable.uploadDir = "/home/lf/uploads/"
	grails.attachmentable.path = "uploads/"

	grails.mail.default.from="do.not.reply@elinguafolio.org"
if(0)
{
grails {
   mail {
	host = "smtp.gmail.com"
     port = 465
	username = "elinguafolio@gmail.com"
	password = "Beatles65!"
     props = ["mail.smtp.auth":"true", 					   
              "mail.smtp.socketFactory.port":"465",
              "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
              "mail.smtp.socketFactory.fallback":"false"]
   }
}
}
//if(1) { 
xgrails { 
	mail {
		host = "email-smtp.us-east-1.amazonaws.com"
		port = 25
		username = "AKIAIWDIENERB5RN4PMQ"
		password = "AsUlI2vNlXZWcU8+J5AaRutAvfKzbVZIG2fS1QWw6adg"

//		username = "AKIAJIUWUP67T4OURG3A"
//		password = "Al9/7HrX4ZMGwia8vSJHVooUW8Ft5FQT097Be0GZ8g9m"
xprops = ["mail.smtp.starttls.enable":"true",
		"mail.smtp.auth":"true",
                "mail.smtp.port":"25"]
		props = [
// from http://stackoverflow.com/questions/17399879/aws-ses-mail-plugin-and-grails-configuration
//"mail.smtp.starttls.enable":"true", 
"mail.smtp.auth": "true",
"mail.smtp.socketFactory.port": "25",
"mail.smtp.socketFactory.class": "javax.net.ssl.SSLSocketFactory",
]
	}
}
beans {
//	mailSender.host = ""
}
// from http://roshandawrani.wordpress.com/2011/04/06/grails-using-mail-plugin-with-amazon-email-service/
//}


	grails.attachmentable.maxInMemorySize = 6024
	grails.attachmentable.maxUploadSize = 10024000000
	grails.attachmentable.poster.evaluator = { getPrincipal() }
	grails.attachmentable.searchableFileConverter="attachmentFileConverter"



fileuploader {
	avatar {
		maxSize = 941024 * 1256 //256 kbytes
		allowedExtensions = ["jpg","jpeg","gif","png"]
		path = "/srv/avatar/"
	}
	docs {
		maxSize = 1000 * 1024 * 90 //90 mbytes
		allowedExtensions = ["txt","wmv","wma","m4v","mp3","mp4","wav","ogg","mpeg","mpg","jpg","png","gif","jpeg","bmp","mov","avi","xls","xlsx","ppt","pptx","doc","docx", "pdf", "rtf","odt","ods", "key","pages","zip","flv", "TXT","WMV","WMA","M4V","MP3","MP4","WAV","OGG","MPEG","MPG","JPG","PNG","GIF","JPEG","BMP","MOV","AVI","XLS","XLSX","PPT","PPTX","DOC","DOCX", "PDF", "RTF","ODT","ODS", "KEY","PAGES","ZIP","FLV"]
		path = "/srv/docs/"
	}
}

// log4j configuration
log4j = {
    appenders {
        console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
		file name:"mylog", file:"/tmp/mylog.txt", layout: pattern(conversionPattern: '%d{dd-MM-yyyy HH:mm:ss,SSS} %5p %c{1} - %m%n')
    }


/*
    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
	       'org.codehaus.groovy.grails.web.pages', //  GSP
	       'org.codehaus.groovy.grails.web.sitemesh', //  layouts
	       'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
	       'org.codehaus.groovy.grails.web.mapping', // URL mapping
	       'org.codehaus.groovy.grails.commons', // core / classloading
	       'org.codehaus.groovy.grails.plugins', // plugins
	       'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
	       'org.springframework',
	       'org.hibernate',
           'net.sf.ehcache.hibernate'
*/

    warn   'org.mortbay.log'

	info 	'grails.app'

	root
	{
//		info 'stdout','mylog'
		error 'mylog'
	}
}
     
//authenticationUserClass=LfUser
grails.sitemesh.default.layout='main'



lf.layout="main"

// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'lf.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'lf.UserRole'
grails.plugins.springsecurity.authority.className = 'lf.Role'
grails.plugins.springsecurity.securityConfigType = SecurityConfigType.InterceptUrlMap
grails.plugins.springsecurity.useSwitchUserFilter=true

grails.plugins.springsecurity.interceptUrlMap = [
'/j_spring_security_switch_user': ['ROLE_ADMIN', 'ROLE_SWITCH_USER', 'IS_AUTHENTICATED_FULLY'],
'/admin/**':    ['ROLE_ADMIN'],
'/user/**':    ['ROLE_ADMIN'],
'/finance/**':   ['ROLE_FINANCE', 'IS_AUTHENTICATED_FULLY'],
'/main/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/logout/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/login/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/confirm/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/register/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/js/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/css/**':       ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/images/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/*':            ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/login/**':     ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/logout/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
'/securityInfo/**':    ['ROLE_ADMIN'],
'/registrationCode/**':    ['ROLE_ADMIN'],
'/persistentLogin/**':    ['ROLE_ADMIN'],
'/layout/**':    ['ROLE_ADMIN'],
'/aclObjectIdentity/**':    ['ROLE_ADMIN'],
'/aclSid/**':    ['ROLE_ADMIN'],
'/auth/**':    ['ROLE_ADMIN'],
'/aclClass/**':    ['ROLE_ADMIN'],
'/aclEntry/**':    ['ROLE_ADMIN'],
'/skill/**':    ['ROLE_ADMIN'],
'/subject/**':    ['ROLE_ADMIN'],
'/topic/**':    ['ROLE_ADMIN'],
'/uFile/**':    ['ROLE_ADMIN'],
'/school/**':    ['ROLE_ADMIN'],
'/scale/**':    ['ROLE_ADMIN'],
'/scaleSet/**':    ['ROLE_ADMIN'],
'/portfolio/**':    ['ROLE_ADMIN'],
'/mode/**':    ['ROLE_ADMIN'],
'/lfUser/**':    ['ROLE_ADMIN'],
'/folio/**':    ['ROLE_ADMIN'],
'/candoEntry/**':    ['ROLE_ADMIN'],
'/cando/**':    ['ROLE_ADMIN'],
'/emaildomain/**':    ['ROLE_ADMIN'],
'/checklist/**':    ['ROLE_ADMIN'],
'/student':    ['ROLE_ADMIN'],
'/teacher':    ['ROLE_ADMIN'],
'/student/**':    ['ROLE_ADMIN'],
'/teacher/**':    ['ROLE_ADMIN'],
'/assessmentStatement/**':    ['ROLE_ADMIN']
]

// per the http://www.grails.org/plugin/jquery notes
grails.views.javascript.library="jquery"
jquery {
	sources = 'jquery' // Holds the value where to store jQuery-js files /web-app/js/
	version = '1.7.1' // The jQuery version in use
}


grails.plugins.appinfo.dotPath = "/usr/local/bin/dot"

lf.clam = "/usr/bin/clamscan --quiet -i "

convertpath = "/usr/bin/convert"
ffmpegpath = "/usr/bin/ffmpeg"

chartbeat.uid=38412
chartbeat.domain="elinguafolio.org"
ga.uid="UA-224419-19"
ga.domain="elinguafolio.org"

ckeditor {
	config = "/js/myckconfig.js"
	skipAllowedItemsCheck = false
	defaultFileBrowser = "ofm"
	upload {
		basedir = "/home/lf/uploads/"
		baseurl = "/uploads/"
		overwrite = false
		link {
			browser = true
			upload = true
			allowed = ['doc','docx','pdf','xls','xlsx','ppt','pptx']
			denied = ['html', 'htm', 'php', 'php2', 'php3', 'php4', 'php5',
					'phtml', 'pwml', 'inc', 'asp', 'aspx', 'ascx', 'jsp',
					'cfm', 'cfc', 'pl', 'bat', 'exe', 'com', 'dll', 'vbs', 'js', 'reg',
					'cgi', 'htaccess', 'asis', 'sh', 'shtml', 'shtm', 'phtm']
		}
		image {
			browser = true
			upload = true
			allowed = ['jpg', 'gif', 'jpeg', 'png']
			denied = []
		}
		flash {
			browser = false
			upload = false
			allowed = ['swf']
			denied = []
		}
	}
}

lf.support.email="support@elinguafolio.org"

plugin.platformCore.site.name="eLinguafolio"
plugin.platformCore.organization.name="eLinguafolio.org"

plugin.emailConfirmation.from = '"Do not reply" <do.not.reply@elinguafolio.org>'
grails.cache.config = {
  provider {
     name "ehcache-lf-"+(new Date().format("yyyyMMddHHmmss"))
     }
}


