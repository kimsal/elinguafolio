<%@ page import="lf.State" %>



<div class="fieldcontain ${hasErrors(bean: stateInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="state.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${stateInstance?.name}"/>
</div>

