<html>

<head>
<title><g:message code='spring.security.ui.resetPassword.title'/></title>
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
</head>

<body>

<p/>

<div class="centered" style="margin: 0 auto; text-align:center;">
<s2ui:form width='475' height='250' elementId='resetPasswordFormContainer'
           titleCode=' ' center='true'>
<div class="alert">
Note:  your new password must have at least one upper case letter, one lower case letter, one number/digit, and one "special" character (!@#$%^&)
</div>


	<g:form action='resetPassword' name='resetPasswordForm' autocomplete='off'>
	<g:hiddenField name='t' value='${token}'/>
	<div class="sign-in">

	<br/>
	<h4><g:message code='spring.security.ui.resetPassword.description'/></h4>

	<table>
		<s2ui:passwordFieldRow name='password' labelCode='resetPasswordCommand.password.label' bean="${command}"
                             labelCodeDefault='Password' value="${command?.password}"/>

		<s2ui:passwordFieldRow name='password2' labelCode='resetPasswordCommand.password2.label' bean="${command}"
                             labelCodeDefault='Password (again)' value="${command?.password2}"/>
	</table>
<input type='submit' value='Reset my password' class='btn btn-info' elementId='reset'/>

	</div>
	</g:form>

</s2ui:form>
</div>

<script>
$(document).ready(function() {
	$('#password').focus();
});
</script>

</body>
</html>
