<head>
<title>Forgot your password?</title>
<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
</head>

<body>

 <style>
 .ui-widget-content {
     border: 0px;
 }
     </style>



<div class="span12" style="text-align:center; margin: 0 auto;">

    <g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='off'>

	<g:if test='${emailSent}'>
	<br/>
	
	</g:if>

	<g:else>

	<br/>
        <h2 class="centered">Forgot password</h2>

	<table class="logintable">
		<tr>
			<td><label id="usernameLabel" for="username"><g:message code='spring.security.ui.forgotPassword.username'/></label></td>
			<td><g:textField name="username" size="25" /></td>
		</tr>
        <tr>
            <td colspan="2">or</td>
        </tr>
		<tr>
			<td><label for="email"><g:message code='spring.security.ui.forgotPassword.email'/></label></td>
			<td><g:textField name="email" size="25" /></td>
		</tr>


	</table>

        <div class="clear"></div>
        <g:submitButton name="submit" value="Reset my password" class="btn btn-primary"/>
        <div class="clear"></div>
	</g:else>

	</g:form>
 </div>

<script>
$(document).ready(function() {
	$('#username').focus();
});
</script>

</body>
