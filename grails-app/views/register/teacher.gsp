<%@ page import="lf.Teacher" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <g:set var="entityName" value="Teacher"/>
        <title>Create a teacher account</title>
    </head>
    <body>
        <div class="body">
            <h1>Create a teacher account</h1>
            <g:if test="${flash.errors}">
            <div class="errors">${flash.errors}</div>
            </g:if>
            <g:if test="${message}">
            <div class="errors">${message}</div>
            </g:if>

            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${teacherInstance}">
            <div class="errors">
                <g:renderErrors bean="${teacherInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="teacherProcess" method="post" name="registerForm" >
						<p>
We'll ask you for more information after you've confirmed you can receive emails at the email address you'll supply below.
						</p>
                <div class="dialog">
                    <table>
                        <tbody>
                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="teacher.fullName.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'fullName', 'errors')}">
                                    <g:textField minlength="5" class="required"  name="fullName" value="${teacherInstance?.fullName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="username"><g:message code="teacher.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'username', 'errors')}">
                                    <g:textField minlength="5" class="checkUser required" name="username" value="${teacherInstance?.username}" />
                                </td>
                            </tr>
                        
                       
                       
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="teacher.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'password', 'errors')}">
                                    <g:passwordField class="required" minlength="5" name="password" value="${teacherInstance?.password}" />
                                </td>
                            </tr>

                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="teacher.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'email', 'errors')}">
                                    <g:textField class="required email" name="email" value="${teacherInstance?.email}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="">
                    <g:submitButton name="create" class="btn btn-success" value="Register" />
                </div>
            </g:form>

<hr/>
Note - if you see an error message about your email not being 'unique', it's very likel there is already account registered with that email address.  Visit the <g:link action="forgotPassword">forgot password</g:link> page to reset your password.
        </div>

    <script>
        $(document).ready(function() {

            jQuery.validator.addMethod("checkUser",function(value) {
                var re=/^[_a-z0-9]+$/i;
                return re.test(value);
            }, "Please use only letters, numbers and underscore characters");

            jQuery.validator.classRuleSettings.checkUser = { checkUser: true };

            $("#registerForm").validate({
                submitHandler: function(form) {
                    $("#username").val($("#username").val().toLowerCase());
                    form.submit();
                }
            });
            $('#fullName').focus();
        });


    </script>

    </body>
</html>
