<head>
<title>Forgot your username?</title>
<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
</head>

<body>

<style>
.ui-widget-content {
    border: 0px;
}
</style>


<div class="span12" style="text-align:center; margin: 0 auto;">
	<g:form action='forgotUsername' name="forgotUsernameForm" autocomplete='off'>

	<g:if test='${emailSent}'>
	<br/>
        Username was sent to ${user.email}
	</g:if>

	<g:else>

	<br/>
        <h2 class="centered">Forgot username?</h2>

	<table class="logintable">
		<tr>
			<td><label for="email"><g:message code='spring.security.ui.forgotUsername.email'/></label></td>
			<td><g:textField name="email" size="25" /></td>
		</tr>


	</table>

        <div class="clear"></div>
        <g:submitButton name="submit" value="Reset my password" class="btn btn-primary"/>
        <div class="clear"></div>
	</g:else>

	</g:form>

</div>

<script>
$(document).ready(function() {
	$('#email').focus();
});
</script>

</body>
