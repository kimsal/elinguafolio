<%@ page import="lf.Student" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}" />
        <title>Create a student account</title>
    </head>
    <body>
        <div class="body">
            <h1>Create a student account</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${studentInstance}">
            <div class="errors">
                <g:renderErrors bean="${studentInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="studentProcess" method="post" name="registerForm" class="form-horizontal" >
						<p>
We'll ask you for more information after you've confirmed you can receive emails at the email address you'll supply below.
						</p>
                <div class="dialog">
                    <table class="">
                        <tbody>
                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fullName"><g:message code="student.fullName.label" default="Full name"  /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'fullName', 'errors')}">
                                    <g:textField minlength="5" class="required" name="fullName"  value="${studentInstance?.fullName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="username"><g:message code="student.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'username', 'errors')}">
                                    <g:textField  minlength="5" maxlength="30" class="checkUser required" name="username" value="${studentInstance?.username}" />
                                </td>
                            </tr>
                        
                       
                       
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="student.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'password', 'errors')}">
                                    <g:passwordField minlength="5" name="password" value="${studentInstance?.password}" />
                                </td>
                            </tr>

                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="student.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${studentInstance?.email}" class="required email" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="">
                    <g:submitButton name="create" class="btn btn-success" value="Register" />
                </div>
            </g:form>
<hr/>
Note - if you see an error message about your email not being 'unique', it's very likely your teacher created an account for you with that email address.  Visit the <g:link action="forgotPassword">forgot password</g:link> page to reset your password.
        </div>

    <script>
        $(document).ready(function() {

            jQuery.validator.addMethod("checkUser",function(value) {
                var re=/^[_a-z0-9]+$/i;
                return re.test(value);
            }, "Please use only letters, numbers and underscore characters");

            jQuery.validator.classRuleSettings.checkUser = { checkUser: true };

            $("#registerForm").validate({
                submitHandler: function(form) {
                    $("#username").val($("#username").val().toLowerCase());
                    form.submit();
                }
            });
            $('#fullName').focus();
        });

    </script>
    </body>
</html>
