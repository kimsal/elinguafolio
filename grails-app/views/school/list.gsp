
<%@ page import="lf.School" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'school.label', default: 'School')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:form method='post' action='search'>
                <g:textField name="search"/>
                <g:submitButton name="submit" value="Search" class="btn btn-primary"/>
            </g:form>
            <g:if test="${search}">Searching for '${search}' <g:link action="clear">clear search</g:link> </g:if>

            <div class="list">
                <table class="table">
                    <thead>
                        <tr>

                            <th></th>

                            <g:sortableColumn property="code" title="${message(code: 'school.code.label', default: 'Code')}" />
                        
                            <th><g:message code="school.district.label" default="District" /></th>
                        
                            <g:sortableColumn property="name" title="${message(code: 'school.name.label', default: 'Name')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${schoolInstanceList}" status="i" var="schoolInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link class="btn btn-info" action="show" id="${schoolInstance.id}">show</g:link></td>
                        
                            <td>${fieldValue(bean: schoolInstance, field: "code")}</td>
                        
                            <td>
                                <%  try {
                                    if(schoolInstance.district?.id>0) { %>
                                ${schoolInstance.district?.name}
                                <% }
                                } catch (Exception e) {} %>
                            </td>
                        
                            <td>${fieldValue(bean: schoolInstance, field: "name")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${schoolInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
