
<%@ page import="lf.Student" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${studentInstance}">
            <div class="errors">
                <g:renderErrors bean="${studentInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${studentInstance?.id}" />
                <g:hiddenField name="version" value="${studentInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="student.fullName.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'fullName', 'errors')}">
                                    <g:textField name="fullName" value="${studentInstance?.fullName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="username"><g:message code="student.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'username', 'errors')}">
                                    <g:textField name="username" value="${studentInstance?.username}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="password"><g:message code="student.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'password', 'errors')}">
                                    <g:textField name="password" value=""/>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="email"><g:message code="student.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${studentInstance?.email}" />
                                </td>
                            </tr>

                             <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="grade"><g:message code="student.grade.label" default="Grade" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'grade', 'errors')}">
                                    <g:textField name="grade" value="${studentInstance?.grade}" />
                                </td>
                            </tr>
                                               
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="portfolios"><g:message code="student.myportfolios.label" default="Portfolios" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'myportfolios', 'errors')}">
                                    
<ul>
<g:each in="${studentInstance?.myportfolios?}" var="p">
    <li><g:link controller="myportfolio" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="myportfolio" action="create" params="['student.id': studentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'myportfolio.label', default: 'Portfolio')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="friends"><g:message code="student.friends.label" default="Friends" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'friends', 'errors')}">
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="teachers"><g:message code="student.teachers.label" default="Teachers" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'teachers', 'errors')}">
                                    
                                </td>
                            </tr>
                              <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="school"><g:message code="student.school.label" default="School" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'school', 'errors')}">
                                    <g:select name="school" from="${lf.School.list()}"  optionKey="id" value="${studentInstance?.school?.id}" />

                                </td>
                            </tr>

                       
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="myCode"><g:message code="student.myCode.label" default="My Code" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'myCode', 'errors')}">
                                    <g:textField name="myCode" value="${studentInstance?.myCode}" />
                                </td>
                            </tr>
                             <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="enabled"><g:message code="student.myCode.label" default="Enabled" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'enabled', 'errors')}">
                                    <g:checkBox name="enabled" value="${studentInstance?.enabled}" />
                                </td>
                            </tr>
                              <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="accountExpired"><g:message code="student.myCode.label" default="Expired" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'accountExpired', 'errors')}">
                                    <g:checkBox name="accountExpired" value="${studentInstance?.accountExpired}" />
                                </td>
                            </tr>
                        
                               <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="accountLocked"><g:message code="student.myCode.label" default="Locked" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'accountLocked', 'errors')}">
                                    <g:checkBox name="accountLocked" value="${studentInstance?.accountLocked}" />
                                </td>
                            </tr>
                                <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="accountConfirmed"><g:message code="student.myCode.label" default="Confirmed" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'accountConfirmed', 'errors')}">
                                    <g:checkBox name="accountConfirmed" value="${studentInstance?.accountConfirmed}" />
                                </td>
                            </tr>
                                 <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="passwordExpired"><g:message code="student.myCode.label" default="Password Expired" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'passwordExpired', 'errors')}">
                                    <g:checkBox name="passwordExpired" value="${studentInstance?.passwordExpired}" />
                                </td>
                            </tr>
                        
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
