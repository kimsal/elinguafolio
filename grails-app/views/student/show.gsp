
<%@ page import="lf.Student" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.lastUpdated.label" default="Last Updated" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${studentInstance?.lastUpdated}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.dateCreated.label" default="Date Created" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${studentInstance?.dateCreated}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.fullName.label" default="Name" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "fullName")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.username.label" default="Username" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "username")}</td>
                            
                        </tr>
                    
                   
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.email.label" default="Email" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "email")}</td>
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.grade.label" default="Grade" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "grade")}</td>
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myportfolios.label" default="Portfolios" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${studentInstance.myportfolios}" var="p">
                                    <li><g:link controller="account" action="viewShared" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.friends.label" default="Friends" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${studentInstance.friends}" var="f">
                                    <li><g:link controller="student" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.teachers.label" default="Teachers" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${studentInstance.teachers}" var="t">
                                    <li><g:link controller="teacher" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.school.label" default="School" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "school")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="My Code" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "myCode")}</td>
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Enabled" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "enabled")}</td>
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Expired" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "accountExpired")}</td>
                        </tr>
                          <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Locked" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "accountLocked")}</td>
                        </tr>
                           <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Confirmed" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "accountConfirmed")}</td>
                        </tr>
                            <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Password expired" /></td>
                            <td valign="top" class="value">${fieldValue(bean: studentInstance, field: "passwordExpired")}</td>
                        </tr>
                                          
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${studentInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
