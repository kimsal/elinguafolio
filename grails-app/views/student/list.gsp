<%@ page import="lf.Student" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>

    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

            <g:form method='post' action='search'>
                <g:textField name="search"/>
                <g:submitButton name="submit" value="Search" class="btn btn-primary"/>
            </g:form>
            <g:if test="${search}">Searching for '${search}' <g:link action="clear">clear search</g:link> </g:if>


            <div class="list">
<filterpane:filterButton text="Filter" />
                <table class="table">
                    <thead>
                        <tr>
                        
                            <th></th>

                            <g:sortableColumn property="fullName" title="${message(code: 'student.fullName.label', default: 'Name')}" />
                        
                            <g:sortableColumn property="username" title="${message(code: 'student.username.label', default: 'Username')}" />
                            <g:sortableColumn property="email" title="${message(code: 'student.email.label', default: 'Email')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${studentInstanceList}" status="i" var="studentInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>
<sec:ifAllGranted roles='ROLE_ADMIN, ROLE_SWITCH_USER'>
<g:form method="post" url="${createLink(controller:'j_spring_security_switch_user', absolute:true)}">
      <g:link class="btn btn-info" action="show" id="${studentInstance.id}">show</g:link>
      <input type='hidden' name='j_username' value='${studentInstance.username}'/>
      <input class="btn btn-warning" type='submit' value='become'/>
   </g:form>
</sec:ifAllGranted>
</td>
                        
                            <td>${fieldValue(bean: studentInstance, field: "fullName")}</td>
                        
                            <td>${fieldValue(bean: studentInstance, field: "username")}</td>
                            <td>${fieldValue(bean: studentInstance, field: "email")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">

                <g:paginate total="${studentInstanceTotal}" />
            </div>
        </div>


    </body>
</html>
