
<%@ page import="lf.Subject" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'subject.id.label', default: 'Id')}" />
                        
                            <th><g:message code="subject.folio.label" default="Folio" /></th>
                   	    
                            <g:sortableColumn property="name" title="${message(code: 'subject.name.label', default: 'Name')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${subjectInstanceList}" status="i" var="subjectInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>
                                <g:link action="show" id="${subjectInstance.id}">show</g:link>
                                <g:link action="edit" id="${subjectInstance.id}">edit</g:link>
                            </td>

                            <td>${fieldValue(bean: subjectInstance, field: "folio")}</td>
                        
                            <td>${fieldValue(bean: subjectInstance, field: "name")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${subjectInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
