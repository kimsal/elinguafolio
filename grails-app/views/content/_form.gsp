<%@ page import="lf.Content" %>


<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'name', 'error')} ">
    <label for="name">
        <g:message code="content.name.label" default="Name" />

    </label>
    <g:textField name="name" value="${contentInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'type', 'error')} ">
	<label for="type">
		<g:message code="content.type.label" default="Type" />
		
	</label>
	<g:select name="type" from="${contentInstance.constraints.type.inList}" value="${contentInstance?.type}" valueMessagePrefix="content.type" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="content.status.label" default="Status" />
		
	</label>
	<g:select name="status" from="${contentInstance.constraints.status.inList}" value="${contentInstance?.status}" valueMessagePrefix="content.status" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'body', 'error')} ">
	<label for="body">
		<g:message code="content.body.label" default="Body" />
		
	</label>
    <ckeditor:editor name="body" height="400px" width="80%">${contentInstance?.body}</ckeditor:editor>
</div>


<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'startDate', 'error')} required">
	<label for="startDate">
		<g:message code="content.startDate.label" default="Start Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="startDate" precision="day"  value="${contentInstance?.startDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'endDate', 'error')} required">
    <label for="endDate">
        <g:message code="content.endDate.label" default="End Date" />
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="endDate" precision="day"  value="${contentInstance?.endDate}"  />
</div>


