
<%@ page import="lf.Evidence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'evidence.label', default: 'Evidence')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${evidenceInstance}">
            <div class="errors">
                <g:renderErrors bean="${evidenceInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="evidenceText"><g:message code="evidence.evidenceText.label" default="Evidence Text" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: evidenceInstance, field: 'evidenceText', 'errors')}">
                                    <g:textField name="evidenceText" value="${evidenceInstance?.evidenceText}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="evidenceFile"><g:message code="evidence.file.label" default="Evidence File" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: evidenceInstance, field: 'file', 'errors')}">
                                    <g:select name="file.id" from="${lf.UFile.list()}" optionKey="id" value="${evidenceInstance?.file?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="status"><g:message code="evidence.status.label" default="Status" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: evidenceInstance, field: 'status', 'errors')}">
                                    <g:select name="status" from="${evidenceInstance.constraints.status.inList}" value="${evidenceInstance?.status}" valueMessagePrefix="evidence.status" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="candoEntry"><g:message code="evidence.candoEntry.label" default="Cando Entry" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: evidenceInstance, field: 'candoEntry', 'errors')}">
                                    <g:select name="candoEntry.id" from="${lf.CandoEntry.list()}" optionKey="id" value="${evidenceInstance?.candoEntry?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
