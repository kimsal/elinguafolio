
<%@ page import="lf.Evidence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'evidence.label', default: 'Evidence')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'evidence.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="evidenceText" title="${message(code: 'evidence.evidenceText.label', default: 'Evidence Text')}" />
                        
                            <th><g:message code="evidence.file.label" default="Evidence File" /></th>
                   	    
                            <g:sortableColumn property="status" title="${message(code: 'evidence.status.label', default: 'Status')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${evidenceInstanceList}" status="i" var="evidenceInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${evidenceInstance.id}">${fieldValue(bean: evidenceInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: evidenceInstance, field: "evidenceText")}</td>
                        
                            <td>${fieldValue(bean: evidenceInstance, field: "file")}</td>
                        
                            <td>${fieldValue(bean: evidenceInstance, field: "status")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${evidenceInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
