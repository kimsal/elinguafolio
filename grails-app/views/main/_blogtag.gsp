<br/>
<div class="blogfront">
<h3>${header}</h3>

<g:each in="${entries}" var="e">
<div class="blogfrontentry">
<h3>
<g:link controller="blog" action="view" id="${e.id}">${e.title}</g:link></h3>
<div class="blogfrontdate">
<g:formatDate date="${e.publishDate}" format="yyyy-MM-dd" />
</div>
<div class="blogfrontentrybody">
<g:link controller="blog" action="view" id="${e.id}">read more...</g:link></h3>
</div>
</div>
</g:each>

<div class="allblog">
<p>
<g:link controller="blog" action="index">read all blog entries</g:link>
</p>
</div>

</div>
