<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="front"/>
<style>
.body p {
line-height: 1.5em;
margin-top: 1.7em;
}
</style>
    </head>
    <body>
	        <div class="body">
<g:each var="s" in="${sticky}">
<div class="news sticky">
<h1>${s.title}</h1>
${s.body}
</div>
</g:each>
<g:each var="s" in="${nonsticky}">
<div class="news nonsticky">
<h1>${s.title}</h1>
${s.body}
</div>
</g:each>

        </div>
    </body>
</html>
