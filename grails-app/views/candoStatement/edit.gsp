
<%@ page import="lf.CandoStatement" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'candoStatement.label', default: 'CandoStatement')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${candoStatementInstance}">
            <div class="errors">
                <g:renderErrors bean="${candoStatementInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${candoStatementInstance?.id}" />
                <g:hiddenField name="version" value="${candoStatementInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="statement"><g:message code="candoStatement.statement.label" default="Statement" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoStatementInstance, field: 'statement', 'errors')}">
                                    <g:textField name="statement" value="${candoStatementInstance?.statement}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="skill"><g:message code="candoStatement.skill.label" default="Scale" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoStatementInstance, field: 'skill', 'errors')}">
                                    <g:select name="skill.id" from="${lf.Scale.list()}" optionKey="id" value="${candoStatementInstance?.skill?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sortOrder"><g:message code="candoStatement.sortOrder.label" default="Sort Order" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoStatementInstance, field: 'sortOrder', 'errors')}">
                                    <g:textField name="sortOrder" value="${fieldValue(bean: candoStatementInstance, field: 'sortOrder')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="skill"><g:message code="candoStatement.skill.label" default="Skill" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoStatementInstance, field: 'skill', 'errors')}">
                                    <g:select name="skill.id" from="${lf.Skill.list()}" optionKey="id" value="${candoStatementInstance?.skill?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
