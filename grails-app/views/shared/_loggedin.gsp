<%@ page import="lf.Content" %>
Hello ${session.user?.fullName}!
  |
<sec:ifNotGranted roles="ROLE_ADMIN">
    <%
        def c = Content.findByNameAndStatus("files","on")
        def pages  = Content.findAllByStatusAndType("on","page").sort{it.name}
    %>
    <g:if test="${pages}">
	<g:each var="${con}" in="${pages}">
        <g:link controller="content" action="view" id="${con.id}">${con.name}</g:link>
        |
	</g:each>
    </g:if>
<g:if test="${session?.user?.userType=='teacher'}">
<g:link controller="invite" action="index">Invite</g:link>
 | 
</g:if>
<g:link controller="account" action="myPortfolios">My portfolios</g:link>
 | 
<g:link controller="account" action="friends">My friends</g:link>
 | 
<g:link controller="account" action="my">My account</g:link>
 |
</sec:ifNotGranted>

<sec:ifSwitched>
<a href='${request.contextPath}/j_spring_security_exit_user'>
   Resume as <sec:switchedUserOriginalUsername/>
</a>
</sec:ifSwitched>
<sec:ifNotSwitched>
<g:link controller="logout" >Log out</g:link> 
</sec:ifNotSwitched>

