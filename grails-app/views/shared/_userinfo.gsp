<div class="userblock">
<div class="userinfo">

<div class="userinfo_image">
    <g:link class="" controller="account" action="viewSharedFriend" id="${user.id}">
        <lf:userphoto user="${user}"/>
        </g:link>
</div>
<div class="userinfo_detail">
<p class="userinfo_name">
    <g:if test="${linkShared}">
        <g:link class="" controller="account" action="viewSharedFriend" id="${user.id}">${user.username}</g:link>
    </g:if>
    <g:else>
    ${user.username}
    </g:else>
</p>
<g:if test="${user.userType=='teacher'}">
    <p class="userinfo_type">teacher</p>
</g:if>
<g:if test="${confirm}">
    <p>
        <g:link class="btn btn-small btn-info" controller="account" action="confirmFriend" id="${user.id}">Confirm</g:link>
        <g:link class="btn btn-small btn-danger" onClick="if(!confirm('Are you sure?')) { return false; }" controller="account" action="denyFriend" id="${user.id}">Reject</g:link>
    </p>
</g:if>
<g:if test="${cancel}">
    <p>
        <g:link class="btn btn-small btn-danger" onClick="if(!confirm('Are you sure?')) { return false; }" action="cancelFriend" id="${user.id}">Cancel friend</g:link>
    </p>
</g:if>

</div>


</div>
    <g:if test="${user?.lastLogin!=null}">
        <p class="lastlogin">Last login: <g:formatDate date="${user.lastLogin}" format="MM/dd/yyyy"/></p>
    </g:if>
</div>
