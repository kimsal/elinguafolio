<%@ page import="lf.InvService" %>
<%
    def myInv = grailsApplication.classLoader.loadClass('lf.InvService').newInstance()
%>

<g:set var="viewaction" value="viewStatements"/>
<g:set var="viewmapping" value="candoview"/>
<g:if test="${viewingFriend}">
    <g:set var="viewaction" value="viewSharedStatements"/>
    <g:set var="viewmapping" value="candoviewShared"/>
</g:if>

<table class="portfolio">
<tr><td>


<h3>${portfolio.topic.checklist.name } ${portfolio.topic }</h3>

<div id="tabView_${portfolio.id }">
    <ul class="nav nav-tabs" data-tabs="tabs">
	<li class="active"><a  data-toggle="tab"  href="#cando_${portfolio.id }">Can Dos</a></li>
<g:if test="${viewingFriend==null}">
	<li><a data-toggle="tab"  href="#sharing_${portfolio.id }">Sharing</a></li>
    </g:if>
<g:if test="${portfolio.myportfolio.folio.title=='Linguafolio'}">
	<li><a data-toggle="tab"  href="#inv_${portfolio.id }">Learning Inventory</a></li>
</g:if>
</ul>

    <div class="tab-content">
        <div class="tab-pane active" id="cando_${portfolio.id }">
                        <div class="portfolioInner">
                        <g:set var="lastds" value=""/>
                        <div class="portfolioRowHeader">
                            <div class="portfolioDs">&nbsp;</div>
                            <g:each var="ss" in="${portfolio.topic.checklist.scaleSets.sort{it.sortOrder}}">
                                    <div class="portfolioScaleSetHeader">
                                    <div class="portfolioScaleSetName">${ss.name}</div>
                                    <g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
                                        <div class="portfolioScaleName">
    <g:link action="${viewaction}" params="['id':portfolio.id, 'scale':s.id]">${s.name}</g:link>
    </div>
                                    </g:each>
                                    </div>
                            </g:each>
                        </div>
                        <div class="clear"></div>


                        <g:each var="ds" in="${portfolio.topic.checklist.modes.sort{it.sortOrder}}">
                        <g:each var="d" in="${ds.skills.sort{it.sortOrder}}">
                        <g:if test="${ds!=lastds}">
                                <g:set var="lastds" value="${ds}"/>
                                <div class="portfolioRowSectionHeader">
                                    <div class="portfolioDs portfolioDsName">
                                    ${ds.name}
                                    </div>
                                    <g:each var="ss" in="${portfolio.topic.checklist.modes.sort{it.sortOrder}}">
                                        <g:each var="s" in="${ss.skills.sort{it.sortOrder}}">
                                            <div class="portfolioDsEntry">&nbsp;</div>
                                        </g:each>
                                        <div class="portfolioDsScaleSpacer">&nbsp;</div>
                                    </g:each>
                                </div>
                        <div class="clear"></div>
                        </g:if>
                            <div class="portfolioRow">
                                <div class="portfolioDs">
    <g:link action="${viewaction}" params="['id':portfolio.id, 'skill':d.id]">${d.name}
    </g:link>
                                    <attachments:each bean="${d}">
                                        <img class="portfolioStatementImage" src="/${attachment?.imagePath}"/>
                                    </attachments:each>
    </div>
                                <g:set var="lastds" value="${ds}"/>
                                <g:each var="ss" in="${portfolio.topic.checklist.scaleSets.sort{it.sortOrder}}">
                                <g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
                                <div class="portfolioDsEntry">
                                <%
											  def m1 = lf.CandoEntry.createCriteria()
											  def myCandos = m1.list {
												  eq('user', user)
												  eq('portfolio', portfolio)
												  statement {
													  eq('scale', s)
													  eq('skill', d)
												  }
											  }
//                                def cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
//                                def myCandos = lf.CandoEntry.findAllByUserAndPortfolio(user, portfolio).findAll{cs*.toString().contains(it.statement.toString())}
										  %>
                                <g:link mapping="${viewmapping}" controller="account" action="${viewaction}" params='[portfolio:"${portfolio.id}", skill:"${d.id}",  scale:"${s.id}"]'>
<g:if test="${csCounts.find{it[0]==d.id && it[1]==s.id}!=null}">
<lf:renderCandoProgress myCandos="${myCandos}" 
candopoolsize="${csCounts.find{it[0]==d.id && it[1]==s.id}[2]}"/>
</g:if>
                                </g:link>

                                </div>
                                </g:each>
                                <div class="portfolioDsScaleSpacer">&nbsp;</div>
                                </g:each>
                                </div><!-- close the row -->
                        <div class="clear"></div>

                        </g:each>
                        </g:each>
                        </div>


    </div>
<g:if test="${viewingFriend==null}">
        <div id="sharing_${portfolio.id }" class="tab-pane ">
                        <div class="sharingTab">
                            <g:if test="${friends}">
                                <g:form method='post' action="shareWith">
                                <p>Share this subject with
                            <g:select name="friend" from="${friends}" optionKey="id"/>
                            <g:hiddenField name="portfolio" value="${portfolio.id}"/>
                            <g:submitButton class="btn" name="share" value="Share!"/>
                            </g:form>
                                </p>
                                </g:if>

    <%
    def sharedWithStudent= lf.Sharing.findAllByPortfolioAndOwner(portfolio, user).findAll{it.sharedWith.userType=='student'}
    def sharedWithTeacher = lf.Sharing.findAllByPortfolioAndOwner(portfolio, user).findAll{it.sharedWith.userType=='teacher'}
    %>

    <g:if test="${sharedWithStudent.size()>0}">
                            <p>
                                    <h3>Students who can see this portfolio</h3>
                            </p>


                            <p>
                            <g:each var="f" in="${sharedWithStudent}">
                            <div class="sharedFriend">
                            <lf:userinfo user="${f.sharedWith}"/>
                            <p>
                            <g:link action="stopSharing" id="${f.sharedWith.id}" params="[pid:portfolio.id]">stop sharing</g:link>
                            </div>
                            </g:each>
                            </p>
    </g:if>
                            <div class="clear"></div>
                            <p>&nbsp;</p>

                            <g:if test="${teachers}">
                                <p>Share this subject with my teacher
                                <g:form method='post' action="shareWith">
                            <g:select name="friend" from="${teachers}" optionKey="id"/>
                            <g:hiddenField name="portfolio" value="${portfolio.id}"/>
                            <g:submitButton class="btn" class="btn" name="share" value="Share!"/>
                            </g:form>
                                </p>
                                </g:if>
    <g:if test="${sharedWithTeacher.size()>0}">
                            <p>
                                    <h3>Teachers who can see this portfolio</h3>
                            </p>
                            <p>
                            <g:each var="f" in="${sharedWithTeacher}">
                            <div class="sharedFriend">
                            <lf:userinfo user="${f.sharedWith}"/>
                            <p>
                            <g:link action="stopSharing" id="${f.sharedWith.id}" params="[pid:portfolio.id]">stop sharing</g:link>
                            </div>
                            </g:each>
                            </p>
    </g:if>

                        </div>

    </div>

    </g:if>

<g:if test="${portfolio.myportfolio.folio.title=='Linguafolio'}">
        <div id="inv_${portfolio.id }" class="tab-pane ">
					<div class="portfolioInner">
			<g:set var="myli" 
			value="${myInv.getLearningInventoryForPortfolioAndUser(portfolio, user ) }" />
					<g:formRemote name="foo" url="[action:'updateLearningInventory', controller:'account']" 
				onSuccess="lisuccess()"	
				onFailure="lifailure()"
				onLoading="liloading()"
				onLoaded="liloaded()"
					method="post">
					<g:each var='sec' in ="${myli.inventory?.sections?.sort{it.sortOrder}}">
					<h2>${sec}</h2>
					<lf:renderLearningInventoryHeader type="${sec.sectionType}"/>
					<g:each var="st" in="${sec.statements.sort{it.sortOrder}}">
						<lf:renderLearningInventoryStatement portfolio="${portfolio}" statement="${st}" user="${user}" type="${sec.sectionType}"/>
					</g:each>
					</g:each>
					<input type="hidden" name="myli" value="${myli.id }" />
                        <g:if test="${viewingFriend==null}">
					<input type="submit" value="Update!" class="btn"/>
                        </g:if>
					</g:formRemote>
					</div>
                        </g:if>
        </div>
    </div><!--tab-content-->
</div>

</td></tr></table>
<div class="note"></div>
<script>
function lifailure()
{
}
function lisuccess()
{
}
function liloaded()
{
	alert('Updated');
}
function liloading()
{
}
</script>

