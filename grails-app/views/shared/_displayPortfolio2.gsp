<table class="portfolio">
<tr><td>


<h2>${portfolio.topic.checklist.name } ${portfolio.topic }</h2>

<richui:tabView id="tabView_${portfolio.id }">
    <richui:tabLabels>
        <richui:tabLabel selected="true" title="Can Dos" />
        <richui:tabLabel title="Sharing" />
        <richui:tabLabel title="Activities" />
        <richui:tabLabel title="Learning Inventory" />
        <richui:tabLabel title="Experiences" />
        <richui:tabLabel title="Encounters" />
    </richui:tabLabels>

    <richui:tabContents>
        <richui:tabContent>
        
					<div class="portfolioInner">
					<g:set var="lastds" value=""/>
					<div class="portfolioRowHeader">
						<div class="portfolioDs">&nbsp;</div>
						<g:each var="ss" in="${portfolio.topic.checklist.scaleSets.sort{it.sortOrder}}">
								<div class="portfolioScaleSetHeader">
								<div class="portfolioScaleSetName">${ss.name}</div>
								<g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
									<div class="portfolioScaleName">${s.name}</div>
								</g:each>
								</div>
						</g:each>
					</div>
					<div class="clear"></div>


					<g:each var="ds" in="${portfolio.topic.checklist.modes.sort{it.sortOrder}}">
					<g:each var="d" in="${ds.skills.sort{it.sortOrder}}">
					<g:if test="${ds!=lastds}">
							<g:set var="lastds" value="${ds}"/>
							<div class="portfolioRowSectionHeader">
								<div class="portfolioDs portfolioDsName">
								${ds.name}
								</div>
								<g:each var="ss" in="${portfolio.topic.checklist.modes.sort{it.sortOrder}}">
									<g:each var="s" in="${ss.skills.sort{it.sortOrder}}">
										<div class="portfolioDsEntry">&nbsp;</div>
									</g:each>
									<div class="portfolioDsScaleSpacer">&nbsp;</div>
								</g:each>
							</div>
					<div class="clear"></div>
					</g:if>
						<div class="portfolioRow">
							<div class="portfolioDs">${d.name}</div>
							<g:set var="lastds" value="${ds}"/>
							<g:each var="ss" in="${portfolio.topic.checklist.scaleSets.sort{it.sortOrder}}">
							<g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
							<div class="portfolioDsEntry">
							<% 
							def cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
							def myCandos = lf.CandoEntry.findAllByUserAndPortfolio(session.user, portfolio).findAll{cs*.toString().contains(it.statement.toString())}
							%>
							<p><g:link mapping="candoview" controller="account" action="viewStatements" params='[portfolio:"${portfolio.id}", skill:"${d.id}",  scale:"${s.id}"]'>
							<lf:renderCandoProgress myCandos="${myCandos}" candoPool="${lf.CandoStatement.findAllByScaleAndSkill(s,d)}"/>
							</g:link>
							</p>
							</div>
							</g:each>
							<div class="portfolioDsScaleSpacer">&nbsp;</div>
							</g:each>
							</div><!-- close the row -->
					<div class="clear"></div>

					</g:each>
					</g:each>
					</div>

				</richui:tabContent>

				<richui:tabContent>
					<div class="sharingTab">
						<g:if test="${friends}">
							<g:form method='post' action="shareWith">
							<p>Share this subject with my friend 
						<g:select name="friend" from="${friends}" optionKey="id"/>
						<g:hiddenField name="portfolio" value="${portfolio.id}"/>
						<g:submitButton class="btn" name="share" value="Share!"/>
						</g:form>
							</p>
							</g:if>
						<p>
								<h3>My Friends who can see this portfolio</h3>
						</p>

						<p>
						<g:each var="f" in="${lf.Sharing.findAllByPortfolioAndOwner(portfolio, user).findAll{it.sharedWith.userType=='student'}}">
						<div class="sharedFriend">
						<lf:userinfo user="${f.sharedWith}"/>
						<p>
						<g:link action="stopSharing" id="${f.sharedWith.id}" params="[pid:portfolio.id]">stop sharing</g:link>
						</div>
						</g:each>
						</p>

						<div class="clear"></div>
						<p>&nbsp;</p>
						<g:if test="${teachers}">
							<g:form method='post' action="shareWith">
							<p>Share this subject with my teacher 
						<g:select name="teacher" from="${teachers}" optionKey="id"/>
						<g:submitButton class="btn" name="share" value="Share!"/>
						</g:form>
							</p>
							</g:if>
						<p>
								<h3>My Teachers who can see this portfolio</h3>
						</p>
						<p>
						<g:each var="f" in="${lf.Sharing.findAllByPortfolioAndOwner(portfolio, user).findAll{it.sharedWith.userType=='teacher'}}">
						<div class="sharedFriend">
						<lf:userinfo user="${f.sharedWith}"/>
						<p>
						<g:link action="stopSharing" id="${f.sharedWith.id}" params="[pid:portfolio.id]">stop sharing</g:link>
						</div>
						</g:each>
						</p>

					</div>
			</richui:tabContent>

			<richui:tabContent>
				Activities
			</richui:tabContent>

			<richui:tabContent>
				<h2>My Learning Inventory for - ${mybio.bio.folio}</h2>
				<g:form action="updateLearningInventory">
				</g:form>
			</richui:tabContent>

			<richui:tabContent>
				exp
			</richui:tabContent>

			<richui:tabContent>
	enc
			</richui:tabContent>
			<richui:tabContent>
	enc2
			</richui:tabContent>

    </richui:tabContents>
</richui:tabView>


</td></tr></table>
