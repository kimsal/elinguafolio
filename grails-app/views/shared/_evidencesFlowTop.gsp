<style>
div.evidences {
	width: 80%;
  border: .1em solid #999;
  background-color: #fff;
  }
div.spacer {
  clear: both;
  }
div.evidence {
	border: thin solid #eee;
  float: left;
	width:200px;
	height: 180px;
  }
  
div.evidence p {
   text-align: center;
   }
</style>
${evidences}
<div class="evidences">
<richui:flow reflection="true" slider="true" onClickScroll="false" caption="true">
<g:each var="ev" in="${evidences}">
<g:if test="${ev.evidenceFile!=null}">
<g:render template="/shared/evidenceFlowFile" model="${[e:ev]}"/>
</g:if>
<g:else>
<g:render template="/shared/evidenceFlowText" model="['e':ev]"/>
</g:else>

</g:each>

</richui:flow>
</div>
