<style>
div.evidences {
	width: 80%;
  border: .1em solid #999;
  background-color: #fff;
  }
div.spacer {
  clear: both;
  }
div.evidence {
	border: thin solid #eee;
  float: left;
	width:200px;
	height: 250px;
  }
  
div.evidence p {
   text-align: center;
   }
</style>
${evidences}
<div class="evidences">
<richui:carousel direction="horizontal"  carouselStyle="height: 250px; width: 600px;" itemsStyle="width: 600px; height: 250px;">

<g:each var="ev" in="${evidences}">
<richui:carouselItem>
<g:if test="${ev.evidenceFile!=null}">
<g:render template="/shared/evidenceFlowFile" model="${[e:ev]}"/>
</g:if>
<g:else>
<g:render template="/shared/evidenceFlowText" model="['e':ev]"/>
</g:else>

</richui:carouselItem>
</g:each>

</richui:carousel>
</div>
