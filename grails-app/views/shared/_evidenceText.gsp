<%@ page import="lf.*" %>
<%@ page import="lf.EvidenceService" %>
<div class="evidence evidenceFile">
<table>

<tr>
<td>
		<p><g:link class="evidenceFileLink" controller="download" id="${e.file.id}">${e.file.name}</g:link>
		</p>
		<p><g:link onClick="if(!confirm('Are you sure?')) { return false; }" class="evidenceFileLink" controller="account" action="deleteEvidence" id="${e.id}">remove</g:link></p>
		<p><g:link class="evidenceFileLink" controller="download" id="${e.file.id}">download original</g:link></p>
		<p>Created: ${e.dateCreated.prettyDate()}</p>
		<p>Status: ${e.status}</p>
</td>
</tr>
</table>

</div>
