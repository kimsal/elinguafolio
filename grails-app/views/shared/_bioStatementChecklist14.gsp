<g:set var="dis" value=""/>
<g:if test="${viewingFriend}">
	<g:set var="dis" value=" disabled='disabled'"/>
</g:if>
<g:set var="checked1" value=""/>
<g:set var="checked2" value=""/>
<g:set var="checked3" value=""/>
<g:set var="checked4" value=""/>
<g:if test="${a?.value=='1'}"><g:set var="checked1" value="checked='checked'"/></g:if>
<g:if test="${a?.value=='2'}"><g:set var="checked2" value="checked='checked'"/></g:if>
<g:if test="${a?.value=='3'}"><g:set var="checked3" value="checked='checked'"/></g:if>
<g:if test="${a?.value=='4'}"><g:set var="checked4" value="checked='checked'"/></g:if>
<div class="bioStatementAndAnswer">
<div class="bioStatement">
${s}
</div>
<div class="bioAnswer">
	<input type="radio" ${dis} name="bioAnswer.${s?.id}" value="4" ${checked4}/>
</div>
<div class="bioAnswer">
	<input type="radio" ${dis} name="bioAnswer.${s?.id}" value="3" ${checked3}/>
</div>
<div class="bioAnswer">
	<input type="radio" ${dis} name="bioAnswer.${s?.id}" value="2" ${checked2}/>
</div>
<div class="bioAnswer">
	<input type="radio" ${dis} name="bioAnswer.${s?.id}" value="1" ${checked1}/>
</div>
</div>
<div class="clear"></div>
