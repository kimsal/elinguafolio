<%@ page import="lf.InvService" %>
<%
    def myInv = grailsApplication.classLoader.loadClass('lf.InvService').newInstance()
	 def viewaction
	if(viewingFriend)
	{
		viewaction="viewSharedStatements"
	}
%>

<table class="portfolio">
<tr><td>

    <h3>${portfolio.topic.checklist.name } ${portfolio.topic }</h3>

    <div id="tabView_${portfolio.id }">
        <ul class="nav nav-tabs" data-tabs="tabs">
            <li class="active"><a  data-toggle="tab"  href="#cando_${portfolio.id }">Can Dos</a></li>
            <li><a data-toggle="tab"  href="#inv_${portfolio.id }">Learning Inventory</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="cando_${portfolio.id }">
                <div class="portfolioInner">
                    <g:set var="lastds" value=""/>
                    <div class="portfolioRowHeader">
                        <div class="portfolioDs">&nbsp;</div>
                        <g:each var="ss" in="${portfolio.topic.checklist.scaleSets.sort{it.sortOrder}}">
                            <div class="portfolioScaleSetHeader">
                                <div class="portfolioScaleSetName">${ss.name}</div>
                                <g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
                                    <div class="portfolioScaleName">
                                        <g:link controller="account" action="viewSharedStatements" params="['id':portfolio.id, 'scale':s.id]">${s.name}</g:link>
                                    </div>
                                </g:each>
                            </div>
                        </g:each>
                    </div>
                    <div class="clear"></div>


                    <g:each var="ds" in="${portfolio.topic.checklist.modes.sort{it.sortOrder}}">
                        <g:each var="d" in="${ds.skills.sort{it.sortOrder}}">
                            <g:if test="${ds!=lastds}">
                                <g:set var="lastds" value="${ds}"/>
                                <div class="portfolioRowSectionHeader">
                                    <div class="portfolioDs portfolioDsName">
                                        ${ds.name}
                                    </div>
                                    <g:each var="ss" in="${portfolio.topic.checklist.modes.sort{it.sortOrder}}">
                                        <g:each var="s" in="${ss.skills.sort{it.sortOrder}}">
                                            <div class="portfolioDsEntry">&nbsp;</div>
                                        </g:each>
                                        <div class="portfolioDsScaleSpacer">&nbsp;</div>
                                    </g:each>
                                </div>
                                <div class="clear"></div>
                            </g:if>
                            <div class="portfolioRow">
                                <div class="portfolioDs">
                                    <g:link action="${candoviewShared}" params="['id':portfolio.id, 'skill':d.id]">${d.name}
                                    </g:link>
                                    <attachments:each bean="${d}">
                                        <img class="portfolioStatementImage" src="/${attachment?.imagePath}"/>
                                    </attachments:each>
                                </div>
                                <g:set var="lastds" value="${ds}"/>
                                <g:each var="ss" in="${portfolio.topic.checklist.scaleSets.sort{it.sortOrder}}">
                                    <g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
                                        <div class="portfolioDsEntry">
                                            <%
                                                def cs = lf.CandoStatement.findAllByScaleAndSkill(s,d)
                                                def myCandos = lf.CandoEntry.findAllByUserAndPortfolio(user, portfolio).findAll{cs*.toString().contains(it.statement.toString())}
                                            %>
                                            <g:link mapping="${viewmapping}" controller="account" action="${viewaction}" params='[portfolio:"${portfolio.id}", skill:"${d.id}",  scale:"${s.id}"]'>
                                                <lf:renderCandoProgress myCandos="${myCandos}" candoPool="${cs}" candopoolsize="${cs.size()}"/>
                                            </g:link>

                                        </div>
                                    </g:each>
                                    <div class="portfolioDsScaleSpacer">&nbsp;</div>
                                </g:each>
                            </div><!-- close the row -->
                            <div class="clear"></div>

                        </g:each>
                    </g:each>
                </div>


            </div>
            <div id="inv_${portfolio.id }" class="tab-pane ">
                <div class="portfolioInner">
                    <g:set var="myli"
                           value="${myInv.getLearningInventoryForPortfolioAndUser(portfolio, user ) }" />
                    <g:formRemote name="foo" url="[action:'none', controller:'account']"
                                  onSuccess="lisuccess()"
                                  onFailure="lifailure()"
                                  onLoading="liloading()"
                                  onLoaded="liloaded()"
                                  method="post">
                        <g:each var='sec' in ="${myli.inventory?.sections?.sort{it.sortOrder}}">
                            <h2>${sec}</h2>
                            <lf:renderLearningInventoryHeader type="${sec.sectionType}"/>
                            <g:each var="st" in="${sec.statements.sort{it.sortOrder}}">
                                <lf:renderLearningInventoryStatement portfolio="${portfolio}" statement="${st}" user="${user}" type="${sec.sectionType}"/>
                            </g:each>
                        </g:each>
                        <input type="hidden" name="myli" value="${myli.id }" />

                    </g:formRemote>
                </div>
            </div>
        </div>
    </div>

</td></tr></table>
<div id="note"></div>

