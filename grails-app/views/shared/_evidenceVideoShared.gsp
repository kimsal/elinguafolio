<%@ page import="lf.*" %>
<%@ page import="lf.EvidenceService" %>
<div class="evidence evidenceFile">
<table>
<tr>
<td>
		<p>
<video id="test_${e.file.id}" class="projekktor" poster="<g:resource dir="/pics/t" file="${e.file.pathToThumbnail}"/>" title="this is Projekktor" width="180" height="165" controls>

	<source src="${createLinkTo(dir:'', file:e.file.pathToProcessedFile)}" type="video/mp4" />  
	<source src="${createLinkTo(dir:'', file:e.file.pathToProcessedFile?.replace("mp4","ogv"))}" type="video/ogg" />  
    </video>

		</p>
</td>
</tr>
<tr>
<td>
		<p><g:link class="evidenceFileLink" controller="download" id="${e.file.id}">${e.file.name}</g:link>
		</p>
		<p><g:link class="evidenceFileLink" controller="download" id="${e.file.id}">download original</g:link></p>
		<p>Created: ${e.dateCreated.prettyDate()}</p>
</td>
</tr>
</table>

</div>
