<g:set var="disabled1" value=""/>
<g:if test="${disabled}">
	<g:set var="disabled1" value=" disabled='disabled' "/>
</g:if>
<g:set var="checked1" value=""/>
<g:set var="checked2" value=""/>
<g:set var="checked3" value=""/>
<g:set var="checked4" value=""/>
<g:if test="${a?.value=='1'}"><g:set var="checked1" value="checked='checked'"/></g:if>
<g:if test="${a?.value=='2'}"><g:set var="checked2" value="checked='checked'"/></g:if>
<g:if test="${a?.value=='3'}"><g:set var="checked3" value="checked='checked'"/></g:if>
<g:if test="${a?.value=='4'}"><g:set var="checked4" value="checked='checked'"/></g:if>
<div class="statementAndAnswer">
<div class="statement">
${s}
</div>
<div class="answer">
	<input type="radio" name="answer.${s?.id}" value="4" ${checked4} ${disabled1}/>
</div>
<div class="answer">
	<input type="radio" name="answer.${s?.id}" value="3" ${checked3} ${disabled1}/>
</div>
<div class="answer">
	<input type="radio" name="answer.${s?.id}" value="2" ${checked2} ${disabled1}/>
</div>
<div class="answer">
	<input type="radio" name="answer.${s?.id}" value="1" ${checked1} ${disabled1}/>
</div>
</div>
<div class="clear"></div>
