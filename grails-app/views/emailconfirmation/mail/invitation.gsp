You've been invited to eLinguafolio by ${inviter}.

Your account name (to log in with) is ${username}.

Your initial password is "${password}" (without the quotes).

Please click the link below to log in:

${uri}




