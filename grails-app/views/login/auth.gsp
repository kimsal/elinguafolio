<head>
<title><g:message code='spring.security.ui.login.title'/></title>
<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
</head>

<body>

<p/>
<p/>
<p/>

<div class="span12" style="text-align:center; margin: 0 auto;">
	<form action='${postUrl}' method='POST' id="loginForm" name="loginForm" autocomplete='off'>
<fieldset>

<h2 class="centered">Sign in</h2>



	<table class="logintable">
		<tr>
			<td><label for="username"><g:message code='spring.security.ui.login.username'/></label></td>
			<td><input name="j_username" id="username" size="20" /></td>
		</tr>
		<tr>
			<td><label for="password"><g:message code='spring.security.ui.login.password'/></label></td>
			<td><input type="password" name="j_password" id="password" size="20" /></td>
		</tr>
		<tr>
			<td colspan='2'>
				<input type="checkbox" class="checkbox" name="${rememberMeParameter}" id="remember_me" checked="checked" />
				<label for='remember_me'><g:message code='spring.security.ui.login.rememberme'/></label>
<br/>
				<span class="forgot-link">
					<g:link controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>
				</span>
 | 
				<span class="forgot-link">
					<g:link controller='register' action='forgotUsername'>Forgot username?</g:link>
				</span>
			</td>
		</tr>
		<tr>
			<td colspan='2'>

<p>
				<g:submitButton class="btn btn-primary" name="loginButton" elementId='loginButton' form='loginForm' value="Log in" messageCode='spring.security.ui.login.login'/>
</p>
<p>
				<g:link class="btn btn-primary whitebtn centered" style="width:92%;" elementId='registerStudent' controller='register' action="student" messageCode='Register as a student'>Register as a student</g:link>
</p>
<p>
				<g:link class="btn btn-primary whitebtn centered"  style="width:92%;" elementId='registerTeacher' controller='register' action="teacher" messageCode='Register as a teacher'>Register as a teacher</g:link>
</p>
			</td>
		</tr>
	</table>
</fieldset>
	</form>

</div>

<script>
$(document).ready(function() {
	$('#username').focus();
    $("#loginForm").validate();
});


</script>

</body>
