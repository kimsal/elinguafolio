
<%@ page import="lf.Skill" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'skill.label', default: 'Skill')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${skillInstance}">
            <div class="errors">
                <g:renderErrors bean="${skillInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${skillInstance?.id}" />
                <g:hiddenField name="version" value="${skillInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="candoStatements"><g:message code="skill.candoStatements.label" default="Cando Statements" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: skillInstance, field: 'candoStatements', 'errors')}">
                                    
<ul>
<g:each in="${skillInstance?.candoStatements?}" var="c">
    <li><g:link controller="candoStatement" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="candoStatement" action="create" params="['skill.id': skillInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'candoStatement.label', default: 'CandoStatement')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sortOrder"><g:message code="skill.sortOrder.label" default="Sort Order" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: skillInstance, field: 'sortOrder', 'errors')}">
                                    <g:textField name="sortOrder" value="${fieldValue(bean: skillInstance, field: 'sortOrder')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="mode"><g:message code="skill.mode.label" default="Skill Set" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: skillInstance, field: 'mode', 'errors')}">
                                    <g:select name="mode.id" from="${lf.Mode.list()}" optionKey="id" value="${skillInstance?.mode?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="skill.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: skillInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${skillInstance?.name}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
