
<%@ page import="lf.Scale" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'scale.label', default: 'Scale')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'scale.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="sortOrder" title="${message(code: 'scale.sortOrder.label', default: 'Sort Order')}" />
                        
                            <g:sortableColumn property="name" title="${message(code: 'scale.name.label', default: 'Name')}" />
                        
                            <th><g:message code="scale.scaleSet.label" default="Scale Set" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${scaleInstanceList}" status="i" var="scaleInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${scaleInstance.id}">${fieldValue(bean: scaleInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: scaleInstance, field: "sortOrder")}</td>
                        
                            <td>${fieldValue(bean: scaleInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: scaleInstance, field: "scaleSet")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${scaleInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
