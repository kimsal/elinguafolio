
<%@ page import="org.grails.plugins.settings.Setting" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'setting.label', default: 'Setting')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'setting.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="code" title="${message(code: 'setting.code.label', default: 'Code')}" />
                        
                            <g:sortableColumn property="type" title="${message(code: 'setting.type.label', default: 'Type')}" />
                        
                            <g:sortableColumn property="value" title="${message(code: 'setting.value.label', default: 'Value')}" />
                        
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${settingInstanceList}" status="i" var="settingInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${settingInstance.id}">${fieldValue(bean: settingInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: settingInstance, field: "code")}</td>
                        
                            <td>${fieldValue(bean: settingInstance, field: "type")}</td>
                        
                            <td>${fieldValue(bean: settingInstance, field: "value")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${settingInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
