
<%@ page import="org.grails.plugins.settings.Setting" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'setting.label', default: 'Setting')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${settingInstance}">
            <div class="errors">
                <g:renderErrors bean="${settingInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="code"><g:message code="setting.code.label" default="Code" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: settingInstance, field: 'code', 'errors')}">
                                    <g:textField name="code" maxlength="100" value="${settingInstance?.code}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="type"><g:message code="setting.type.label" default="Type" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: settingInstance, field: 'type', 'errors')}">
                                    <g:select name="type" from="${settingInstance.constraints.type.inList}" value="${settingInstance?.type}" valueMessagePrefix="setting.type"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="value"><g:message code="setting.value.label" default="Value" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: settingInstance, field: 'value', 'errors')}">
                                    <g:textField name="value" maxlength="100" value="${settingInstance?.value}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
