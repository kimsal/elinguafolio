
<%@ page import="lf.News" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'news.label', default: 'News')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}">Admin</a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'news.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="endDate" title="${message(code: 'news.endDate.label', default: 'End Date')}" />
                        
                            <g:sortableColumn property="lastUpdated" title="${message(code: 'news.lastUpdated.label', default: 'Last Updated')}" />
                        
                            <g:sortableColumn property="dateCreated" title="${message(code: 'news.dateCreated.label', default: 'Date Created')}" />
                        
                            <g:sortableColumn property="body" title="${message(code: 'news.body.label', default: 'Body')}" />
                        
                            <g:sortableColumn property="startDate" title="${message(code: 'news.startDate.label', default: 'Start Date')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${newsInstanceList}" status="i" var="newsInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${newsInstance.id}">${fieldValue(bean: newsInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${newsInstance.endDate}" /></td>
                        
                            <td><g:formatDate date="${newsInstance.lastUpdated}" /></td>
                        
                            <td><g:formatDate date="${newsInstance.dateCreated}" /></td>
                        
                            <td>${fieldValue(bean: newsInstance, field: "body")}</td>
                        
                            <td><g:formatDate date="${newsInstance.startDate}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${newsInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
