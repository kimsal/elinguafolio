
<%@ page import="lf.News" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'news.label', default: 'News')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}">Admin</a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: newsInstance, field: "id")}</td>
                            
                        </tr>
                    
                   
                   
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.lastUpdated.label" default="Last Updated" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${newsInstance?.lastUpdated}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.dateCreated.label" default="Date Created" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${newsInstance?.dateCreated}" /></td>
                            
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.title.label" default="Title" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: newsInstance, field: "title")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.body.label" default="Body" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: newsInstance, field: "body")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.startDate.label" default="Start Date" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${newsInstance?.startDate}" /></td>
                            
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.endDate.label" default="End Date" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${newsInstance?.endDate}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="news.sticky.label" default="Sticky" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${newsInstance?.sticky}" /></td>
                            
                        </tr>
                    
                   
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${newsInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
