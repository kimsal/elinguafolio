<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="title" value="eLinguafolio blog"/>
	<meta name="layout" content="front"/>
	</head>
	<body>
<h2>eLinguafolio blog</h2>
<g:each in="${entries}" var="e">
	<h2>${e.title}</h2>
	<div class="blogdate">
		<g:formatDate date="${e.publishDate}" format="yyyy-MM-dd" />
	</div>
	<div class="blogentrybody">
		${e.message.encodeAsHTML().replace("\n","<br/>")}
	</div>
</g:each>
</body>
</html>
