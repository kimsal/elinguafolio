<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="title" value="eLinguafolio blog: ${e.title.encodeAsHTML()}"/>
	<meta name="layout" content="front"/>
	</head>
	<body>
<h2>${e.title.encodeAsHTML()}</h2>
<div class="blogdate">
<g:formatDate date="${e.publishDate}" format="yyyy-MM-dd" />
</div>
<div class="blogentrybody">
${e.message.encodeAsHTML().replace("\n","<br/>")}
</div>

</body>
</html>
