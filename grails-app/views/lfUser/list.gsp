
<%@ page import="lf.LfUser" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lfUser.label', default: 'LfUser')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'lfUser.id.label', default: 'Id')}" />
                                                
                            <g:sortableColumn property="name" title="${message(code: 'lfUser.name.label', default: 'Name')}" />
                        
                            <g:sortableColumn property="username" title="${message(code: 'lfUser.username.label', default: 'Username')}" />
                        
                            <g:sortableColumn property="password" title="${message(code: 'lfUser.password.label', default: 'Password')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${lfUserInstanceList}" status="i" var="lfUserInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${lfUserInstance.id}">${fieldValue(bean: lfUserInstance, field: "id")}</g:link></td>
                                                
                            <td>${fieldValue(bean: lfUserInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: lfUserInstance, field: "username")}</td>
                        
                            <td>${fieldValue(bean: lfUserInstance, field: "password")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${lfUserInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
