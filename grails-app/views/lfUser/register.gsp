<%@ page import="lf.LfUser" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <g:set var="entityName" value="${message(code: 'lfUser.label', default: 'LfUser')}" />
    </head>
    <body>
        <div class="body">
        <h1>Register an account</h1>
<p><g:link action='registerStudent'>I am a student</g:link></p>
<p><g:link action='registerTeacher'>I am a teacher</g:link></p>
        </div>
    </body>
</html>
