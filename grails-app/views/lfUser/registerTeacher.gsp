<%@ page import="lf.Teacher" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <g:set var="entityName" value="${message(code: 'teacher.label', default: 'Teacher')}" />
        <title>Create a teacher account</title>
    </head>
    <body>
        <div class="body">
            <h1>Create a teacher account</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${teacherInstance}">
            <div class="errors">
                <g:renderErrors bean="${teacherInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="registerTeacherProcess" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="teacher.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${teacherInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="username"><g:message code="teacher.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'username', 'errors')}">
                                    <g:textField name="username" value="${teacherInstance?.username}" />
                                </td>
                            </tr>
                        
                       
                       
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="teacher.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'password', 'errors')}">
                                    <g:passwordField name="password" value="${teacherInstance?.password}" />
                                </td>
                            </tr>

                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="teacher.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${teacherInstance?.email}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Register')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
