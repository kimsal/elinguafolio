<%@ page import="lf.LfUser" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <title>Log in</title>
    </head>
    <body>

        <div class="body">
        <h1>Log in</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
<g:form method="post" action="loginProcess">
<p>Username: <input type='text' name='username'/></p>
<p>Password: <input type='password' name='password'/></p>
<input type='submit' value="Log in"/>
</g:form>
        </div>
    </body>
</html>
