<%@ page import="lf.Student" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}" />
        <title>Create a student account</title>
    </head>
    <body>
        <div class="body">
            <h1>Create a student account</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${studentInstance}">
            <div class="errors">
                <g:renderErrors bean="${studentInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="registerStudentProcess" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="student.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${studentInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="username"><g:message code="student.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'username', 'errors')}">
                                    <g:textField name="username" value="${studentInstance?.username}" />
                                </td>
                            </tr>
                        
                       
                       
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="student.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'password', 'errors')}">
                                    <g:passwordField name="password" value="${studentInstance?.password}" />
                                </td>
                            </tr>

                             <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="student.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studentInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${studentInstance?.email}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Register')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
