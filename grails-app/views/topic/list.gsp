
<%@ page import="lf.Topic" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'topic.label', default: 'Topic')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'topic.id.label', default: 'Id')}" />
                        
                            <th><g:message code="topic.checklist.label" default="Checklist" /></th>
                   	    
                            <g:sortableColumn property="name" title="${message(code: 'topic.name.label', default: 'Name')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${topicInstanceList}" status="i" var="topicInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>
                                <g:link action="show" id="${topicInstance.id}">show</g:link>
                                <g:link action="edit" id="${topicInstance.id}">edit</g:link>
                            </td>
                        
                            <td>${fieldValue(bean: topicInstance, field: "checklist")}</td>
                        
                            <td>${fieldValue(bean: topicInstance, field: "name")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${topicInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
