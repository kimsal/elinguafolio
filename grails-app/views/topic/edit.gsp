
<%@ page import="lf.Topic" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'topic.label', default: 'Topic')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${topicInstance}">
            <div class="errors">
                <g:renderErrors bean="${topicInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${topicInstance?.id}" />
                <g:hiddenField name="version" value="${topicInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="checklist"><g:message code="topic.checklist.label" default="Checklist" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: topicInstance, field: 'checklist', 'errors')}">
                                    <g:select name="checklist.id" from="${lf.Checklist.list()}" optionKey="id" value="${topicInstance?.checklist?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="topic.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: topicInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${topicInstance?.name}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
