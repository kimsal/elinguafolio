<%@ page import="lf.District" %>



<div class="fieldcontain ${hasErrors(bean: districtInstance, field: 'code', 'error')} ">
	<label for="code">
		<g:message code="district.code.label" default="Code" />
		
	</label>
	<g:textField name="code" value="${districtInstance?.code}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: districtInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="district.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${districtInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: districtInstance, field: 'state', 'error')} required">
	<label for="state">
		<g:message code="district.state.label" default="State" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="state" name="state.id" from="${lf.State.list()}" optionKey="id" required="" value="${districtInstance?.state?.id}" class="many-to-one"/>
</div>

