
<%@ page import="lf.District" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'district.label', default: 'District')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
		<div id="show-district" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list district">

			
				<g:if test="${districtInstance?.schools}">
				<li class="fieldcontain">
					<span id="schools-label" class="property-label"><g:message code="district.schools.label" default="Schools" /></span>
					
						<g:each in="${districtInstance.schools}" var="s">
						<span class="property-value" aria-labelledby="schools-label"><g:link controller="school" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link><br/></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${districtInstance?.code}">
				<li class="fieldcontain">
					<span id="code-label" class="property-label"><g:message code="district.code.label" default="Code" /></span>
					
						<span class="property-value" aria-labelledby="code-label"><g:fieldValue bean="${districtInstance}" field="code"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${districtInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="district.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${districtInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${districtInstance?.state}">
				<li class="fieldcontain">
					<span id="state-label" class="property-label"><g:message code="district.state.label" default="State" /></span>
					
						<span class="property-value" aria-labelledby="state-label"><g:link controller="state" action="show" id="${districtInstance?.state?.id}">${districtInstance?.state?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${districtInstance?.id}" />
					<g:link class="edit" action="edit" id="${districtInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
