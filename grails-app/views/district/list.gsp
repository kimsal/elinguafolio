
<%@ page import="lf.District" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'district.label', default: 'District')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
		<div id="list-district" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

<g:form method='post' action='search'>
  <g:textField name="search"/>
    <g:submitButton name="submit" value="Search" class="btn btn-primary"/>
</g:form>
<g:if test="${search}">Searching for '${search}' <g:link action="clear">clear search</g:link> </g:if>

			<table class="table">
				<thead>
					<tr>
					     <th></th>
						<g:sortableColumn property="code" title="${message(code: 'district.code.label', default: 'Code')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'district.name.label', default: 'Name')}" />
					
						<th><g:message code="district.state.label" default="State" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${districtInstanceList}" status="i" var="districtInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link  class="btn btn-info" action="show" id="${districtInstance.id}">show</g:link></td>
					
						<td>${fieldValue(bean: districtInstance, field: "code")}</td>
					
						<td>${fieldValue(bean: districtInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: districtInstance, field: "state")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${districtInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
