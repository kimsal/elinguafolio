<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>
        <div class="body">
<h2>Invite students to eLinguafolio</h2>
<p>We'll send email invitations to each student, and automatically create a friendship between your account and theirs.</p>
<p>NOTE: this will only work for student accounts that have not yet been created - a new, unique email address is required for each student account.
</p>
<br/>
<g:form method='post' action="choose">
<table class="table">
<tr>
<th>#</th>
<th>Full name</th>
<th>System username</th>
<th>Email address</th>
</tr>
<g:each in="${1..8}" var="c">

<tr>
<td>${c}</td>
<td>
<g:textField name="name.$c"/><br/>
</td>
<td>
<g:textField name="username.$c"/><br/>
</td>
<td>
<g:textField name="email.$c"/><br/>
</td>
</tr>
</g:each>
</table>
Initial password to set the students' accounts to: <g:textField name="password" value="initialpass"/>
<br/>
<g:submitButton class="btn btn-info" name="submit" value="Choose subjects"/>
</g:form>
        </div>
    </body>
</html>
