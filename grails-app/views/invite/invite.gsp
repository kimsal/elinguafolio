<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>
        <div class="body">
<h2>Invitations sent!</h2>
<p>
We've sent invitations to the following students.  Their initial password was set to "${pass}" and their system usernames (to log in with) and emails are below.
</p>
<br/>
<table class="table">
<tr>
<th>Full name</th>
<th>System username</th>
<th>Email address</th>
</tr>
<g:each in="${names}" var="u">

<tr>
<td>
${names[u.key]}
</td>
<td>
${usernames[u.key]}
</td>
<td>
${emails[u.key]}
</td>
</tr>
</g:each>
</table>
        </div>
    </body>
</html>
