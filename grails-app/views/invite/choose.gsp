<html>
<head>
    <title>eLinguafolio</title>
    <meta name="layout"
          content="${session.layout ?: grailsApplication.config.lf.layout}"/>
</head>

<body>

<h2 class="biography">Select active subjects for your students</h2>
    <g:each var="folio" in="${folios}">

            <g:form method="post" action="invite">
                <g:each var="cl" in="${lf.Checklist.findAllByFolio(folio)}">
                    <div class="portfolioList span2">
                        <lf:displayChecklistPortfolios checklist="${cl}"
                                                       portfolios="${portfolios}"/>
                    </div>
                </g:each>
                <div class="clear"></div>
						 <div class="row">
							 <div class="span12 centered">
                <g:submitButton name="Update" value="Invite students for these subjects" class="centered btn btn-info"/>
							 </div>
						 </div>
            </g:form>

    </g:each>

</body>
</html>
