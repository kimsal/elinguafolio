
<%@ page import="lf.ScaleSet" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'scaleSet.label', default: 'ScaleSet')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'scaleSet.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="sortOrder" title="${message(code: 'scaleSet.sortOrder.label', default: 'Sort Order')}" />
                        
                            <g:sortableColumn property="name" title="${message(code: 'scaleSet.name.label', default: 'Name')}" />
                        
                            <th><g:message code="scaleSet.checklist.label" default="Checklist" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${scaleSetInstanceList}" status="i" var="scaleSetInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${scaleSetInstance.id}">${fieldValue(bean: scaleSetInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: scaleSetInstance, field: "sortOrder")}</td>
                        
                            <td>${fieldValue(bean: scaleSetInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: scaleSetInstance, field: "checklist")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${scaleSetInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
