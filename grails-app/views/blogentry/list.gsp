
<%@ page import="lf.Blogentry" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'blogentry.label', default: 'Blogentry')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-blogentry" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-blogentry" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
				<th></th>	
					
						<g:sortableColumn property="title" title="${message(code: 'blogentry.title.label', default: 'Title')}" />
					
						<g:sortableColumn property="publishDate" title="${message(code: 'blogentry.publishDate.label', default: 'Publish Date')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'blogentry.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'blogentry.lastUpdated.label', default: 'Last Updated')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${blogentryInstanceList}" status="i" var="blogentryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${blogentryInstance.id}">edit</g:link></td>
					
						<td>${fieldValue(bean: blogentryInstance, field: "title")}</td>
					
						<td><g:formatDate date="${blogentryInstance.publishDate}" /></td>
					
						<td><g:formatDate date="${blogentryInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${blogentryInstance.lastUpdated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${blogentryInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
