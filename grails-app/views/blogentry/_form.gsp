<%@ page import="lf.Blogentry" %>



<div class="fieldcontain ${hasErrors(bean: blogentryInstance, field: 'message', 'error')} ">
	<label for="message">
		<g:message code="blogentry.message.label" default="Message" />
		
	</label>
	<g:textArea name="message" cols="40" rows="5" maxlength="50000" value="${blogentryInstance?.message}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: blogentryInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="blogentry.title.label" default="Title" />
		
	</label>
	<g:textField name="title" value="${blogentryInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: blogentryInstance, field: 'publishDate', 'error')} required">
	<label for="publishDate">
		<g:message code="blogentry.publishDate.label" default="Publish Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="publishDate" precision="day"  value="${blogentryInstance?.publishDate}"  />
</div>

