<%@ page import="lf.Notice" %>

<div class="datetimenotice">

<div class="fieldcontain ${hasErrors(bean: noticeInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="notice.status.label" default="Status" />
		
	</label>
	<g:select name="status" from="${noticeInstance.constraints.status.inList}" value="${noticeInstance?.status}" valueMessagePrefix="notice.status" noSelection="['': '']"/>
</div>

       <!--
    <div class="fieldcontain ${hasErrors(bean: noticeInstance, field: 'start', 'error')} required">
        <label for="start">
            <g:message code="notice.start.label" default="Start" />
            <span class="required-indicator">*</span>
        </label>
        <g:datePicker name="start" precision="minute"  value="${noticeInstance?.start}"  />
    </div>

<div class="fieldcontain ${hasErrors(bean: noticeInstance, field: 'end', 'error')} required">
	<label for="end">
		<g:message code="notice.end.label" default="End" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="end" precision="minute"  value="${noticeInstance?.end}"  />
</div>
               -->

<div class="fieldcontain ${hasErrors(bean: noticeInstance, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="notice.text.label" default="Text" />
		
	</label>
	<g:textField name="text" value="${noticeInstance?.text}"/>
</div>

    </div>