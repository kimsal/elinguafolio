
<%@ page import="lf.Notice" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'notice.label', default: 'Notice')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
		<div id="show-notice" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list notice">
			
				<g:if test="${noticeInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="notice.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${noticeInstance}" field="status"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${noticeInstance?.end}">
				<li class="fieldcontain">
					<span id="end-label" class="property-label"><g:message code="notice.end.label" default="End" /></span>
					
						<span class="property-value" aria-labelledby="end-label"><g:formatDate date="${noticeInstance?.end}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${noticeInstance?.start}">
				<li class="fieldcontain">
					<span id="start-label" class="property-label"><g:message code="notice.start.label" default="Start" /></span>
					
						<span class="property-value" aria-labelledby="start-label"><g:formatDate date="${noticeInstance?.start}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${noticeInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="notice.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${noticeInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${noticeInstance?.id}" />
					<g:link class="edit" action="edit" id="${noticeInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
