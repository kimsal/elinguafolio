
<%@ page import="lf.Notice" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'notice.label', default: 'Notice')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
		<div id="list-notice" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					    <th width="120"></th>
						<g:sortableColumn property="status" title="${message(code: 'notice.status.label', default: 'Status')}" />

						<g:sortableColumn property="text" title="${message(code: 'notice.text.label', default: 'Text')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${noticeInstanceList}" status="i" var="noticeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>
                            <g:link class="btn btn-info btn-small" action="edit" id="${noticeInstance.id}">edit</g:link>
                            <g:link class="btn btn-alert btn-small" action="toggle" id="${noticeInstance.id}">toggle</g:link>
                        </td>
                        <td>
                            ${fieldValue(bean: noticeInstance, field: "status")}
                        </td>
										
						<td>${fieldValue(bean: noticeInstance, field: "text")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${noticeInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
