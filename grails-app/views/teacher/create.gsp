
<%@ page import="lf.Teacher" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'teacher.label', default: 'Teacher')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${teacherInstance}">
            <div class="errors">
                <g:renderErrors bean="${teacherInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="teacher.fullName.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'fullName', 'errors')}">
                                    <g:textField name="fullName" value="${teacherInstance?.fullName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="username"><g:message code="teacher.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'username', 'errors')}">
                                    <g:textField name="username" value="${teacherInstance?.username}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="teacher.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'password', 'errors')}">
                                    <g:textField name="password" value="${teacherInstance?.password}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="teacher.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${teacherInstance?.email}" />
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="school"><g:message code="teacher.school.label" default="School" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'school', 'errors')}">
                                    <g:select name="school" from="${lf.School.list()}"  optionKey="id" value="${teacherInstance?.school?.id}" />

                                </td>
                            </tr>

                       
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="myCode"><g:message code="teacher.myCode.label" default="My Code" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: teacherInstance, field: 'myCode', 'errors')}">
                                    <g:textField name="myCode" value="${teacherInstance?.myCode}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
