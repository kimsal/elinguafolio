
<%@ page import="lf.Teacher" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'teacher.label', default: 'Teacher')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="teacher.fullName.label" default="Name" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "fullName")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="teacher.username.label" default="Username" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "username")}</td>
                            
                        </tr>
                    
                   
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="teacher.email.label" default="Email" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "email")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="teacher.school.label" default="School Name" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "school")}</td>
                            
                        </tr>
                    
                   
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="teacher.colleagues.label" default="Colleagues" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${teacherInstance.colleagues}" var="c">
                                    <li><g:link controller="teacher" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="teacher.myCode.label" default="My Code" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "myCode")}</td>
                            
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Enabled" /></td>
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "enabled")}</td>
                        </tr>
                         <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Expired" /></td>
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "accountExpired")}</td>
                        </tr>
                          <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Locked" /></td>
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "accountLocked")}</td>
                        </tr>
                           <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Confirmed" /></td>
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "accountConfirmed")}</td>
                        </tr>
                            <tr class="prop">
                            <td valign="top" class="name"><g:message code="student.myCode.label" default="Password expired" /></td>
                            <td valign="top" class="value">${fieldValue(bean: teacherInstance, field: "passwordExpired")}</td>
                        </tr>
 
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${teacherInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
