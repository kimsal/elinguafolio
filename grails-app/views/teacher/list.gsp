
<%@ page import="lf.Teacher" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'teacher.label', default: 'Teacher')}" />
<filterpane:includes />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>
        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

            <g:form method='post' action='search'>
                <g:textField name="search"/>
                <g:submitButton name="submit" value="Search" class="btn btn-primary"/>
            </g:form>
            <g:if test="${search}">Searching for '${search}' <g:link action="clear">clear search</g:link> </g:if>


            <div class="list">

                <table class="table">
                    <thead>
                        <tr>

                            <th></th>
                            <g:sortableColumn property="name" title="${message(code: 'teacher.fullName.label', default: 'Name')}" />
                        
                            <g:sortableColumn property="username" title="${message(code: 'teacher.username.label', default: 'Username')}" />
                            <g:sortableColumn property="email" title="${message(code: 'teacher.username.label', default: 'Email')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${teacherInstanceList}" status="i" var="teacherInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>
<sec:ifAllGranted roles='ROLE_ADMIN, ROLE_SWITCH_USER'>
<g:form method="post" url="${createLink(controller:'j_spring_security_switch_user', absolute:true)}">
      <g:link class="btn btn-info" action="show" id="${teacherInstance.id}">show</g:link>
      <input type='hidden' name='j_username' value='${teacherInstance.username}'/>
      <input class="btn btn-warning" type='submit' value='become'/>
   </g:form>
</sec:ifAllGranted>

</td>
                        
                            <td>${fieldValue(bean: teacherInstance, field: "fullName")}</td>
                        
                            <td>${fieldValue(bean: teacherInstance, field: "username")}</td>
                            <td>${fieldValue(bean: teacherInstance, field: "email")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${teacherInstanceTotal}" />
            </div>
        </div>

    </body>
</html>
