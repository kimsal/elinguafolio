<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="${domainLayout}"/>
</head>
<body>
<div class="errorMessage">

    <h2>Whoops!</h2>
    <p class="errorText">
        Something broke on this end - we've notified someone about it,
        although we don't have an estimated time that it'll get resolved.
    </p>
    <p class="errorText">
        In the meantime, here's a picture of a cute puppy...
    </p>
    <p class="dog">
        <img src="${resource(dir:"images", file:dog)}"

    </p>
</div>
</body>
</html>