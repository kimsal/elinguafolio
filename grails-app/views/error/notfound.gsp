<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
    <meta name="layout" content="${domainLayout}"/>
</head>
<body>
<div class="errorMessage">

<h2>Whoops!</h2>
<p class="errorText">
    We can't find what you're looking for, but we've made a note of it
    and we'll see if there's anything we can do to make things better
    in the future.
</p>
<p class="errorText">
    In the meantime, here's a picture of a cute puppy...
</p>
    <p class="dog">
        <img src="${resource(dir:"images", file:dog)}"

    </p>
</div>
</body>
</html>