<%@ page import="lf.Notice" %>
<!DOCTYPE html>
<html>
    <head>

        <g:javascript src="jquery/jquery-1.7.min.js"/>
<r:layoutResources/>
        <title><g:layoutTitle default="${grailsApplication.config.siteTitle ?:'eLinguaFolio'}"/></title>
        <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
        <g:layoutHead />
        <link rel="stylesheet" href="${resource(dir:'css/bootstrap/css', file:'bootstrap.css')}">
        <jqval:resources/>
        <r:require modules="jquery-validation-ui" />

<script src="${resource(dir:'js',file:'bootstrap-tab.js')}"></script>
<script src="${resource(dir:'js',file:'bootstrap-tooltip.js')}"></script>
<script src="${resource(dir:'js',file:'bootstrap-popover.js')}"></script>
<script src="${resource(dir:'js',file:'bootstrap-modal.js')}"></script>

<script src="${resource(dir:'js/flowplayer',file:'flowplayer-3.2.8.min.js')}"></script>
<script src="${resource(dir:'js/p1',file:'projekktor-1.0.07r22.min.js')}"></script>
<link ref="stylesheet" media="screen" href="${resource(dir:'js/p1/theme',file:'style.css')}"/>

<link rel="stylesheet" type='text/css' href="${resource(dir:'css',file:'main.css')}" />
<link rel="stylesheet" type='text/css' href="${resource(dir:'css/fontawesome/css',file:'font-awesome.css')}" />
    </head>
    <body >
        <lf:ga/>
	<div class="topbar" id="header">
		<div class="container">
		<a href="/" class="brand">${grailsApplication.config.brand}</a>

		<span class="login_links">
<lf:login security="${session.security}"/>
		</span>
		</div>
	</div>
    <div class="container">

    <g:each var="m" in="${Notice.findAllByStatus('on')}">
        <div class="row-fluid">
            <div class="span12 btn btn-warning">
                ${m.text}
            </div>
        </div>
    </g:each>
    </div>

        <div class="container mainbody">
<div class="row">
<div class="span9">
        <g:layoutBody />
</div>
<div class="span3">
<blog:render number="3" header="Recent news"/>
</div>
</div>
	</div>

<div class="clear"></div>


    <div id="footer" class="footer">
        <footer>
            <div class="container">
                <p class="_contact">
                    <lf:supportTag/>
                </p>
                <p class="_version">
                version: <g:meta name="app.version"/><g:if env="development" test="${1}">d</g:if><g:if env="production" test="${1}">p</g:if><g:if env="test" test="${1}">t</g:if> <g:meta name="app.buildNumber"/> | <g:meta name="app.buildDate"/>
 |                 ${request.getHeader('x-forwarded-for')}
                </p>
            </div>
        </footer>
    </div>



<r:layoutResources/>

    </body>
</html>
