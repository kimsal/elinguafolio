<%@ page import="lf.Emaildomain" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName"
           value="${message(code: 'emaildomain.label', default: 'Emaildomain')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="subnav">
    <ul class="nav nav-pills"><li><a class="home"
                                     href="${createLink(uri: '/admin')}">Admin</a></li>
    </ul>
</div>

<div class="body">
    <h1>Acceptable email domains</h1>

    <p>
        Below is a list of email domains that are allowed for teacher registrations.
        If a teacher is to register, they have to register with one of the email domains below.
    </p>
    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <g:form method="post" action="update">
        <g:textArea id="emaildomaintextarea" name="domains" rows="50" cols="40"><g:each
                in="${all}" var="domain">${domain.domain}
        </g:each></g:textArea>
        <br/>
        <g:submitButton name="submit" class="btn" value="Update"/>
    </g:form>

</div>

</body>
</html>
