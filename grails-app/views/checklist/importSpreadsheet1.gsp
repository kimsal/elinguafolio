<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
    <meta name="layout" content="${domainLayout}"/>
</head>
<body>
<h1>Import spreadsheet</h1>

<a href="/visartscheclist.xlsx">sample Excel file</a> (keep the tab as "World Languages")
<hr/>
<g:form method="post" action="importSpreadsheet2" enctype="multipart/form-data">
<input type="file" name="file"/>
    <g:submitButton name="submit" value="Upload" class="btn btn-info"/>
</g:form>

<p>
    Importing will take approximately 15 minutes
</p>
</body>
</html>
