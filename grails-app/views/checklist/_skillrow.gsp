<%@ page import="lf.CandoStatement" %>
<tr>
    <td>
        <div class="skillname">${skill.name}</div>
        <g:link action="moveskill" id="${skill.id}"
                params="['direction':'up','checklist':checklist.id]">^</g:link>
        <g:link action="moveskill" id="${skill.id}"
                params="['direction':'down','checklist':checklist.id]">v</g:link>
        <g:link action="delskill" id="${skill.id}" params="['checklist':checklist.id]">x</g:link>

        <a href="#editSkill${skill.id}" data-toggle="modal"> edit </a>

        <div class="modal hide" id="editSkill${skill.id}">
            <g:form method="post" action="updateskill" enctype="multipart/form-data">

                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Edit skill for this mode</h3>
                </div>
                <div class="modal-body">
                    Skill name? <input type="text" id="input1" value="${skill.name}" name="skill"/>
                    <br/>
                    Sort order? <input type="text" id="input1" value="${skill.sortOrder}" name="sortOrder" size="3"/>
                    <input type="hidden" name="mid" value="${checklist.id}" />
                    <input type="hidden" name="id" value="${skill.id}" />
                    <br/>
                    Picture? <input type="file" name="pictures"/>
                </div>
                <div class="modal-footer">
                    <g:submitButton name="submit" value="Add" class="btn btn-primary" />
                </div>
            </g:form>
        </div>

        <attachments:each bean="${skill}">
            <g:form method="post" action="delskillpic">

                <g:hiddenField name="id" value="${skill?.id}"/>
                <g:hiddenField name="checklist" value="${checklist.id}"/>
                <g:submitButton name="submit" value="delete"/>
            </g:form>
            <img width="100" src="/${attachment.imagePath}"/>
        </attachments:each>
    </td>
    <g:each in="${scalesets.sort{it.sortOrder}}" var="ss">
        <g:each in="${ss.scales.sort{it.sortOrder}}" var="s">

            <td>
<g:link class="candopopover btn btn-success"
    rel="popover"
    data-original-title="Statements"
        action="modifycandos"
data-content="
${CandoStatement.findAllByScaleAndSkill(s,skill).sort{it.sortOrder}.join("<hr/>")}
"
        params="['scale':s.id, 'skill':skill.id, 'checklist':checklist.id]">edit</g:link>
            </td>


        </g:each>
    </g:each>
</tr>