
<%@ page import="lf.Checklist" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'checklist.label', default: 'Checklist')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${checklistInstance}">
            <div class="errors">
                <g:renderErrors bean="${checklistInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${checklistInstance?.id}" />
                <g:hiddenField name="version" value="${checklistInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="assessmentStatements"><g:message code="checklist.assessmentStatements.label" default="Assessment Statements" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'assessmentStatements', 'errors')}">
                                    
<ul>
<g:each in="${checklistInstance?.assessmentStatements?}" var="a">
    <li><g:link controller="assessmentStatement" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="assessmentStatement" action="create" params="['checklist.id': checklistInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'assessmentStatement.label', default: 'AssessmentStatement')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sortOrder"><g:message code="checklist.sortOrder.label" default="Sort Order" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'sortOrder', 'errors')}">
                                    <g:textField name="sortOrder" value="${fieldValue(bean: checklistInstance, field: 'sortOrder')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="modes"><g:message code="checklist.modes.label" default="Skill Sets" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'modes', 'errors')}">
                                    
<ul>
<g:each in="${checklistInstance?.modes?}" var="d">
    <li><g:link controller="mode" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="mode" action="create" params="['checklist.id': checklistInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'mode.label', default: 'Mode')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="topics"><g:message code="checklist.topics.label" default="Topics" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'topics', 'errors')}">
                                    
<ul>
<g:each in="${checklistInstance?.topics?}" var="t">
    <li><g:link controller="topic" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="topic" action="create" params="['checklist.id': checklistInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'topic.label', default: 'Topic')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="folio"><g:message code="checklist.folio.label" default="Folio" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'folio', 'errors')}">
                                    <g:select name="folio.id" from="${lf.Folio.list()}" optionKey="id" value="${checklistInstance?.folio?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="checklist.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${checklistInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="modes"><g:message code="checklist.modes.label" default="Scale Sets" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: checklistInstance, field: 'modes', 'errors')}">
                                    
<ul>
<g:each in="${checklistInstance?.modes?}" var="s">
    <li><g:link controller="mode" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="mode" action="create" params="['checklist.id': checklistInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'mode.label', default: 'ScaleSet')])}</g:link>

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
