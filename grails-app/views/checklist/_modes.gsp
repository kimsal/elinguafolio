<% def scaleTotal = scalesets.scales.flatten().size() %>
<g:each in="${modes.sort{it.sortOrder}}" var="mode">
    <tr>
        <td>
            <div class="modename">${mode.name}</div>
            <g:link action="movemode" id="${mode.id}"
                    params="['direction':'up','checklist':checklist.id]">^</g:link>
            <g:link action="movemode" id="${mode.id}"
                    params="['direction':'down','checklist':checklist.id]">v</g:link>
            <g:link action="delmode" id="${mode.id}" params="['checklist':checklist.id]">x</g:link>

            <a href="#addSkillTo${mode.id}" data-toggle="modal"> add </a>

            <div class="modal hide" id="addSkillTo${mode.id}">
                <g:form method="post" action="addskill" enctype="multipart/form-data">

                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">×</a>
                        <h3>Add a new skill for this mode</h3>
                    </div>
                    <div class="modal-body">
                        New skill name? <input type="text" id="input1" name="skill"/>
                        <br/>
                        New sort order? <input type="text" id="input1" name="sortOrder" size="3"/>
                        <input type="hidden" name="mid" value="${checklist.id}" />
                        <input type="hidden" name="id" value="${mode.id}" />
                        <br/>
                        Picture? <input type="file" name="pictures"/>
                    </div>
                    <div class="modal-footer">
                        <g:submitButton name="submit" value="Add" class="btn btn-primary" />
                    </div>
                </g:form>
                </div>


        </td>
        <td colspan="<%= scaleTotal %>">&nbsp;</td>
    </tr>
    <g:each in="${mode.skills.sort{it.sortOrder}}" var="skill">
        <g:render template="skillrow"
            model="['skill':skill,
                    'scalesets':scalesets,
                'checklist':checklistInstance,
                'mode':mode]" />

    </g:each>
</g:each>