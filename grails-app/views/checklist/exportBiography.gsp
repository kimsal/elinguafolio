
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'checklist.label', default: 'Checklist')}" />
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>
        </div>
        <div class="body">
		<h1>Exported biog data</h1>
<textarea style="width:500px;" name="exported" rows="30" cols="100">${export}</textarea>
		
        </div>
    </body>
</html>
