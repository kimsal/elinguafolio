<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 3/11/12
  Time: 12:23 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="lf.Scale; lf.Skill; lf.Checklist" contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
</head>
<body>
<h2>Modifying statements for checklist ${Checklist.get(checklist)}</h2>
<h3>
    Skill: ${Skill.get(skill).mode} /  ${Skill.get(skill)}
</h3>
<h3>
    Scale: ${Scale.get(scale).scaleSet} /  ${Scale.get(scale)}
</h3>
<g:link action="modify" id="${checklist as Long}">back</g:link>
<hr/>
<table class="table">
    <tr>
    <th>&nbsp;</th>
        <th>&nbsp;</th>
    <th>Statement</th>
        <th>&nbsp;</th>
    </tr>
<g:each in="${candos}" var="c">
       
    <tr>
        <td>
<g:link action="movestatement" id="${c.id}"
            params="['direction':'up','checklist':checklist]">^</g:link>
<g:link action="movestatement" id="${c.id}"
        params="['direction':'down','checklist':checklist]">v</g:link>
<g:link action="delstatement" id="${c.id}" params="['checklist':checklist]">x</g:link>
<g:link action="modifycandos" id="${c.id}"
        params="['st':c.id, 'checklist':checklist, 'skill':skill, 'scale':scale]"
> edit</g:link>
        </td>
        <td>${c.sortOrder}</td>
        <td>${c.statement}</td>
        <td>
            <attachments:each bean="${c}">
                <g:form method="post" action="delpic">

                <g:hiddenField name="skill" value="${skill}"/>
                <g:hiddenField name="scale" value="${scale}"/>
                <g:hiddenField name="id" value="${c?.id}"/>
                <g:hiddenField name="checklist" value="${checklist.toInteger()}"/>
                <g:submitButton name="submit" value="delete"/>
                </g:form>
               <img width="200" src="/${attachment.imagePath}"/>
            </attachments:each>
        </td>

    </tr>

</g:each>

</table>
<g:form method="post" action="addstatement" enctype="multipart/form-data">
    <g:hiddenField name="skill" value="${skill}"/>
    <g:hiddenField name="scale" value="${scale}"/>
    <g:hiddenField name="id" value="${st?.id}"/>
    <g:hiddenField name="checklist" value="${checklist.toInteger()}"/>
    Sort order? <g:textField name="sortOrder" value="${st?.sortOrder}" size="3"/>
<br/>
    Statement?
    <g:textField name="statement" value="${st?.statement}"/>
    <br/>
    Picture? <input type="file" name="pictures"/>
    <br/>

    <g:submitButton name="submit" value="Add" class="btn"/>


</g:form>

</body>
</html>