
<%@ page import="lf.Checklist" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'checklist.label', default: 'Checklist')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="subnav">
        <ul class="nav nav-pills">
            <li>
                <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
            </li>
            <li>
                <g:link class="list" action="create"><g:message code="default.create.label" args="[entityName]" /></g:link>
            </li>

            <li>
                <g:link class="list" action="imp">import json</g:link>
            </li>
            <li>
                <g:link class="list" action="importSpreadsheet1">import spreadsheet</g:link>
            </li>

        </ul>
    </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'checklist.id.label', default: 'Id')}" />
                        
                            <th><g:message code="checklist.folio.label" default="Folio" /></th>

                            <th>Checklist</th>

                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${checklistInstanceList}" status="i" var="checklistInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>
                                <g:link action="show" id="${checklistInstance.id}">show</g:link>

                                <g:link action="edit" id="${checklistInstance.id}">edit</g:link>

                                <g:link action="modify" id="${checklistInstance.id}">modify</g:link>
                                <g:link action="export" id="${checklistInstance.id}">export</g:link>
                            </td>
                        
                            <td>${fieldValue(bean: checklistInstance, field: "folio")}</td>
                        
                            <td>${fieldValue(bean: checklistInstance, field: "name")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${checklistInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
