
<%@ page import="lf.Checklist" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'checklist.label', default: 'Checklist')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${checklistInstance}">
            <div class="errors">
                <g:renderErrors bean="${checklistInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" action="import2" >
Attach to folio: <g:select name="folio" from="${lf.Folio.list()}" optionKey="id"/>
<br/>
New checklist name: <g:textField name="name"/>


<br/>
<textarea name="imp" rows="10" cols="20"></textarea>
<br/>
<g:submitButton name="submit" value="Import"/>
            </g:form>
        </div>
    </body>
</html>
