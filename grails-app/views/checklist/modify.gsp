<%@ page import="lf.Folio" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'folio.label', default: 'Folio')}" />
        <title>Modify Checklist ${checklistInstance.name}</title>
        <gui:resources components="dialog"/>
    </head>
    <body>
    <!--
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        -->
        <div class="body">
        <div class="subnav">
            <ul class="nav nav-pills">
                <li>
                    <a class="home" href="${createLink(uri: '/admin')}">Admin</a>
                </li>
                <li>
                    <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
                </li>
            </ul>
        </div>
<h2>Modifying checklist ${checklistInstance.name}</h2>

            <div style="float:left; width:40%;">

            <h3>Add a scale set</h3>
            <g:form method="post" action="addscaleset">
                <g:hiddenField name="id" value="${checklistInstance.id}"/>
                Name: <g:textField name="scaleset"/>
                <g:submitButton name="submit" value="Add"/>
            </g:form>
            </div>

            <div style="float:left; width:40%;">
            <h3>Add a mode</h3>
            <g:form method="post" action="addmode">
                <g:hiddenField name="id" value="${checklistInstance.id}"/>
                Name: <g:textField name="mode"/>
                <g:submitButton name="submit" value="Add"/>
            </g:form>
</div>
            <div style="clear:both"></div>

<hr/>
<h3>Topics</h3>
            <g:form method='post' action='addtopic'>
                Name? <g:textField name="name"/>
                <g:hiddenField name="checklist" value="${checklistInstance.id}"/>
                <g:submitButton name="submit" value="Add"/>
            </g:form>
<g:each in="${checklistInstance.topics.sort{it.name}}" var="t">
<div class="topiccloud">
    <g:link action="deltopic" id="${t.id}" params="['checklist':checklistInstance.id]">del</g:link>
    ${t.name}
</div>


</g:each>
            <hr/>
            <table class="table checklistgridtable" width="900">
                <tr>
                    <th>Modes</th>
                    <th colspan="${checklistInstance.scaleSets.scales.flatten().size()}">Scale sets</th>
                </tr>
            <g:render template="scalesets"
                      model="['scalesets':checklistInstance.scaleSets,
                      'checklist':checklistInstance]" />
            <g:render template="modes"
                      model="['scalesets':checklistInstance.scaleSets,
                              'checklist':checklistInstance,
                                'modes':checklistInstance.modes]" />

            </table>

        <hr/>
        <h2>Copy scales and modes</h2>
        <p>
            Jumpstart this checklist with scales and modes from a different checklist.
             Note that this will erase all the data in this checklist.
        </p>
        <g:form method="post" action="copyscalemode">
            <g:hiddenField name="to" value="${checklistInstance.id}"/>
<g:select name="from" optionKey="id" optionValue="name"
    from="${lf.Checklist.list()}"/>
            <g:submitButton name="submit" value="Copy" class="btn btn-primary"/>
        </g:form>
        </div>
    <script>
        $(".candopopover").popover('hide');
    </script>
    </body>
</html>
