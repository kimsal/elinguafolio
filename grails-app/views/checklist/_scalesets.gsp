

<tr>
<td>&nbsp;</td>
<g:each var="ss" in="${scalesets.sort{it.sortOrder}}">
<td colspan="${ss.scales.size()}">
    <g:link action="movescaleset" id="${ss.id}" params="['direction':'left', 'mid':checklist.id]"> < </g:link>
    ${ss.name}
    <g:link action="movescaleset" id="${ss.id}" params="['direction':'right', 'mid':checklist.id]"> > </g:link>
    <g:link action="delscaleset" id="${ss.id}" params="[ 'mid':checklist.id]"> x </g:link>
    <a href="#addScaleTo${ss.id}" data-toggle="modal"> v </a>

    <div class="modal hide" id="addScaleTo${ss.id}">
<g:form method="post" action="addscale">

        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>
            <h3>Add a new scale for this scale set</h3>
        </div>
        <div class="modal-body">
            New scale name? <input type="text" id="input1" name="scale"/>
            <br/>
            New scale order? <input type="text" id="input1" name="sortOrder" size="3"/>
<input type="hidden" name="mid" value="${checklist.id}" />
<input type="hidden" name="id" value="${ss.id}" />
        </div>
        <div class="modal-footer">
            <g:submitButton name="submit" value="Add" class="btn btn-primary" />
        </div>
</g:form>

        </div>



</td>
</g:each>
</tr>
<tr>
    <td>&nbsp;</td>
<g:each var="ss" in="${scalesets.sort{it.sortOrder}}">
    <g:if test="${ss.scales.size()==0}">
        <td>&nbsp;</td>
    </g:if>
<g:each var="s" in="${ss.scales.sort{it.sortOrder}}">
<td>
    <g:link action="movescale" id="${s.id}" params="['direction':'left', 'mid':checklist.id]"> < </g:link>
    ${s.name}
    <g:link action="movescale" id="${s.id}" params="['direction':'right', 'mid':checklist.id]"> > </g:link>
    <g:link action="delscale" id="${s.id}" params="[ 'mid':checklist.id]"> x </g:link>
</td>
</g:each>
</g:each>
</tr>
