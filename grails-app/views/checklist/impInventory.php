<%@ page import="lf.Inventory" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'inventory.label', default: 'Inventory')}" />
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>
        </div>
        <div class="body">
            <g:form action="save" method="post" action="importInventory2" >
Attach to folio: <g:select name="folio" from="${lf.Folio.list()}" optionKey="id"/>
<br/>
Name: <g:textField name="name"/>
<br/>
<textarea name="imp" rows="10" cols="20"></textarea>
<br/>
<g:submitButton name="submit" value="Import"/>
            </g:form>
        </div>
    </body>
</html>
