<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
<gui:resources components="dialog"/>
<resource:tabView/>
    </head>
    <body>

    <g:set var="action" value="learningActivityAdd"/>
    <g:set var="navaction" value="myPortfolios"/>
    <g:if test="${viewingFriend}">
        <g:set var="action" value="NONE"/>
        <g:set var="navaction" value="sharedFriend"/>
    </g:if>


    <div class="navcrumbs">
<g:link action="index">Home</g:link> 
>
<g:link action="${navaction}" id="${myportfolio.id}">${myportfolio.folio}</g:link>
> 
My Learning Activities
</div>

            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

    <g:render template="viewingFriend" model="['friend':viewingFriend]"/>



    <h2 class="biography">My Learning Activities for ${myportfolio.folio.title}</h2>

<div id="ladiv">

<g:if test="${viewingFriend==null}">
<div id="la_add">
<h2>Add a Learning Activity</h2>
<g:form action="${action}">
<input type='hidden' name='p' value="${myportfolio.id}"/>

<p>
<label for="description">Description</label>
<input type="text" name="description" size="50"/>
</p>
<p>
<label for="date">Date</label>
<input type="text" name="date" size="50"/>
</p>
<p>
<label for="activity">Activity</label>
<g:select name="activity" from="${new lf.LanguageActivity().constraints.activity.inList.toList()}"/>
</p>
<p>
<label for="topic">Language</label>
<g:select name="topic" optionKey="id" value="fullName()" from="${myportfolio.portfolios.findAll{it.status==lf.StatusCode.PORTFOLIO_INUSE}*.topic}"/>
</p>
<br/>
<div class="centered">
<g:submitButton class="primary btn" name="add" value="Add activity"/>
</div>

</g:form>

</div>
</g:if>



<div id="la_list">
<h2>Learning Activities</h2>
<table class="table">
<tr>
<th width="35%">Description</th>
<th>Topic</th>
<th>Date</th>
<th><th/>
</tr>

<g:each var="a" in="${new lf.LanguageActivity().constraints.activity.inList.toList()}">
<g:set var="l" value="${activityList.activities.findAll{it.activity==a}}"/>

<g:if test="${l.size()>0}">
<tr><td colspan="3">
<h3>${a}</h3>
</td>
</tr>
<g:each var="ac" in="${l}">
<tr>
<td>${ac.description}</td>
<td>${ac.topic.fullName()}</td>
<td>${ac.activityDate}</td>
<td>
    <g:if test="${viewingFriend==null}">
<g:link onclick="if(!confirm('Are you sure?')) { return false; } else { return true; }"
 action="learningActivityDel" id="${ac.id}" class="btn danger">del</g:link>
        </g:if>
</td>
</tr>
</g:each>
</g:if>

</g:each>
</table>



</div>


</div>



<script>
function error(e) { 
	alert('e' + e);
}
function message(e) { 
	alert('m' + e);
}
</script>
    </body>
</html>
