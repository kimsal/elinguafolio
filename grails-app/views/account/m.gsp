<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
<gui:resources components="dialog"/>
<g:javascript library="jquery"/>

<attachments:style />
    </head>
    <body>

<attachments:uploadForm bean="${topicInstance}" />
<attachments:script updateInterval="100"/>
    </body>
</html>
