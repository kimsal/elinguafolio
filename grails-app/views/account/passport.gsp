<html>
<head>
<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
</head>
<body>



<g:set var="action" value="learningEncounterAdd"/>
<g:set var="navaction" value="myPortfolios"/>
<g:if test="${viewingFriend}">
    <g:set var="action" value="NONE"/>
    <g:set var="navaction" value="sharedFriend"/>
</g:if>

<div class="navcrumbs">
<g:link action="index">Home</g:link> 
>
<g:link action="myPortfolios" id="${myportfolio.id}">${myportfolio.folio}</g:link>
> 
My passport
</div>

<g:if test="${flash.message}">
<div class="message">${flash.message}</div>
</g:if>

<g:render template="viewingFriend" model="['friend':viewingFriend]"/>


<h2 class="biography">My Passport for ${myportfolio.folio.title}</h2>

<div id="tabView">

<g:set var="fam" value="false"/>
<g:set var="lin" value="false"/>
<g:set var="lout" value="false"/>
<g:set var="exp" value="false"/>
<g:set var="test" value="false"/>
<g:if test="${tab=='fam' || tab==null || tab==true}"><g:set var="fam" value="active"/></g:if>
<g:if test="${tab=='in'}"><g:set var="lin" value="active"/></g:if>
<g:if test="${tab=='out'}"><g:set var="lout" value="active"/></g:if>
<g:if test="${tab=='exp'}"><g:set var="exp" value="active"/></g:if>
<g:if test="${tab=='test'}"><g:set var="test" value="active"/></g:if>

<ul class="tabs nav nav-tabs" data-tabs="tabs">
<li class="${fam}"><a data-toggle="tab" href="#fam">From Family</a></li>
<li class="${lin}"><a data-toggle="tab" href="#lin">In School</a></li>
<li class="${lout}"><a data-toggle="tab" href="#lout">Outside School</a></li>
<li class="${exp}"><a data-toggle="tab" href="#exp">Experiences</a></li>
<li class="${test}"><a data-toggle="tab" href="#test">Tests/Competitions</a></li>
</ul>

<div class="tab-content">

<div class="${fam} tab-pane" id="fam">
<g:form action="updatePassportLanguageLearnedFromFamily">

	<div class="section">
	<h2>Languages I have learned in my family</h2>
	<input type='hidden' name='p' value='${passport.id}'/>
    <g:if test="${viewingFriend==null}">
	<g:textArea name="family" value="${passport.family}"/>
	<g:submitButton class="btn primary" name="submit" value="Update!"/>
        </g:if>
        <g:else>
            ${passport?.family?.replace("\n","<BR/>")}
        </g:else>
	</div>

</g:form>
</div>



<div class="${lin} tab-pane" id="lin">

	<g:form name="add1" action='addPassportLanguageLearnedInSchool'
	onSuccess='message' onError='error'>


	<div class="section">
	<h2>Languages I have learned in school</h2>
	<table class="table">
		<tr>
			<th>Language</th>
			<th>Program</th>
			<th>Years</th>
			<th>Avg hours/week</th>

		<th>&nbsp;</th>
		</tr>
		<g:each var="p" in="${passport.schools}">
		<tr>
			<td>${p.language}</td>
			<td>${p.program}</td>
			<td>${p.years}</td>
			<td>${p.hours}</td>
			<td>
            <g:if test="${viewingFriend==null}">
                <g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="delPassportLanguageLearnedInSchool" id="${p.id}" class="btn danger">del</g:link>
            </g:if>
            </td>
		</tr>
	</g:each>
        <g:if test="${viewingFriend==null}">
	<input type='hidden' name='p' value='${passport.id}'/>
		<tr>
			<td><input type='text' name='language'/></td>
			<td><input type='text' name='program'/></td>
			<td><input type='text' name='years'/></td>
			<td><input type='text' name='hours'/></td>
			<td>
			<g:submitButton class="btn primary" name="add1" value="Add"/>
			</td>
		</tr>
            </g:if>
	</table>
	</div>

	</g:form>
</div>



<div class="${lout} tab-pane" id="lout">

<g:form name="add1" action='addPassportLanguageLearnedOutsideSchool'
onSuccess='message' onError='error' >
	<div class="section">
		<h2>Languages I have learned outside of school</h2>
		<table class="table">
		<tr>
		<th>Language</th>
		<th>Type of Acquisition</th>
		<th>Age / From-To</th>
<th>&nbsp;</th>
		</tr>
		<g:each var="p" in="${passport.outsideSchools}">
		<tr>
		<td>${p.language}</td>
		<td>${p.acquisition}</td>
		<td>${p.age}</td>
		<td>
            <g:if test="${viewingFriend==null}">
            <g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="delPassportLanguageLearnedOutsideSchool" id="${p.id}" class="btn danger">del</g:link>
                </g:if>
        </td>
		</tr>
		</g:each>
    <g:if test="${viewingFriend==null}">
		<!--url="[controller:'account',action:'addPassportLanguageLearnedInSchool']" -->
		<input type='hidden' name='p' value='${passport.id}'/>
		<tr>
		<td><input type='text' name='language'/></td>
		<td><input type='text' name='acquisition'/></td>
		<td><input type='text' name='age'/></td>
		<td>
		<g:submitButton class="btn primary" name="add2" value="Add"/>
		</td>
		</tr>
        </g:if>
		</table>
	</div>

		</g:form>
</div>



<div class="${exp} tab-pane" id="exp">

<g:form name="add1" action='addPassportLanguageExperience'
onSuccess='message' onError='error'>
	<div class="section">
		<h2>Language Experiences</h2>
		<table class="table">
			<tr>
			<th>Country</th>
			<th>Type of Encounter</th>
			<th>Age</th>
			<th>Length of Stay</th>
<th>&nbsp;</th>
			</tr>
			<g:each var="p" in="${passport.experiences}">
			<tr>
			<td>${p.country}</td>
			<td>${p.type}</td>
			<td>${p.age}</td>
			<td>${p.length}</td>
			<td>
                <g:if test="${viewingFriend==null}">
                <g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="delPassportLanguageExperience" id="${p.id}" class="btn danger">del</g:link>
                    </g:if>
            </td>
			</tr>
			</g:each>
    <g:if test="${viewingFriend==null}">
			<input type='hidden' name='p' value='${passport.id}'/>
			<tr>
			<td><input type='text' name='country'/></td>
			<td><input type='text' name='type'/></td>
			<td><input type='text' name='age'/></td>
			<td><input type='text' name='length'/></td>
			<td>
			<g:submitButton class="btn primary" name="add3" value="Add"/>
			</td>
			</tr>
        </g:if>
		</table>
	</div>
</g:form>
</div>


<div class="${test} tab-pane" id="test">

		<g:form name="add1" action='addPassportLanguageTest'
		onSuccess='message' onError='error'>
	<div class="section">
		<h2>Language Tests, Competitions, Certificates, Diplomas</h2>
		<table class="table">
			<tr>
				<th>Language</th>
				<th>Description</th>
				<th>Date</th>
				<th>Score/Results</th>
<th>&nbsp;</th>
			</tr>
		<g:each var="p" in="${passport.tests}">
			<tr>
				<td>${p.language}</td>
				<td>${p.description}</td>
				<td>${p.testDate}</td>
				<td>${p.score}</td>
				<td>
            <g:if test="${viewingFriend==null}">
                    <g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="delPassportLanguageTest" id="${p.id}">del</g:link>
                </g:if>
                </td>
			</tr>
		</g:each>
            <g:if test="${viewingFriend==null}">
		<input type='hidden' name='p' value='${passport.id}'/>
			<tr>
				<td><input type='text' name='language'/></td>
				<td><input type='text' name='description'/></td>
				<td><input type='text' name='testDate'/></td>
				<td><input type='text' name='score'/></td>
				<td>
				<g:submitButton class="btn primary" name="add4" value="Add"/>
				</td>
			</tr>
                </g:if>
		</table>
	</div>
</g:form>
</div>


</div>



</div><!--tabView-->



<script>
function error(e) { 
	alert('e' + e);
}
function message(e) { 
	alert('m' + e);
}
</script>
<script>
$(function () {
$('#tabs').tab();
})
</script>
</body>
</html>
