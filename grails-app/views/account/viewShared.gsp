<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
		<resource:tabView/>
    </head>
    <body>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
> 
<g:link controller="account" action="shares" id="${myportfolio.id}">Portfolios shared with me</g:link>
> 
${shared.portfolio.topic}
</div>

<g:render template="viewingFriend" model="['viewingFriend':shared.portfolio.myportfolio.owner]"/>

<g:render template="/shared/displaySharedPortfolio" model="['viewingFriend':shared.portfolio.myportfolio.owner, 'user':user, 'portfolio':shared.portfolio]"/>


    </body>
</html>
