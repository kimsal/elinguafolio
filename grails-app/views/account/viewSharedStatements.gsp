<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errors}">
            <div class="errors">
                <g:renderErrors bean="${flash.errors}" as="list" />
            </div>
            </g:if>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
 > 
<g:link controller="account" action="shares" id="${myportfolio.id}">Portfolios shared with me</g:link>
 > 
<g:link controller="account" action="viewShared" id="${portfolio.id}">${portfolio.topic}</g:link>
>
${skill.name} - ${scale.scaleSet.name}/${skill.name}
</div>

<g:render template="viewingFriend" model="['viewingFriend':shared.portfolio.myportfolio.owner]"/>

<g:set var="clist" value="${portfolio.topic.checklist.assessmentStatements.sort{it.sortOrder}.toList()}"/>
<%
clist << "N/A"
%>
<form>
<table id="candoStatements" class="table">
<tr>
<th id="header_statement">Statement</th>
<th id="header_assessment">Assessment</th>
<th id="header_evidence">Evidence</th>
</tr>
<tr>
<td></td>
<td>
<g:each var='c' in="${clist}">
<div style="width: 120px; float:left; ">${c}</div>
</g:each>
</td>
<td></td>
</tr>

<g:each var="cs" in="${candoStatements}">
<tr class="statementRow">
<td>${cs.statement}</td>
<g:set var="mycandoentry" value="${myCandos.find{it.statement==cs}}"/>
<td>
<g:each var='c' in="${clist}">
<div style="width: 120px; float:left; ">
<g:set var="checked" value="${false}"/>
<g:if test="${mycandoentry?.candoStatus.toString()==c.toString()}">
<g:set var="checked" value="${true}"/>
<input type='radio' name='candoStatus.${mycandoentry?.id}' value="${c}" checked="checked" disabled="disabled"/>
</g:if>
<g:else>
<input type='radio' name='candoStatus.${mycandoentry?.id}' value="${c}" disabled="disabled"/>
</g:else>
</div>
</g:each>
</td>
<td>
<lf:displayEvidencesCountShared evidences="${mycandoentry?.evidences}" portfolio="${portfolio.id}" id="${cs.id}"/>
</td>
</tr>

</g:each>
</table>
</form>
    </body>
</html>
