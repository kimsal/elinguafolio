<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
<gui:resources components="dialog"/>
    </head>
    <body>
    
<div class="navcrumbs">
Home
</div>


<div class="myPortfolios">
<h2>My portfolios</h2>
<p><g:link action="addPortfolios">Add a portfolio</g:link></p>
<g:if test="${myportfolios}">
	<g:each var="p" in="${myportfolios}">
		<div class="listportfolio">
		<h3>${p.folio.title}</h3>
				<g:link action="viewPortfolios" id="${p.id}">
				<img class="listportfolio_image" src="${resource(dir:"images",file:"folder_blue.png")}" alt="${p}" border="0"/>
				</g:link>
			<div class="listportfolio_links">
				<p>
				<g:link action="biography" id="${p.id}">View and edit my biography for this portfolio</g:link>
				</p>
				<p>
				<g:link action="viewPortfolios" id="${p.id}">View and edit my subjects</g:link>
				</p>
				<p>
				<g:link action="editPortfolios" id="${p.id}">Change my subjects in this portfolio</g:link>
				</p>
			</div>
		</div>
	</g:each>
</g:if>
<div style="display:none">
<p><g:link action="viewPortfolios">View my portfolios</g:link></p>
<p><g:link action="editPortfolios">Edit my portfolios</g:link></p>
</div>
</div>

<div class="clear"></div>
<div class="myPortfolios">
<h2>My friends </h2>
<p><g:link action="viewFriends">View my friends</g:link></p>
<p><g:link action="editFriends">Update my friends</g:link></p>
</div>


    </body>
</html>
