<!DOCTYPE HTML>
<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
		 <r:require modules="uploadr"/>

<attachments:style />
    </head>
    <body>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errors}">
            <div class="errors">
                <g:renderErrors bean="${flash.errors}" as="list" />
            </div>
            </g:if>


<hr/>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
 > 
<g:link controller="account" action="viewPortfolios" id="${portfolio.myportfolio.id}">My portfolios</g:link>
 > 
<g:link mapping="candoview" controller="account" action="viewStatements" params='[portfolio:"${portfolio.id}", skill:"${cando.statement.skill.id}",  scale:"${cando.statement.scale.id}"]'>${skill?.name}

 - ${cando.statement.skill.mode.name}/${cando.statement.skill.name}
</g:link> > 
${cando.statement}
</div>


	 <div class="row">
		 <div class="span8">
			 <table class="table">
				 <tr>
					 <th></th>
					 <th>Name</th>
					 <th>Uploaded</th>
				 </tr>
				<g:each in="${evidences}" var="e">
					<tr>
						<td>
							<g:link onClick="if(!confirm('Are you sure?')) { return false; }" controller="account" action="deleteEvidence" id="${e.id}">
								<i class="icon-remove"></i>
							</g:link>
							<g:link class="evidenceFileLink" controller="download" id="${e.file.id}"><i class="icon-arrow-down"></i></g:link>
						</td>
						<td>
							<g:link class="evidenceFileLink" controller="download" id="${e.file.id}">${e.file.name}</g:link>
						</td>
						<td>
							${e.dateCreated.prettyDate()}
						</td>
					</tr>
				</g:each>
			 </table>


		 </div>
		 <div class="span4">
			 <h2>Upload evidence files</h2>
			 <uploadr:add controller="upload"
							  deletable="false" rating="false" viewable="false"
							  noSound="true" action="handle" name="myuploader"
							  maxSize="${maxBytes}" downloadable="false"
							  path="/Users/michael/Desktop/mySecondUploadr" allowedExtensions="${allowedExtensions?.join(",")}"
							  direction="down" maxVisible="8" unsupported="/upload/warning">
				 <uploadr:onStart>
					 if(this.uri.substr(-5)!="&lf=y") {
					 	this.uri +="/?cde="+${cando.id?:0}+"&top="+${topicId?:0}+"&cds="+${candoStatement.id?:0}+"&lf=y";
					 }

				 </uploadr:onStart>
			 </uploadr:add>
			 <div class="alert allowed">Max file size allowed: ${maxSize} MB</div>
			 <div class="alert allowed">Allowed file types: ${allowedExtensions?.join(", ")}</div>
			 <div class="alert alert-info">Reload this page when you're done uploading</div>


		 </div>
	 </div>






    <div class="modal" id="videoModal">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>
            <h3>Video</h3>
        </div>
        <div class="modal-body">
            <a id="videoModalA" class="video"></a>
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery(".close").click(function(){
                jQuery("#videoModal").hide();
            });
            jQuery(".videoLink").click(function(){
                var tempId = this.id.replace("videoLink","");
                jQuery("#videoModal").show();
                flowplayer("videoModalA", "${resource(dir:'js/flowplayer',file:'flowplayer-3.2.8.swf')}",
                        {
                            plugins: {
                                controls: {
                                    url: "${resource(dir:'js/flowplayer',file:'flowplayer.controls-3.2.8.swf')}"
                                }
                            },
                            clip:  {
                                url: "/download/vid/"+tempId,
                                autoPlay: false,
                                autoBuffering: true
                            }
                        }
                );
            });
        });

    </script>

    </body>
</html>
