<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>

    <g:set var="action" value="learningEncounterAdd"/>
    <g:set var="navaction" value="myPortfolios"/>
    <g:if test="${viewingFriend}">
        <g:set var="action" value="NONE"/>
        <g:set var="navaction" value="sharedFriend"/>
    </g:if>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
> My portfolios
</div>
    <g:render template="viewingFriend" model="['friend':viewingFriend]"/>
<g:if test="${portfolios.findAll{it?.status==lf.StatusCode.PORTFOLIO_INUSE}.size()>0}">
    <g:each var="portfolio" in="${portfolios.sort{it?.topic?.checklist}.sort{it?.topic?.name}}">
<g:if test="${portfolio.status==lf.StatusCode.PORTFOLIO_INUSE}">
<g:render template="/shared/displayPortfolio" model="['csCounts':csCounts,'viewingFriend':viewingFriend, 'portfolio':portfolio, 'friends':myFriends, 'teachers':myTeachers,'user':user]"/>
</g:if>
</g:each>
</g:if>
	 <g:else>
		 You haven't selected any languages to work on!
		 <br/><br/>
		 <g:link class="btn btn-info" controller="account" action="editPortfolios" id="${myPortfolio.id}">Select a language to work on</g:link>
	 </g:else>
    <script>
        jQuery(function () {
            jQuery('#tabs').tab();
        })
    </script>

    </body>
</html>
