<g:if test="${viewingFriend}">
    <div class="alert alert-danger">
        You are viewing information shared to you by ${viewingFriend}
    </div>
</g:if>