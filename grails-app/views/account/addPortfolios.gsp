<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
		<resource:tabView/>
    </head>
    <body>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
> Add Portfolios
</div>


<g:each var="folio" in="${folios}">
<g:render template="/shared/listPortfolio" model="['folio':folio, 'user':user]"/>
</g:each>


    </body>
</html>
