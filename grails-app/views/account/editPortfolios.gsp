<html>
<head>
    <title>eLinguafolio</title>
    <meta name="layout"
          content="${session.layout ?: grailsApplication.config.lf.layout}"/>
</head>

<body>

<g:set var="action" value="updatePortfolios"/>
<g:set var="navaction" value="myPortfolios"/>
<g:if test="${viewingFriend}">
    <g:set var="action" value="NONE"/>
    <g:set var="navaction" value="sharedFriend"/>
</g:if>


<div class="navcrumbs">
    <g:link controller="account" action="index">Home</g:link>

    >
    <g:link action="${navaction}" id="${myportfolio.id}">${myportfolio.folio}</g:link>
    >
    Edit my portfolios
</div>

<g:render template="viewingFriend" model="['friend':viewingFriend]"/>


<h2 class="biography">Select my active subjects for this portfolio</h2>
<ul class="tabs nav nav-tab" data-tabs="tabs">
    <g:each var="folio" in="${folios}">
        <li class="active"><a data-toggle="tab" href="#f_${folio.id}"><!--${folio}--></a></li>
    </g:each>
</ul>

            <g:form method="post" action="${action}">
<div class="tab-content">
    <g:each var="folio" in="${folios}">
        <div class="tab-pane active" id="f_${folio.id}">

                <g:each var="cl" in="${lf.Checklist.findAllByFolio(folio)}">
                    <div class="portfolioList span2">
                        <lf:displayChecklistPortfolios checklist="${cl}"
                                                       portfolios="${portfolios}"/>
                    </div>
                </g:each>
                <div class="clear"></div>
                <g:if test="${viewingFriend==null}">
						 <div class="row">
							 <div class="span12 centered">
                <g:submitButton name="Update" value="Update" class="centered btn btn-info"/>
							 </div>
						 </div>
                    </g:if>

        </div>
    </g:each>
            </g:form>
</div>

<script>
    $(function () {
        $('#tabs').tab();
    })
</script>
</body>
</html>
