<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errors}">
            <div class="errors">
                <g:renderErrors bean="${flash.errors}" as="list" />
            </div>
            </g:if>

<% def lastSkill='' %>
<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
 > 
<g:link controller="account" action="viewPortfolios" id="${portfolio.myportfolio.id}">My portfolios</g:link>

 > 
${portfolio.topic.fullName()} - ${scale.scaleSet.name} - ${scale.name}
</div>

<g:set var="clist" value="${portfolio.topic.checklist.assessmentStatements.sort{it.sortOrder}.toList()}"/>
<%
clist << "N/A"
%>
<g:form method='post' action='updateCandoStatus' >
<table id="candoStatements">
<g:each var="cs" in="${candoStatements}">

<% if(cs.skill.name!=lastSkill) { %>
<tr><td colspan="3"><h2>${cs.skill.name}</h2></td></tr>
<tr>
<th id="header_statement">Statement</th>
<th id="header_assessment">Assessment</th>
<th id="header_evidence">Evidence</th>
</tr>
<tr>
<td></td>
<td>
<g:each var='c' in="${clist}">
<div style="width: 120px; float:left; ">${c}</div>
</g:each>
</td>
<td></td>
</tr>

<% } 
lastSkill = cs.skill.name
%>


<tr class="statementRow">
<td>
    <lf:renderStatement originalStatement="${cs.statement}"
                        topic="${portfolio.topic}" cando="${cs}"/>

    <attachments:each bean="${cs}">
        <br/>
        <br/>
        <img class="portfolioCandoImage" src="/${attachment?.imagePath}"/>
    </attachments:each>
</td>
<g:set var="mycandoentry" value="${myCandos.find{it.statement==cs}}"/>
<td>
<g:each var='c' in="${clist}">
<div style="width: 120px; float:left; ">
<g:set var="checked" value="${false}"/>
<g:if test="${mycandoentry?.candoStatus.toString()==c.toString()}">
<g:set var="checked" value="${true}"/>
</g:if>
<g:radio name="candoStatus.${mycandoentry?.id}" checked="${checked}" value="${c}"/>
</div>
</g:each>
</td>
<td>
    <lf:displayEvidencesCount portfolioId="${portfolio.id}" topic="${portfolio.topic.id}" evidences="${mycandoentry?.evidences}" candoentryid="${mycandoentry.id}" id="${cs.id}"/>
</td>
</tr>

</g:each>
</table>
<input type="submit" value="Update" class="btn"/>
</g:form>

    <input type='button' id="toggleLang" style="display:none;" value="Toggle language"/>

    <script>
        var lang = "noneng";
        if(jQuery(".alt_statement").size()>0) {
            jQuery("#toggleLang").show();
            jQuery("#toggleLang").click(function(){
                if(lang=="noneng") {
                    jQuery(".alt_statement").hide();
                    jQuery(".eng_statement").show();
                    lang="eng";
                } else {
                    jQuery(".alt_statement").show();
                    jQuery(".eng_statement").hide();
                    lang="noneng";
                }
            });
        }
    </script>
    </body>
</html>
