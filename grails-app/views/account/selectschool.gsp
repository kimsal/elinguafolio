<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
        <title>Select your school</title>
				<resource:autoComplete skin="default" />
    </head>
    <body>
        <div class="body">
            <h2>Select your school</h2>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${studentInstance}">
            <div class="errors">
                <g:renderErrors bean="${studentInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="setschool" method="post" >
						<input type='hidden' name='school' id='schoolid' value=''/>
<g:if test="${userType=='student'}">
						<p>
							<label for="grade">What grade are you in?</label>
							<input type='text' name='grade' id='grade'/>
						</p>
</g:if>
						<p>
<g:if test="${userType=='student'}">
						What school do you attend?
</g:if>
<g:if test="${userType=='teacher'}">
						What school do you teach at?
</g:if>
<g:if test="${userType=='user'}">
						What school do you represent?
</g:if>
						</p>
<richui:autoComplete 
minQueryLength = "1"
queryDelay=".1"
class="schoolselect"
shadow="false"
name="name" 
action="${createLinkTo('dir': 'account/schoolsearch')}" 
onItemSelect = "f(id);"
/>
<script>
function f(i)
{
	document.getElementById('schoolid').value = i;
	document.getElementById('submitbutton').disabled = false;
}
</script>
                <div class="buttons">
                    <span class="button"><g:submitButton class="btn" id="submitbutton" disabled disabled="true" name="create" class="save" value="Set my school info"/></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
