<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
</head>
<body>

<div class="navcrumbs">
    <g:link controller="account" action="index">Home</g:link>
    > My friends
</div>

<div class="container">
 <div class="row">
<div class="friendsmain span9">

        <h2>My friends</h2>

<p class="sharing_warning">
When you are friends with someone, they can
    see all of your information (biography and global profile) <strong>except</strong>
    for your specific language portfolios.  Each language portfolio
    will need to be shared and unshared individually after you've become friends.
<br/><br/>
    If you cancel a friendship, the other person will not be able to
    see any of your information.
</p>

        <g:each var="f" in="${friends.findAll{it!=session.user}}">
            <div class="userinfo_confirmed">
                <div class="userinfo_sharing">
                    <lf:userinfo user="${f}" linkShared="${true}" cancel="${true}"/>
                </div>
            </div>
        </g:each>


    </div>


<div class="span3">

    <div class="friendsidebar">

    <h3>My friend code</h3>
    <form>
        <input type='text' length="40" name='code' value="${session.user.myCode}"/>
    </form>
    <p>Copy and send this code to your friends to let them share your portfolios.</p>

    </div>

    <div class="friendsidebar">
        <h3>Add a friend</h3>
    <g:form method="post" action="initiateFriend">
        <p>Enter your friend's code and we'll send them a friend request.</p>
        <p>
            <g:textField name="friendCode"/>
            <g:submitButton class="btn btn-primary" name="submit" value="Submit this code"/>
        </p>
    </g:form>
    </div>

    <div class="friendsidebar">

        <h3>Pending requests</h3>
    <g:if test="${requests.size()>0}">
        <p>
            You've given these people your code and now
            they'd like to be your friend.  Please confirm or reject them.
        </p>

        <g:each var="r" in="${requests}">
            <div class="userinfo userinfo_unconfirmed">
                <lf:userinfo user="${r }" confirm="${true}"/>
            </div>
            <div class="clear"></div>
        </g:each>
    </g:if>
    <g:else>
        <p>
            You have no friend requests right now.
        </p>
    </g:else>
     </div>


    </div>
    </div>

</body>
</html>
