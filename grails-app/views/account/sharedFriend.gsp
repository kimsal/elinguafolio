<html>
<head>
    <meta name="layout"
          content="${session.layout ?: grailsApplication.config.lf.layout}"/>
</head>

<body>

<div class="navcrumbs">
    <g:link controller="account" action="index">Home</g:link>
    >
    ${viewingFriend}'s portfolios
</div>


<g:render template="viewingFriend" model="['friend':viewingFriend]"/>


<ul class="nav nav-tabs" data-tabs="tabs">
    <% def loopcount = 0 %>
    <g:each var="p" in="${myportfolios}">
        <li class="<% if (loopcount==0) { %>active<% }; loopcount++ %>"><a data-toggle="tab" href="#f_${p.folio.id}">${p.folio.title}</a></li>
    </g:each>
</ul>

<div class="tab-content">
    <g:each var="p" in="${myportfolios}">
        <div class="tab-pane active" id="f_${p.folio.id}">
            <div class="listportfolio">
                <div class="listportfolio_links">

                    <div class="listportfolio_block">
                        <div class="listportfolio_image">
                            <img src="<g:resource dir="images" file="bio.png"/>"
                                 width="128" height="128"/>

                            <h1>Biography</h1>
                        </div>
                    </div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="biography"
                                    id="${p.id}">How Do I Learn?</g:link>
                        </div>
                    </div>


                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="activity"
                                    id="${p.id}">Learning Activities</g:link>
                        </div>
                    </div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="viewPortfolios"
                                    id="${p.id}">"I Can" Proficiency Checklists and Learning Inventory</g:link>
                        </div>
                    </div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="experience"
                                    id="${p.id}">Learning Experiences</g:link>
                        </div>
                    </div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="encounter"
                                    id="${p.id}">Learning Encounters</g:link>
                        </div>
                    </div>


                    <div class='clear'></div>
                    <hr class="bar"/>

                    <div class="clear"></div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_image">
                            <img src="<g:resource dir="images" file="info.png"/>"
                                 width="128" height="128"/>

                            <h1>Global Profile</h1>
                        </div>
                    </div>


                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="passport"
                                    id="${p.id}">Background Information</g:link>
                        </div>
                    </div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="editPortfolios"
                                    id="${p.id}">Subjects I am Learning</g:link>
                        </div>
                    </div>

                    <div class="listportfolio_block">
                        <div class="listportfolio_link">
                            <g:link action="viewPortfolios"
                                    id="${p.id}">Proficiency Grids</g:link>
                        </div>
                    </div>

                    <div class='clear'></div>
                    <hr class="bar"/>

                    <div class="clear"></div>

                </div>
            </div>
        </div><!--f_id#-->
    </g:each>
</div>


<script>
    $('#tabs').tab();
</script>


<div style="display:none">
    <p><g:link action="viewPortfolios">View my portfolios</g:link></p>

    <p><g:link action="editPortfolios">Edit my portfolios</g:link></p>
</div>
</div>

<div class="clear"></div>

</body>
</html>
