<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
	<resource:tabView/>
    </head>
    <body>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
> My friends
</div>

<g:if test="${flash.message}">
<div class="message">${flash.message}</div>
</g:if>

<g:if test="${flash.errors}">
<div class="errors">${flash.errors}</div>
</g:if>




<richui:tabView id="tabview">

    <richui:tabLabels>
            <richui:tabLabel selected="true" title="Add a friend"/>
            <richui:tabLabel title="My friends"/>
            <richui:tabLabel title="Requests sent"/>
            <richui:tabLabel title="Requests from others"/>
    </richui:tabLabels>

    <richui:tabContents>
        <richui:tabContent>
<h2>My friend code is ${session.user.myCode}</h2>
<p>Give this code to your friends to let them share your portfolios.</p>

<div id="addFriend">
<h2>Add a friend</h2>
<g:form method="post" action="initiateFriend">
<p>
Do you have a friend's code? Enter it here! 
</p>
<p>
<g:textField name="friendCode"/>
<g:submitButton class="btn" name="submit" value="Submit this code"/>
</p>
</g:form>
</div>
        </richui:tabContent>

        <richui:tabContent>
<div id="currentFriends">
<h2>My friends</h2>
<g:each var="friend" in="${friends.findAll{it!=session.user}}">
<div class="userinfo_confirmed">
<lf:userinfo user="${friend }"/>
<div class="userinfo_sharing">
</div>
<p>
<g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="cancelFriend" id="${friend.id}">Cancel friend</g:link>
</p>
</div>
</g:each>

<h2>My teachers</h2>
<g:each var="friend" in="${teachers.findAll{it!=session.user}}">
<div class="userinfo_confirmed">
<lf:userinfo user="${friend }"/>
<div class="userinfo_sharing">
</div>
<p>
<g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="cancelFriend" id="${friend.id}">Cancel friend</g:link>
</p>
</div>
</g:each>

</div>
        </richui:tabContent>

        <richui:tabContent>


<div id="unconfirmedFriends">
<h2>Waiting to hear back from....</h2>
<g:each var="friend" in="${waitingToHearBackFriends.findAll{it!=session.user}}">

	<div class="userinfo userinfo_unconfirmed">
	<lf:userinfo user="${friend }"/>
	<p>
	<g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="cancelFriend" id="${friend.id}">Cancel request</g:link>
	</p>
	</div>

</g:each>
<div class="clear"></div>
<h2>Waiting to hear back from teachers....</h2>
<g:each var="friend" in="${waitingToHearBackTeachers.findAll{it!=session.user}}">

	<div class="userinfo userinfo_unconfirmed">
	<lf:userinfo user="${friend }"/>
	<p>
	<g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="cancelFriend" id="${friend.id}">Cancel request</g:link>
	</p>
	</div>

</g:each>

</div>

        </richui:tabContent>
        <richui:tabContent>

<div id="friendRequests">
<h2>People who want to be friends with you...</h2>
<g:each var="friend" in="${requests.findAll{it.userType=='student'}}">
	<div class="userinfo userinfo_unconfirmed">
		<lf:userinfo user="${friend }"/>
		<p>
		<g:link action="confirmFriend" id="${friend.id}">Confirm friend</g:link>
		</p>
		<p>
		<g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="denyFriend" id="${friend.id}">Deny request</g:link>
		</p>

	</div>
</g:each>
<div class="clear" style="clear:both;"></div>
<h2>Teachers who want to be friends with you...</h2>
<g:each var="friend" in="${requests.findAll{it.userType=='teacher'}}">
	<div class="userinfo userinfo_unconfirmed">
		<lf:userinfo user="${friend }"/>
		<p>
		<g:link action="confirmFriend" id="${friend.id}">Confirm friend</g:link>
		</p>
		<p>
		<g:link onClick="if(!confirm('Are you sure?')) { return false; }" action="denyFriend" id="${friend.id}">Deny request</g:link>
		</p>

	</div>
<div class="clear" style="clear:both;"></div>
</g:each>
<div class="clear"></div>

</div>


        </richui:tabContent>
        </richui:tabContents>
        </richui:tabView>




    </body>
</html>
