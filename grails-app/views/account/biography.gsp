<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
<gui:resources components="dialog"/>
    </head>
    <body>

    <g:set var="action" value="updateHowDoILearn"/>
    <g:set var="navaction" value="myPortfolios"/>
    <g:if test="${viewingFriend}">
        <g:set var="action" value="NONE"/>
        <g:set var="navaction" value="sharedFriend"/>
    </g:if>


    <div class="navcrumbs">
<g:link action="index">Home</g:link> 
>
<g:link action="${navaction}" id="${myportfolio.id}">${myportfolio.folio}</g:link>
> 
My biography
</div>

            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

<g:render template="viewingFriend" model="['friend':viewingFriend]"/>

<h2 class="biography">My Biography for How I Learn - ${mybio.bio.folio}</h2>


<g:form action="${action}">
<g:each var='sec' in ="${mybio.bio.sections?.sort{it.sortOrder}}">
<h3 class="biography">${sec}</h3>
<lf:renderSectionHeader type="${sec.sectionType}"/>

<g:each var="st" in="${sec.statements.sort{it.sortOrder}}">
<lf:renderBioStatement viewingFriend="${viewingFriend}" statement="${st}" user="${user}" type="${sec.sectionType}"/>
 </g:each>
</g:each>
<g:if test="${viewingFriend==null}">
    <g:submitButton class="btn" class="btn" name="submit" value="Update!"/>
</g:if>
</g:form>
    </body>
</html>
