<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
<gui:resources components="dialog"/>
<resource:tabView/>
    </head>
    <body>


    <g:set var="action" value="learningExperienceAdd"/>
    <g:set var="navaction" value="myPortfolios"/>
    <g:if test="${viewingFriend}">
        <g:set var="action" value="NONE"/>
        <g:set var="navaction" value="sharedFriend"/>
    </g:if>


    <div class="navcrumbs">
<g:link action="index">Home</g:link> 
>
<g:link action="${navaction}" id="${myportfolio.id}">${myportfolio.folio}</g:link>
> 
My Learning Experiences
</div>

            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
    <g:render template="viewingFriend" model="['friend':viewingFriend]"/>

    <h2 class="biography">My Learning Experiences for ${myportfolio.folio.title}</h2>

<div id="lexdiv">


<g:if test="${viewingFriend==null}">
<div id="lex_add">
<h2>Add a Learning Experience</h2>
<g:form action="learningExperienceAdd">
<input type='hidden' name='p' value="${myportfolio.id}"/>

<p>
<label for="description">
<p>Write a short summary about your experience.  Include the following information:</p>
<ul>
<li>language spoken</li>
<li>location</li>
<li>length of stay</li>
<li>purpose</li>
<li>summary of your feelings</li>
</ul>
</label>
<g:textArea name="description" rows="8" cols="120"></g:textArea>
</p>
<p>
<label for="date">When</label>
<input type="text" name="date" size="50"/>
</p>
<p>
<label for="experience">Type of Experience</label>
<g:select name="experience" from="${new lf.LanguageExperience().constraints.experience.inList.toList()}"/>
</p>
<p>
<label for="topic">Language</label>
<g:select name="topic" optionKey="id" value="fullName()" from="${myportfolio.portfolios.findAll{it.status==lf.StatusCode.PORTFOLIO_INUSE}*.topic}"/>
</p>
<br/>
<div class="centered">
<g:submitButton class="btn primary" name="add" value="Add experience"/>
</div>
</g:form>

</div>
    </g:if>



<div id="lex_list">
<h2>Learning Experiences</h2>
<table class="table">
<tr>
<th width="35%">Summary</th>
<th>Topic</th>
<th>When</th>
<th><th/>
</tr>

<g:each var="a" in="${new lf.LanguageExperience().constraints.experience.inList.toList()}">
<g:set var="l" value="${experienceList.experiences.findAll{it.experience==a}}"/>

<g:if test="${l.size()>0}">
<tr><td colspan="3">
<h3>${a}</h3>
</td>
</tr>
<g:each var="ac" in="${l}">
<tr>
<td>${ac.description}</td>
<td>${ac.topic.fullName()}</td>
<td>
    <g:if test="${viewingFriend==null}">
<g:link onclick="if(!confirm('Are you sure?')) { return false; } else { return true; }"
 action="learningExperienceDel" id="${ac.id}" class="btn danger">del</g:link>
        </g:if>
</td>
</tr>
</g:each>
</g:if>

</g:each>
</table>



</div>


</div>



<script>
function error(e) { 
	alert('e' + e);
}
function message(e) { 
	alert('m' + e);
}
</script>
    </body>
</html>
