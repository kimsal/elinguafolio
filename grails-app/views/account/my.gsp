<html>
<head>
    <meta name="layout"
          content="${session.layout ?: grailsApplication.config.lf.layout}"/>
    <resource:autoComplete skin="default"/>
</head>

<body>

<div class="navcrumbs">
    <g:link action="index">Home</g:link>
    >
    My account
</div>

<g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
</g:if>
<g:if test="${flash.errors}">
    <div class="errors">
        <g:renderErrors bean="${flash.errors}" as="list"/>
    </div>
</g:if>


<div class="row">
    &nbsp;
</div>

<div class="row">

            <div class="myInfo span6">
                <h2><i class="icon-key"></i> My password</h2>

                <p>Enter a new password here</p>
                <g:form action="updatePassword">
                    <p><label for="password">Password</label><g:passwordField
                            name="password" value=""/></p>

                    <p><label for="password2">Confirm password</label><g:passwordField
                            name="password2" value=""/></p>
                    <g:submitButton class="btn primary" name="Update"
                                    value="Update my password"/>
                </g:form>
            </div>

            <div class="myInfo span6">
                <h2><i class="icon-envelope"></i> My email address</h2>

                <p>Current email is <em>${session.user.email}</em></p>
                <g:form action="updateEmail">
                    <p><label for="email">Email address</label><g:textField name="email"
                                                                            value=""/></p>

                    <p><label for="email2">Confirm email address</label><g:textField
                            name="email2" value=""/></p>
                    <g:submitButton class="btn primary" name="Update"
                                    value="Update my email"/>
                </g:form>
            </div>

</div>
<div class="row">
    &nbsp;
</div>

<div class="row">

            <div class="myInfo span6">
                <h2><i class="icon-camera"></i> My photo</h2>

                <lf:userphoto user="${session.user}"/>
                <div class="clear"></div>

                <p>Upload a new photo here</p>

                <p>

                    <fileuploader:photoform upload="docs"
                                            buttonValue="Upload photo"
                                            successAction="updatePhoto"
                                            successController="account"
                                            errorAction="updatePhoto"
                                            errorController="account"/>

                </p>

            </div>

<div class="span6">
            <g:form action="updateschool" method="post">
                <input type='hidden' name='school' id='schoolid' value=''/>

                <h2><i class="icon-book"></i> My school and grade</h2>

                <g:if test="${userType == 'student'}">
                    <p>Current grade: ${session.user.grade}</p>

                    <p>
                        <input type='text' name='grade' value='${session.user.grade}'/>
                    </p>

                </g:if>
                <p>Current school: ${session.user.school}</p>

                <p>

                    <g:if test="${userType == 'student'}">
                        What school do you attend?
                    </g:if>
                    <g:if test="${userType == 'teacher'}">
                        What school do you teach at?
                    </g:if>
                    <g:if test="${userType == 'user'}">
                        What school do you represent?
                    </g:if>
                </p>
                <richui:autoComplete
                        minQueryLength="3"
                        queryDelay="1"
                        class="schoolselect"
                        shadow="false"
                        name="name"
                        action="${createLinkTo('dir': 'account/schoolsearch')}"
                        onItemSelect="f(id);"/>
                <script>
                    function f(i) {
                        document.getElementById('schoolid').value = i;
                        document.getElementById('submitbutton').disabled = false;
                    }
                </script>
                <g:submitButton class="btn primary" id="submitbutton" name="Update"
                                value="Update my school or grade"/>
            </g:form>

</div>
</div>




</body>
</html>
