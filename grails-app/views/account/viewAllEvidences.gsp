<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errors}">
            <div class="errors">
                <g:renderErrors bean="${flash.errors}" as="list" />
            </div>
            </g:if>
<div class="navcrumbs">
<g:link action="index">Home</g:link> 
> All Evidences
</div>


<g:link class="mbutton" controller="download" action="all">Click here to download all evidences</g:link>
<br/>
<br/>

<lf:displayEvidences evidences="${evidences}" user="${user}"/>
<!--
<table>
<tr>
<th>Date uploaded</th>
<th>File name</th>
<th>Link</th>
</tr>
<g:each var="e" in="${evidences}">
<tr>
<td>
</td>
</tr>

</g:each>
</table>
-->

    </body>
</html>
