<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
		<resource:tabView/>
		<!--g:javascript library="yui" /-->
    </head>
    <body>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
> ${myportfolio.folio} 
</div>

<h2>${myportfolio.folio}</h2>
<p>
<g:link action="biography" id="${myportfolio.id}">View and edit my biography for this portfolio</g:link>
</p>
<p>
<g:link action="viewPortfolios" id="${myportfolio.id}">View and edit my subjects</g:link>
</p>

<p>
<g:link action="editPortfolios" id="${myportfolio.id}">Change my subjects in this portfolio</g:link>
</p>



    </body>
</html>
