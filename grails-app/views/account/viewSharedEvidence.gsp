<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />

<gui:resources components="dialog"/>
				<g:javascript library="jquery"/>
<script type="text/javascript" src="<g:resource dir="js/player" file="projekktor.js" />"></script>
<link rel="stylesheet" href="<g:resource dir="js/player/style/projekktor_theme_tll" file="style.css"/>" type="text/css" media="screen" />
    </head>
    <body>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errors}">
            <div class="errors">
                <g:renderErrors bean="${flash.errors}" as="list" />
            </div>
            </g:if>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
 > 
<g:link controller="account" action="shares" id="${portfolio.myportfolio.folio.id}">Portfolios shared with me</g:link>
 > 
<g:link controller="account" action="viewShared" id="${portfolio.id}">${portfolio.topic}</g:link>
>

<g:link mapping="candoviewShared" controller="account" action="viewSharedStatements" params='[portfolio:"${portfolio.id}", skill:"${skill.id}",  scale:"${scale.id}"]'>
${skill.name} - ${scale.scaleSet.name}/${skill.name}
</g:link>
>
${candoStatement?.statement}
</div>

<g:render template="viewingFriend" model="['viewingFriend':shared.portfolio.myportfolio.owner]"/>



<g:if test="${evidences}">
	 <div class="row">
		 <div class="span8">
			 <table class="table">
				 <tr>
					 <th></th>
					 <th>Name</th>
					 <th>Uploaded</th>
				 </tr>
				 <g:each in="${evidences}" var="e">
					 <tr>
						 <td>
							 <g:link onClick="if(!confirm('Are you sure?')) { return false; }" controller="account" action="deleteEvidence" id="${e.id}">
								 <i class="icon-remove"></i>
							 </g:link>
							 <g:link class="evidenceFileLink" controller="download" id="${e.file.id}"><i class="icon-arrow-down"></i></g:link>
						 </td>
						 <td>
							 <g:link class="evidenceFileLink" controller="download" id="${e.file.id}">${e.file.name}</g:link>
						 </td>
						 <td>
							 ${e.dateCreated.prettyDate()}
						 </td>
					 </tr>
				 </g:each>
			 </table>


		 </div>

	 </div>
</g:if>
	 <g:else>
		 No evidences found
	 </g:else>
    </body>
</html>
