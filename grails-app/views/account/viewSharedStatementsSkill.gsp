<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errors}">
            <div class="errors">
                <g:renderErrors bean="${flash.errors}" as="list" />
            </div>
            </g:if>

<% def lastScale='' %>


<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
 >
<g:link controller="account" action="viewPortfolios" id="${portfolio.myportfolio.id}">My portfolios</g:link>

 >
${portfolio.topic.fullName()} - ${skill.name}
</div>

<g:render template="viewingFriend" model="['viewingFriend':shared.portfolio.myportfolio.owner]"/>

<g:set var="clist" value="${portfolio.topic.checklist.assessmentStatements.sort{it.sortOrder}.toList()}"/>
<%
clist << "N/A"
%>
<table id="candoStatements">


<g:each var="cs" in="${candoStatements}">

<% if(lastScale!=cs?.scale?.scaleSet?.name +"-"+cs?.scale?.name) { %>

<tr>
<td colspan="3"><h2>${cs?.scale?.scaleSet?.name} - ${cs?.scale?.name}</h2></td>
</tr>
<tr>
<th id="header_statement">Statement</th>
<th id="header_assessment">Assessment</th>
<th id="header_evidence">Evidence</th>
</tr>
<tr>
<td></td>
<td>
<g:each var='c' in="${clist}">
<div style="width: 120px; float:left; ">${c}</div>
</g:each>
</td>
<td></td>
</tr>





<% }
lastScale=cs?.scale?.scaleSet?.name +"-"+cs?.scale?.name
%>

<tr class="statementRow">
<td>
    <lf:renderStatement originalStatement="${cs.statement}"
                        topic="${portfolio.topic}" cando="${cs}"/>
    <attachments:each bean="${cs}">
        <br/>
        <br/>
        <img class="portfolioCandoImage" src="/${attachment?.imagePath}"/>
    </attachments:each>
</td>
<g:set var="mycandoentry" value="${myCandos.find{it.statement==cs}}"/>
<td>
<g:each var='c' in="${clist}">
<div style="width: 120px; float:left; ">
<g:set var="checked" value="${false}"/>
<g:if test="${mycandoentry?.candoStatus.toString()==c.toString()}">
<g:set var="checked" value="${true}"/>
</g:if>
<g:radio name="candoStatus.${mycandoentry?.id}" checked="${checked}" value="${c}" disabled="disabled"/>
</div>
</g:each>
</td>
<td>

<lf:displayEvidencesCountShared evidences="${mycandoentry?.evidences}" portfolio="${portfolio.id}" id="${cs.id}"/>
</td>
</tr>

</g:each>
</table>

    <input type='button' id="toggleLang" style="display:none;" value="Toggle language"/>

    <script>
        var lang = "noneng";
        if(jQuery(".alt_statement").size()>0) {
            jQuery("#toggleLang").show();
            jQuery("#toggleLang").click(function(){
                if(lang=="noneng") {
                    jQuery(".alt_statement").hide();
                    jQuery(".eng_statement").show();
                    lang="eng";
                } else {
                    jQuery(".alt_statement").show();
                    jQuery(".eng_statement").hide();
                    lang="noneng";
                }
            });
        }
    </script>
    </body>
</html>
