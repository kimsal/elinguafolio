<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
		<gui:resources components="dialog"/>
                <resource:tabView/>
    </head>
    <body>

<div class="navcrumbs">
<g:link controller="account" action="index">Home</g:link>
 >
Portfolios shared with me
</div>



    <h2>Portfolios shared with me</h2>
<div class="tabview">


    <div class="tab-content">

        <% def owners = sharedWithMe*.owner?.unique() %>
        <g:each var="o" in="${owners}">
            <div class="container">
                <h4 class="sharing_owner">Shared by ${o}</h4>
                <div class="sharing_listportfolio_links">
                    <g:each var="p" in="${sharedWithMe.findAll {it.owner == o}}">
                        <div class="sharing_listportfolio_block">
                            <div class="sharing_listportfolio_link">
                            <g:link action="viewShared" class="viewShared" id="${p?.portfolio.id}">
                                <i class="icon-list-alt"></i> <p>${p.portfolio.topic}</p>
                            </g:link>
                            </div>
                        </div>
                    </g:each>
                </div>
            </div>
        </g:each>


        </div>
</div>
<div class="clear"></div>

    </body>
</html>
