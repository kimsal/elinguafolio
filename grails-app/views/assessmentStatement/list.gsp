
<%@ page import="lf.AssessmentStatement" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'assessmentStatement.label', default: 'AssessmentStatement')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'assessmentStatement.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="lastUpdated" title="${message(code: 'assessmentStatement.lastUpdated.label', default: 'Last Updated')}" />
                        
                            <g:sortableColumn property="dateCreated" title="${message(code: 'assessmentStatement.dateCreated.label', default: 'Date Created')}" />
                        
                            <th><g:message code="assessmentStatement.checklist.label" default="Checklist" /></th>
                   	    
                            <g:sortableColumn property="isDone" title="${message(code: 'assessmentStatement.isDone.label', default: 'Is Done')}" />
                        
                            <g:sortableColumn property="sortOrder" title="${message(code: 'assessmentStatement.sortOrder.label', default: 'Sort Order')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${assessmentStatementInstanceList}" status="i" var="assessmentStatementInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${assessmentStatementInstance.id}">${fieldValue(bean: assessmentStatementInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${assessmentStatementInstance.lastUpdated}" /></td>
                        
                            <td><g:formatDate date="${assessmentStatementInstance.dateCreated}" /></td>
                        
                            <td>${fieldValue(bean: assessmentStatementInstance, field: "checklist")}</td>
                        
                            <td><g:formatBoolean boolean="${assessmentStatementInstance.isDone}" /></td>
                        
                            <td>${fieldValue(bean: assessmentStatementInstance, field: "sortOrder")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${assessmentStatementInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
