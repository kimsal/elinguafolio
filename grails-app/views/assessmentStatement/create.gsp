
<%@ page import="lf.AssessmentStatement" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'assessmentStatement.label', default: 'AssessmentStatement')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${assessmentStatementInstance}">
            <div class="errors">
                <g:renderErrors bean="${assessmentStatementInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="checklist"><g:message code="assessmentStatement.checklist.label" default="Checklist" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assessmentStatementInstance, field: 'checklist', 'errors')}">
                                    <g:select name="checklist.id" from="${lf.Checklist.list()}" optionKey="id" value="${assessmentStatementInstance?.checklist?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="isDone"><g:message code="assessmentStatement.isDone.label" default="Is Done" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assessmentStatementInstance, field: 'isDone', 'errors')}">
                                    <g:checkBox name="isDone" value="${assessmentStatementInstance?.isDone}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sortOrder"><g:message code="assessmentStatement.sortOrder.label" default="Sort Order" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assessmentStatementInstance, field: 'sortOrder', 'errors')}">
                                    <g:textField name="sortOrder" value="${fieldValue(bean: assessmentStatementInstance, field: 'sortOrder')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="assessmentStatement.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assessmentStatementInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${assessmentStatementInstance?.name}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
