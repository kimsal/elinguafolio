
<%@ page import="lf.CandoEntry" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'candoEntry.label', default: 'CandoEntry')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${candoEntryInstance}">
            <div class="errors">
                <g:renderErrors bean="${candoEntryInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${candoEntryInstance?.id}" />
                <g:hiddenField name="version" value="${candoEntryInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="candoStatus"><g:message code="candoEntry.candoStatus.label" default="Cando Status" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'candoStatus', 'errors')}">
                                    <g:select name="candoStatus" from="${candoEntryInstance.constraints.candoStatus.inList}" value="${candoEntryInstance?.candoStatus}" valueMessagePrefix="candoEntry.candoStatus" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="statement"><g:message code="candoEntry.statement.label" default="Statement" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'statement', 'errors')}">
                                    <g:select name="statement.id" from="${lf.CandoStatement.list()}" optionKey="id" value="${candoEntryInstance?.statement?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="portfolio"><g:message code="candoEntry.portfolio.label" default="Portfolio" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'portfolio', 'errors')}">
                                    <g:select name="portfolio.id" from="${lf.Portfolio.list()}" optionKey="id" value="${candoEntryInstance?.portfolio?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="evidences"><g:message code="candoEntry.evidences.label" default="Evidences" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'evidences', 'errors')}">
                                    
<ul>
<g:each in="${candoEntryInstance?.evidences?}" var="e">
    <li><g:link controller="evidence" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="evidence" action="create" params="['candoEntry.id': candoEntryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'evidence.label', default: 'Evidence')])}</g:link>

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
