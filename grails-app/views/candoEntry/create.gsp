
<%@ page import="lf.CandoEntry" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'candoEntry.label', default: 'CandoEntry')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${candoEntryInstance}">
            <div class="errors">
                <g:renderErrors bean="${candoEntryInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="candoStatus"><g:message code="candoEntry.candoStatus.label" default="Cando Status" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'candoStatus', 'errors')}">
                                    <g:select name="candoStatus" from="${candoEntryInstance.constraints.candoStatus.inList}" value="${candoEntryInstance?.candoStatus}" valueMessagePrefix="candoEntry.candoStatus" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="statement"><g:message code="candoEntry.statement.label" default="Statement" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'statement', 'errors')}">
                                    <g:select name="statement.id" from="${lf.CandoStatement.list()}" optionKey="id" value="${candoEntryInstance?.statement?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="portfolio"><g:message code="candoEntry.portfolio.label" default="Portfolio" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: candoEntryInstance, field: 'portfolio', 'errors')}">
                                    <g:select name="portfolio.id" from="${lf.Portfolio.list()}" optionKey="id" value="${candoEntryInstance?.portfolio?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
