
<%@ page import="lf.CandoEntry" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'candoEntry.label', default: 'CandoEntry')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'candoEntry.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="candoStatus" title="${message(code: 'candoEntry.candoStatus.label', default: 'Cando Status')}" />
                        
                            <th><g:message code="candoEntry.statement.label" default="Statement" /></th>
                   	    
                            <th><g:message code="candoEntry.portfolio.label" default="Portfolio" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${candoEntryInstanceList}" status="i" var="candoEntryInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${candoEntryInstance.id}">${fieldValue(bean: candoEntryInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: candoEntryInstance, field: "candoStatus")}</td>
                        
                            <td>${fieldValue(bean: candoEntryInstance, field: "statement")}</td>
                        
                            <td>${fieldValue(bean: candoEntryInstance, field: "portfolio")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${candoEntryInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
