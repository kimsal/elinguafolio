

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'encryptedData.label', default: 'EncryptedData')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'encryptedData.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="dataItem" title="${message(code: 'encryptedData.dataItem.label', default: 'Data Item')}" />
                        
                            <g:sortableColumn property="decryptedData" title="${message(code: 'encryptedData.decryptedData.label', default: 'Decrypted Data')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${encryptedDataInstanceList}" status="i" var="encryptedDataInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${encryptedDataInstance.id}">${fieldValue(bean: encryptedDataInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: encryptedDataInstance, field: "dataItem")}</td>
                        
                            <td>${fieldValue(bean: encryptedDataInstance, field: "decryptedData")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${encryptedDataInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
