

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'encryptedData.label', default: 'EncryptedData')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/admin')}"><g:message code="default.admin.label"/></a></span>

            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${encryptedDataInstance}">
            <div class="errors">
                <g:renderErrors bean="${encryptedDataInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dataItem"><g:message code="encryptedData.dataItem.label" default="Data Item" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encryptedDataInstance, field: 'dataItem', 'errors')}">
                                    <g:textArea name="dataItem" cols="40" rows="5" value="${encryptedDataInstance?.dataItem}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="decryptedData"><g:message code="encryptedData.decryptedData.label" default="Decrypted Data" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: encryptedDataInstance, field: 'decryptedData', 'errors')}">
                                    
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
