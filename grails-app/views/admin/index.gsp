<html>
    <head>
		<meta name="layout" content="${session.layout ?: grailsApplication.config.lf.layout}" />
    </head>
    <body>

    <h2>Portfolio management</h2>
<div class="adminLinks">

<p>
    <g:link controller="folio">Folios</g:link>
</p>
    <p>
        A folio is the top-level container for topics and checklists
    </p>

<p>
    <g:link controller="checklist">Folio checklists</g:link>
</p>
    <p>
        Manage all the checklist information - can do statements, pictures, etc.
    </p>


    <h2>Users</h2>
<p>
    <g:link controller="teacher" action="index">Teachers</g:link>
</p>
<p>
    <g:link controller="student" action="index">Student</g:link>
</p>

    <h2>Locations</h2>
<p>
    <g:link controller="school" action="index">Schools</g:link>
</p>
    <p>
        <g:link controller="district" action="index">Districts</g:link>
    </p>
    <p>
        <g:link controller="state" action="index">States</g:link>
    </p>
    <p>
        <g:link controller="emaildomain" action="index">Email domains for teachers</g:link>
    </p>

    <h2>Misc</h2>
<p>
    <g:link controller="admin" action="changePassword">Change admin password</g:link>
</p>
<p>
    <g:link controller="adminnews" action="index">Front page news</g:link>
</p>
    <p>
        <g:link controller="notice" action="index">Top banner messages</g:link>
    </p>
    <p>
        <g:link controller="content" action="list">Manage content</g:link>
    </p>
    <p>
        <g:link controller="blogentry" action="index">Manage blog stuff</g:link>
    </p>


</div>

    </body>
</html>
