package lf

import org.apache.tika.Tika
import org.grails.plugins.settings.Setting

class ThumbnailingService {

    static rabbitQueue = 'thumbnail'

	void handleMessage(id) {
		def ev = Evidence.get(id)
		def f = ev?.file
		if(ev==null)
		{
			println "ev is null!"
			return
		}
		def config = new UFile().domainClass.grailsApplication.config
		def cpath = config.convertpath
		def convertImageDimensions = config.thumbnailDimensions ?: "150x150"
		def ffmpeg = config.ffmpegpath

		println "2Received Message: ${id}"

		def command
		def nd

		try {
			switch(f.mime)
			{
				case ["image/jpg", "image/jpeg", "image/png", "image/gif"]:
					nd = f.path + ".thumb.jpg"
					command = cpath + " " + f.path + " -thumbnail " + convertImageDimensions + "  " + nd
					command.execute()
					ev.file.pathToThumbnail = nd
					ev.fileType = "image"
					ev.file.fileType = "image"
					ev.status = "finished"
					ev.file.save()
					ev.save()
					break

				case ["video/quicktime"]:
					ev.fileType = "mov"
					ev.status = "processing"
					ev.save(flush:true)

					nd = f.path + ".thumb.mov"

					def thumbname =  ev.file.path + ".thumb.jpg"
					def thumbnameTemp = ev.file.path + ".temp.jpg"
					def movname = ev.file.path + ".mp4"
					def oggname = ev.file.path + ".ogv"
					command = ffmpeg + " -i " + ev.file.path
					command += " -s 640x480 -y -f flv -acodec libfaac "
					command += "-ac 1 -ab 8k -b:v 224k -ar 22050 " + movname
					def p3 = command.execute()
					p3.waitFor()
					println "full convert="+command
					log.info command
					ev.file.pathToProcessedFile = movname

					/*
					def theoraCommand = ffmpeg2theora + " -o " + oggname
					theoraCommand += " --videoquality 6 --audioquality 6 "
					theoraCommand += " --max_size=640x480 " + ev.file.path
					theoraCommand.execute()
					log.info theoraCommand
					*/

//				def command2 = "convert " + e.file.path + " -thumbnail 150x150  " + base + name

					def thumbCommand = ffmpeg + " -i " +  movname
					thumbCommand += " -ss 5 -vframes 1 " +  thumbnameTemp
					println "thumbvid1="+thumbCommand
					def p1 = thumbCommand.execute()
					p1.waitFor()
					thumbCommand = cpath + " " + thumbnameTemp
					thumbCommand += " -thumbnail 150x150 " + thumbname
					println "thumbvid="+thumbCommand
					def p2 = thumbCommand.execute()
					p2.waitFor()

					def cleanupCommand = "rm " +  thumbnameTemp
					cleanupCommand.execute()
					println cleanupCommand

					ev.file.pathToThumbnail = thumbname

					ev.fileType = "movie"
					ev.file.fileType = "movie"
					ev.status = "finished"
					ev.file.save()
					ev.save()
					break

			}



		} catch (Exception e ) {
			println "e="+e
			println e.getMessage()
			println e.getStackTrace()
		}
		println "Received Message: ${id}"
	}

}
