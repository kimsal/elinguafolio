package lf
import lf.*

class InvService {

    static transactional = false

    def loadByPortfolioAndInventory(portfolio, inv, user ) 
		{
			def m = MyInventory.findByPortfolioAndInventory(portfolio,inv)
			if(m==null)
			{
				m = new MyInventory(portfolio:portfolio,inventory:inv, owner: user)
			}

			m
    }
		
	def getLearningInventoryForPortfolioAndUser(p,user)
	{
		def myi = MyInventory.findByPortfolioAndOwner(p, user)
		if(!myi || myi==null)
		{
			def i = Inventory.findByFolio(p.myportfolio.folio)
			if(i==null || !i)
			{
				i = new Inventory()
				i.folio=p.myportfolio.folio
				i.name = "inventory"
				i.validate()
				log.info "errors for inv=" + i.errors
				i.save()
			}
			myi = new lf.MyInventory(inventory:i, owner:user, portfolio: p)
			myi.validate()
			log.info  myi.errors
			myi.save()
			log.info "making a new one"
		}
		log.info myi
		myi
	}	
		
}
