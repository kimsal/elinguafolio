package lf

import lf.*

class TeacherService {

    boolean transactional = true

    def addStudent(Teacher t1, Student s1) {
		t1.addToStudents(s1)
		t1.save()
    }

	def removeStudent(Teacher t1, Student s1)
	{
		t1.removeFromStudents(s1)
		t1.save()
	}

    def addColleague(Teacher t1, Teacher t2, String code) {
		if(t2.myCode==code)
		{
			t1.addToColleagues(t2)
			t1.save()
			t2.save()
		}
    }

	def removeColleague(Teacher t1, Teacher t2)
	{
		t1.removeFromColleagues(t2)
		t1.save()
		t2.save()
	}

}
