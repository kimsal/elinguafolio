package lf
import cr.co.arquetipos.crypto.Blowfish
import org.grails.plugins.settings.*
import lf.*

import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.codehaus.groovy.grails.commons.ConfigurationHolder



class UserService implements ApplicationContextAware {

    boolean transactional = true
	ApplicationContext applicationContext
	def cryptoService
	def mailService	
	def grailsApplication

    def basePath(String path = "/") {
        return applicationContext.getResource(path)?.getFile().toString()
	}

	def encrypt(pw) 
	{
		Blowfish.encryptBase64(pw.getBytes(), "mypass")
	}

	def decrypt(pw)
	{
		Blowfish.decryptBase64(pw,"mypass")
	}

	def goodLogin(user, pass) 
	{
		def u = User.countByUsernameAndPassword(user,pass)
		if(u==0)
		{
			def p = encrypt(pass)
			u = User.countByUsernameAndPassword(user,p)
		}
		if(u==1) 
		{
			return true
		} 
		else 
		{
			return false
		}
	}

	def loadUser(user,pass) 
	{
		def u = User.findByUsernameAndPassword(user,pass)
		if(u==null) 
		{
				def p = encrypt(pass)
				u = User.findByUsernameAndPassword(user,p)
		}
		return u
	}

	def initialStudent(user)
	{
		
		
	}
	
	
/**
 * for a given evidence, return a thumbnail image
 */
	def String getThumbnail(e)
	{
println "e="+e
println "efile="+e?.file?.id
		def thumbnail2 = Setting.valueFor("defaultProfileThumbnail")
println "t2="+thumbnail2
		if(e!=null && e?.file!=null)
		{
println "eptt="+e?.file?.pathToThumbnail
if( e?.file?.pathToThumbnail==null) {
	makethumb(e.file.id)
println "makethumb"
	thumbnail2 = e.file.pathToThumbnail
println "eptt2="+e?.file?.pathToThumbnail
} else { 
			def tempf = new File(e?.file?.pathToThumbnail)
			if(tempf && tempf?.exists())
			{
				thumbnail2 = e.file.pathToThumbnail	
			} else {
				makethumb(e.file.id)
				thumbnail2 = e.file.pathToThumbnail
			}
}
		} else {
		}

		thumbnail2		
	}

	def makethumb(fileid)
	{
		def file = UFile.get(fileid)
		def ext = file.extension.toLowerCase()
		switch(ext.toLowerCase()) 
		{
			case ['jpg','jpeg','gif','png','bmp']:
				try {
					def name = file.path+".thumb"
					def command = "/usr/local/bin/convert " + file.path + " -thumbnail 80x80  " + name
println "nonstatic convert = "+command
					command.execute()
					file.pathToThumbnail = name
					file.save()
				} catch(Exception ex) {
					println ex.message
				}
				break

			default:

			break

		}
	}


	static String getThumbnailStatic(e)
	{
		def thumbnail2 = Setting.valueFor("defaultProfileThumbnail")

		if(e!=null && e?.file!=null)
		{
			if(new File(e?.file?.pathToThumbnail)?.exists())
			{
				thumbnail2 = e.file.pathToThumbnail
			} else {
				makethumbStatic(e.file.id)
				thumbnail2 = e.file.pathToThumbnail
			}
		} else {
		}

		thumbnail2
	}

	static makethumbStatic(fileid)
	{
		def file = UFile.get(fileid)
		def ext = file.extension.toLowerCase()

		switch(ext.toLowerCase())
		{
			case ['jpg','jpeg','gif','png','bmp']:
				try {
					def name = file.path+".thumb"
					def command = "/usr/local/bin/convert " + file.path + " -thumbnail 80x80  " + name
println "static convert = "+command
					command.execute()
					file.pathToThumbnail = name
					file.save()
				} catch(Exception ex) {
					println ex.message
				}
				break

			default:

				break

		}
	}

	def sharePortfolioWith(owner, sharedWith, portfolio)
	{
		def sh = Sharing.findByOwnerAndSharedWith(owner, sharedWith).findAll { it.portfolio==portfolio}

		
						
	}

	static getRelationship(User user, User friend)
	{
		def f = Relationship.findByParty1AndParty2(user, friend)
		if(f) {
			return f
		}  else {
			def f2 = Relationship.findByParty1AndParty2(friend, user)
			if(f2) {
				return f2
			}
		}
		return null
	}

	/**
	 * get all relationships where the 'user' is the owning side
	 */
	static getAllMyRelationships(User user)
	{
		def f = Relationship.findAllByParty1(user)
		def f1 = Relationship.findAllByParty2(user)
		f +f1
	}

	/**
	 * get all relationships where the 'user' is the owning side
	 */
	static getAllMyRelationshipsByStatus(User user, String status)
	{
		def f = Relationship.findAllByParty1AndStatus(user, status)
		f
	}

	/**
	 * get all relationships where I'm the party2 and initiated
	 */
	static getAllMyFriendRequests(User user)
	{
		def f = Relationship.findAllByParty2AndStatus(user, StatusCode.INITIATED.toString()).collect{it.party1}
		f
	}


	/**
	 * get all relationships where the user is on either side
	 * (whether owning or a recipient of a request)
	 */
	static getAllRelationships(User user)
	{
		def f = Relationship.findAllByParty1(user)
		def f2 = Relationship.findAllByParty2(user)
		f+f2
	}

	static getAllByStatus(User user, User friend, String status)
	{
		def f = Relationship.findAllByParty1AndParty2(user, friend).findAll{it.status.toString()==status}
		def f2 = Relationship.findAllByParty1AndParty2(friend, user).finaAll{it.status.toString()==status}
		f+f2
	}

	static getAllTeachers(User user, User friend, String status)
	{
		def all = getAllRelationships(user, friend, status)
		all.findAll{
			(it.party1==user && it.party2.userType=='teacher')
				or
			(it.party2==user && it.party1.userType=='teacher')
		}

	}


	static getAllFriends(User user)
	{
		def all = getAllRelationships(user)
		def friends = []
		all.each { r->
			if(r.party2==user && r.party1.userType=='student')
			{
				friends << r.party1
			}
			if(r.party1==user && r.party2.userType=='student')
			{
				friends << r.party2
			}
		}
		friends

	}


	static getAllFriendsByStatus(User user, String status)
	{
		def all = getAllRelationships(user)
		def friends = []
		all.each { r->
			if(r.status==status) {
				if(r.party2==user && r.party1.userType=='student')
				{
					friends << r.party1
				}
				if(r.party1==user && r.party2.userType=='student')
				{
					friends << r.party2
				}
				if(r.party1==user && r.party2.userType=='teacher' && r.party1.userType=='teacher')
				{
					friends << r.party2
				}
				if(r.party2==user && r.party2.userType=='teacher' && r.party1.userType=='teacher')
				{
					friends << r.party2
				}

			}
		}
		friends
	}

	static getAllTeachersByStatus(User user, User friend, String status)
	{
		def all = getAllTeachers(user, friend, status)
		all.findAll{it.status.toString()==status}.collect{it.party2}
	}

	static getAllMyFriendsByStatus(User user, String status)
	{
		def all = getAllMyRelationships(user)
		def g = []
		all.each { it->
			if(it.status.toString()==status && it.party2.userType=='student') 
			{
				g<<it.party2
			}
		}
		g
	}

	static getAllMyTeachersByStatus(User user, String status)
	{
		def all = getAllMyRelationships(user)
println all
		def g = []
		all.each { it->
			if(it.status.toString()==status && it.party1.userType=='teacher') 
			{
				g<<it.party1
			}
			if(it.status.toString()==status && it.party2.userType=='teacher') 
			{
				g<<it.party2
			}
		}
		g
	}

	static getAllMyTeachersByStatus(lf.Teacher user, String status)
	{
		def all = getAllMyRelationships(user)
println all
		def g = []
		all.each { it->
			if(it.status.toString()==status && it.party2.userType=='teacher') 
			{
				g<<it.party2
			}
			if(it.status.toString()==status && it.party1.userType=='teacher') 
			{
				g<<it.party1
			}
		}
		g
	}

static getAllMyTeachersByStatus(lf.Student user, String status)
	{
		def all = getAllMyRelationships(user)
println "a="+all
		def g = []
		all.each { it->
			if(it.status.toString()==status && it.party2.userType=='teacher') 
			{
				g<<it.party2
			}
			if(it.status.toString()==status && it.party1.userType=='teacher') 
			{
				g<<it.party1
			}

		}
		g
	}





	static getAllMyTeachers(User user)
	{
		def all = getAllRelationships(user)
		def friends = []
		all.findAll{ ait->
			if(ait.party2==user && ait.party1.userType=='teacher' && ait.status==StatusCode.ACCEPTED)
			{
				friends << ait.party1
			}
			if(ait.party1==user && ait.party2.userType=='teacher' && ait.status==StatusCode.ACCEPTED)
			{
				friends << ait.party2
			}
		}
		friends
	}


	static getAllMyFriends(User user)
	{
		def all = getAllRelationships(user)
		def friends = []
		all.findAll{ ait->
			if(ait.party2==user && ait.party1.userType=='student' && ait.status==StatusCode.ACCEPTED)
			{
				friends << ait.party1
			}
			if(ait.party1==user && ait.party2.userType=='student' && ait.status==StatusCode.ACCEPTED)
			{
				friends << ait.party2
			}
		}
		friends
	}


	static initiateRelationship(User user, User friend)
	{
			def r = new Relationship(party1:user, party2:friend, 
					status:StatusCode.INITIATED).save()

	}

	static makeRelationship(User user, User friend, status)
	{
		def r= Relationship.findByParty1AndParty2(user, friend)
		if(!r)
		{
			r = new Relationship()	
		}
		r = new Relationship(party1:user, party2:friend, status:status).save()
	}


	static getAllBlockedUsers(User user)
	{
		def b = Relationship.findAllByParty1AndStatus(user,StatusCode.BLOCKED)*.party2
		b
	}

	static checkParameterPassing(foo)
	{
		return foo
	}

	static isValidTeacherDomain(email)
	{
		def d = email.trim().split("@")[1]
println "domain="+d
		if(d=="") 
		{
			return false
		}
		def domain = Emaildomain.findByDomain(d)
//		def domain = ''
		if(domain==null)
		{
			return false
		}
		return true
	}
}
