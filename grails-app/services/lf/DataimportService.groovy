package lf
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

class DataimportService implements ApplicationContextAware {

	ApplicationContext applicationContext

    static transactional = false

    def basePath(String path = "/") {
	return applicationContext.getResource(path)?.getFile().toString()

    }
}
