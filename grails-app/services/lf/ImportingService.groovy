package lf

import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Cell

class ImportingService {

    static transactional = true

    def serviceMethod() {

    }


		def loadBioHow(fileName,bio)
		{

			def base = System.properties['base.dir'] + "/imports"
			def infile = base + "/" + fileName
			def lines = new File(infile).text.split("\n")
			def top = null
			lines.each { line->
				line = line.trim()
				if(line.length()>0)
				{
					if(line[0]=='*')
					{
						line = line[2..-1]
						def bioSection = BioSection.findByCaptionAndBio(line,bio)
						if(bioSection==null)
						{
							bioSection = new BioSection(caption:line,bio:bio).save()
						}
					} else {
						def bioStatement = BioStatement.findByBioSectionAndCaption(bioSection,line)
						if(bioStatement==null)
						{
							bioStatement = new BioStatement(bioSection:bioSection, caption:line).save()
						}
					}
				}

			}


		}


	def checklistImportIntoFolio(filename, folio)
	{
		InputStream inp = new FileInputStream(filename);

		Workbook wb = WorkbookFactory.create(inp);
		Sheet sheet = wb.getSheet("World Languages")

		def counter = 0
		def headerRow = sheet.getRow(0)
		def headerNames = [:]
		def headerIndex = [:]
		headerRow.eachWithIndex { hr,index ->
			headerNames[index.toInteger()] = hr.toString()
			headerIndex[hr.toString()] = index.toInteger()
		}
		def rowCount = 1
		/**
		 * headers
		 * 0 Checklist
		 * 1 Communication Mode
		 * 2 Skill
		 * 3 Proficiency Level
		 * 4 Proficiency Sub-Level
		 * 5 Sort order
		 *
		 */
		log.info "Last row num = "+sheet.getLastRowNum()
		while(rowCount < sheet.getLastRowNum())
		{
			println "rowCount="+rowCount
			def currentRow = sheet.getRow(rowCount)
			def cellChecklist = currentRow?.getCell(0)?.toString()
			def cellMode = currentRow?.getCell(1)?.toString()
			def cellSkill = currentRow?.getCell(2)?.toString()
			def cellLevel = currentRow?.getCell(3)?.toString()
			def cellSublevel = currentRow?.getCell(4)?.toString()
			def cellOrder = currentRow?.getCell(5)?.getNumericCellValue()?.toInteger()
			def statement = currentRow?.getCell(6)?.toString()

			if(cellChecklist!='' && cellChecklist!=null && statement!=null)
			{
				def checklist = Checklist.findByName(cellChecklist)
				if(checklist == null)
				{
					log.info "no checklist for " + cellChecklist + " - making one"
					checklist = new Checklist(name: cellChecklist, folio:folio, sortOrder: 1)
					checklist.validate()
					log.info checklist.errors
					checklist.save()
					log.info "new checklist id = "+checklist.id
					def a1 = new AssessmentStatement(isDone: false, name:"I can do this", checklist: checklist, sortOrder:1).save()
					def a2 = new AssessmentStatement(isDone: false, name:"This is a goal", checklist: checklist, sortOrder:2).save()
				}

				def mode = checklist.modes.find{it.name==cellMode}
				if(mode==null)
				{
					log.info "no mode "+cellMode + " for checklist "+ cellChecklist
					mode = new Mode(checklist: checklist, name:  cellMode, sortOrder: rowCount)
					mode.validate()
					println mode.errors
					mode.save()
					checklist.addToModes(mode)
					log.info "new mode has id "+mode.id
				}

				def skill = mode.skills.find{it.name==cellSkill}
				if(skill==null)
				{
					log.info "no skill "+cellSkill + " for mode "+ mode
					skill = new Skill(mode: mode, name:  cellSkill, sortOrder: rowCount)
					skill.validate()
					println skill.errors
					skill.save()
					mode.addToSkills(skill)
					log.info "new skill has id "+skill.id
				}

				def scaleSet = checklist.scaleSets.find{it.name==cellLevel}
				if(scaleSet==null)
				{
					log.info "no scaleset "+cellLevel + " for checklist "+ checklist
					scaleSet = new ScaleSet(checklist: checklist, name: cellLevel, sortOrder: rowCount)
					scaleSet.validate()
					log.info scaleSet.errors
					scaleSet.save()
					log.info "new scaleset id is "+scaleSet.id
					checklist.addToScaleSets(scaleSet)
				}

				def scale  = scaleSet.scales.find{it.name==cellSublevel}
				if(scale==null)
				{
					log.info "no scale "+cellSublevel + " for scaleset "+ scaleSet
					scale = new Scale(scaleSet: scaleSet, name: cellSublevel, sortOrder: rowCount)
					scale.validate()
					log.info scale.errors
					scale.save()
					log.info "new scale id is "+scale.id
					scaleSet.addToScales(scale)
				}

				def candoStatement = CandoStatement.findBySkillAndScaleAndStatement(skill, scale, statement)
				if(candoStatement==null)
				{
					log.info "no cando for skill and scale ${skill} and ${scale}"
					candoStatement = new CandoStatement(scale:scale, skill: skill)
					candoStatement.sortOrder = rowCount
					candoStatement.statement = statement
					candoStatement.validate()
					log.info candoStatement.errors
					candoStatement.save()
					log.info "new statement id is "+candoStatement.id
				}

				def extraColumnCount = 7
				def inLoop = true
				while(inLoop)
				{
					def newStatement = currentRow.getCell(extraColumnCount)
					log.info " new statement = "+newStatement
					if(newStatement!=null && newStatement!="")
					{
						def topicName = headerNames[extraColumnCount]
						def topic = Topic.findByNameAndChecklist(topicName, checklist)
						if(topic == null)
						{
							topic = new Topic(checklist:checklist, name:topicName)
							topic.save()
						}
						def topicStatement = new StatementForTopic()
						topicStatement.statement = newStatement
						topicStatement.topic = topic
						topicStatement.candoStatement = candoStatement
						topicStatement.validate()
						log.info topicStatement.errors
						topicStatement.save()
						candoStatement.addToTopicStatements(topicStatement)
					} else {
						inLoop = false
					}
					extraColumnCount++
				}
			} else {
				log.info "skipping row "+rowCount
			}

			rowCount++
		}

	}

}
