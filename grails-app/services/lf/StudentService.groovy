package lf

import lf.*

class StudentService {

    boolean transactional = true

	/**
	 * add student2 as a friend of student1 
	 * but only if the friendCode supplied matches
	 * the friend code of student2
	 */
    def addFriend(Student student1, Student student2, String friendCode) {
		if(student2.myCode==friendCode)
		{
			student1.addToFriends(student2)
			student1.save()
			student2.addToFriends(student1)
			student2.save()
		} 
		else
		{
			throw new Exception("Invalid friend code")
		}
    }


	/**
	 * remove the friendship relationship between student1 and student 2
	 * @param Student
	 * @param Student
	 */
	def removeFriend(Student student1, Student student2) 
	{
		student1.removeFromFriends(student2)
		student2.removeFromFriends(student1)
		student1.save()
		student2.save()

	}

	/**
	 * does student1 have a friendship with student2?
	 * @param Student
	 * @param Student
	 * @return boolean
	 */
	def hasFriend(Student s1, Student s2)
	{
		if(s1.friends==null)
		{
			return false
		} else {
			return s1.friends?.contains(s2)
		}
	}



	/**
	 * can a student do a majority of the canDo Statements in a given pool of statements?
	 * @param List list of student's can do statements
	 * @param Integer size of pool to compare against
	 * @return Boolean
	 */
	static def canDoMajority(candoEntries, poolSize)
	{
		def candoTotal = 0
		candoEntries.each { ce->
			def doneSkills = ce.portfolio.topic.checklist.assessmentStatements.findAll{it.isDone==true}
			if(doneSkills*.toString().contains(ce.candoStatus))
			{
					candoTotal++
			}
		}
		println candoTotal
		println poolSize
		println "---------"
		if(candoTotal>(poolSize/2))
		{
				return true
		}
		return false
	}

	/**
	 * can a student do any of the cando statements in a given pool?
	 * @param List list of student's can do statements
	 * @param Integer size of pool to compare against
	 * @return Boolean
	 */

	static def canDoAny(candoEntries, poolSize)
	{
		def candoTotal = 0
		candoEntries.each { ce->

			def doneSkills = ce.portfolio.topic.checklist.assessmentStatements.findAll{it.isDone==true}
			if(doneSkills*.toString().contains(ce.candoStatus))
			{
					candoTotal++
			}
		}
		if(candoTotal>=1)
		{
				return true
		}
		return false
	}


	/**
	 * can a student do all of the cando statements in a given pool?
	 * @param List list of student's can do statements
	 * @param Integer size of pool to compare against
	 * @return Boolean
	 */

	static def canDoAll(candoEntries, poolSize)
	{
		def candoTotal = 0
		candoEntries.each { ce->
			if(ce.candoStatus== StatusCode.CANDO_COMPLETE)
			{
					candoTotal++
			}
		}
		if(candoTotal==poolSize)
		{
				return true
		}
		return false
	}

	/**
	 * can a student do none of the cando statements in a given pool?
	 * @param List list of student's can do statements
	 * @param Integer size of pool to compare against
	 * @return Boolean
	 */

	static def canDoNone(candoEntries, poolSize)
	{
		def candoTotal = 0
		candoEntries.each { ce->
			if(ce.candoStatus== StatusCode.CANDO_COMPLETE)
			{
					candoTotal++
			}
		}
		if(candoTotal==0)
		{
				return true
		}
		return false
	}

	static def canDoEmpty(candoEntries, poolSize)
	{
		def candoTotal = 0
		candoEntries.each { ce->
			def doneSkills = ce.portfolio.topic.checklist.assessmentStatements
			doneSkills.each { ds->
				if(ds.toString()==ce.candoStatus.toString()){
					candoTotal++
				}
			}
			
		}
		println candoTotal
		if(candoTotal==0)
		{
				return true
		}
		return false
	}

	def getCanDoStatus(candoEntries, poolSize)
	{
		def candoTotal = 0
		def candoEmpty = 0
		candoEntries.each { ce->
			def doneSkills2 = ce.portfolio.topic.checklist.assessmentStatements
			doneSkills2.each { ds->
				if(ds.toString()==ce.candoStatus.toString()){
					candoEmpty++
				}
			}
			def doneSkills = doneSkills2.findAll{it.isDone==true}
			if(doneSkills*.toString().contains(ce.candoStatus))
			{
				candoTotal++
			}
		}
		// empty, none, majority, any
		if(candoEmpty == 0)
		{
			return "empty"
		}
		if(candoTotal == 0)
		{
			return "none"
		}
		if(candoTotal>(poolSize/2))
		{
			return "majority"
		}
		if(candoTotal==poolSize)
		{
			return "all"
		}
		if(candoTotal>0)
		{
			return "any"
		}


	}


	static def sharedPortfoliosFromAndTo(fromUser, toUser)
	{
		def r = Sharing.findAllByOwnerAndSharedWith(fromUser, toUser)
		r*.portfolio	
	}
}
