package lf
import lf.*

import org.grails.plugins.settings.*
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class mqvideoService {

	static transactional = false

	def evidenceService

	void handleMessage(Integer id)
	{
		log.info "Thumbnailing evidence " + id
		println "Thumbnailing evidence " + id
		this.thumbnail(id)
		println "Done thumbnailing " + id
		log.info "Done thumbnailing " + id
	}
    void handleMessage(String textMessage) {
        println "Received Video Message: ${textMessage}"
        log.info "Received Video Message: ${textMessage}"
    }
    
/*
    void handleMessage(Map mapMessage) {
        println "Received Map Message..."
        mapMessage?.each { key, val ->
            println "   ${key}: ${val}"
        }
    }
*/


/**
 * for a given evidence, return a thumbnail image
 */
	static String getThumbnail(e)
	{
		def thumbnail = Setting.valueFor("defaultThumbnail")
		if(e!=null)
		{
			if(e.file.pathToThumbnail!=null)
			{
				thumbnail = e.file.pathToThumbnail	
			}
		} else {
			
		}

		thumbnail		
	}


	def thumbnail(eid) { 
		def e = Evidence.get(eid)
		def ext = e.file.extension.toLowerCase()
println "ext=" + ext
//println "e3=" + e.file
//		def base = System.properties['base.dir'] + "/web-app"
//		def base = dataimportService.basePath()
//.replace("/web-app","") //  + "" // "/web-app"
//		base = base.replaceFirst("/web-app","")
//		base = "/home/mgkimsal/tomcat7/webapp/ROOT"
def base = ConfigurationHolder.config.lf.base



println "base = " + base
		switch(ext) 
		{
			case ['jpg','jpeg','gif','png','bmp']:
				try {
					def name = Setting.valueFor('thumbnailPath') + e.file.id + ".jpg"
					def command = "convert " + e.file.path + " -thumbnail 150x150  " + base + name 
println "command2 = " + command
					command.execute()
					e.fileType = "image"
					e.file.pathToThumbnail = name
					e.file.save()
					e.status = "finished"
					e.save()
				} catch(Exception ex) {
						e.status = "failed"
println "failed = " + ex.getMessage()
						e.save()
				}
				break

			case "pdf":
println "pdf wow"
log.info "pdf wow"
				e.file.pathToThumbnail = "/images/defaultPdf.png"
				e.fileType = "pdf"
				e.file.save()
				e.status = "finished"
				e.save()
				break

			case ["txt","rtf","doc","docx"]:
				e.fileType = "doc"
				e.file.pathToThumbnail = "/images/defaultDoc.png"
println "general doc/word file!"
				e.file.save()
				e.status = "finished"
				e.save()
				break

			case ["xls","xlsx"]:
				e.fileType = "xls"
				e.file.pathToThumbnail = "/images/defaultXls.png"
				e.file.save()
				e.status = "finished"
				e.save()
				break
			case ["ppt","pptx"]:
				e.fileType = "ppt"
				e.file.pathToThumbnail = "/images/defaultPpt.png"
				e.file.save()
				e.status = "finished"
				e.save()
				break

			case ["m4v","mov","mpeg","avi","wmv"]:
println "m4v wow"
log.info "m4v wow"
				e.fileType = "mov"
				e.status = "processing"
				e.save(flush:true)

				def thumbname = Setting.valueFor('thumbnailPath') + e.file.id + ".jpg"
				def thumbnameTemp = Setting.valueFor('thumbnailPath') + e.file.id + ".jpg.temp"
				def movname = Setting.valueFor('thumbnailPath') + e.file.id + ".mp4"
				def oggname = Setting.valueFor('thumbnailPath') + e.file.id + ".ogv"
				def command = "ffmpeg -i " + e.file.path 
				command += " -s 640x480 -y -f flv -acodec libfaac "
				command += "-ac 1 -ab 8k -b 224k -ar 22050 " + base + movname
				command.execute()
log.info command
				e.file.pathToProcessedFile = movname

				def theoraCommand = "ffmpeg2theora -o " + base + oggname 
				theoraCommand += " --videoquality 6 --audioquality 6 "
				theoraCommand += " --max_size=640x480 " + e.file.path
				theoraCommand.execute()
log.info theoraCommand

//				def command2 = "convert " + e.file.path + " -thumbnail 150x150  " + base + name 

				def thumbCommand = "ffmpeg -i " + base + movname 
				thumbCommand += " -ss 50 -vframes 1 " + base + thumbnameTemp 
				thumbCommand += " | convert " + base + thumbnameTemp 
				thumbCommand += " -thumbnail 150x150 " + base + thumbname
				thumbCommand.execute()
println thumbCommand

				def cleanupCommand = "rm " + base + thumbnameTemp
				cleanupCommand.execute()
println cleanupCommand

				e.file.pathToThumbnail = thumbname
				e.file.save()
				e.status = "finished"
				e.save(flush:true)

			break


			case ["wma","m4a","ogg"]:
				e.fileType = "mp3"
				e.status = "processing"
				e.save(flush:true)

				def mp3name = Setting.valueFor('thumbnailPath') + e.file.id + ".mp3"
				def tempname = Setting.valueFor('thumbnailPath') + e.file.id + ".wav"
				def command = "faad " + e.file.path +  " " + base + tempname
				command.execute()
println command
				command = "lame " +base + tempname + " " +  base + mp3name
				command.execute()
println command
				e.file.pathToProcessedFile = mp3name


				def cleanupCommand = "rm " + base + tempname
				cleanupCommand.execute()
println cleanupCommand

				e.file.pathToThumbnail = mp3name
				e.file.save()
				e.status = "finished"
				e.save(flush:true)

			break

			case "wav":

println "wav wow"
log.info "wav wow"
log.info "ext wav = " + ext
				e.fileType = "mp3"
				e.status = "processing"
e.validate()
log.info e.errors
				e.save(flush:true)
log.info e

				def mp3name = Setting.valueFor('thumbnailPath') + e.file.id + ".mp3"
log.info "mp3name= " + mp3name
log.info "base = " + base
				def command = "/usr/bin/lame" + e.file.path +  " " + base + mp3name
log.info "command = " + command
				command.execute()
println "command = " + command
				e.file.pathToProcessedFile = mp3name

				e.file.pathToThumbnail = mp3name
				e.file.save()
				e.status = "finished"
				e.save(flush:true)

			break

			case "mp3":
				e.fileType = "mp3"
				e.status = "processing"
				e.save(flush:true)

				def mp3name = Setting.valueFor('thumbnailPath') + e.file.id + ".mp3"
				def command = "cp " + e.file.path +  " " + base + mp3name
				command.execute()
println command
				e.file.pathToProcessedFile = mp3name

				e.file.pathToThumbnail = mp3name
				e.file.save()
				e.status = "finished"
				e.save(flush:true)

			break


			default:
println "general file!"
				e.fileType = "general"
				e.file.save()
				e.status = "finished"
				e.save()
				break

			break

	}
	}


	def delete(Evidence e)
	{
println "e2=" + e.id
		if(e?.file?.pathToProcessedFile!=null) 
		{
			def fprocessed = new File(e?.file?.pathToProcessedFile)
			if(fprocessed.exists())
			{
				fprocessed.delete()
			}
		}

		if(e?.file?.pathToThumbnail!=null) 
		{
			def f = new File(e?.file?.pathToThumbnail)
			if(f.exists())
			{
				f.delete()
			}
		}

/*
		def f2 = new File(e?.file?.path)
		if(f2.exists())
		{
			f2.delete()
		}
*/
		e?.delete()
	}
}
