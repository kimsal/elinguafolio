package lf

import org.grails.plugins.settings.*



class SetupService {

	def emailConfirmationService
	def springSecurityService
	def userService
	def grailsApplication

	static transactional = true


	def development() {
		importSchools()
		importChecklists()
		createRolesAndUsers()
		createSettings()
		createPortfolioAndBio()
		setupNews()
	}

	def production() {
		importSchools()
		importChecklists()
		createRolesAndUsers()
		createSettings()
		createPortfolioAndBio()
		setupNews()
	}

	def test() {
		createRolesAndUsers()
	}



	def setupNews() {
		def newscount = News.count()
		if(newscount==0)
		{
			def n = new News(title:  "Welcome", body: "Welcome to the elinguafolio platform!")
			n.sticky = true
			n.startDate = new Date()-50
			n.endDate = new Date()+300
			n.save()
		}
	}

	def createRolesAndUsers() {
		// assume we have users/roles if student is set up
		// should only do this one, but boot/init will call it every time
		if (!Role.findByAuthority("ROLE_STUDENT")) {
			def role1 = new Role(authority: "ROLE_STUDENT")
			role1.validate()
			println role1.errors
			role1.save()
			def role2 = new Role(authority: "ROLE_TEACHER").save()
			def role3 = new Role(authority: "ROLE_ADMIN").save()

			def password3 = springSecurityService.encodePassword('p')
			def student1 = new Student(fullName: "Student 1", username: "student1", password: password3, email: "student1@gmail.com", myCode: "123",
					accountLocked: false, accountExpired: false, enabled: true).save()

			def student2 = new Student(fullName: "Student 2", username: "student2", password: password3, email: "student2@gmail.com", myCode: "456",
					accountLocked: false, accountExpired: false, enabled: true).save()
			def student3 = new Student(fullName: "Student 3", username: "student3", password: password3, email: "student3@gmail.com", myCode: "333",
					accountLocked: false, accountExpired: false, enabled: true).save()
			def student4 = new Student(fullName: "Student 4", username: "student4", password: password3, email: "student4@gmail.com", myCode: "444",
					accountLocked: false, accountExpired: false, enabled: true).save()
			def student5 = new Student(fullName: "Student 5", username: "student5", password: password3, email: "student5@gmail.com", myCode: "555",
					accountLocked: false, accountExpired: false, enabled: true).save()
			UserRole.create student1, role1
			UserRole.create student2, role1
			UserRole.create student3, role1
			UserRole.create student4, role1
			UserRole.create student5, role1

			def teacher1 = new Teacher(fullName: "Teacher 1", username: "teacher1", password: password3, email: "teacher1@gmail.com", myCode: "t1code").save();
			def teacher2 = new Teacher(fullName: "Teacher 2", username: "teacher2", password: password3, email: "teacher2@gmail.com", myCode: "t2code").save();
			UserRole.create teacher1, role2
			UserRole.create teacher2, role2

			userService.initiateRelationship(student1, student2)
			if (student1 && student2 && student3) {
				student1.addToFriends(student2)
				student1.addToFriends(student3)
			}

			def defaultAdminPassword = grailsApplication.config.defaultAdminPassword
			def password1 = springSecurityService.encodePassword(defaultAdminPassword)
			def password2 = springSecurityService.encodePassword('u')
			def u1 = new User(fullName: "admin", username: "admin", password: password1, email: "mgkimsal@gmail.com",
					accountLocked: false, accountExpired: false, enabled: true, myCode: "12443").save()
			UserRole.create u1, role3

		}
	}

	def createSettings() {
		if (!Setting.findByCode("defaultThumbnail")) {
			def set1 = new Setting(code: "defaultThumbnail", type: "string", value: "/images/default.png").save()
		}
		if (!Setting.findByCode("thumbnailPath")) {
			def set2 = new Setting(code: "thumbnailPath", type: "string", value: "/pics/t/").save()
		}
		if (!Setting.findByCode("thumbnailX")) {
			def set3 = new Setting(code: "thumbnailX", type: "integer", value: "150").save()
		}
		if (!Setting.findByCode("thumbnailY")) {
			def set4 = new Setting(code: "thumbnailY", type: "integer", value: "150").save()
		}
		if (!Setting.findByCode("defaultProfileThumbnail")) {
			def set9 = new Setting(code: "defaultProfileThumbnail", type: "string", value: "/images/defaultUser.png").save()
		}

	}

	def createPortfolioAndBio() {
		def myp = new Myportfolio(
				folio: Folio.findByTitle('LinguaFolio'),
				owner: Student.findByUsername("student1")).save()
		def bio1 = new Biography(
				name: "LF Bio questions",
				folio: Folio.findByTitle('LinguaFolio')).save()
		def inv1 = new Inventory(
				name: "LF Learning inventory",
				folio: Folio.findByTitle('LinguaFolio')).save()

		loadBioHow("linguafolio_how.txt", bio1)
		loadLearningInv("linguafolio_inv.txt", inv1)
	}
}
