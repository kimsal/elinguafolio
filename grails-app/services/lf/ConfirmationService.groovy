package lf;

import com.grailsrocks.emailconfirmation.PendingEmailConfirmation

import grails.events.Listener 
class ConfirmationService {
    @Listener(namespace='plugin.emailConfirmation', topic='confirmed')
    def userConfirmedSubscription(confirmation) {
println "c="+confirmation
	def user = User.findByEmail(confirmation.email) 
	if (user) {
	    user.accountConfirmed = true
user.validate()
println "ue="+user.errors
		user.save()
println "yes record of confirmation "+confirmation.id
println "yes record of email "+confirmation.email
		def p = PendingEmailConfirmation.findByEmailAddress(confirmation.email)
		p.delete()
		//pluginFlash.message = "Your account has been confirmed!"
		return [controller:"login", action:"index", params:['conf':'y']]
	} else {
println "no record of confirmation "+confirmation.id
		return [controller:"login", action:"index", params:['conf':'n']]
//		pluginFlash.error = "We have no record of this confirmation token!"
	}
	} 
}
