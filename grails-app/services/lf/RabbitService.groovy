package lf
import lf.*

class RabbitService {

//    static rabbitQueue = 'foo'

    void handleMessage(String textMessage) {
	Thread.currentThread().sleep(6*1000)
        println "Received Message: ${textMessage}"
    }
    
    void handleMessage(Map mapMessage) {
        println "Received Map Message..."
        mapMessage?.each { key, val ->
            println "   ${key}: ${val}"
        }
    }
}
